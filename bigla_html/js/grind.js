$(document).ready(function() {


$('.tabs .tab').click(function() {
  $('.tabs .tab').removeClass("selected");
  $(this).addClass("selected");
  var tabBody = $(this).attr('data-tab');
  $(".tab-body").hide(); $(tabBody).show();
});

$("#scroll-top").click(function() {
  $("html, body").animate({ scrollTop: 0 }, "slow");
});


$(window).bind('scroll', function() {
   if ($(window).scrollTop() > 56) {
     $('nav').addClass('fixed');
   } else {
     $('nav').removeClass('fixed');
   }
});


$("#categorySelect").change(function() {

});




$('#element').click(function() {
   if($('#radio_button').is(':checked')) { alert("it's checked"); }
});


$('.changeCurrency').click(function() {
  var getCurrnecy = $(this).attr("data-currnecy");
  $.post( "/libs/ajax.php", { changeCurrency: 'ture', cid: getCurrnecy }, function(data) {
    if (data == 'true') {
      var pathname = window.location.pathname;
      window.location.replace(pathname);
    } 
  });
});


$('#itemPerPageChange').change(function() {
  var NumOfItems = $(this).val();
  $.post( "/libs/ajax.php", { changeItemPerPage: 'ture', numItems: NumOfItems }, function(data) {
    if (data == 'true') {
      var pathname = window.location.pathname,
          exploded = pathname.split('/');
      window.location.replace('/' + exploded[1]);
    } 
  });
});


$('#map-center').click(function(e) {
    e.preventDefault();
    var data = $('#map-center').mouseout().data('maphilight') || {};
    data.alwaysOn = !data.alwaysOn;
    $('#map-center').data('maphilight', data).trigger('alwaysOn.maphilight');
});


$('.map, .map-lozenets, .centerMapAreas, .oborishteMapAreas').maphilight({
    fillColor: 'ffca2b',
    fillOpacity: 1,
    stroke: false
});
  
$('.group').click(function(e) {
  e.preventDefault();
  var data = $(this).mouseout().data('maphilight') || {};
  data.alwaysOn = !data.alwaysOn;
  $(this).data('maphilight', data).trigger('alwaysOn.maphilight');
  var aid = $(this).data('id'),
  AreaInput = '<input type="text" class="areaSelectInput" id="areaSelect'+aid+'" name="searchArea[]" value="'+aid+'" />';
   if ($(this).hasClass('selected')){
    $(this).removeClass( "selected" );
    $("#areaSelect"+aid).remove();
   } else {
    $(this).addClass( "selected" );
    $( "#areasInputsCont" ).append( AreaInput );
   }
});



$('#containerTour').hover(function() {
    $(document).bind('mousewheel DOMMouseScroll',function(){ 
        stopWheel(); 
    });
}, function() {
    $(document).unbind('mousewheel DOMMouseScroll');
});


function stopWheel(e){
    if(!e){ /* IE7, IE8, Chrome, Safari */ 
        e = window.event; 
    }
    if(e.preventDefault) { /* Chrome, Safari, Firefox */ 
        e.preventDefault(); 
    } 
    e.returnValue = false; /* IE7, IE8 */
}


$('.selectAllAreas').click(function (e) {
  e.preventDefault();

  $('.selectAllAreas').hide();
  $( ".group" ).each(function() {
    if ($(this).hasClass('selected')){
    } else {
      var data = $(this).mouseout().data('maphilight') || {};
      data.alwaysOn = !data.alwaysOn;
      $(this).data('maphilight', data).trigger('alwaysOn.maphilight'); 
      var aid = $(this).data('id'),
      AreaInput = '<input type="hidden" class="areaSelectInput" id="areaSelect'+aid+'" name="searchArea[]" value="'+aid+'" />';
      $(this).addClass( "selected" );
      $( "#areasInputsCont" ).append( AreaInput );
    }
  });  
  $('.unSelectAreas').show();
});

$('.unSelectAreas').click(function (e) {
  e.preventDefault();
  $('.unSelectAreas').hide();
  $( ".group" ).each(function() {
    if ($(this).hasClass('selected')){ 
      var data = $(this).mouseout().data('maphilight') || {};
      data.alwaysOn = !data.alwaysOn;
      $(this).data('maphilight', data).trigger('alwaysOn.maphilight');
      $(this).removeClass( "selected" );
    }  
  });
  $('.areaSelectInput').remove();
  
  $('.selectAllAreas').show();
});



$('.group, .mapGroup').hover(function(){
  var title = $(this).data('title');
  $('<p class="tooltip"></p>').text(title).appendTo('body').fadeIn('slow');  
}, function() {
        $('.tooltip').remove();
}).mousemove(function(e) {
        var mousex = e.pageX + 20; //Get X coordinates
        var mousey = e.pageY + 10; //Get Y coordinates
        $('.tooltip').css({ top: mousey, left: mousex })
});   


  
  $("#lozenets").click(function() {
    $(".map-sofia").fadeOut(100).delay(100);
    $(".map-sofia-lozenets").fadeIn();
  });
  
  $("#mapCenter").click(function() {
    $(".map-sofia").fadeOut(100).delay(100);
    $(".map-sofia-center").fadeIn();
  });
  
  $("#oborishte").click(function() {
    $(".map-sofia").fadeOut(100).delay(100);
    $(".map-sofia-oborishte").fadeIn();
  });
  
  
  
  $(".back-map").click(function() {
    $(".popUpMap").fadeOut(100).delay(100);
    $(".map-sofia").fadeIn();
  });

    
  //b18  b19  b5 serdica lagera krivareka hipodruma ivanvazov strelbishte  belibrezi krasnoselo  borovo gotsedelchev bukston pavlovo manastirskilivadi boyna kniajevo dragalevci simeonovo americancollege pancharevo bistrica mladost nzp-iztok drujba poligona krastova-voda vitosha studenstkigrad malinovadolina lozenets reduta qvorov geomilev   


  $(".inputType").change(function() {
    var Type = $(this).val();
    $(".buyrent div").removeClass('selected');
    $(this).parent().parent().addClass('selected');
    if (Type == 1) { 
      $('.more-opt').attr('href', '/buy/moreOptions'); 
      $('.map').attr('href', '/buy'); 
      $('a.location').attr('href', '/buy/nearme'); 
    } else { 
      $('.more-opt').attr('href', '/rent/moreOptions'); 
      $('.map').attr('href', '/rent'); 
      $('a.location').attr('href', '/rent/nearme'); 
    }
    
    $.post( "/libs/ajax.php", { changeType: 'ture', postType: Type }, function(data) {});
   });  
   
  $("#loginForm").submit(function() {
    $('.msg').remove();
    var data = $(this).serialize();
    $.post( "/auth/loginDo",  data ,  function( r ) {
      if (r != 'false') {
        window.location.href = "/profile";
      } else {
       var ErrMsg = $("#loginForm").data('errmsg');
       $('.login-first').prepend('<div class="msg errorMsg">'+ErrMsg+'</div>');
      }
    }); 
    return false;
  });
  
  $("#forgottenForm").submit(function () {
    // TO DO - trqbva da se mahat suobshteniata za greshak
    $('.msg').remove();
    var data = $(this).serialize(), 
        user = $('#username').val();
      if (user != '') {
        if( isValidEmailAddress(user) ) { 
        $.post( "/auth/forgottenDo",  data ,  function( r ) {
          if (r == 'true') {
            var Msg = $('#forgottenForm').data('successmsg');
            $('.login-first').prepend('<div class="msg errMsg">'+Msg+'</div>'); 
          } else {
            var Msg = $('#forgottenForm').data('nousermsg');
            $('.login-first').prepend('<div class="msg errMsg">'+Msg+'</div>'); 
          }      
        }); 
        } else { 
          var Msg = $('#forgottenForm').data('validemailmsg');
          $('.login-first').prepend('<div class="msg errMsg">'+Msg+'</div>');
        }
      } else { 
        var Msg = $('#forgottenForm').data('emptymsg');
        $('.login-first').prepend('<div class="msg errMsg">'+Msg+'</div>');
      }
    return false;
  });
   
  
  $(".saveShortList").on('click', function() {
    var pid = $(this).data('pid'),
        thisObject = $(this);
    $.post( "/property/saveShortListDo",  { porpertyId: pid } ,  function(r) {
      if (r == 'true') {
        var Parent = $(thisObject).parent();
        $(Parent).find(".saveShortList").fadeOut(200);
        $(Parent).find(".saved").delay(400).fadeIn().delay(800).fadeOut(1000); 
        $(Parent).find(".mySavedLink").delay(2800).fadeIn(300);      
      } else {
        window.location.href = "/auth/login";
      }   
    }); 
    return false;
  });
  
  
  $("#resetPasswordForm").submit(function() {
    $('.msg').remove();
    var data = $(this).serialize(), 
        newPass = $('#newPassword').val(), 
        rePass  = $('#rePassword').val();
    if (newPass == '' && rePass == '') {
      var Msg = $('#resetPasswordForm').data('empty');
      $('.login-first').prepend('<div class="msg errMsg">'+Msg+'</div>');
    } else {
      if ( newPass != rePass) {
        var Msg = $('#resetPasswordForm').data('nocoincidence');
        $('.login-first').prepend('<div class="msg errMsg">'+Msg+'</div>');
      } else {
        if (newPass.length <= 5) { 
          var Msg = $('#resetPasswordForm').data('shortpass');
          $('.login-first').prepend('<div class="msg errMsg">'+Msg+'</div>');  
        } else {
          $.post( "/auth/resetPasswordDo",  data ,  function( r ) {
            if (r == 'true') {
              window.location.href = "/";
            } else {
              var Msg = $('#resetPasswordForm').data('error');
              $('.login-first').prepend('<div class="msg errMsg">'+Msg+'</div>'); 
            }   
          });   
        }
      }
    }
    
    return false;
  });
  
  
  $('#sendValuationFormEmail').submit(function(){
    var address = $('#valAddress').val(),
        city    = $('#city').val(),
        firstname = $('#firstName').val(),
        lastname = $('#lastname').val(),
        email   = $('#email').val(),
        error =   0;
        
        $('#valAddress, #city, #firstName, #lastname, #email').css('border', 'none');
        $('.msgEmail').hide();
    
    if (address == '') {
      $('#valAddress').css('border', 'solid 1px red');
      error++;
    }
    
    if (city == '') {
      $('#city').css('border', 'solid 1px red');
      error++;
    } 
    
    if (firstname == '') {
      $('#firstName').css('border', 'solid 1px red');
      error++;
    } 
    
    if (lastname == '') {
      $('#lastname').css('border', 'solid 1px red');
      error++;
    } 
    
    if (email == '') {
      $('#email').css('border', 'solid 1px red');
      error++;
    } else {
      if (!isValidEmailAddress(email)) {
        $('#email').css('border', 'solid 1px red'); 
        error++;
        $('.msgEmail').fadeIn(); 
      }
    }   
    
    
    if (error == 0) {
      var data = $(this).serialize();
      $.post( "/valuation/sendValuation",  data ,  function( r ) {
        if (r == 'sent') {
          $('.val-form').slideUp(300, function() {
             $('.val-form').remove(); 
             $('.msg').slideDown();   
          });
          
        }
      });
    }
        
    return false;
  });
  
  
  
  
  
  
  $('#registrationForm').submit(function(){
    $('.msg').remove();
    var data = $(this).serialize(), 
        user = $('#username').val(),
        pass = $('#password').val(), 
        rePass  = $('#rePassword').val();
    if ( user == '' || pass == '' || rePass == '' ) {
      var Msg = $('#registrationForm').data('empty');
      $('.login-first').prepend('<div class="msg errMsg">'+Msg+'</div>');  
    } else {
    
      if( isValidEmailAddress(user) ) { 
        if ( pass != rePass) {
          var Msg = $('#registrationForm').data('nocoincidence');
          $('.login-first').prepend('<div class="msg errMsg">'+Msg+'</div>');
        } else {
          if (pass.length <= 5) { 
            var Msg = $('#registrationForm').data('shortpass');
            $('.login-first').prepend('<div class="msg errMsg">'+Msg+'</div>');  
          } else {
            $.post( "/auth/registrationDo",  data ,  function( r ) {
              if (r == 'exist') {
                var Msg = $('#registrationForm').data('exist');
                $('.login-first').prepend('<div class="msg errMsg">'+Msg+'</div>'); 
              } else {
                window.location.href = "/";  
              }  
            }); 
          }
        }    
      } else {
        var Msg = $('#registrationForm').data('validemailmsg');
        $('.login-first').prepend('<div class="msg errMsg">'+Msg+'</div>');
      }    
    }
    return false;
  }); 
  
    $('.my-menu > li').bind('mouseover', openSubMenu);
		$('.my-menu > li').bind('mouseout', closeSubMenu);
		
		function openSubMenu() {
			$(this).find('ul').show();	
		};
		
		function closeSubMenu() {
			$(this).find('ul').hide();	
		};
    
    $(".doRate").on('click', function() {
      var rating = $(this).data('star'), 
          pid = $(this).data('pid'),
          thisObject = $(this);
      
      $.post( "/property/doRating", { doRating: 'ture', postPid: pid, postStar: rating }, function(data) {
        if (data == 'nouser') {
          window.location.href = "/auth"; 
        } else {
          var result = data;
          $(thisObject).parent().parent().parent().append(result); 
          $(thisObject).parent().parent().remove(); 
        }
      });
    });   


  $('.close-property').click(function() {
    var pid = $(this).data('pid'),
        thisObject = $(this);
    
    $.post( "/profile/delSavedPropery", { doDelSavedPropery: 'ture', postPid: pid }, function(data) {
      if (data == 'nouser') {
        window.location.href = "/auth"; 
      } else {      
        thisObject.parent().slideUp();
        if (data == 0) {
          window.location.href = "/profile/mySavedProperties "; 
        }
      }
    });
  }); 
  
  $('.close-search').click(function() {
    var sid = $(this).data('sid'),
        thisObject = $(this);
    
    $.post( "/profile/delSavedSearch", { doDelSavedSearch: 'ture', postSid: sid }, function(data) {
      if (data == 'nouser') {
        window.location.href = "/auth"; 
      } else {      
        thisObject.parent().slideUp();
        if (data == 0) {
          window.location.href = "/profile/mySavedSearches";   
        }
      }
    });
  }); 
  
  
  $('#myProfileForm').submit(function() {
    var data = $(this).serialize();
    $.post( "/profile/myProfile",  data ,  function( r ) {
      $('#myProfileForm .msg.SaveMsg').slideDown().delay(2000).slideUp();
    }); 
    return false;
  }); 
  
  $('#changePass').submit(function () { 
    var data = $(this).serialize(),
        oldpass = $('#oldpass'),
        newpass = $('#newpass'),
        repass = $('#repass');
        
      
      
    if (oldpass != '' || newpass != '' || repass != '' ) {
      $.post( "/profile/chnagePassword",  data ,  function( r ) {
        if (r == 'change') {
          $('#changePass .msg.SaveMsg').slideDown().delay(2000).slideUp(); 
        } else {
          $('#changePass .msg.ErrorMsg').slideDown().delay(2000).slideUp(); 
        }
        //$('#changePass .msg.SaveMsg').slideDown().delay(2000).slideUp();
      }); 
    } else {
      $('#changePass .msg.ErrorMsg').slideDown().delay(2000).slideUp();   
    }
    return false;
    
  });
  
  $('#saveSearchBtn').click(function() {
    var ThisObject = $(this);
    $.post( "/search/saveCurrentSearch",  function( r ) {
      if (r == 'nouser')  {
        window.location.href = "/auth";
      } else {
        $(ThisObject).addClass('selected');  
      }
    });
  });
  
  $('#RemoveSaveSearchBtn').click(function() {
    var ThisObject = $(this);
    $.post( "/search/RemoveSaveCurrentSearch",  function( r ) {
      if (r == 'nouser')  {
        window.location.href = "/auth";
      } else {
        $(ThisObject).removeClass('selected');  
      }
    });
  });
  
  
  $('#feedbackForm').submit( function() {
    var names = $('#names').val(),
        email = $('#email').val(),
        subject = $('#subject').val(), 
        comments = $('#comments').val(),
        errors = 0,
        thisForm = $(this);
        
        $('input[type="text"], textarea').css('border', 'none');
        $('.msgEmail').hide();
        
        if (names == '' ) {
          $('#names').css('border', 'solid 1px red');
          errors++;
        }
        
        if (subject == '' ) {
          $('#subject').css('border', 'solid 1px red');
          errors++;
        }
        
        if (comments == '' ) {
          $('#comments').css('border', 'solid 1px red');
          errors++;
        }
        
        if (email == '' ) {
          $('#email').css('border', 'solid 1px red');
          errors++;
        } else {
          if (!isValidEmailAddress(email)) {
            $('#email').css('border', 'solid 1px red');
            $('.msgEmail').fadeIn();
            errors++;
          }
        }
  
        if (errors == 0) {
          var data = $(this).serialize();
          $.post( "/contacts/feedbackForm",  data ,  function( r ) {
            if (r == 'sent') {
              thisForm.slideUp('200').delay(300).remove();
              $("html, body").animate({ scrollTop: 0 }, "slow");
              $('.msg.SaveMsg').fadeIn();
            } else {
              alert('error');
            }
          }); 
        } else {
          return false;
        }
        
    return false;
  });
  
  $('#feedbackForm .rating a').click(function () {
    var Rating = $(this).data('star');
    $('#workRate').val(Rating);
    $('#feedbackForm .rating a').removeClass('fill');
    $(this).addClass('fill');
  });
  
  
  $('#arrangeFrom').submit(function() {
    var FirstName = $('#firstName').val(),
        LastName = $('#lastName').val(),
        Phone = $('#phone').val(),
        Email = $('#email').val(),
        errors = 0,
        thisForm = $(this);
        
    $("#firstName, #lastName, #phone, #email").css('border', 'none');
      
    if (FirstName == '') {
      $("#firstName").css('border', 'solid 1px red');
      errors++;
    }   
    
    if (LastName == '') {
      $("#lastName").css('border', 'solid 1px red');
      errors++;
    } 
    
    if (Phone == '') {
      $("#phone").css('border', 'solid 1px red');
      errors++;
    } 
    
    if (Email == '') {
      $("#email").css('border', 'solid 1px red');
      errors++;
    } else {
      if (!isValidEmailAddress(Email)) {
        $('#email').css('border', 'solid 1px red');
        $('.msgEmail').fadeIn();
        errors++;
      }
    } 
        
    if (errors == 0) {
     var data = $(this).serialize();
      $.post( "/property/arrangeForm",  data ,  function( r ) {
        if (r == 'sent') {
          thisForm.slideUp('200').delay(300).remove();
          $("html, body").animate({ scrollTop: 0 }, "slow");
          $('.msg.SaveMsg').fadeIn();
        } else {
          alert('error');
        }
      
      }); 
    }
    return false;
  });
  
/* Search Form Session */  
 $('#categorySelect, #roomSelect, #sizeInput, #priceInput, #ExtensionOption, #addLastDays').change(function () { 
    var val = $(this).val(),
        sessionName = $(this).data('sname');   
    $.post( "/buy/setSession", { sessionNamePost: sessionName, postVal: val },  function (r) {   });
});

 $('#incSoldInput, #onlyNew, #onlyWithExtension, #OnlyAddedLast').change(function () { 
    var sessionName = $(this).data('sname'); 
    if ($(this).is(":checked")) {
      var val = 1;
    } else {
      val = null;
    }     
    $.post( "/buy/setSession", { sessionNamePost: sessionName, postVal: val },  function (r) {  });
});

/* End Search Form Session */
  

$('#jobApplyForm').submit(function() {
  var firstName = $('#firstname').val(),
      lastname = $('#lastname').val(),
      Phone = $('#phone').val(),
      Email = $('#email').val(),
      file = $('#textFileShow').val(),
      thisForm = $(this),
      errors = 0;
    
    $(".msgEmail").hide();  
    $("#firstname, #lastname, #phone, #email").css('border', 'none');
    $("#textFileShow").css('background', '#fff');
       
    if (firstName == '') {
      $("#firstname").css('border', 'solid 1px red');
      errors++;
    }   
    
    if (lastname == '') {
      $("#lastname").css('border', 'solid 1px red');
      errors++;
    } 
    
    if (Phone == '') {
      $("#phone").css('border', 'solid 1px red');
      errors++;
    } 
    
    if (Email == '') {
      $("#email").css('border', 'solid 1px red');
      errors++;
    } else {  
      if (!isValidEmailAddress(Email)) {
        $('#email').css('border', 'solid 1px red');
        $('.msgEmail').fadeIn();
        errors++;
      }
    } 
    
    if (file == '') {
      $('#textFileShow').css('background', '#FFA3A3'); 
      errors++;  
    }
    
    if (errors == 0) {
      thisForm.submit();
    /*  var filess = $('#fileInput').val();
      var data = $(this).serialize();
          data = data + '&selectedFils='+encodeURIComponent(filess);
     
     
     
      $.post( "/careers/applyJobFromSent",  data ,  function( r ) {
        /*if (r == 'sent') {
          thisForm.slideUp('200').delay(300).remove();
          $("html, body").animate({ scrollTop: 0 }, "slow");
          $('.msg.SaveMsg').fadeIn();
        } else {
          alert('error');
        } */
     /*   alert(r);
      });  */
    }
      
      
  return false;
});

$("#browseBtn, #textFileShow").click(function() {
  $("#fileInput").click();
});

$("#fileInput").change(function() {
  var InputVal = $(this).val();
  $('#textFileShow').val(InputVal);
});


$("#SearchByReference").submit(function () {
  var refNum = $('#refNum').val(),
      error = 0;
  
  if (refNum == '') {
    $('#refNum').css('border', 'solid 1px red');
    error++;
  }
  
  if (error == 0) {
    $.post( "/property/refNumSearch",  {postRefNum: refNum} ,  function( r ) {
      if (r == 'no') {
        $('.errorMsg').slideDown(); 
      } else {
        window.location.href = "/property/overview/" + r ;
      }
    });
  }
  
  
  
  return false;
});        
  
  
});

function openSubMenu() {
	$('.ddMenu').slideDown();	
};
		
function closeSubMenu() {
	$('.ddMenu').slideUp();	
};

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};








    




