<?php

$lang['cal_su']			= "Неделя";
$lang['cal_mo']			= "Понеделник";
$lang['cal_tu']			= "Вторник";
$lang['cal_we']			= "Сряда";
$lang['cal_th']			= "Четвъртък";
$lang['cal_fr']			= "Петък";
$lang['cal_sa']			= "Събота";
$lang['cal_sun']		= "Неделя";
$lang['cal_mon']		= "Mon";
$lang['cal_tue']		= "Tue";
$lang['cal_wed']		= "Wed";
$lang['cal_thu']		= "Thu";
$lang['cal_fri']		= "Fri";
$lang['cal_sat']		= "Sat";
$lang['cal_sunday']		= "Sunday";
$lang['cal_monday']		= "Monday";
$lang['cal_tuesday']	= "Tuesday";
$lang['cal_wednesday']	= "Wednesday";
$lang['cal_thursday']	= "Thursday";
$lang['cal_friday']		= "Friday";
$lang['cal_saturday']	= "Saturday";
$lang['cal_jan']		= "Jan";
$lang['cal_feb']		= "Feb";
$lang['cal_mar']		= "Mar";
$lang['cal_apr']		= "Apr";
$lang['cal_may']		= "May";
$lang['cal_jun']		= "Jun";
$lang['cal_jul']		= "Jul";
$lang['cal_aug']		= "Aug";
$lang['cal_sep']		= "Sep";
$lang['cal_oct']		= "Oct";
$lang['cal_nov']		= "Nov";
$lang['cal_dec']		= "Dec";
$lang['cal_january']	= "Януари";
$lang['cal_february']	= "Февруари";
$lang['cal_march']		= "Март";
$lang['cal_april']		= "Април";
$lang['cal_mayl']		= "Май";
$lang['cal_june']		= "Юни";
$lang['cal_july']		= "Юли";
$lang['cal_august']		= "Август";
$lang['cal_september']	= "Септември";
$lang['cal_october']	= "Октомври";
$lang['cal_november']	= "Ноември";
$lang['cal_december']	= "Декември";


/* End of file calendar_lang.php */
/* Location: ./system/language/english/calendar_lang.php */