<?php
class MY_Pagination extends CI_Pagination {
    var $first_link          = false;
    var $next_link          = '';
    var $prev_link          = '';
    var $first_tag_open		= '<li>';
    var $first_tag_close	= '</li>';
    var $last_tag_open		= '<li>';
    var $last_tag_close		= '</li>';
    var $cur_tag_open		= '<li class="selected">';
    var $cur_tag_close		= '</li>';
    var $next_tag_open		= '<li class="next">';
    var $next_tag_close		= '</li>';
    var $prev_tag_open		= '<li class="prev">';
    var $prev_tag_close		= '</li>';
    var $num_tag_open		= '<li>';
    var $num_tag_close		= '</li>';
    var $last_link          = false;
    //var $num_links          = 10;
}