<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Memcache extends Memcached {
	
	var	
		$time_create = 0,
		$time_destroy = 0,

		$get_time = 0,
		$get_count = 0,
		$get_keys = array(),

		$set_time = 0,
		$set_count = 0,
		$set_keys = array(),

		$debug = false;

	function __construct() {
		$this->time_create = $this->getTime();

		if(func_num_args() == 1) {
			$arg_list = func_get_args();

			$this->debug = $arg_list[0];
		}
        $ci = CI_Controller::get_instance();
        $ci->config->load('memcache', true);
        $memServer = $ci->config->item('memcache');
		parent::__construct();
        self::addServer($memServer['server'], $memServer['port']);
	}

	function getTime() {
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}

	function get($key, $cache_cb=null, &$cas_token=null) {

		if($this->debug) $start_time = $this->getTime();

		$res = parent::get($key, $cache_cb, $cas_token);

		if($this->debug) {
			$end_time = $this->getTime();
			$this->get_time += ($end_time - $start_time);

			$this->get_count++;

			array_push($this->get_keys, 'get: '.$key);
		}

		return $res;
	}

	function getMulti($keys, &$cas_tokens=null, $flags=null) {

		if($this->debug) $start_time = $this->getTime();

		$res = parent::getMulti($keys, $cas_tokens, $flags);

		if($this->debug) {
			$end_time = $this->getTime();
			$this->get_time += ($end_time - $start_time);
			$this->get_count++;

			$this->get_keys = array_merge($this->get_keys, $keys);
		}

		return $res;
	}


	function set($key, $value=null, $expiration=null) {

		if($this->debug) $start_time = $this->getTime();

		$res = parent::set($key, $value, $expiration);

		if($this->debug) {
			$end_time = $this->getTime();
			$this->set_time += ($end_time - $start_time);

			$this->set_count++;
			array_push($this->set_keys, $key);
		}

		return $res;
	}

	function setMulti($keys, $expiration=null) {

		if($this->debug) $start_time = $this->getTime();

		$res = parent::setMulti($keys, $expiration);

		if($this->debug) {
			$end_time = $this->getTime();
			$this->set_time += ($end_time - $start_time);
			$this->set_count++;

			$this->set_keys += $keys;
		}

		return $res;
	}
}
?>
