<div class="clearfix inside-top contact-page">

        <div class="left-img" style="background-image: url(<?php echo base_url().$info['images'][0]['path'];?>)"></div>
      
        <div class="right-part text-field">
          <div class="padding20">
            <h2><?php echo $info['title_'.$this->lang->lang()];?></h2>
            <p><?php echo $info['text_'.$this->lang->lang()];?></p>
        </div>
  </div>
</div><!-- end of inside-top -->


<div id="contact-form" class="clearfix">
  <div class="c-c">
    <?php echo form_open('/'.$this->lang->lang().'/contacts#contact-form'); ?>
      <h1><?php echo lang('form_text');?></h1>
      <?php if($success){?>
      <p class="success"><?php echo lang('csuccess');?></p>
      <?php } ?>
  
      <div class="row clearfix">
        <div class="cell">
          <label><?php echo lang('cname');?>: <span<?php echo form_error('cname')? ' class="red"':'';?>>*</span></label>
          <input type="text" name="cname" value="<?php echo set_value('cname'); ?>" />
      </div>

      <div class="cell">
          <label><?php echo lang('cphone');?>: <span<?php echo form_error('cphone')? ' class="red"':'';?>>*</span></label>
          <input type="text" name="cphone" value="<?php echo set_value('cphone'); ?>" />
      </div>
  </div><!-- row -->

  <div class="row clearfix">
    <div class="cell">
      <label><?php echo lang('corg');?>:</label>
      <input type="text" name="corg" value="<?php echo set_value('corg'); ?>" />
  </div>

  <div class="cell">
      <label><?php echo lang('cemail');?> : <span<?php echo form_error('cemail')? ' class="red"':'';?>>*</span></label>
      <input type="text" name="cemail" value="<?php echo set_value('cemail'); ?>" />
  </div>
</div><!-- row -->

<div class="row clearfix">
  <label><?php echo lang('ctext');?>: <span<?php echo form_error('ctext')? ' class="red"':'';?>>*</span></label>
  <textarea name="ctext"><?php echo set_value('ctext'); ?></textarea>
</div><!-- row -->
<input type="submit" class="button" value="<?php echo lang('csend');?>" />

</form>
</div>
    </div><!-- end of shalevi -->