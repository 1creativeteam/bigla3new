  <div class="clearfix inside-top projects-page">















    <div class="left-img">







      <ul class="nav-projects">







        <?php $i=1; foreach($menup as $v){?>







        <li<?php echo $v['selected']? ' class="selected"':'';?>><a href="<?php echo site_url($v['link']);?>" title="<?php echo htmlspecialchars($v['title_'.$this->lang->lang()]);?>"><?php echo $v['title_'.$this->lang->lang()];?></a></li>







        <?php $i++;} ?>







      </ul>







      <?php







      $actual_link = "$_SERVER[REQUEST_URI]";







      $actual_link = explode('/', $actual_link);







      $lang = $actual_link[1];







      ?>







      <select id="navigation-mobile-projects">







        <option value="<?php if($lang=='bg') {echo "http://bigla3.com/bg/projects";} else{ echo "http://bigla3.com/en/projects";}?>">







          <?php if($lang=='bg') {echo "Изберете";} else{ echo "Choose";}?></option>







          <?php $i=1; foreach($menup as $v){?>















          <option value="<?php echo site_url($v['link']);?>"<?php echo $v['selected']? ' selected="selected"':'';?>><?php echo $v['title_'.$this->lang->lang()];?></option>







          <?php $i++;} ?>







        </select>















      </div>















      <div class="right-part text-field">







        <div class="padding20">







          <div class="short-links">







            <ul>







              <li class="icon1">







                <a href="<?php if($lang=='bg') {echo "http://bigla3.com/bg/projects/index/realizirani-proekti_14";} else{ echo "http://bigla3.com/en/projects/index/realizirani-proekti_14";}?>"><p><?php if($lang=='bg') { ?> Реализирани <span>проекти</span> <?php } else { ?> Realized <span>projects</span> <?php } ?></p></a>







              </li>







              <li class="icon2">







                <a href="<?php if($lang=='bg') {echo "http://bigla3.com/bg/projects/index/aktualni-proekti_5";} else{ echo "http://bigla3.com/en/projects/index/aktualni-proekti_5";}?>"><p><?php if($lang=='bg') { ?> Актуални <span>проекти</span> <?php } else { ?> Current <span>projects</span> <?php } ?></p></a>







              </li>







              <li class="icon3">







                <a href="<?php if($lang=='bg') {echo "http://bigla3.com/bg/projects/index/badeshti-proekti_15";} else{ echo "http://bigla3.com/en/projects/index/badeshti-proekti_15";}?>"><p><?php if($lang=='bg') { ?> Бъдещи <span>проекти</span> <?php } else { ?> Future <span>projects</span> <?php } ?></p></a>







              </li>







            </ul>







          </div>







          <h2><?php echo lang('projects_title');?> <span><?php echo $cat_title['title_'.$this->lang->lang()]? '/ '.$cat_title['title_'.$this->lang->lang()]: ''?></span></h2>















            <!--<div class="filters clearfix">







                <div class="styled-select">







                <?php print_r($this->config->item('towns'));?>







                  <select class="town" name="town">







                  	<option value="0"><?php echo lang('choose_town');?></option>







                    	<?php foreach($towns as $key => $val){?>







                    	<option value="<?php echo $val['id'];?>"><?php echo $val['town_'.$this->lang->lang()];?></option>







                    	<?php } ?>







                  </select>







                </div>















                <!--div class="styled-select">







                  <select>







                    <option>изберете вид имот</option>







                  </select>







                </div-->















              <!--  <div class="styled-select">







                  <select class="floor" name="floor">







                    <option value="0"><?php echo lang('choose_floor');?></option>







                    <?php for ($f=1; $f <= $max_floors ; $f++) {?>







                    <option value="<?php echo $f;?>"><?php echo $f;?></option>







                    <?php } ?>







                  </select>







                </div>























                <div class="right">







                  <span><?php echo lang('floor_area');?>: (<?php echo lang('area_units');?>)</span>







                  <input type="text" class="from" placeholder="<?php echo lang('from_text');?>" value="" />







                  <input type="text" class="to" placeholder="<?php echo lang('to_text');?>" value="" />















                </div>















              </div>-->







              <!-- end of filters -->















              <?php if(count($projects_list)>0){?>







              <div class="pr-list">







               <ul class="news-list clearfix">






                <?php if($showBellevue === 1){ ?>



                <li class="clearfix">







                  <a href="/<?= $this->lang->lang() ?>/projects/belvyu-rezidans" class="news-thumb" title="Bellevue">







                    <img src="/images/bellevue/thumbs/-2.jpg" alt="Bellevue Picture" />







                  </a>







                  <h4><a href="/<?= $this->lang->lang() ?>/projects/belvyu-rezidans" title="Bellevue">



                    <?php echo $belvue_name[$lang]?>



                  </a>



                </h4>







                <span class="date"></span>







                <p>



                  <?php echo character_limiter($belvue_desc[$lang], 300, ' ...');?>



                </p>







              </li>



              <?php } ?>







              <?php if($showTintyava === 1){ ?>



              <li class="clearfix">







                <a href="/<?= $this->lang->lang() ?>/projects/tintyava" class="news-thumb" title="Tintyava">







                  <img src="/<?= $tintyava[0]['main_image']?>" alt="Tintyava Picture" />







                </a>







                <h4><a href="/<?= $this->lang->lang() ?>/projects/tintyava" title="Tintyava"><?php echo character_limiter($tintyava[0]['free_text_'.$this->lang->lang()], 440, ' ...');?></a></h4>







                <span class="date"></span>







                <p>



                  <?php echo character_limiter($tintyava[0]['text_'.$this->lang->lang()], 440, ' ...');?>



                </p>







              </li>



              <?php } ?>







              <?php foreach($projects_list as $v){?>







              <li class="clearfix">







                <a href="<?php echo $v['link'];?>" class="news-thumb" title="<?php echo htmlspecialchars($v['title_'.$this->lang->lang()]);?>">







                 <?php if(!empty($v['images'][0]['path'])){?>







                 <img src="<?php echo base_url().$v['images'][0]['path'];?>" alt="<?php echo htmlspecialchars($v['title_'.$this->lang->lang()]);?>" />







                 <?php } ?>







               </a>







               <h4><a href="<?php echo $v['link'];?>" title="<?php echo htmlspecialchars($v['title_'.$this->lang->lang()]);?>"><?php echo $v['title_'.$this->lang->lang()];?></a></h4>







               <span class="date"><?php //echo date('d.m.Y', strtotime($v['datein']));?></span>







               <p><?php echo character_limiter($v['text_'.$this->lang->lang()], 440, ' ...');?></p>







             </li>







             <?php } ?>







           </ul>







           <?php } ?>







           <?php if(isset($paging)){?>







           <div class="pagination">







             <ul class="clearfix">







              <?php echo $paging;?>







            </ul>







          </div>







          <?php } ?>







        </div>







      </div>















    </div>







  </div><!-- end of inside-top -->















  <script type="text/javascript">







  $(function() {







    $('#navigation-mobile-projects').change(function(){







      var href = $(this).val();







      location.href = href;







    });















    $(document).delegate(".styled-select select, .from, .to", 'change', function(event) {







     var sdata = {id: '<?php echo $id;?>',







     floor: $('.floor').val(),







     town: $('.town').val(),







     from: $('.from').val(),







     to: $('.to').val()};







     $.ajax({







       url: '<?php echo site_url("projects/search");?>',







       type: "post",







       dataType: 'html',







       data: sdata,







       success: function(data, textStatus, XMLHttpRequest){







        $('.pr-list .news-list.clearfix').remove();







        $('.pr-list').html(data);







      },







      error: function(data) {







        console.warn(data);







      }







    });







   })















    $(document).delegate(".from, .to", 'keyup', function(event) {







     var sdata = {id: '<?php echo $id;?>',







     floor: $('.floor').val(),







     town: $('.town').val(),







     from: $('.from').val(),







     to: $('.to').val()};







     $.ajax({







       url: '<?php echo site_url("projects/search");?>',







       type: "post",







       dataType: 'html',







       data: sdata,







       success: function(data, textStatus, XMLHttpRequest){







        $('.pr-list .news-list.clearfix').remove();







        $('.pr-list').html(data);







      },







      error: function(data) {







        console.warn(data);







      }







    });







   })







  });







  </script>







