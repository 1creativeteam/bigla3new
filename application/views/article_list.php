<div class="clearfix inside-top news-page">

	<?php if(count($categories) > 0) { ?>

    <div class="left-img">

		<ul class="nav-projects">

			<?php if($this->lang->lang() == 'bg') { ?>
				<li><a href="/<?=$this->lang->lang()?>/news">Всички</a></li>
			<?php } else { ?>
				<li><a href="/<?=$this->lang->lang()?>/news">All</a></li>
			<?php } ?>

		<?php foreach ($categories as $cat) { ?>

			<li><a href="<?= $prepared_categories_url . $cat['id'] ?>"><?= $cat['name_'.$this->lang->lang()]?></a></li>

		<?php } ?>

		</ul>

    </div>

    <?php } ?>

    <div class="right-part text-field">

      <div class="padding20">

            <h2><?php echo $this->uri->segment(2)=='careers'? lang('careers'): lang('news');?></h2>

            <?php if(count($article_list)>0){?>

            <ul class="news-list clearfix">

            	<?php foreach($article_list as $v){?>

              	<li class="clearfix">

	                <a href="<?php echo $v['link'];?>" class="news-thumb" title="<?php echo htmlspecialchars($v['title_'.$this->lang->lang()]);?>">

	                	<?php if(!empty($v['images'][0]['path'])){?>

	                	<img src="<?php echo base_url().$v['images'][0]['path'];?>" alt="<?php echo htmlspecialchars($v['title_'.$this->lang->lang()]);?>" />

	                	<?php } ?>

	               	</a>

	                <h4><a href="<?php echo $v['link'];?>" title="<?php echo htmlspecialchars($v['title_'.$this->lang->lang()]);?>"><?php echo $v['title_'.$this->lang->lang()];?></a></h4>

	                <span class="date"><?php //echo date('d.m.Y', strtotime($v['datein']));?></span>

	                <p><?php echo character_limiter($v['text_'.$this->lang->lang()], 300);?></p>

              	</li>

              	<?php } ?>

            </ul>

            <?php } else { ?>

            	Няма намерени резултати.

          	<?php } ?>

            <?php if(isset($paging)){?>

			<div class="pagination">

		        <ul class="clearfix">

					<?php echo $paging;?>

				</ul>

			</div>

			<?php } ?>

    	</div>

  	</div>

</div><!-- end of inside-top -->

