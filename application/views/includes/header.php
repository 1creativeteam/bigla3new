<!DOCTYPE html>
<html>
	<head>
		<title><?php echo isset($page_title)?$page_title." :: ".SITE_NAME:SITE_NAME;?></title>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" user-scalable="no">
		<meta name='robots' content='index, follow' />
		<meta name="format-detection" content="telephone=no">
  		<link rel="shortcut icon" href="<?php echo base_url().$this->config->item('img');?>favicon.ico" type="image/x-icon" />
	    <meta name="description" content="<?php echo isset($page_desc)?$page_desc:SITE_DESC;?>">
	    <meta name="keywords" content="<?php echo isset($page_key)?$page_key:SITE_KEY;?>">
	    <meta name="author" content="">
	    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
  		<link rel="stylesheet" href="<?php echo base_url().$this->config->item('css');?>bootstrap.min.css">
  		<link rel="stylesheet" href="<?php echo base_url().$this->config->item('css');?>main.css">
  		<link rel="stylesheet" href="<?php echo base_url().$this->config->item('tintyavacss');?>lightbox.min.css">

  		<link rel="stylesheet" href="<?php echo base_url().$this->config->item('tintyavacss');?>threesixty.css">
  		<link rel="stylesheet" href="<?php echo base_url().$this->config->item('tintyavacss');?>style.css">

       	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="<?php echo base_url().$this->config->item('js');?>/jquery.js"><\/script>');</script>

    	<script type="text/javascript" src="<?php echo base_url().$this->config->item('js');?>bootstrap.min.js"></script>
    	<!-- <script type="text/javascript" src="<?php echo base_url().$this->config->item('js');?>jquery.js"></script> -->

    	<!-- move at the bottom before head close -->
		<script type="text/javascript" src="<?php echo base_url().$this->config->item('tintyavajs');?>jquery-2.2.4.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url().$this->config->item('tintyavajs');?>jquery.imagemapster.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url().$this->config->item('tintyavajs');?>threesixty.js"></script>

		<script type="text/javascript" src="<?php echo base_url().$this->config->item('js');?>jquery.ui.js"></script>
		<script type="text/javascript" src="<?php echo base_url().$this->config->item('js');?>unslider.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url().$this->config->item('js');?>jquery.maphilight.js"></script>
		<script type="text/javascript" src="<?php echo base_url().$this->config->item('js');?>grind.js"></script>
		<script type="text/javascript" src="<?php echo base_url().$this->config->item('js');?>ui.dropdownchecklist.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url().$this->config->item('js');?>magicsuggest-min.js"></script>


	</head>

	<body class="<?php echo isset($section)?$section:'';?>">		<!-- header -->
		<header>
		    <nav>
		      <div class="c-c">
		        <a href="/<?php echo $this->lang->lang();?>" title="logo" id="logo"></a>						<a id="tel" href="tel:+359 2 868 8005"><span ></span>+359 2 868 8005</a>						<a id="mail" href="mailto:office@bigla3.com"><span ></span>office@bigla3.com</a>		          <ul id="lang">
		            <li><a href="<?php echo str_replace('/en', '/bg', $_SERVER['REQUEST_URI']);?>"<?php echo $this->lang->lang()=='bg'? ' class="selected"':''?> title=""><span>BG</span></a></li>
		            <li><a href="<?php echo str_replace('/bg', '/en', $_SERVER['REQUEST_URI']);?>"<?php echo $this->lang->lang()=='en'? ' class="selected"':''?> title=""><span>EN</span></a></li>
		          </ul>
		          <ul id="navigation">
		            <?php $i=1; foreach($menu as $v){?>
						<li><a href="<?php echo site_url($v['link']);?>"<?php echo $v['target'];?> class="link<?php echo $i;?>" title="<?php echo htmlspecialchars($v['title_'.$this->lang->lang()]);?>"><?php echo $v['title_'.$this->lang->lang()];?></a></li>
					<?php $i++;} ?>
		          </ul>
		          <br clear="all" />

		          <select id="navigation-mobile">
		          	<option value="0"><?php echo lang('menu_choose');?></option>
		          	<?php foreach($menu as $v){?>
						<option value="<?php echo site_url($v['link']);?>"<?php echo $v['target'];?><?php echo $v['selected'];?><?php echo '';?>><?php echo $v['title_'.$this->lang->lang()];?></option>
					<?php } ?>
		          </select>
		      </div>
		    </nav>

		</header>		<script type="text/javascript">

				$(function() {
						$('#navigation-mobile').find(":selected").attr('selected', false);

						$('#navigation-mobile').change( function() {

							var href = $(this).val();

							if(href!=0) {

								location.href = href;

							}

						});

				});

		</script>
	<!-- content -->
	<div class="content">