<?php  	
$menu['menu'] = $this->menu_model->get_menu();
//$menu['sub'] = $this->menu_model->get_submenu(13);
if (!file_exists('application/views/'.$main_content.'.php')){
	redirect('/');
}
$this->load->view('includes/header', $menu);
$this->load->view($main_content);
$this->load->view('includes/footer'); 