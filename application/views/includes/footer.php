﻿  <div id="map">
              <span class="map-top"></span>
      <div class="c-c">

        <ul>
          <li class="i1">
            <p><span><?php echo lang('phone')?>: +359 2 868 8005</span><?php echo lang('fax')?>: +359 2 868 3095</p>
          </li>

          <li class="i2">
            <!-- <p><?php echo lang('address')?></p> -->
          </li>

          <li class="i3">
            <p><span>e-mail: office@bigla3.com</span></p>
          </li>
        </ul>

      </div>
    </div><!-- end of map -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAoZQNxbhZcc43kLJ_ff2I5KACTBCQ7mLg"></script>
    <script>

    var default_coordinates = {};
    default_coordinates.lat = 42.7027018;
    default_coordinates.lng = 23.311127899999974;
    var default_info = "гр. София ул. 'Опълченска' № 46-48";

    if ($('.custom_coordinates').length > 0) {
      default_coordinates.lat = $('.custom_coordinates .custom_lat').attr('data-cord');
      default_coordinates.lng = $('.custom_coordinates .custom_lng').attr('data-cord');
      default_info = $('.custom_coordinates').attr('data-info');
    }



    function initialize()
      {
      var mapProp = {
      center: {lat: Number(default_coordinates.lat) , lng: Number(default_coordinates.lng)},/*koardinata na centara na kartata za desctop*/
      zoom:16,
      scrollwheel: false,
      draggable: true,
      mapTypeId:google.maps.MapTypeId.ROADMAP
      };
      var mapProp768 = {
      center: {lat: Number(default_coordinates.lat) , lng: Number(default_coordinates.lng)},/*koardinata na centara na kartata za telefon*/
      zoom:17,
      scrollwheel: false,
      draggable: true,
      mapTypeId:google.maps.MapTypeId.ROADMAP
      };


      if (screen.width < 768) {
      var map=new google.maps.Map(document.getElementById("googleMap"),mapProp768);
      var thePanorama = map.getStreetView();

      google.maps.event.addListener(thePanorama, 'visible_changed', function() {

          if (thePanorama.getVisible()) {
            $('#googleMap>div>div>div>div>div:nth-of-type(n+5)').css({
              "-webkit-filter": "inherit",
              "filter": "inherit",

            });

          } else {

            $('#googleMap>div>div>div>div>div:nth-of-type(n+5)').css({
              "-webkit-filter": "invert(90%)  grayscale(1)",
              "filter": "invert(90%)  grayscale(1)",

            });

          }

      });

      }
      else {
      var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

          var thePanorama = map.getStreetView();

          google.maps.event.addListener(thePanorama, 'visible_changed', function() {

              if (thePanorama.getVisible()) {
                $('#googleMap>div>div>div>div>div:nth-of-type(n+5)').css({
                  "-webkit-filter": "inherit",
                  "filter": "inherit",

                });

              } else {

                $('#googleMap>div>div>div>div>div:nth-of-type(n+5)').css({
                  "-webkit-filter": "invert(90%)  grayscale(1)",
                  "filter": "invert(90%)  grayscale(1)",

                });

              }

          });
      }

      /*masiv - texa na info windowsa i koardinati na markera*/
      var locations = [
      ['<div><p>'+default_info+'</p><a href="https://goo.gl/mjs3xy" target="_blank" >See in Google Maps</a></div>',
       Number(default_coordinates.lat), Number(default_coordinates.lng)]];
      var infowindow = new google.maps.InfoWindow;
      var marker, i, image;
      /*kartinkara za marker*/
      if ($('.custom_marker').length) {
        image = $('.custom_marker').attr('data-src');
      } else {
        image = '<?php echo base_url().$this->config->item('css');?>map-m.png';
      }
      for (i = 0; i < locations.length; i++) {
      marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[i][1], locations[i][2]),
      map: map,
      icon: image
      });
      /*info windows*/
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
            infowindow.setContent(locations[i][0]);
            infowindow.open(map, marker);

      }
      })(marker, i));
      /*zatvarqne na info windows pri klik kadeto i da e na kartata*/
      google.maps.event.addListener(map, 'click', function(event) {
      if (infowindow) {
       infowindow.close();
      };
      })
      }
      }
      google.maps.event.addDomListener(window, 'load', initialize);

    </script>
    <style>

      .black {
      position: absolute;
      width: 100%;

      z-index: 9999;
      top: 0;
      bottom: 0;
      }
      .out {
      position: relative;
      }
      #map {
      background: none;
      height: auto;
      }
      #map ul{
      top: 0;
      position: absolute;
      z-index: 9999999;
      width: 100%;
      top: 194px;
      padding-top: 0;
      pointer-events: none;
      }
      #googleMap {
      width: 100%;
      height: 440px;
      }
      #map ul li.i2 {
      background: none;
      }
      #googleMap p {
      color: #000;
      }
      #googleMap a {
      color: #cf1c23;
      }

        @media (max-width: 480px) {
          #googleMap {
            width: 100%;
            height: 600px;
          }
          #map ul{
            top: 100px;
          }
        }
    </style>
<div id="googleMap"></div>
  </div><!-- end of content div -->

  <footer class="container">
    <div class="c-c row">
      <div class="col-sm-3 text-center">
        <p>Copyright © <?php echo date('Y');?> Bigla3.com. <?php echo lang('all_rights')?></p>
      </div>
      <div class="col-sm-3 text-center ">
          <a href="http://shalevigroup.com/" target="_blank" title="Shalev Investment Group" id="logo-footer"><img src="<?php echo base_url().$this->config->item('css');?>shalevi.png" alt=""></a>
      </div>
    <div class="col-sm-4  text-center">
      <ul>
        <li class="social si1"><a href="https://www.facebook.com/bigla3" target="_blank" title="bigla facebook"></a></li>
        <li class="social si2"><a href="https://www.linkedin.com/company/bigla-iii-ltd." target="_blank" title="bigla linkedin"></a></li>
        <li class="social si3 soc-p"><a href="https://www.youtube.com/channel/UC1ApxGfqf5Ob33uMt_JjFIw" target="_blank" title="bigla youtube"></a></li>
        <li><a href="<?php echo site_url('about_us');?>" title=""><?php echo lang('about_us')?></a></li>
        <li><span>|</span></li>
        <li><a href="<?php echo site_url('contacts');?>" title=""><?php echo lang('contacts')?></a></li>
      </ul>
    </div>
    <div class="col-sm-2 text-center">
      <a href="http://onecreative.eu/" target="_blank" title="OneCreative" alt="OneCreative Web Design Studio"><p>Created By</p> <span></span></a>
    </div>
    </div><!-- end of center div -->
  </footer>
<!--
  <footer>
    <div class="container">
    <div class="row">
        <div class="col-sm-3 text-center">
          <p class="pull-left">Copyright &copy; <?php echo date("Y") ?> </br>Bigla3.com. Всички права запазени.</p>
      </div>
      <div class="col-sm-3 text-center ">
          <a href="http://shalevigroup.com/" target="_blank" title="Shalev Investment Group" id="logo-footer"><img src="img/shalevi.png" alt=""></a>
      </div>
      <div class="col-sm-3  text-center">

          <ul class="">
              <li class="social si1"><a href="{!! url('') !!}" target="_blank" title="bigla facebook"></a></li>
              <li class="social si2"><a href="{!! url('') !!}" target="_blank" title="bigla linkedin"></a></li>
              <li class="social si3 soc-p"><a href="{!! url('') !!}" target="_blank" title="bigla youtube"></a></li>
          </ul>
      </div>
        <div class="col-sm-3 text-center">
        <a href="http://onecreative.eu/" target="_blank" title="OneCreative" alt="OneCreative Web Design Studio"><p>Created By</p> <span></span></a>
        </div>
    </div>
    </div>
</footer> -->
<script type="text/javascript" src="<?php echo base_url().$this->config->item('tintyavajs');?>lightbox.min.js"></script>		<script type="text/javascript" src="<?php echo base_url().$this->config->item('tintyavajs');?>scripts.js"></script>
</body>
</html>
