<div class="clearfix inside-top-big news-page">
		<div class="c-c text-field clearfix">
		  <div class="padding20 clearfix">
		  	<div class="big-thumb ">
			<?php if(!empty($info['images'])){?>
			<?php foreach ($info['images'] as $img) {?>
				<img src="<?php echo $this->uri->segment(2)=='apartments'?  base_url().str_replace('_622X422', '_300X', $img['path']): base_url().$img['path'];?>" alt="<?php echo htmlspecialchars($info['title_'.$this->lang->lang()]);?>" />
			<?php } ?>
			<?php }else{ ?>
				&nbsp;
			<?php } ?>
			</div>
			<div class="news-content ">
			  	<h2><?php echo $info['title_'.$this->lang->lang()];?></h2>
			  	<span class="date"><?php echo isset($info['sqm']) && $info['sqm']>0? lang('floor_area').' '.$info['sqm'].' '.lang('area_units'): date('d.m.Y', strtotime($info['datein']));;?></span>
			  	<p><?php echo $info['text_'.$this->lang->lang()];?></p>
				<?php if($this->uri->segment(2)=='apartments'){?>
			  	<a href="" class="back" onclick="window.history.back();return false;"><?php echo lang('back');?></a>
			  	<?php } ?>
			</div>
		</div><?php if(!empty($gallery)){?>			<div class="padding20 clearfix">			<div class="row gallery">				<?php foreach ($gallery as $picture) { ?>					<div class="col-sm-3">										<a href="<?php echo base_url() . $picture['picture'] ?>" data-lightbox="image-1">												<img src="<?php echo base_url() . $picture['picture'] ?>" alt="">										</a>								</div>				<?php } ?>			</div>		</div><?php } ?>
	  </div>

</div><!-- end of inside-top -->
<!-- GALLERY -->


<?php if(!empty($more_news)){?>
<div class="more-news c-c clearfix">

  <h2><?php echo $this->uri->segment(2)=='careers'? lang('more_careers'): lang('more_news');?></h2>
	<ul class="news-list clearfix">
		<?php foreach($more_news as $v){?>
		<li class="clearfix">
			<a href="<?php echo $v['link'];?>" class="news-thumb" title="<?php echo htmlspecialchars($v['title_'.$this->lang->lang()]);?>">
				<?php if(!empty($v['images'][0]['path'])){?>
				<img src="<?php echo base_url().$v['images'][0]['path'];?>" alt="<?php echo htmlspecialchars($v['title_'.$this->lang->lang()]);?>" />
				<?php } ?>
			</a>
			<h4><a href="<?php echo $v['link'];?>" title=""><?php echo $v['title_'.$this->lang->lang()];?></a></h4>
			<span class="date"><?php echo date('d.m.Y', strtotime($v['datein']));?></span>
			<p><?php echo character_limiter($v['text_'.$this->lang->lang()], 300);?></p>
		</li>
		<?php } ?>
	</ul>
</div><!-- end of more-news -->
<?php } ?>