<div class="row margin2">
    <div class="span8 floatLeft767">
        <div class="bg1">
            <div>
                <h1><?php echo isset($info['title'])?$info['title']:'';?></h1>
            </div>
            <hr>
            <div> 
			  <?php if(isset($info['images'][0]['path']) && $info['showimages']=='n'){?><img src="<?php echo base_url().$info['images'][0]['path'];?>" alt="<?php echo htmlspecialchars($info['title']);?>" class="img-left"><?php } ?>
			  <?php echo (isset($info['text'])?$info['text']:'');?>
			</div>
        </div>
    </div>
<?php if($info['images']!=false && count($info['images'])>1){?>           
    <div class="span4 column2">
        <div class="bg1">
            <div>
                <h1>ГАЛЕРИЯ</h1>
            </div>
            <hr>
            <div>
            <ul class="list3">
            	<?php foreach($info['images'] as $image){?>
				<li><a href="<?php echo base_url().str_replace($size[2]['enl'], '', $image['path']);?>"><img src="<?php echo base_url().str_replace($size[2]['enl'], $size[0]['enl'], $image['path']);?>" alt="<?php echo htmlspecialchars($info['title']);?>"></a></li>
				<?php } ?>
				</ul>
				<script>
			    $(function (){
			        $('.list3>li>a')
			        .prepend('<span class="sitem_over"><strong></strong></span>')
			        .touchTouch();
			    })
			</script>
			</div>
		</div>
	</div>
<?php } ?>
</div>
     
