<div class="clearfix inside-top-big" style="overflow-x: hidden;">

  <div class="c-c text-field clearfix">

    <div class="padding20">

      <h2 class="text-center"><?= $translations[$lang]['page_title']?></h2>

      <div class="row video" style="margin-top: 50px;">

        <div class="col-sm-8 col-sm-offset-2">

          <div class="embed-responsive embed-responsive-16by9">

            <iframe width="700" height="394" src="https://www.youtube.com/embed/ff2a5FqM35E?list=PLDx_vRX4isyDdldZzqEkNOdrGprYKmh4w?autoplay=false" frameborder="0" allowfullscreen></iframe>

          </div>

        </div>

      </div>

      <div class="row video" style="margin-top: 50px;">

        <div class="col-sm-8 col-sm-offset-2">

          <div class="embed-responsive embed-responsive-16by9">

            <iframe width="700" height="394" src="https://www.youtube.com/embed/J6csORCJ-dg?list=PLDx_vRX4isyDdldZzqEkNOdrGprYKmh4w?autoplay=false" frameborder="0" allowfullscreen></iframe>

          </div>

        </div>

      </div>

      <div class="row description" style="margin-top: 50px;">

        <div class="col-sm-10 col-sm-offset-1 text-center">
          <?= $translations[$lang]['description']?>
        </div>

      </div>

      <div class="row gallery">

        <h3><?= $translations[$lang]['gallery_title']?></h3>

        <div class="grid">

          <div class="grid-sizer"></div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-1.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-1.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-2.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-2.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-3.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-3.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-4.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-4.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-5.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-5.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-6.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-6.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-7.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-7.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-8.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-8.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-9.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-9.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-10.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-10.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-11.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-11.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-12.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-12.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-13.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-13.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-14.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-14.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-15.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-15.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-16.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-16.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-17.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-17.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-18.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-18.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-19.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-19.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-20.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-20.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-21.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-21.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-22.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-22.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-23.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-23.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-24.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-24.jpg" class="img-responsive">

            </a>

          </div>

          <div class="grid-item">

            <a data-lightbox="image-1" href="http://bigla3.com/images/bellevue/-25.jpg">

              <img alt="" src="http://bigla3.com/images/bellevue/thumbs/-25.jpg" class="img-responsive">

            </a>

          </div>

        </div>

      </div>

    </div>

  </div>

</div>

<div class="custom_coordinates" style="display:none;" data-info=" гр. София, бул. „Арсеналски” 121">

  <div class="custom_lat" data-cord="42.672546"></div>

  <div class="custom_lng" data-cord="23.309604"></div>

</div>

<div class="custom_marker hide" data-src="http://bigla3.com/assets/css/house_icon.png"></div>

<style>

  .grid-sizer,

  .grid-item { width: 20%; }

  .grid-item { padding: 0 5px 5px 0; }

</style>

<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>

<script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>

<script>

  $(document).ready(function () {

    var $grid = $('.grid').imagesLoaded( function() {

      // init Isotope after all images have loaded

      $('.grid').isotope({

        // set itemSelector so .grid-sizer is not used in layout

        itemSelector: '.grid-item',

        percentPosition: true,

        masonry: {

          // use element for option

          columnWidth: '.grid-sizer'

        }

      });

    });

  });

</script>
