    <section id="slider-index">
      <ul>
      <?php foreach ($home as $v) {?>
        <?php if(isset($v['image']['path'])){?>
          <li class="slide1" data-href="<?php echo $v['link_'.$this->lang->lang()];?>" style="background: url(<?php echo base_url().$v['image']['path'];?>) ; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
              <h1 class="c-c"><a href="<?php echo $v['link_'.$this->lang->lang()];?>" title="<?php echo htmlspecialchars($v['title_'.$this->lang->lang()]);?>"><?php echo !$this->agent->is_mobile()? $v['title_'.$this->lang->lang()]: '';?></a></h1>
         </li>
         <?php }elseif($v['video_type']){?>
          <li class="slide1" data-video="<?php echo $v['video_path'];?>" style="background: url(<?php echo isset($v['image']['path'])? base_url().$v['image']['path']: $v['video_image'];?>) ; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
             
              <h1 class="c-c"> <span class="play"></span><a href="<?php echo $v['link_'.$this->lang->lang()];?>" title="<?php echo htmlspecialchars($v['title_'.$this->lang->lang()]);?>"><?php echo !$this->agent->is_mobile()? $v['title_'.$this->lang->lang()]: '';?></a></h1>
         </li>
         <?php } ?>
      <?php } ?>
      </ul>
        <div class="slider-arrows"><a href="#" class="unslider-arrow next" data-distance="1">next</a><a href="#" class="unslider-arrow prev" data-distance="-1">prev</a></div>
    </section><!-- end of slider index -->
    <div id="i-projects" class="clearfix">
      <div class="c-c">
        <ul>
          <li>
            <a href="<?php echo '/'.$this->lang->lang().'/projects/index/реализирани-проекти_14';?>" class="icon1" title="">
              <span></span>
              <h2><?php echo lang('home_link_text1');?></h2>
            </a>
          </li>
          
          <li>
            <a href="<?php echo '/'.$this->lang->lang().'/projects/index/актуални-проекти_5';?>" class="icon2" title="">
              <span></span>
              <h2><?php echo lang('home_link_text2');?></h2>
            </a>
          </li>
          
          <li>
            <a href="<?php echo '/'.$this->lang->lang().'/projects/index/бъдещи-проекти_15';?>" class="icon3" title="">
              <span></span>
              <h2><?php echo lang('home_link_text3');?></h2>
            </a>
          </li>
        </ul>
      </div>
    </div><!-- end of i-projects -->
    <script type="text/javascript" src="<?php echo base_url().$this->config->item('js');?>fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().$this->config->item('js');?>fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
    <script>
        var sslider = $('#slider-index').unslider({ speed: 500, delay: 3000,  complete: function() {}, keys: true, dots: true, fluid: true });
    
         $(function() {
            
            $('.unslider-arrow').click(function() {
                var fn = this.className.split(' ')[1];
                sslider.data('unslider')[fn]();
            });

            $('.slide1').click(function(){
                var href = $(this).data('href');
                var video = $(this).data('video');
                
                if(video!=undefined){
                    $.fancybox.open({
                     'href' : video.replace(new RegExp("watch\\?v=", "i"), 'v/'),
                      type  : 'iframe',
                      padding : 5
                    });
                }else{
                  location.href = href;
                }
                return false;
            });
        });
    </script>
