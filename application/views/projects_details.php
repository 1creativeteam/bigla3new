<div class="clearfix inside-top-big project-inside">


        <div class="c-c text-field clearfix">
          <div class="padding20">
            <h2><?php echo lang('projects_title');?> <span><?php echo $cat_title['title_'.$this->lang->lang()]? '/ '.$cat_title['title_'.$this->lang->lang()]: ''?></span></h2>

            <div class="gallery-p clearfix">
             <a rel="gallery" data-lightbox="gallery" href="<?php echo base_url().str_replace('_622X422', '', $info['images'][0]['path']);?>" title="<?php echo htmlspecialchars($info['title_'.$this->lang->lang()]);?>" class="big-img fancybox">
                 <?php if(!empty($info['images'])){?>
              <img src="<?php echo base_url().$info['images'][0]['path'];?>" alt="<?php echo htmlspecialchars($info['title_'.$this->lang->lang()]);?>" />
              <?php } ?>
              </a>



              <ul class="all-images">
                <?php if(!empty($info['images'])){?>
                <?php $i=1; foreach ($info['images'] as $img) {?>
                    <li><a class="fancybox" rel="gallery" href="<?php echo base_url().str_replace('_622X422', '', $img['path']);?>" title="<?php echo htmlspecialchars($info['title_'.$this->lang->lang()]);?>">
                      <img src="<?php echo base_url().str_replace('_622X422', '_200X136', $img['path']);?>" data-src="<?php echo base_url().$img['path'];?>" data-bigsrc="<?php echo base_url().str_replace('_622X422', '', $img['path']);?>" alt="<?php echo htmlspecialchars($info['title_'.$this->lang->lang()]);?>" /></a></li>
                <?php $i++;} ?>
                <?php } ?>
             </ul>

            </div><!-- end of gallery -->


            <div class="ad-info clearfix">
              <h3>"<?php echo $info['title_'.$this->lang->lang()];?>" <?php echo isset($town['town_'.$this->lang->lang()])? ', '.lang('town').' '.$town['town_'.$this->lang->lang()]:'';?></h3>
              <?php if($info['sqm']){?><span class="floor"><?php echo lang('floor_area');?> <?php echo $info['sqm'];?> <?php echo lang('area_units');?></span><?php } ?>
              <?php if($info['rooms']){?><span class="room"> <?php echo $info['rooms'];?> <?php echo lang('rooms');?></span><?php } ?>
              <p><?php echo $info['text_'.$this->lang->lang()];?></p>
            </div><!-- end of ad-info -->

            <?if (!empty($floors)){?>
            <div class="floor-plan clearfix">
                <?if (count($floors)>1){?>
                <div class="floors-nav clearfix">
                    <ul>
                        <?php foreach ($floors as $k => $v) { ?>
                           <li><a href="" class="allfl af<?php echo $k;?>" data-show="<?php echo $k;?>" title="<?php echo htmlspecialchars($v['title_'.$this->lang->lang()]);?>"><?php echo $v['title_'.$this->lang->lang()];?></a></li>
                        <? } ?>
                    </ul>
                </div>
                 <?php } ?>

                <div class="sketch">
                    <?php foreach ($floors as $key => $val) {?>
                    <?php if(isset($val['images'][0]['path'])){?>
                    <img class="show<?php echo $key;?> hide show-img" src="<?php echo base_url().$val['images'][0]['path'];?>" alt="<?php echo htmlspecialchars($val['title_'.$this->lang->lang()]);?>" />
                    <?php } ?>
                    <? } ?>
                </div>

            </div><!-- end floor plan -->
            <?php } ?>
            <div class="ap-list">

              <h5><?php echo $info['free_text_'.$this->lang->lang()];?></h5>
                <?php foreach ($floors as $zkey => $mval) {
                    $app = $this->projects_model->get_articles2parent($mval['id']);
                    if(count($app)>0){
                ?>

              <ul class="news-list clearfix hide ap<?php echo $zkey;?>">
                <?php foreach ($app as $key => $val) {?>
                <li class="clearfix">
                    <a href="<?php echo $val['link'];?>" class="news-thumb" title="<?php echo htmlspecialchars($val['title_'.$this->lang->lang()]);?>">
                        <?php if(isset($val['images'][0]['path'])){?>
                        <img src="<?php echo base_url().$val['images'][0]['path'];?>" alt="<?php echo htmlspecialchars($val['title_'.$this->lang->lang()]);?>" />
                        <?php } ?>
                    </a>
                    <h4><a href="<?php echo $val['link'];?>" title="<?php echo htmlspecialchars($val['title_'.$this->lang->lang()]);?>"><?php echo $val['title_'.$this->lang->lang()];?></a></h4>
                    <span class="date"><?php echo $val['sqm']>0? lang('floor_area').' '.$val['sqm'].' '.lang('area_units'):'';?></span>
                    <p><?php echo character_limiter($val['text_'.$this->lang->lang()], 300);?></p>
                </li>
                <? } ?>
              </ul>
              <? } } ?>

            </div><!-- end of ap-list -->
          </div>
      </div>
    </div><!-- end of inside-top -->
    <link rel="stylesheet" href="<?php echo base_url().$this->config->item('js');?>fancybox/jquery.fancybox.css">
    <script type="text/javascript" src="<?php echo base_url().$this->config->item('js');?>fancybox/jquery.fancybox.js"></script>
    <script>
        $(function() {

            $(".fancybox").fancybox({
                openEffect  : 'none',
                closeEffect : 'none',
                padding     : 5
             });

            $('.all-images li a').hover(function(){
                var src = $(this).find('img').data('src');
                var bigsrc = $(this).find('img').data('bigsrc');
                $('.gallery-p a.big-img').attr('href', bigsrc);
                $('.gallery-p a.big-img').find('img').attr('src', src);
                return false;
            })
            .click(function(){
                var src = $(this).find('img').data('src');
                var bigsrc = $(this).find('img').data('bigsrc');
                $('.gallery-p a.big-img').attr('href', bigsrc);
                $('.gallery-p a.big-img').find('img').attr('src', src);
                return false;
            });

            $('.floors-nav li a.af0').addClass('selected');
            $('img.show0').removeClass('hide').show();
            $('.news-list.ap0').show();

            $('.allfl').click(function(){
                $('.floors-nav li a.allfl').removeClass('selected');
                var id = $(this).data('show');
                $(this).addClass('selected');
                $('.show-img').addClass('hide');
                $('img.show' + id).removeClass('hide').show();
                $('.news-list').hide();
                $('.news-list.ap' + id).show();
                return false;
            });
        });
    </script>
