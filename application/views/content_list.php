<section id="home" class="content-list">
	<article class="article home" id="contlist">
		<h2 class="title articles"><?php echo $page_title;?><span class="display-inline"></span></h2>
	<?php if(count($content_list)>0){?>
	<?php foreach($content_list as $v){?>
		<article>
			<?php if(isset($v['image']['path'])){?>
				<a href="<?php echo base_url().$this->uri->segment(1).'/detailed/'.friendlyURL($v['title'], 'latin').'_'.$v['id'];?>">
					<img src="<?php echo base_url().$v['image']['path'];?>" class="display-inline" alt="<?php echo htmlspecialchars($v['title']);?>" />
				</a>
			<?php } ?>
			<div class="text display-inline<?php echo (!isset($v['image']['path'])?' auto':'')?>">
				<h2><a href="<?php echo base_url().$this->uri->segment(1).'/detailed/'.friendlyURL($v['title'], 'latin').'_'.$v['id'];?>" title="<?php echo htmlspecialchars($v['title']);?>"><?php echo character_limiter($v['title'], 90);?></a></h2>
				<p><?php echo strip_tags(character_limiter($v['text'], 190));?></p>
				<a href="<?php echo base_url().$this->uri->segment(1).'/detailed/'.friendlyURL($v['title'], 'latin').'_'.$v['id'];?>" class="more">прочети още</a>
			</div>
		</article>
	<?php } } ?>
	</article>
	<!--pagination-->
	<?php if(isset($paging)){?>
	<ul class="paging">
		<?php echo $paging;?>
	</ul>
	<?php } ?>
</section>
