   <div class="clearfix inside-top-big proud-page">
        <div class="c-c text-field clearfix">
          <div class="padding20">
            <h2><?php echo $parent['title_'.$this->lang->lang()];?></h2>      
            <ul class="proud-nav clearfix">
              <?php $j=1; for ($i=0; $i < 3; $i++) {?>
                <li>
                <a href="#" class="p<?php echo $j;?>" data-id="<?php echo $j;?>" title="<?php echo htmlspecialchars($rows[$i]['title_'.$this->lang->lang()]);?>">
                  <span></span>
                  <h3><?php echo $rows[$i]['title_'.$this->lang->lang()]?></h3>
                </a>
              </li>
              <?php $j++; } ?>         
            </ul>
          </div>
      </div>
    </div><!-- end of inside-top -->
    <?php $l=1; foreach ($rows as $v) {?>
    <div class="clearfix c-c prices" id="p<?php echo $l;?>">
        <h2><?php echo $v['title_'.$this->lang->lang()];?></h2>
        <p><?php echo $v['text_'.$this->lang->lang()];?></p>

        <ul class="price-list clearfix">
            <?php foreach ($v['images'] as $img) {?>
            <li><a rel="gallery<?php echo $l;?>" class="fancybox" href="<?php echo base_url().str_replace('_800X530', '', $img['path']);?>"><img src="<?php echo base_url().str_replace('800X530', '155X250', $img['path']);?>" alt="<?php echo htmlspecialchars($v['title_'.$this->lang->lang()]);?>" /></a></li>
            <?php } ?>
        </ul>
    </div>
    <?php $l++;} ?>
    <!-- end of inside-top -->
    <link rel="stylesheet" href="<?php echo base_url().$this->config->item('js');?>fancybox/jquery.fancybox.css">
    <script type="text/javascript" src="<?php echo base_url().$this->config->item('js');?>fancybox/jquery.fancybox.js"></script>
        
    <script>
        $(function() {
            $(".fancybox").fancybox({
                openEffect  : 'none',
                closeEffect : 'none',
                padding     : 5
            });

            $('.proud-nav li a.p1').addClass('selected');
            $('.c-c.prices').hide();
            $('#p1').show();
            $('.proud-nav li a').click(function(){
                $('.proud-nav li a').removeClass('selected');
                var id = $(this).data('id');
                $(this).addClass('selected');
                $('.prices').hide();
                $('#p' + id).show();
                return false;
            });
        });
    </script>