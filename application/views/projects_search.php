
            
 				<?php if(count($projects_list)>0){?>
        <ul class="news-list clearfix">
        	<?php foreach($projects_list as $v){?>
          	<li class="clearfix">
              <a href="<?php echo $v['link'];?>" class="news-thumb" title="<?php echo htmlspecialchars($v['title_'.$this->lang->lang()]);?>">
              	<?php if(!empty($v['images'][0]['path'])){?>
              	<img src="<?php echo base_url().$v['images'][0]['path'];?>" alt="<?php echo htmlspecialchars($v['title_'.$this->lang->lang()]);?>" />
              	<?php } ?>
             	</a>
              <h4><a href="<?php echo $v['link'];?>" title="<?php echo htmlspecialchars($v['title_'.$this->lang->lang()]);?>"><?php echo $v['title_'.$this->lang->lang()];?></a></h4>
              <span class="date"><?php //echo date('d.m.Y', strtotime($v['datein']));?></span>
              <p><?php echo character_limiter($v['text_'.$this->lang->lang()], 300);?></p>
          	</li>
          	<?php } ?>
        </ul>
        <?php } ?>
	            