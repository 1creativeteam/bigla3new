    <div class="clearfix inside-top service-page">
      
      <div class="left-img" style="background: url(<?php echo base_url($parent['image']['path']);?>) no-repeat right;"></div>
      
        <div class="right-part text-field">
          <div class="padding20">
            <h2><?php echo $parent['title_'.$this->lang->lang()];?></h2>
            
            <ul class="service-nav">
              <?php $j=1; for ($i=0; $i < 4; $i++) {?>
                <li>
                <a href="#" class="s<?php echo $j;?>" data-id="<?php echo $j;?>" title="<?php echo htmlspecialchars($parent['title_'.$this->lang->lang()]);?>">
                  <span></span>
                  <h3><?php echo $rows[$i]['title_'.$this->lang->lang()]?></h3>
                </a>
              </li>
              <?php $j++; } ?>
            </ul>
          </div>
          
  
      
      </div>
    </div><!-- end of inside-top -->
    
    <?php $l=1; foreach ($rows as $v) {?>
    <div class="clearfix inside-top inside-middle service-page" id="s<?php echo $l;?>">
      
      <div class="right-img" style="background: url(<?php echo base_url().$v['images'][0]['path'];?>) no-repeat right;"></div>
      
        <div class="left-part text-field">
          <div class="padding20">
            <h2><?php echo strip_tags($v['title_'.$this->lang->lang()]);?></h2>
            <p><?php echo $v['text_'.$this->lang->lang()];?></p>
          </div>
      </div>
    </div> 
    <?php  $l++;} ?>
    <script>
        $(function() {
            $('.service-page.inside-middle').hide();
            $('.service-nav li a.s1').addClass('selected');
            $('#s1').show();
            $('.service-nav li a').click(function(){
                $('.service-nav li a').removeClass('selected');
                var id = $(this).data('id');
                $(this).addClass('selected');
                $('.inside-middle').hide();
                $('#s' + id).show();
                return false;
            });
        });
    </script>
   <!-- end of inside-top -->