<div class="clearfix inside-top about-page">
      
      <div class="left-img" style="background-image: url(<?php echo base_url().$info['images'][0]['path'];?>)"></div>
      
        <div class="right-part text-field">
          <div class="padding20">
            <h2><?php echo $info['title_'.$this->lang->lang()];?></h2>
            <p><?php echo $info['text_'.$this->lang->lang()];?></p>
          </div>
      </div>
    </div><!-- end of inside-top -->

    <div id="shalevi" class="clearfix">
      <div class="c-c">
        <ul>
          <li class="shalev1">
              <span class="us"></span>
              <h2><?php echo lang('founder_name');?></h2>
              <p>/<?php echo lang('founder');?>/</p>
              <span class="sign"></span>
          </li>
          
          <li class="shalev2">
              <span class="us"></span>
              <h2><?php echo lang('co_founder_name');?></h2>
              <p>/<?php echo lang('co_founder');?>/</p>
              <span class="sign"></span>
          </li>
          
        </ul>
      </div>
    </div><!-- end of shalevi -->