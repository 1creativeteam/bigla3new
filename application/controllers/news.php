<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class News extends CI_Controller {



	public function __construct()

	{

		parent::__construct();

		$this->load->model('news_model');

		$this->load->model('categories_model');

		$this->load->helper('func_helper');

	}



	public function index()

	{

		$this->load->helper('text');

		$this->load->library('pagination');

		

		// page settings

		$data['page_title'] = lang('news');

		$data['page_key'] = '';

		$data['page_desc'] = '';

		//pagination

		$config['base_url'] = base_url().$this->lang->lang()."/news/index";

		$config['total_rows'] = $this->news_model->count_news();

		$config['per_page'] = 5;

		$config["uri_segment"] = 4;

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

		$data['article_list'] = $this->news_model->get_article($config['per_page'], $page);

		$data['paging'] = $this->pagination->create_links();

		$data['categories'] = $this->categories_model->get_categories();

		$data['prepared_categories_url'] = base_url().$this->lang->lang()."/news/category/";

		$data['section'] = "news";

		$data['main_content'] = "article_list";



		//breadcrumbs

		//$this->config->set_item("breadcrumbs", "<a href='/'>Начало</a>".$this->config->item('breadcrumbs_delimiter')."<a href='".base_url('news')."'>Новини</a>");

		$this->load->view("includes/template", $data);

	}



	public function detailed()
	{
		//get categeries id

		if($this->uri->segment(3)!=""){

			$id = explode("_", $this->uri->segment(3));

			if(count($id)<2){

				redirect("/");

			}

			$id = $id[1];

		}

		$data['info'] = $this->news_model->details($id);

		$queryGallery = $this->db->get_where('news_gallery', array('news_id'=>$id));
		$newsGallery = $queryGallery->result_array();
		$data['gallery'] = $newsGallery;


		if(!isset($data['info'])){

			redirect("/");

		}

		

		//pagination

		// meta

		$this->load->helper('text');

		$data['page_title'] = $data['info']['title_'.$this->lang->lang()];

		$data['page_key'] = '';

		$data['page_desc'] = strip_tags(character_limiter($data['info']['text_'.$this->lang->lang()], 200));

		

		$data['info']['page_url'] = base_url().'news/detailed/'.friendlyURL($data['info']['title_'.$this->lang->lang()], 'latin')."_".$data['info']['id'];

		$data['canonical'] = $data['info']['page_url'];



		$data['more_news'] = $this->news_model->get_article(3, '', $id);

		//breadcrumbs

		//$this->config->set_item("breadcrumbs", "<a href='/'>Начало</a>".$this->config->item('breadcrumbs_delimiter')."<a href='".base_url('news')."'>Новини</a>".$this->config->item('breadcrumbs_delimiter')."<a href='".$data['info']['page_url']."'>".character_limiter($data['info']['title'], 40)."</a>");

		$data['section'] = "news";

		$data['main_content'] = "news_details";

		$this->load->view("includes/template", $data);

	}



	public function category()

	{

		if ($this->uri->segment(4) != "") {



			$id = $this->uri->segment(4);



			if($id != intval($id)) {

				redirect("/news");

			}

		}



		$this->load->helper('text');



		$data['page_title'] = lang('news');

		$data['article_list'] = $this->news_model->get_by_category($id);



		$data['categories'] = $this->categories_model->get_categories();

		$data['prepared_categories_url'] = base_url().$this->lang->lang()."/news/category/";

		$data['section'] = "news";

		$data['main_content'] = "article_list";



		$this->load->view("includes/template", $data);

	}



}

