<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Language extends CI_Controller{

    private $_lang = '';

    function __construct(){
        parent::__construct();
        $this->_lang = $this->input->cookie('LANGUAGE', TRUE);
    }

    public function index(){
        $this->bg();
    }

    public function bg(){
        if($this->_lang != 'bg'){
            delete_cookie('LANGUAGE');
        }
        $cookie = array(
                    'name'   => 'LANGUAGE',
                    'value'  => 'bg',
                    'expire' => 604800
                );
        $this->input->set_cookie($cookie);
        //redirect(site_url('index'));
    }

    public function en(){
        if($this->_lang != 'en'){
            delete_cookie('LANGUAGE');
        }
        $cookie = array(
                    'name'   => 'LANGUAGE',
                    'value'  => 'en',
                    'expire' => 604800
                );
        $this->input->set_cookie($cookie);
        //redirect(site_url('index'));
    }

}
?>