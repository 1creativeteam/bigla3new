<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');







class Projects extends CI_Controller {







	public function __construct()



	{



		parent::__construct();



		$this->load->model('projects_model');



		$this->load->helper('func_helper');



	}







	public function index()

	{

		$id = $title = $data['id'] = "";



		$segment = 0;

		$showTintyava = 1;

		$showBellevue = 1;

		if ($this->uri->segment(2)=='projects' && $this->uri->segment(3)!="") {



			$segment = $this->uri->segment(4);



			$string = strstr($segment, '-', true);

			if ($string != 'aktualni') {

				$showTintyava = 0;

			}



			if ($string == 'badeshti') {

				$showBellevue = 0;

			}



			$id = explode("_", $this->uri->segment(4));



			$id = $id[1];



			$data['id'] = $id;



		}



		$this->load->helper('text');

		$this->load->library('pagination');



		// page settings

		$data['page_desc'] = "";

		//menu

		$data['menup'] = $this->projects_model->menu();

		//cat title

		$data['cat_title'] = $this->projects_model->get_title2id($id);

		$data['page_title'] = lang('projects_title').(isset($data['cat_title']['title_'.$this->lang->lang()])? ' :: '. $data['cat_title']['title_'.$this->lang->lang()]:'');

		//max floors

		$data['max_floors'] = $this->projects_model->get_max_floors();

		//towns

		$data['towns'] = $this->projects_model->get_towns();



		//pagination

		$config['base_url'] = base_url().$this->lang->lang().'/'.$this->uri->segment(2)."/index/".$segment;

		$config['total_rows'] = $this->projects_model->count_results($id);

		$config["uri_segment"] = 5;

		$config["per_page"] = 1000;

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;



		$list_data = array(

					'id' 	=>  $id,

					'limit' =>  $config["per_page"],

					'start' =>  $page

					);



		$data['projects_list'] = $this->projects_model->get_article($list_data);

		$data['paging'] = $this->pagination->create_links();



		$queryTintyava = $this->db->get('tintyava');

		$tintyavaInfo = $queryTintyava->result_array();



		$data['tintyava'] = $tintyavaInfo;

		$data['showTintyava'] = $showTintyava;

		$data['showBellevue'] = $showBellevue;



		$data['belvue_name']['bg'] = '"Белвю Резидънс" , гр. София';

		$data['belvue_name']['en'] = '"Bellevue Residence" , Sofia.';



		$data['belvue_desc']['bg'] = 'Обектът се намира в гр. София, бул. „Арсеналски” 121 и се състои от сутерен, партер, пет жилищни етажа и подпокривен етаж. На североизток сградата е изградена „на калкан ” със съседната, като калканните стени се покриват изцяло. На ниво партер се намират както входно фоайе, стълбищна клетка, асансьор, така и проход към дворната част. Жилищните етажи съдържат 8 апартамента, за които има изграден подземен гараж и са осигурени необходимите 8 паркоместа. Сградата се отличава със своята индивидуалност, изчистен дизайн и се вписва хармонично в околоното пространство.';

		$data['belvue_desc']['en'] = 'The site is located in the town. Sofia, bul. "Arsenalski" 121 and consists of a basement, ground floor, five floors and an attic floor. Northeast building was built "detached" with its neighbor, the blind walls entirely covered. On the ground floor are as entrance lobby, stairwell, elevator and passage to the yard. Residential floors contain eight apartments for which there is a garage and 8 are provided the necessary parking spaces. The building is distinguished by its individuality, clear design and fits harmoniously into okolonoto space.';



		//breadcrumbs

		//$this->config->set_item("breadcrumbs", "<a href='/'>Начало</a>".$this->config->item('breadcrumbs_delimiter')."<span>Статии</span>");

		$data['section'] = "projects";

		$data['main_content'] = "projects_list";

		$this->load->view("includes/template", $data);

	}







	public function detailed()

	{

		//get categeries id



		if($this->uri->segment(3)!=""){



			$id = explode("_", $this->uri->segment(3));







			if(count($id)<2){



				redirect("/");



			}







			$id1 = $id[2];



			$id = $id[1];



		}







		$data['info'] = $this->projects_model->details($id);







		if(!isset($data['info'])){



			redirect("/");



		}







		//cat title



		$data['cat_title'] = $this->projects_model->get_title2id($id1);







		//town



		$data['town'] = $this->projects_model->get_town2id($data['info']['town']);







		//floors



		$data['floors'] = $this->projects_model->get_articles2parent($id, 'floor');



		$data['current_page'] = $data['info']['id1'];







		// meta



		$this->load->helper('text');



		$data['page_title'] = $data['info']['title_'.$this->lang->lang()];



		$data['page_key'] = '';



		$data['page_desc'] = strip_tags(character_limiter($data['info']['text_'.$this->lang->lang()], 200));



		$data['info']['page_url'] = base_url().'projects/'.friendlyURL($data['info']['title_'.$this->lang->lang()], 'latin')."_".$data['info']['id']."_".$data['info']['id1'];



		$data['canonical'] = $data['info']['page_url'];







		//breadcrumbs



		//$this->config->set_item("breadcrumbs", "<a href='/'>Начало</a>".$this->config->item('breadcrumbs_delimiter')."<a href='".base_url()."content/detailed/".friendlyURL($data['info']['title'], 'latin')."_".$data['info']['id']."'>".$data['info']['title']."</a>");



		$data['section'] = "projects";



		$data['main_content'] = "projects_details";







		$this->load->view("includes/template", $data);



	}







	public function apartments()



	{



		//get categeries id



		if($this->uri->segment(3)!=""){



			$id = explode("_", $this->uri->segment(3));



			if(count($id)<2){



				redirect("/");



			}



			$id = $id[1];



		}



		$data['info'] = $this->projects_model->details($id);







		if(!isset($data['info'])){



			redirect("/");



		}







		//pagination



		// meta



		$this->load->helper('text');



		$data['page_title'] = $data['info']['title_'.$this->lang->lang()];



		$data['page_key'] = '';



		$data['page_desc'] = strip_tags(character_limiter($data['info']['text_'.$this->lang->lang()], 200));



		$data['info']['page_url'] = base_url().'projects/'.friendlyURL($data['info']['title_'.$this->lang->lang()], 'latin')."_".$data['info']['id'];



		$data['canonical'] = $data['info']['page_url'];



		//breadcrumbs



		//$this->config->set_item("breadcrumbs", "<a href='/'>Начало</a>".$this->config->item('breadcrumbs_delimiter')."<a href='".base_url('news')."'>Новини</a>".$this->config->item('breadcrumbs_delimiter')."<a href='".$data['info']['page_url']."'>".character_limiter($data['info']['title'], 40)."</a>");



		$data['section'] = "projects";



		$data['main_content'] = "news_details";



		$this->load->view("includes/template", $data);



	}







	public function search()

	{



		$this->load->helper('text');



		$data = array(



					'id' 	=>  $this->input->post('id'),



					'floor' =>  $this->input->post('floor'),



					'town' 	=>  $this->input->post('town'),



					'from' 	=>  $this->input->post('from'),



					'to' 	=>  $this->input->post('to'),



					'limit' =>  1000,



					'start' =>  0



					);







		$data['projects_list'] = $this->projects_model->get_search($data);



		$data['main_content'] = "projects_search";







		$this->load->view("projects_search", $data);



	}







	public function tintyava()

	{

		$this->load->helper('text');



		$query = $this->db->get('tintyava_apartments');



    	$tintyavaApartmentsData = $query->result_array();



		$templevel = '1';



		$newkey = 0;



		$groupApartments[$templevel] = "";



		foreach ($tintyavaApartmentsData as $key => $val) {



			if ($templevel == $val['floor']) {



				$groupApartments[$templevel][$newkey] = $val;



			} else {



				$groupApartments[$val['floor']][$newkey] = $val;



			}



			$newkey++;



		}



		$translations['bg']['page_title'] = '<h2>Проекти <span>/ Актуални проекти /</span> Жилищна сграда “Тинтява”</h2>';

		$translations['en']['page_title'] = '<h2>Projects <span>/ Current projects /</span> Residential building “Tintyava”</h2>';

		$translations['bg']['gallery_title'] = 'Снимки от проекта';

		$translations['en']['gallery_title'] = 'Gallery';

		$translations['bg']['floor_title'] = 'Етаж';

		$translations['en']['floor_title'] = 'Floor';



		$translations['bg']['carpet_area'] = 'Светла квадратура';

		$translations['en']['carpet_area'] = 'Carpet area';

		$translations['bg']['buildup_area'] = 'Застроена площ';

		$translations['en']['buildup_area'] = 'Built up area';

		$translations['bg']['total_area'] = 'Обща площ';

		$translations['en']['total_area'] = 'Total area';

		$translations['bg']['status_trans'] = 'Статус';

		$translations['en']['status_trans'] = 'Status';





		$queryGallery = $this->db->order_by("ordered", "asc")->get_where('gallery', array('gallery_group'=>'tintyava'));

		$tintyavaGallery = $queryGallery->result_array();



		$queryInfo = $this->db->get('tintyava');

		$info = $queryInfo->result_array();



		$queryParking = $this->db->get('tintyava_parking');

    	$tintyavaParkingData = $queryParking->result_array();



		$data['main_content'] = "test";



		$data['translations'] = $translations;



		$data['lang'] = $this->lang->lang();



		$data['info'] = $info;



		$data['tintyavaApartmentsData'] = $groupApartments;

		

		$data['tintyavaParkingData'] = $tintyavaParkingData;



		$data['tintyavaGallery'] = array_chunk($tintyavaGallery, 4);



		$this->load->view("includes/template", $data);



	}



	public function belvyu() {



		$this->load->helper('text');



		$translations['bg']['page_title'] = '<span>Bellevue Residence</span> - сграда на годината';

		$translations['en']['page_title'] = '<span>Bellevue Residence</span> - building of the year';



		$translations['bg']['description'] = '<h3>"Белвю Резидънс" , гр. София</h3>

          <h4 style="margin-bottom: 30px;"><i>„Жилищна сграда с подземни гаражи "Белвю Резидънс"” – гр. София, кв. Лозенец.</i></h4>

          <p>Обектът се намира в гр. София, бул. „Арсеналски” 121 и се състои от сутерен, партер, пет жилищни етажа и подпокривен етаж. 

          На североизток сградата е изградена „на калкан ” със съседната, като калканните стени се покриват изцяло. 

          На ниво партер се намират както входно фоайе, стълбищна клетка, асансьор, така и проход към дворната част. 

          Жилищните етажи съдържат 8 апартамента, за които има изграден подземен гараж и са осигурени необходимите 8 паркоместа. 

          Сградата се отличава със своята индивидуалност, изчистен дизайн и се вписва хармонично в околоното пространство.</p>';



		$translations['en']['description'] = '<h3>"Bellevue Residence" , Sofia</h3>

          <h4 style="margin-bottom: 30px;"><i>"Residential building with underground garages" Bellevue Residence "" - c. Sofia,. Lozenets.</i></h4>

          <p>The site is located in the town. Sofia, bul. "Arsenalski" 121 and consists of a basement, ground floor, five floors and an attic floor. 

          Northeast building was built "detached" with its neighbor, the blind walls entirely covered. 

          On the ground floor are as entrance lobby, stairwell, elevator and passage to the yard. 

          Residential floors contain eight apartments for which there is a garage and 8 are provided the necessary parking spaces. 

          The building is distinguished by its individuality, clear design and fits harmoniously into okolonoto space.</p>';



		$translations['bg']['gallery_title'] = 'Снимки от проекта';

		$translations['en']['gallery_title'] = 'Gallery';



		$data['main_content'] = "belvyu";



		$data['translations'] = $translations;



		$data['lang'] = $this->lang->lang();



		$this->load->view("includes/template", $data);

	}







}







