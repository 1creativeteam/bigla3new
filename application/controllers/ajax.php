<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {
	
	function __construct()
	{
   		parent::__construct();
	}

	public function all_events()
	{
		$this->load->model('events_model');
		$this->load->helper('func_helper');

		echo $this->events_model->all_events($this->uri->segment(3))? json_encode($this->events_model->all_events($this->uri->segment(3))): '';
 	}
}
//Generate a week view calendar with codeigniter
/* End of file ajax.php */
/* Location: ./application/controllers/ajax.php */