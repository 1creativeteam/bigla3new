<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends CI_Controller {
		
	function __construct()
	{

   		parent::__construct();
	}

	function index()
	{
		$this->load->model('content_model');
		$this->load->helper('text');

		$info = $this->content_model->details(147);
		$data['page_title'] = $info['long_title'];
		$data['page_key'] = $info['keywords'];
		$data['page_desc'] = $info['short_text']!=''? strip_tags(character_limiter($info['short_text'], 200)): strip_tags(character_limiter($info['text'], 200));
		
		$data['username'] = "";
		$data['main_content'] = "index";
		$data['content_list'] = $this->content_model->get_content_home();
		$this->load->view("includes/template", $data);
 	}
}

/* End of file index.php */
/* Location: ./application/controllers/index.php */