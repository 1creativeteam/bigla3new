<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index($page = 'home')
	{	
		$data['page_desc'] = "";
		$data['page_key'] = "";

		$this->load->helper('text');

		$data['section'] = $page;
		$data['main_content'] = $page;
		
		if($page == 'home'){
			$this->load->library('user_agent');
			$this->load->model('home_model');
			$data['home'] = $this->home_model->get_home();
		}elseif($page == 'services'){

			$this->load->model('content_model');
			$data['rows'] = $this->content_model->childs2id(151);
			$data['parent'] = $this->content_model->get_parent(151);

		}elseif($page == 'proud'){

			$this->load->model('content_model');
			$data['rows'] = $this->content_model->childs2id(152);
			$data['parent'] = $this->content_model->get_parent(152);

		}elseif($page == 'contact'){

			$this->load->model('content_model');
			$data['contact_info'] = $this->content_model->contact_info();
			$data['page_title'] = $data['contact_info']['long_title'];
			$data['page_key'] = $data['contact_info']['keywords'];
			$data['page_desc'] = $data['contact_info']['short_text']!=''? strip_tags(character_limiter($data['contact_info']['short_text'], 200)): strip_tags(character_limiter($data['contact_info']['text'], 200));
		
			$page_title = "Контакти";
		}

		$this->load->view("includes/template", $data);
	}
}