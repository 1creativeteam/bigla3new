<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('content_model');
		$this->load->helper('func_helper');
	}

	public function index()
	{
		$this->load->helper('text');
		$this->load->library('pagination');
		
		// page settings
		$data['page_title'] = "";
		$data['page_desc'] = "";
		//pagination
		$config['base_url'] = base_url().$this->uri->segment(1)."/index/";
		$config['total_rows'] = $this->content_model->count_content();
		$config["uri_segment"] = 3;
		$config["per_page"] = 4;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data['content_list'] = $this->content_model->get_article($config["per_page"], $page);
		$data['paging'] = $this->pagination->create_links();
		
		//breadcrumbs
		//$this->config->set_item("breadcrumbs", "<a href='/'>Начало</a>".$this->config->item('breadcrumbs_delimiter')."<span>Статии</span>");

		$data['main_content'] = "content_list";
		$this->load->view("includes/template", $data);
	}

	public function articles()
	{
		$this->load->helper('text');
		$this->load->library('pagination');
		
		if($this->uri->segment(3)!=""){
			$id = explode("_", $this->uri->segment(3));
			if(count($id)<2){
				redirect("/");
			}
			$id = $id[1];
		}

		$data['info'] = $this->content_model->details($id);

		if(!isset($data['info'])){
			redirect("/");
		}	

		$child_ids = implode(",", $this->content_model->content_childs_on_text($id));
		
		//$this->output->enable_profiler(TRUE);
		// page settings
		$data['page_title'] = $data['info']['title'];
		$data['page_desc'] = "";
		
		//pagination
		$config['base_url'] = base_url().$this->uri->segment(1)."/".$this->uri->segment(2)."/".$this->uri->segment(3);
		$config['total_rows'] = $this->content_model->articles_count_content($child_ids);
		$config["uri_segment"] = 4;
		$config["per_page"] = 4;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$data['content_list'] = $this->content_model->get_articles($child_ids, $config["per_page"], $page);
		$data['paging'] = $this->pagination->create_links();
		
		//breadcrumbs
		//$this->config->set_item("breadcrumbs", "<a href='/'>Начало</a>".$this->config->item('breadcrumbs_delimiter')."<span>Статии</span>");

		$data['main_content'] = "content_list";
		$this->load->view("includes/template", $data);
	}

	public function detailed()
	{
		//get categeries id
		if($this->uri->segment(4)!=""){
			$id = explode("_", $this->uri->segment(4));
			if(count($id)<2){
				redirect("/");
			}
			$id = $id[1];
			
		}

		$data['info'] = $this->content_model->details($id);

		if(!isset($data['info'])){
			redirect("/");
		}
		$data['current_page'] = $data['info']['id1'];

		// meta
		$this->load->helper('text');
		$data['page_title'] = $data['info']['title_'.$this->lang->lang()];
		$data['page_key'] = '';
		$data['page_desc'] = strip_tags(character_limiter($data['info']['text_'.$this->lang->lang()], 200));
		$data['size'] = $this->config->item('contentinfo');
		
		$data['info']['page_url'] = base_url().'content/detailed/'.friendlyURL($data['info']['title_'.$this->lang->lang()], 'latin')."_".$data['info']['id'];
		$data['canonical'] = $data['info']['page_url'];
		
		//breadcrumbs
		//$this->config->set_item("breadcrumbs", "<a href='/'>Начало</a>".$this->config->item('breadcrumbs_delimiter')."<a href='".base_url()."content/detailed/".friendlyURL($data['info']['title'], 'latin')."_".$data['info']['id']."'>".$data['info']['title']."</a>");
		
		$data['main_content'] = "details";
		$this->load->view("includes/template", $data);
	}

	public function about_us()
	{
		$data['section'] = "about-us";
		//get categeries id
		$data['info'] = $this->content_model->details(148);
		if(!isset($data['info'])){
			redirect("/");
		}
		$data['current_page'] = $data['info']['id1'];
		// meta
		$this->load->helper('text');
		$data['page_title'] = $data['info']['title_'.$this->lang->lang()];
		$data['page_key'] = '';
		$data['page_desc'] = strip_tags(character_limiter($data['info']['text_'.$this->lang->lang()], 200));
		$data['size'] = $this->config->item('contentinfo');
		
		$data['info']['page_url'] = site_url('about_us');
		$data['canonical'] = $data['info']['page_url'];
		
		//breadcrumbs
		//$this->config->set_item("breadcrumbs", "<a href='/'>Начало</a>".$this->config->item('breadcrumbs_delimiter')."<a href='".base_url()."content/detailed/".friendlyURL($data['info']['title'], 'latin')."_".$data['info']['id']."'>".$data['info']['title']."</a>");
		
		$data['main_content'] = "about_us";
		$this->load->view("includes/template", $data);
	}

	public function contacts()
	{

		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['section'] = "contacts";
		//get categeries id
		$data['info'] = $this->content_model->contact_info();
		if(!isset($data['info'])){
			redirect("/");
		}
		$data['current_page'] = $data['info']['id1'];

		// meta
		$this->load->helper('text');
		$data['page_title'] = $data['info']['title_'.$this->lang->lang()];
		$data['page_key'] = '';
		$data['page_desc'] = strip_tags(character_limiter($data['info']['text_'.$this->lang->lang()], 200));
		$data['size'] = $this->config->item('contentinfo');
		
		$data['info']['page_url'] = site_url('contacts');
		$data['canonical'] = $data['info']['page_url'];
		
		$data['success'] = false;
		if($_SERVER['REQUEST_METHOD']=='POST'){

			$this->form_validation->set_rules('cname', 'lang:cname', 'required');
			$this->form_validation->set_rules('cphone', 'lang:cphone', 'required');
			$this->form_validation->set_rules('corg', 'lang:corg', '');
			$this->form_validation->set_rules('cemail', 'lang:cemail', 'required|valid_email');
			$this->form_validation->set_rules('ctext', 'lang:ctext', 'required');

			if ($this->form_validation->run() == FALSE){
				
			}else{
				//email it
				$em['title'] =  'Запитване през Бигла III';
                $content = $this->load->view("email_request", $em, TRUE);
				
                sendMail($this->input->post('cemail'), array('office@bigla3.com', 'Бигла III'), 'Запитване през Бигла III', $content);

				$data['success'] = true;
			}
		}

		//breadcrumbs
		//$this->config->set_item("breadcrumbs", "<a href='/'>Начало</a>".$this->config->item('breadcrumbs_delimiter')."<a href='".base_url()."content/detailed/".friendlyURL($data['info']['title'], 'latin')."_".$data['info']['id']."'>".$data['info']['title']."</a>");
		
		$data['main_content'] = "contacts";
		$this->load->view("includes/template", $data);
	}

}
