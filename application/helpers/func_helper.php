<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('friendlyURL')){
    function friendlyURL($text, $to="latin", $not_numbers="") {
        $cyr = array('А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е', 'Ж', 'ж', 'З', 'з', 
            'И', 'и', 'Й', 'й', 'К', 'к', 'Л', 'л', 'М', 'м', 'Н', 'н', 'О', 'о', 'П', 'п', 'Р', 'р', 'С', 'с', 'Т', 'т', 'У', 'у', 
            'Ф', 'ф', 'Х', 'х', 'Ц', 'ц', 'Ч', 'ч', 'Ш', 'ш', 'Щ', 'щ', 'Ъ', 'ъ', 'Ь', 'ь', 'Ю', 'ю', 'Я', 'я', 
            'Дж', 'дж', 'Дз', 'дз', 'Ьо', 'ьо', 'Ѝо' ,'ѝо', 'Ия', 'ия');
        $lat = array('A', 'a', 'B', 'b', 'V', 'v', 'G', 'g', 'D', 'd', 'E', 'e', 'Zh', 'Zh', 'Z', 'z', 
            'I', 'i', 'Y', 'y', 'K', 'k', 'L', 'l', 'M', 'm', 'N', 'n', 'O', 'o', 'P', 'p', 'R', 'r', 'S', 's', 'T', 't', 'U', 'u', 
            'F', 'f', 'H', 'h', 'Ts', 'ts', 'Ch', 'ch', 'Sh', 'sh', 'Sht', 'sht', 'A', 'a', 'Y', 'y', 'Yu', 'yu', 'Ya', 'ya', 
            'Dzh', 'dzh', 'Dz', 'dz', 'Yo', 'yo', 'Yo', 'yo', 'Ia', 'ia');
        
        if($to=="latin"){
            $text = str_replace($cyr, $lat, $text);
            $string = $text;
            $string = preg_replace("`\[.*\]`U", "", $string);
            $string = str_replace(",", "", $string);
            $string = preg_replace("/[^a-zA-Z0-9]+/","-", $string);
            // not numbers in the title
            if($not_numbers){
                $string = preg_replace("/[^a-zA-Z]+/"," ", $string);
            }
            $string = preg_replace('`&(amp;)?#?[a-z0-9]+;`i', '-', $string);
            $string = htmlspecialchars($string, ENT_COMPAT, 'utf-8');
            $string = preg_replace("`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i", "\\1", $string);
            //$string = preg_replace(array("`[^a-z0-9]`i", "`[-]+`"), "-", $string);
        }elseif($to=="cyrillic"){
            $text = str_replace($lat, $cyr, $text);
            $string = $text;
            $string = preg_replace("`\[.*\]`U", "", $string);
            $string = str_replace(",", "", $string);
            // not numbers in the title
            if($not_numbers){
                $string = preg_replace("/[^\p{Cyrillic}]/u"," ", $string);
            }
            $string = htmlspecialchars($string, ENT_COMPAT, 'utf-8');
            $string = preg_replace("`&(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i", "\\1", $string);
        }
        
        return strtolower((trim($string, '-')));
    }
}

if ( ! function_exists('emailcheck')){
    function emailcheck($email) {
        
        
        $p = '/^[a-z0-9!#$%&*+-=?^_`{|}~]+(\.[a-z0-9!#$%&*+-=?^_`{|}~]+)*';
        $p.= '@([-a-z0-9]+\.)+([a-z]{2,3}';
        $p.= '|aero|asia|coop|info|jobs|mobi|museum|name|travel)$/ix';

        return (boolean) preg_match($p, $email);
         
        //    Do the basic Reg Exp Matching for simple validation
        if (!preg_match($p, $email)){
            return false;
        }
        //    split the Email Up for Server validation
        list($username,$domain) = explode("@",$email);
        
        //    If you get an mx record then this is a valid email domain
        if(@getmxrr($domain,$mxhost)){
            return true;
        }else{
            //    else use the domain given to try and connect on port 25
            //    if you can connect then it's a valid domain and that's good enough
            if (@fsockopen($domain, 25, $errno, $errstr, 30)){
                return true;
            }else{
                return false;
            }
        }
      
    }
}

if ( ! function_exists('validatorUnique')){
    function validatorUnique($tableName, $fieldNameValueAssocArray)
    {
        $ci=& get_instance();
        $ci->load->database();
        
        $ci->db->where($fieldNameValueAssocArray);
        $ci->db->from($tableName);
        $result = $ci->db->count_all_results();
        
        return $result;
    }
}

if ( ! function_exists('checkMobilePhone')){
function checkMobilePhone($mobile, $zero_check=false){
    $regex = "/^(\+|00)?(?:359|0)+((?:87|88|89|98)+(?:\d{7})+)$/";
    $r = preg_match($regex, $mobile, $m);
    if($r==1){
        if(mb_strlen($m[2]) == 9)
            return ($zero_check?'0':'').$m[2];
        else
            return false;
    }else{
        return false;
    }
}
}

if ( ! function_exists('date_to_bg')){
    function date_to_bg($date){
        $array1=array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"); 
        $array2=array("Понеделник", "Вторник", "Сряда", "Четвъртък", "Петък", "Събота", "Неделя"); 
        $array3=array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); 
        $array4=array("Януари", "Февруари", "Март", "Април", "Май", "Юни", "Юли", "Август", "Септември", "Октомври", "Ноември", "Декември"); 

        $date1=str_replace($array1,$array2,$date); 
        $date2=str_replace($array3,$array4,$date1); 
        return $date2;
    }
}

if ( ! function_exists('sendMail')){
    function sendMail($to, $from, $subject, $message, $attach = NULL, $bcc = NULL, $cc = NULL){
        
        $ci=& get_instance();
        $ci->load->library('email');

        //FROM EMAIL
        $config = array (
                  'mailtype' => 'html',
                  'charset'  => 'utf-8',
                  'priority' => '1'
                   );
        $ci->email->initialize($config);
        
        if(is_array($from)){
            list($fromEmail, $fromName) = $from;
        }else {
            $fromEmail = $from;
            $fromName = '';
        }
        
        $ci->email->from($fromEmail, $fromName);
        $ci->email->reply_to($fromEmail, $fromName);
        
        //TO EMAIL
        if(is_array($to)){
            foreach($to as $val)
                $ci->email->to($val);
        }else
            $ci->email->to($to);
    
        
        $ci->email->subject($subject);
        $ci->email->message($message);
    
        if(!is_null($bcc)){
            $ci->email->bcc($attach);
        }
        
        if(!is_null($cc)){
            $ci->email->cc($attach);
        }
        
        if(!is_null($attach)){
            $ci->email->attach($attach);
        }
        
        //send the message, check for errors
        return $ci->email->send();
    }
}
/* End of file func_helper.php */
/* Location: ./application/helpers/func_helper.php */