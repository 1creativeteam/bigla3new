<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* force_ssl
*  secures (https), all assets called w/ base_url_asset()
*  call before view content
*
* @params - url_string - optional, see codeigniter base_url() docs
*/
function force_ssl()
{
    $CI =& get_instance();
    $CI->force_ssl = true;
}


/**
* base_url_asset
*  use for asset links that may/may not be secure (depending on the page)
*
* @params - url_string - optional, see codeigniter base_url() docs
*/
function base_url_asset($url_string=false){
	$CI =& get_instance();

	if(isset($CI->force_ssl)){
		return str_replace('http', 'https', base_url($url_string));
	} else {
		return base_url($url_string);
	}
}


/**
* base_url_secure
*  converts a non-secure http base_url to https
*
* @params - url_string - optional, see codeigniter base_url() docs
*/
function base_url_secure($url_string=false)
{
	return str_replace('http','https',base_url($url_string));
}  


/* End of file url_helper.php */
/* Location: ./system/helpers/url_helper.php */