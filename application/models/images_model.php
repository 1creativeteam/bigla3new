<?php
class Images_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_image($stat_id, $modul, $size, $enl="", $showimages="", $flid='')
	{
		//$this->output->enable_profiler(TRUE);
		$enl = $enl?$enl:".jpg";
		$showimages = $showimages?$showimages:"y";
		if($showimages){
			$this->db->order_by("id", "asc"); 
			if($modul=='floor'){
				$query = $this->db->get_where('images', array("stat_id"=>$stat_id.$flid, "modul"=>$modul));
				$dirpic = 'images/projects/'.$stat_id."/floor/".$stat_id.$flid.'/';
			}else{
				$query = $this->db->get_where('images', array("stat_id"=>$stat_id, "modul"=>$modul));
				$dirpic = $this->config->item($modul).$stat_id."/";
			}
			if(file_exists($dirpic)){
				$images = array();
				foreach($query->result_array() as $row){
					if ($dir = @opendir($dirpic)){
						$dirArray = array();
						while($name=readdir($dir)){
							if(strpos($name, $size.$row['ext'])!==false){
								//$dirArray[]=$name;
								//echo $row['path'].$size.$enl."------".$dirpic.$name."\n";
								if($row['path'].$size.$row['ext']==$dirpic.$name){
									$row["path"]=$row['path'].$size.$row['ext'];
									$images[] = $row;
								}
							}
						}
						closedir($dir);
					}
				}
				return $images;
			}
			else{
				return false;
			}
		}
	}
}