<?php
class Categories_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_categories()
	{

		$this->db->from('categories');
		$this->db->order_by("id", "asc");
		$query = $this->db->get();
		// $query = $this->db->get('categories');
		$categories = $query->result_array();

		return $categories;
	}

}