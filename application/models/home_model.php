<?php
class Home_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_home()
	{
		$this->load->model('images_model');
		//$this->output->enable_profiler(TRUE);
		$this->db->where('show = 1');
	   	$this->db->order_by('menu');
	   	$query = $this->db->get('home');
		$size = $this->config->item('homeinfo');
		foreach($query->result_array() as $row){
			$row["target"]="";
			$images = $this->images_model->get_image($row['id'], 'home', $size[1]['enl']);
			if(count($images)>0){
				$row['image'] = $images[0];
			}
			else{
				$row['image'] =array();
			}
			if($row['video_type']==1){
				//$row['video_path'] = 'http://youtu.be/'.$row['video_url'];
				$row['video_path'] = 'http://www.youtube.com/watch?v='.$row['video_url'].'&amp;feature=player_embedded&amp;autoplay=1#at=41';
				$row['video_image'] = 'http://i4.ytimg.com/vi/'.$row['video_url'].'/maxresdefault.jpg';
			}elseif($row['video_url']!='' && $row['video_type']==2){
				//$row['video_path'] = 'http://vimeo.com/'.$row['video_url'];
				$row['video_path'] = 'http://player.vimeo.com/video/'.$row['video_url'].'?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1';
				$vmeoApiUrl = 'http://vimeo.com/api/v2/video/'.$row['video_url'].'.json';
				$vmeoApi = json_decode(file_get_contents($vmeoApiUrl));
				$row['video_image'] = $vmeoApi[0]->thumbnail_large;
			}
	
			$content[]=$row;
		}
		return (!empty($content) && is_array($content)? $content: 0);
	}
}