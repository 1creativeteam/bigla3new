<?php
class Content_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_article($limit, $start)
	{
		$this->load->model('images_model');

		$this->db->limit($limit, $start);
		$this->db->order_by("menu", "asc");
		$query = $this->db->get_where('content', array('show'=>'y'));
		$content = array();
		$size = $this->config->item('contentinfo');
		foreach($query->result_array() as $row){
			$row["target"]="";
			$row['image'] = $this->images_model->get_image($row['id'], 'content', $size[0]['enl'], '', $row['showimages']);
			$row['image'] = $row['image'][0];
			if($row["link"]!=""){
				if(strpos($row["link"],"tp://")==0){
					$row["target"]="";
				}
				else{
					$row["target"] = ' target="_blank"';
				}
			}else{
				$row["link"] = base_url()."content/detailed/".friendlyURL($row['title'], 'latin')."_".$row['id'];
			}
			$content[]=$row;
		}
		return $content;
	}

	public function count_content()
	{
		$this->db->where("show = 'y'");
		$this->db->from('content');
		return $this->db->count_all_results();
	}

	public function get_articles($ids, $limit, $start, $fields=array())
	{
		$this->load->model('images_model');
		$and = '';
		if(isset($fields)){
			 foreach($fields as $k=>$v){
			 	$and .= "AND $k$v";
			 }
		}
		$query = $this->db->query("SELECT * FROM content WHERE id1 IN($ids) AND `show`='y' $and ORDER BY menu LIMIT $start, $limit");
		$content = array();
		$size = $this->config->item('contentinfo');
		foreach($query->result_array() as $row){
			$row['image'] = $this->images_model->get_image($row['id'], 'content', $size[0]['enl'], '', $row['showimages']);
			if($row['image']){
				$row['image'] = $row['image'][0];
			}
			else{
				$row['image'] ="";
			}
			$row["target"]="";
			if($row["link"]!=""){
				if(strpos($row["link"],"tp://")==0){
					$row["target"]="";
				}
				else{
					$row["target"] = ' target="_blank"';
				}
			}else{
				$row["link"] = base_url()."content/detailed/".friendlyURL($row['title'], 'latin')."_".$row['id'];
			}
			$content[]=$row;
		}
		return $content;
	}

	public function articles_count_content($ids, $fields=array())
	{
		$and = '';
		if(isset($fields)){
			 foreach($fields as $k=>$v){
			 	$and .= "AND $k$v";
			 }
		}
		$query = $this->db->query("SELECT COUNT(id) alls FROM content WHERE id1 IN($ids) AND `show`='y' $and");
		$row = $query->row_array();
		return $row['alls'];
	}

	public function details($id)
	{
		$query = $this->db->get_where('content', array('id' => $id, 'show'=>'y'));
		$row = $query->row_array();
		if(count($row)>0){
			$this->load->model('images_model');
			$size = $this->config->item('contentinfo');
			$row['images'] = $this->images_model->get_image($row['id'], 'content', $size[1]['enl'], '', $row['showimages']);
			return $row;
		}else{
			redirect('/');
		}
	}

	public function contact_info()
	{
		$query = $this->db->get_where('content', array('title_bg' => 'Контакти', 'title_en' => 'Contacts', 'show'=>'y'));
		$row = $query->row_array();
		if(count($row)>0){
			$this->load->model('images_model');
			$size = $this->config->item('contentinfo');
			$row['images'] = $this->images_model->get_image($row['id'], 'content', $size[1]['enl'], '', $row['showimages']);
			return $row;
		}else{
			redirect('/');
		}
	}

	public function path($id)
	{
		$zapis=$id;
		while ($zapis!=0) {
			$query = $this->db->get_where('recipes_categ', array('id' => $zapis, 'show'=>'y'));
			$row = $query->row_array();
			$zapis=$row["id1"];
			$name[]=$row;
		}
		$count_all = count($name);
		if($count_all>0){
			$i=1;
			foreach($name as $key=>$val) {
				$i++;
				$path = $this->get_child($val['id'])?"details":"articles";
				//if($i!=$count_all){
					$newname[$i] ="<a href='".base_url()."content/".$path."/".friendlyURL($val["title"], 'latin')."_".$val['id']."'>".$val["title"]."</a>";
				//}
				//else{
				//	$newname[$i] ="<span>".$val["title"]."</span>";
				//}
				
			}
			$newname=join($this->config->item('breadcrumbs_delimiter'), array_reverse($newname));
		}
		return $newname;
	}

	public function content_childs_on_text($id){

		$subs[]=$id;
		$query = $this->db->query("SELECT `id` FROM content WHERE id1='$id' AND `text`!='' AND `show`='y'");
		if ($query->num_rows()>0){
			foreach($query->result_array() as $row){
				$subs = array_merge($subs, $this->content_childs_on_text($row["id"]));
			}
		}
		return $subs;
	}

	public function content_childs($id){

		$subs[]=$id;
		$query = $this->db->query("SELECT `id` FROM content WHERE id1='$id' AND `show`='y'");
		if ($query->num_rows()>0){
			foreach($query->result_array() as $row){
				$subs = array_merge($subs, $this->content_childs($row["id"]));
			}
		}
		return $subs;
	}

	public function childs2id($id){

		$rows = array();
		$query = $this->db->query("SELECT * FROM content WHERE id1='$id' AND `show`='y'");
		if ($query->num_rows()>0){
			$this->load->model('images_model');
			$size = $this->config->item('contentinfo');
			foreach($query->result_array() as $row){
				$image = $this->images_model->get_image($row['id'], 'content', $size[1]['enl'], '', $row['showimages']);
				if(count($image)>0){
					$row['images'] = $image;
				}
				else{
					$row['images'] =array();
				}
				$rows[] = $row;
			}
		}
		return $rows;
	}

	public function get_parent($id){

		$query = $this->db->query("SELECT * FROM content WHERE id='$id' AND `show`='y'");
		if ($query->num_rows()>0){
			$row = $query->row_array();
			$this->load->model('images_model');
			$size = $this->config->item('contentinfo');
			$row['image'] = $this->images_model->get_image($row['id'], 'content', $size[1]['enl'], '', $row['showimages']);
			if($row['image']){
				$row['image'] = $row['image'][0];
			}
			else{
				$row['image'] ="";
			}
		}
		return $row;
	}
}