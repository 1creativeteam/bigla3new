<?php
class Projects_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	/*public function get_article($id='', $limit, $start)*/
	public function get_article($data=array())
	{
		$this->load->model('images_model');
		//$this->output->enable_profiler(TRUE);
		
		$this->db->limit($data['limit'], $data['start']);
		
		if($data['id']){
			$this->db->where("id1", $data['id']);
		}else{
			$this->db->where("level", 1);
			$this->db->where("id1 != 1");
			$this->db->where("id1 != 67");
			//$childs = $this->childs2level(1);
			//$this->db->where("id IN(".implode(',', $childs).")");
		}
		$this->db->where("show", 1);
		$this->db->order_by("menu", "asc");
		$query = $this->db->get('projects');
		$content = array();
		$size = $this->config->item('projectsinfo');
		foreach($query->result_array() as $row){
			$image = $this->images_model->get_image($row['id'], 'projects', $size[0]['enl'], '', $row['showimages']);
			if(count($image)>0){
				$row['images'] = $image;
			}
			else{
				$row['images'] =array();
			}
			$row["link"] = site_url("projects/".friendlyURL($row['title_'.$this->lang->lang()], 'latin')."_".$row['id']."_".$row['id1']);
			$content[]=$row;
		}
		return $content;
	}

	public function get_search($data=array())
	{
		$this->load->model('images_model');
		//$this->output->enable_profiler(TRUE);
		
		$this->db->limit($data['limit'], $data['start']);
		
		if($data['town']>0){
			$this->db->where("town", $data['town']);
		}
		
		if($data['id']!=''){
			$this->db->where("id1", $data['id']);
		}
		if($data['floor']>0){
			$this->db->where("floor", $data['floor']);
		}
		if($data['from']>0 && $data['to']>0){
			$this->db->where('`sqm` BETWEEN '.$data['from'].' AND '.$data['to'].'', NULL, FALSE);
		}
		
		$this->db->where("level", 1);
		$this->db->where("id1 != 1");
		$this->db->where("id1 != 67");
		$this->db->where("show", 1);
		$this->db->order_by("menu", "asc");
		$query = $this->db->get('projects');
		$content = array();
		$size = $this->config->item('projectsinfo');
		foreach($query->result_array() as $row){
			$image = $this->images_model->get_image($row['id'], 'projects', $size[0]['enl'], '', $row['showimages']);
			if(count($image)>0){
				$row['images'] = $image;
			}
			else{
				$row['images'] =array();
			}
			$row["link"] = site_url("projects/".friendlyURL($row['title_'.$this->lang->lang()], 'latin')."_".$row['id']."_".$row['id1']);
			$content[]=$row;
		}
		return $content;
	}

	public function menu()
	{
		$id = "";
		if($this->uri->segment(2)=='projects' && $this->uri->segment(3)!=""){
			$id = explode("_", $this->uri->segment(4));
			$id = $id[1];
		}
		$this->db->select('title_bg, title_en, id, id1');
		$this->db->order_by("menu", "asc");
		$query = $this->db->get_where('projects', array('id1'=>0, 'show'=>'y'));
		$menu = array();
		foreach($query->result_array() as $row){
			$row["selected"] =  false;
			if($id==$row['id']){
				$row["selected"] =  true;
			}
			$row["link"] = "projects/index/".friendlyURL($row['title_'.$this->lang->lang()], 'latin')."_".$row['id'];
			$menu[]=$row;
		}
		return $menu;
	}

	public function count_results($id)
	{
		$id = "";
		if($this->uri->segment(2)=='projects' && $this->uri->segment(3)!=""){
			$id = explode("_", $this->uri->segment(4));
			$id = $id[1];
		}
		if($id){
			$this->db->where("id1", $id);
		}else{
			//$childs = $this->childs();
			
		}
		$this->db->where("show = 'y'");
		$this->db->from('projects');
		return $this->db->count_all_results();
	}

	public function count_content()
	{
		$this->db->where("show = 'y'");
		$this->db->from('projects');
		return $this->db->count_all_results();
	}

	public function get_title2id($id)
	{
		$this->db->select('title_bg, title_en');
		$query = $this->db->get_where('projects', array('id' => $id, 'show'=>'y'));
		$row = $query->row_array();
		if(count($row)>0){
			return $row;
		}
	}

	public function details($id)
	{
		$query = $this->db->get_where('projects', array('id' => $id, 'show'=>'y'));
		$row = $query->row_array();
		if(count($row)>0){
			$this->load->model('images_model');
			$size = $this->config->item('projectsinfo');
			$image = $this->images_model->get_image($row['id'], 'projects', $size[2]['enl'], '', $row['showimages']);
			if(count($image)>0){
				$row['images'] = $image;
			}
			else{
				$row['images'] =array();
			}
			return $row;
		}else{
			redirect('/');
		}
	}

	public function childs2level($level){

		$this->db->where('level', $level);
		$this->db->where('show', 'y');
		$query = $this->db->get('projects');
		$subs = array();
		 if ($query->num_rows()>0)
		 {
		 	foreach($query->result_array() as $row)
		 	{
		 		$subs[] = $row['id'];
		 	}
		}
		return $subs;
	}

	public function childs2id($id){

		$rows = array();
		$query = $this->db->query("SELECT * FROM projects WHERE id1='$id' AND `show`='y'");
		if ($query->num_rows()>0){
			$this->load->model('images_model');
			$size = $this->config->item('contentinfo');
			foreach($query->result_array() as $row){
				$image = $this->images_model->get_image($row['id'], 'projects', $size[1]['enl'], '', $row['showimages']);
				if(count($image)>0){
					$row['images'] = $image;
				}
				else{
					$row['images'] =array();
				}
				$rows[] = $row;
			}
		}
		return $rows;
	}

	public function get_parent($id){

		$query = $this->db->query("SELECT * FROM projects WHERE id='$id' AND `show`='y'");
		if ($query->num_rows()>0){
			$row = $query->row_array();
			$this->load->model('images_model');
			$size = $this->config->item('contentinfo');
			$row['image'] = $this->images_model->get_image($row['id'], 'projects', $size[1]['enl'], '', $row['showimages']);
			if($row['image']){
				$row['image'] = $row['image'][0];
			}
			else{
				$row['image'] ="";
			}
		}
		return $row;
	}

	public function get_articles2parent($id, $floor=''){

		$query = $this->db->query("SELECT * FROM projects WHERE id1='$id' AND `show`='y' ORDER BY menu");
		$content = array();
		if ($query->num_rows()>0){
			$this->load->model('images_model');
			$size = $this->config->item('projectsinfo');
            foreach($query->result_array() as $row){
            	$row["link"] = '';
				if($floor){
					$image = $this->images_model->get_image($row['id'], 'projects', $size[3]['enl'], '', $row['showimages']);
				}else{
					$row["link"] = '/'.$this->lang->lang()."/apartments/".friendlyURL($row['title_'.$this->lang->lang()], 'latin')."_".$row['id'];
					$image = $this->images_model->get_image($row['id'], 'projects', $size[0]['enl'], '', $row['showimages']);
				}
				
				if(count($image)>0){
					$row['images'] = $image;
				}
				else{
					$row['images'] =array();
				}
				$content[]=$row;
			}
		}
		return $content;
	}

	public function get_max_floors()
	{
		$this->db->select('floor');
		$this->db->order_by("floor", "desc");
		$this->db->limit(1);
		$query = $this->db->get_where('projects', array('floor != ' => 0, 'show'=>'y'));
		$row = $query->row_array();
		if(count($row)>0){
			return $row['floor'];
		}
	}

	public function get_towns(){

		$query = $this->db->get('towns');
		
		return $query->result_array();
	}

	public function get_town2id($id){

		$this->db->where("id", $id);
		$query = $this->db->get('towns');
		$town = $query->row_array();
		return $town;
	}
}