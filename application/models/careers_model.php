<?php
class Careers_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_article($limit=1000, $start=0, $id='')
	{
		$this->load->model('images_model');

		$this->db->limit($limit, $start);
		$this->db->order_by("datein", "desc");
		if($id){
			$this->db->where("id !=", $id);
		}
		$query = $this->db->get_where('careers', array('show'=>'y'));
		$careers = array();
		$size = $this->config->item('newsinfo');
		foreach($query->result_array() as $row){
			$image = $this->images_model->get_image($row['id'], 'careers', $size[0]['enl'], '', $row['showimages']);
			if(count($image)>0){
				$row['images'] = $image;
			}else{
				$row['images'] =array();
			}
			$row["link"] = site_url("careers/".friendlyURL($row['title_'.$this->lang->lang()], 'latin')."_".$row['id']);
			$careers[]=$row;
		}
		//print_r($careers);
		return $careers;
	}

	public function count_careers()
	{
		$this->db->where("show = 'y'");
		$this->db->from('careers');
		$all = $this->db->count_all_results();
		return $all;
	}

	public function details($id)
	{
		$this->load->model('images_model');
		$query = $this->db->get_where('careers', array('id' => $id, 'show'=>'y'));
		$row = $query->row_array();
		$size = $this->config->item('newsinfo');
		if(count($row)>0){
			$images = $this->images_model->get_image($row['id'], 'careers', $size[1]['enl'], '', $row['showimages']);
			if(!empty($images)){
				$row['images'] = $images;
			}
			else{
				$row['images'] =array();
			}
			return $row;
		}else{
			redirect('/');
		}
	}
}