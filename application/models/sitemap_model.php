<?php
class Sitemap_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_article()
	{
		$query = $this->db->query("
				(SELECT title, id, `link`, menu, 'content' as modul FROM content WHERE `show`='y' ORDER By menu)
				UNION
				(SELECT title, id, '', menu, 'news' as modul FROM news WHERE `text`!='' AND `show`='y' ORDER By menu)
				");
		$rows = array();
		foreach($query->result_array() as $row){
			$row["target"]="";
			if($row['modul'] == "content"){
				if($row["link"]!=""){
					if(strpos($row["link"],"tp://")==0){
						$row["target"]="";
					}
					else{
						$row["target"]=" target=\"_blank\"";
					}
				}else{
					$row["link"] = base_url().$row['modul']."/detailed/".friendlyURL($row['title'], 'latin')."_".$row['id'];
				}
			}else{
				$row['link'] =  base_url().$row['modul'].'/detailed/'.friendlyURL($row['title'], 'latin').'_'.$row['id'];
			}
			$rows[]=$row;
		}
		return $rows;
	}
}