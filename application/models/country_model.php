<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Country_model extends CI_Controller 
{
	
	function __construct()
	{
   		parent::__construct();
		$this->load->driver('cache');
	}
	
	public function get_country(){
		$memkey = 'getcountries';
       	$rtr = $this->cache->memcached->get($memkey);
		if($rtr === false){
			$this -> db -> from('country');
			//$this -> db -> where('user_id = '.$userid.'');
	   		$query = $this -> db -> get();
			if($query -> num_rows() >0){
	   			$rtr = $query->result_array();
			}
	   		else{
	     		$rtr = false;
	   		}
			$this->cache->memcached->save($memkey, $rtr, 84600); // 24h
		}
		return $rtr;
 	}

 	public function get_link_country(){
		$memkey = 'getlinkcountries';
		$link_region = array('BG', 'GB', 'TR');
		$this->cache->memcached->delete($memkey,0);
		$rtr = $this->cache->memcached->get($memkey);
		if($rtr === false){
			$this -> db -> from('country');
			$this->db->where_in('iso2', $link_region);
	   		$query = $this -> db -> get();
			if($query -> num_rows() >0){
	   			$rtr = $query->result_array();
			}
	   		else{
	     		$rtr = false;
	   		}
			$this->cache->memcached->save($memkey, $rtr, 84600); // 24h
		}
		return $rtr;
 	}
}