<?php
Class Events_model extends CI_Model
{
	public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
    public function all_events2day($date, $court_type)
	{
		$sql = "SELECT *, DATE_FORMAT(`start`, '%Y-%m-%d %k:00') as `start` FROM events WHERE DATE_FORMAT(`start`, '%Y-%m-%d')='".$date."' AND court_type=".$this->db->escape($court_type)."";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
   			return $query->result_array();
   		}
   		return false;
	}

	public function all_events($court_type='')
	{
		$sql = "SELECT id, `end` + INTERVAL 1 HOUR as `end`, url, user_id, title, DATE_FORMAT(`start`, '%Y-%m-%d %H:00') as `start`, COUNT(`start`) as allcourts FROM events WHERE court_type=".$this->db->escape($court_type)." GROUP BY `start` ORDER BY id";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$events_data = array();
			$row = $query->result_array();
			foreach($row as $k=>$v){
				if($v['url']==''){
					$v['url'] = base_url("events").date('/Y/m/d/H', strtotime($v['start'])).'/'.$court_type;
				}
				
				$sum_courts = count($this->config->item('corts'))-$v['allcourts'];
				if($this->session->userdata('id')!='' && $this->session->userdata('id')==$v['user_id']){//мои кортове
					$v['class'] = 'green';
				}elseif($sum_courts==0){// вс. заети
					$v['class'] = 'red';
				}elseif($sum_courts != 0 && $sum_courts < 3 && $v['allcourts'] > 0){//и св. и заети
					$v['class'] = 'orange';
				}else{//без клас вс. свободни
					$v['class'] = '';
				}
				$v['allDay'] = false;
				if($court_type==1){
					$free_court_text = ($sum_courts==1? $sum_courts.' свободен корт': $sum_courts.' свободни корта');
					$busy_court_text = ($v['allcourts']==1? $v['allcourts'].' зает корт': $v['allcourts'].' заети корта');
				}elseif($court_type==2){
					$free_court_text = ($v['allcourts']==1? $v['allcourts'].' зает скуош': '1 свободен скуош');
					$busy_court_text = '';
				}
				$v['description'] = $free_court_text.'<br />'.$busy_court_text;
				$events_data[] = $v;
			}	
   			return $events_data;
   		}
   		return false;
	}

	public function my_reservation($user_id)
	{
		$this->db->from('events');
		$this->db->order_by('start', 'desc');
		$this->db->where('user_id = ' . "'" . $user_id . "'");
		$query = $this->db->get();
		if($query -> num_rows() > 0){
   			return $query->result_array();
   		}
   		return false;
	}

	public function delete_reservation($id, $user_id)
	{
		if($this->db->delete('events', array('id' => $id, 'user_id'=>$user_id))){
   			return true;
   		}
   		return false;
	}

	public function new_event($user_id)
	{
		$data = array();
		$insert_data = array();
		foreach ($this->input->post("event") as $key => $value) {
			if($this->input->post("court")==1){
				$data = explode('|', $value);
			}
			$insert_data = array(
				"user_id" => $user_id,
				"start" => isset($data[1])? $data[1]: $value,
				"end" => isset($data[1])? $data[1]: $value,
				"court_type" => $this->input->post("court"),
				"playground" => isset($data[0])? $data[0]: 0
			);
			$insert = $this->db->insert("events", $insert_data);
		}
		return true;
	}
}

/* End of file events.php */
/* Location: ./application/models/events.php */
