<?php
class Menu_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->load->helper('func_helper');
	}

	public function get_menu()
	{
		$id = "";
		if($this->uri->segment(1)=='content' && $this->uri->segment(3)!=""){
			$id = explode("_", $this->uri->segment(3));
			$id = $id[1];
		}
		
		$this->db->order_by("menu", "asc");
		$query = $this->db->get_where('content', array('id1' => 0, 'show' => 'y'));
		$menu = array();
		foreach($query->result_array() as $row){
			$link = $row["link"];	
			$row["target"]="";
			if($row["link"]!=""){
				if(strpos($row["link"],"tp://")==0){
					$row["link"] = $this->lang->lang().'/'.$row["link"];
					$row["target"]="";
				}
				else{
					$row["target"] = ' target="_blank"';
				}
			}else{
				$row["link"] = "content/detailed/".friendlyURL($row['title_'.$this->lang->lang()], 'latin')."_".$row['id'];
			}
			$row['selected'] = ($this->uri->segment(2)==$link?' selected="selected"':'');
			//$row['sub'] = count($this->get_submenu($row['id']))>0? $this->get_submenu($row['id']): '';
			$menu[]=$row;
		}
		return $menu;
	}

	public function get_submenu($id)
	{
		$this->db->order_by("menu", "asc");
		$query = $this->db->get_where('content', array('id1 =' => $id, 'show' => 'y'));
		$menu = array();
		foreach($query->result_array() as $row){
			$row["target"]="";
			if($row["link"]!=""){
				if(strpos($row["link"],"tp://")==0){
					$row["target"]="";
				}
				else{
					$row["target"] = ' target="_blank"';
				}
			}else{
				//$row["link"] = base_url()."content/articles/".friendlyURL($row['title'], 'latin')."_".$row['id'];
				$row["link"] = "content/detailed/".friendlyURL($row['title_'.$this->lang->lang()], 'latin')."_".$row['id'];
			}
			$menu[]=$row;
		}
		return $menu;
	}
}