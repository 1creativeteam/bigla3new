<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Input extends CI_Input {
    function __construct() {
        parent::__construct();
    }

    function _clean_input_keys($str) {
    if ( ! preg_match("/^[a-z0-9\.:_\/-]+$/i", $str)) {
        //exit('Disallowed Key Characters: '.$str);
        header('Location: /');
    }
    // Clean UTF-8 if supported
    if (UTF8_ENABLED === TRUE) {
        $str = $this->uni->clean_string($str);
    }
    return $str;
    }
}

// END MY_Input class

/* End of file MY_Input.php */
/* Location: ./application/hooks/MY_Input.php */ 