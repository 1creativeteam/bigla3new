<?php

$lang['all_rights']  = "Всички права запазени.";
$lang['about_us']  = "ЗА НАС";
$lang['contacts']  = "КОНТАКТИ";
$lang['phone']  = "тел.";
$lang['fax']  = "факс";
$lang['address'] = '<span>гр. София 1303</span>ул. "Опълченска" № 46-48';
$lang['home_link_text1'] = 'Реализирани <span>проекти</span>';
$lang['home_link_text2'] = 'Актуални <span>проекти</span>';
$lang['home_link_text3'] = 'Бъдещи <span>проекти</span>';
$lang['news'] = 'Новини';
$lang['more_news'] = 'още новини';
$lang['careers'] = 'Кариери';
$lang['more_careers'] = 'още кариери';
$lang['founder'] = 'Основател';
$lang['founder_name'] = 'Борис <span>Шалев</span>';
$lang['co_founder'] = 'Съсобственик и управител';
$lang['co_founder_name'] = 'Красимир <span>Шалев</span>';
$lang['menu_choose'] = 'изберете';

//form
$lang['form_text'] = 'ФОРМA ЗА КОНТАКТ';
$lang['cname'] = 'Име';
$lang['cphone'] = 'Телефон';
$lang['corg'] = 'Фирма / организация';
$lang['cemail'] = 'E-mail';
$lang['ctext'] = 'Вашето запитване';
$lang['csend'] = 'Изпрати';
$lang['csuccess'] = 'Вашето запитване беше изпратено успешно. Благодарим Ви.';

//projects
$lang['projects_title'] = 'Проекти';
$lang['floor_area'] = 'Квадратура';
$lang['rooms'] = 'стаи';
$lang['floor'] = 'ЕТАЖ';
$lang['apartments'] = 'Налични апартаменти на този етаж';
$lang['town'] = 'гр.';
$lang['area_units'] = 'm²';
$lang['choose_town'] = 'изберете град';
$lang['choose_floor'] = 'изберете етаж';
$lang['from_text'] = 'От';
$lang['to_text'] = 'До';

$lang['back'] = 'назад';