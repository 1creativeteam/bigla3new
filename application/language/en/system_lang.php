<?php

$lang['all_rights']  = 'All rights reserved.';
$lang['about_us']  = 'ABOUT US';
$lang['contacts']  = 'CONTACTS';
$lang['phone']  = 'phone';
$lang['fax']  = 'fax';
$lang['address'] = '<span>Sofia 1303</span>street "Opulchenska" № 46-48';
$lang['home_link_text1'] = 'Completed <span>projects</span>';
$lang['home_link_text2'] = 'Current <span>projects</span>';
$lang['home_link_text3'] = 'Future <span>projects</span>';
$lang['news'] = 'News';
$lang['more_news'] = 'more news';
$lang['careers'] = 'Careers';
$lang['more_careers'] = 'more careers';
$lang['founder'] = 'Founder';
$lang['founder_name'] = 'Boris<span> Shalev</span>';
$lang['co_founder'] = 'Co-Founder and Manager';
$lang['co_founder_name'] = 'Krassimir<span> Schalev</span>';
$lang['menu_choose'] = 'choose';

//form
$lang['form_text'] = 'CONTACT US';
$lang['cname'] = 'Name';
$lang['cphone'] = 'Phone';
$lang['corg'] = 'Organization';
$lang['cemail'] = 'E-mail';
$lang['ctext'] = 'Text';
$lang['csend'] = 'Send';
$lang['csuccess'] = 'You request send successfuly.';

//projects
$lang['projects_title'] = 'Projects';
$lang['floor_area'] = 'Floor area';
$lang['rooms'] = 'Rooms';
$lang['floor'] = 'FLOOR';
$lang['apartments'] = 'Apartments';
$lang['town'] = '';
$lang['area_units'] = 'md²';
$lang['choose_town'] = 'choose town';
$lang['choose_floor'] = 'choose floor';
$lang['from_text'] = 'From';
$lang['to_text'] = 'To';

$lang['back'] = 'back';
