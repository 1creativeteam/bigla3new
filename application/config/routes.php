<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');



/*



| -------------------------------------------------------------------------



| URI ROUTING



| -------------------------------------------------------------------------



| This file lets you re-map URI requests to specific controller functions.



|



| Typically there is a one-to-one relationship between a URL string



| and its corresponding controller class/method. The segments in a



| URL normally follow this pattern:



|



|	example.com/class/method/id/



|



| In some instances, however, you may want to remap this relationship



| so that a different class/function is called than the one



| corresponding to the URL.



|



| Please see the user guide for complete details:



|



|	http://codeigniter.com/user_guide/general/routing.html



|



| -------------------------------------------------------------------------



| RESERVED ROUTES



| -------------------------------------------------------------------------



|



| There area two reserved routes:



|



|	$route['default_controller'] = 'welcome';



|



| This route indicates which controller class should be loaded if the



| URI contains no data. In the above example, the "welcome" class



| would be loaded.



|



|	$route['404_override'] = 'errors/page_missing';



|



| This route will tell the Router what URI segments to use if those provided



| in the URL cannot be matched to a valid route.



|



*/



//$route['ajax/(:any)'] = 'ajax/$1';



//$route['language'] = 'language';



//$route['language/(:any)'] = 'language/$1';



//$route['^(bg|en)/content/(:any)/(:any)'] = 'content/detailed/$2';



//$route['^(bg|en)/(:any)'] = 'pages/view/$1/';



$route['default_controller'] = "pages/index";



//$route['^(en|bg)/(.+)$'] = "$2";



$route['^(en|bg)$'] = $route['default_controller'];



$route['^(en|bg)/contacts'] = "content/contacts"; // content/contacts references like: "controller" -> "method" (controller/method);



$route['^(en|bg)/about_us'] = "content/about_us";



$route['^(en|bg)/services'] = "pages/index/services";



$route['^(en|bg)/proud'] = "pages/index/proud";



$route['^(en|bg)/news'] = "news";



$route['^(en|bg)/news/category/(:num)'] = "news/category/$1";



$route['^(en|bg)/news/index/(:num)'] = "news/index/$1";



$route['^(en|bg)/news/(:any)'] = "news/detailed/$1";



$route['^(en|bg)/careers'] = "careers";



$route['^(en|bg)/careers/index/(:num)'] = "careers/index/$1";



$route['^(en|bg)/careers/(:any)'] = "careers/detailed/$1";



$route['^(en|bg)/projects'] = "projects";



$route['^(en|bg)/projects/tintyava'] = "projects/tintyava";


$route['^(en|bg)/projects/belvyu-rezidans'] = "projects/belvyu";


$route['^(en|bg)/projects/index/(:any)'] = "projects/index/$1";



$route['^(en|bg)/projects/search'] = "projects/search";



$route['^(en|bg)/projects/(:any)'] = "projects/detailed/$1";;



$route['^(en|bg)/apartments/(:any)'] = "projects/apartments/$1";



$route['404_override'] = '';



/* End of file routes.php */



/* Location: ./application/config/routes.php */