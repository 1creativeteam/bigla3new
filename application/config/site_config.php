<?php
// site meta
define('SITE_NAME',	'Бигла III');
define('SITE_KEY',	'');
define('SITE_DESC',	'');

// assets
$config['js'] = 'assets/js/';
$config['tintyavajs'] = 'assets/tintyava/js/';
$config['css'] = 'assets/css/';
$config['tintyavacss'] = 'assets/tintyava/css/';
$config['img'] = 'assets/img/';
$config['tintyavaimg'] = 'assets/tintyava/img/';

// image dirs
$config['content']="images/content/";
$config['media']="images/media/";
$config['home']="images/home/";
$config['news']="images/news/";
$config['careers']="images/careers/";
$config['projects']="images/projects/";

$config['admin_email']="office@bigla.com";

// get localisation, set language file and set table prefix
/*$ci = &get_instance();
$lang = $ci->input->cookie('LANGUAGE', TRUE);
if($lang==''){
	$lang = 'bg';
}
$ci->lang->load('system', $lang);
$config['lang_prefix'] =  $lang.'_';
*/
/*
ако не искаме да се кропва и оразмеряваме картинката по единия р-р
слагаме crop=false и fit=true. Като попълваме само р-ра по който искаме да се оразмери (пр.width=100)
другата променлива оставяме празна. Също се попълва като разширение само ползвания р-р(enl=_100)
дригия се оставя X
 */
/* for content */
$config['contentinfo'][]=array("enl"=>"_100X67");
$config['contentinfo'][]=array("enl"=>"_800X530");
$config['contentinfo'][]=array("enl"=>"_155X250");

/* for news */
$config['newsinfo'][]=array("enl"=>"_200X136");
$config['newsinfo'][]=array("enl"=>"_300X");

/* for home */
$config['homeinfo'][]=array("enl"=>"_100X67");
$config['homeinfo'][]=array("enl"=>"_1400X576");

/* for projects */
$config['projectsinfo'][]=array("enl"=>"_200X136");
$config['projectsinfo'][]=array("enl"=>"_300X");
//$config['projectsinfo'][]=array("enl"=>"_931X634");
$config['projectsinfo'][]=array("enl"=>"_622X422");
$config['projectsinfo'][]=array("enl"=>"_940X447");

$config['breadcrumbs_delimiter']=" <span class='delimiter'> </span> ";

//paging
$config['per_page'] = 4;

//cyrillic character
define('CYR_FILTER', '/^(?:[А-я \-.\/,])+$/u');
define('CYR_FILTER_ADDRESS', '/^(?:[А-я \-".,\':\d!?@$%()+_№*;\/])+$/u');


/* End of file site_config.php */
/* Location: ./application/config/site_config.php */