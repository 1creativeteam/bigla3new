$( document ).ready(function() {
$('#suterenSlug').append(suterenSlug);
$('#view3').append(viewSlug);// funkcii za preorazmerqvane na viso4inata na dvata boxa pod snimkata na sgradata
  if ($( window ).width()>991) {
    var floorFullHeight = $('.apartment-full').height();
    $(".floor-full").css("height",floorFullHeight+42);
  }
  else {
      $(".floor-full").css("height","100%");
  }

  if ($( window ).width()>991) {
    var buildingFullHeight = $('.two-screan').height();
    $("#floor-table").css("height",buildingFullHeight);
  }
  else {
      $("#floor-table").css("height","100%");
  }
// funkciq za zatvarqne na .floor-full sled click na hiksa
$(".floor-full .close").on("click", function() {
  $(".floor-full").hide(300);
  $(".apartment-full").hide(300);
  $(".floor-full").removeClass('active-zoom');
    $( ".floor-full .floors" ).removeClass( "zoom-big-pic" );
  if ($( ".zoom-out" ).hasClass("active")) {
    $( ".zoom-out" ).removeClass( "active" );
    $( ".zoom" ).addClass( "active" );
  }
  $(".description").show(300);
  $(".price-list").hide(300);
  $( ".apartment-full .floors #apartment" ).eq(1).attr({'src':''});

});// funkciq za zatvarqne na .apartment-full sled click na hiksa
$(".apartment-full .close").on("click", function() {
  $(".apartment-full").hide(300);
  $(".apartment-full").removeClass('active-zoom');
  $( ".apartment-full .floors" ).removeClass( "zoom-big-pic" );
  if ($( ".zoom-out" ).hasClass("active")) {
    $( ".zoom-out" ).removeClass( "active" );
    $( ".zoom" ).addClass( "active" );
  }
  $('.floor-full').removeClass( "none" );
  $( ".apartment-full .floors #apartment" ).eq(1).attr({'src':''});
});
// funkciq za uvelichavane na sekciqta (.floor-full) pri click na lupata
$( ".floor-full #zoom" ).click(function() {
  $( ".floor-full .zoom" ).toggleClass( "active" );
  $('.floor-full').toggleClass( "active-zoom", 300 );
  $('.apartment-full').toggleClass( "none" );
  $( ".floor-full .zoom-out" ).toggleClass( "active" );
  $( ".floor-full .floors" ).toggleClass( "zoom-big-pic" );
});// funkciq za uvelichavane na sekciqta (.apartment-full) pri click na lupata, zarejdat se razlichni snimkina za razlichnata golemina
$( ".apartment-full #zoom" ).click(function() {
  $( ".apartment-full .zoom" ).toggleClass( "active" );
  $('.apartment-full').toggleClass( "active-zoom", 300 );
  $('.floor-full').toggleClass( "none" );
  $( ".apartment-full .zoom-out" ).toggleClass( "active" );
  $( ".apartment-full .floors" ).toggleClass( "zoom-big-pic" );
  var imageSrc = $( ".apartment-full .floors #apartment" ).eq(0).attr("src");
  imageSrc = imageSrc.slice(-5)
  for (var i = 0; i < 6; i++) {
     var j=i+1;
     if ($('.apartment'+j).length>0) {
        $( ".apartment-full .floors #apartment" ).eq(1).attr({'src': '../../../assets/tintyava/'+ ap[i].zoom +imageSrc});
        $( ".apartment-full .floors #apartment" ).addClass("apartment"+j);
      }
  }
});// funkciq na 3D plugina
  if ($('.threesixty').length){
      var product1;
      function init(){
        var images = [
        '1.png','2.png','3.png','4.png','5.png','6.png','7.png','8.png','9.png','10.png','11.png','12.png','13.png','14.png','15.png','16.png',
        '17.png','18.png','19.png','20.png','21.png','22.png','23.png','24.png','25.png','26.png','27.png','28.png','29.png','30.png','31.png',
        '32.png','33.png','34.png','35.png','36.png','37.png','38.png','39.png','40.png','41.png','42.png','43.png','44.png','45.png','46.png',
        '47.png','48.png'];
        var imgArray = [];
        for (var i = 0; i < images.length; i++) {
        imgArray.push("../../../assets/tintyava/img/3D/" + images[i]);
        }
        $( ".startimage" ).click(function() {
if ($( window ).width()<768) {
    $('.phone-rotate').show(300);
}

          $('.threeD').show(300);
          $('.startimage').hide(300);
        product1 = $('.threesixty').ThreeSixty({
        totalFrames: imgArray.length,
        endFrame: 101,
        currentFrame: 1,
        imgList: '.threesixty_images',
        progress: '.spinner',
        filePrefix: '',
        height: 700,
        width: 1280,
        framerate: 40,
        navigation: true,
        responsive: true,
        disableSpin: false,
        imgArray: imgArray
        });
});
      }
      $('.custom_previous').bind('click', function(e) {
        product1.previous();
      });
      $('.custom_next').bind('click', function(e) {
        product1.next();
      });
      $('.custom_play').bind('click', function(e) {
        product1.play();
      });
      $('.custom_stop').bind('click', function(e) {
        product1.stop();
      });
      window.onload = init;
  }
});
// pri klik na elementite ot spisaka ili na nqkoi etaj se pokazva snimkata na etaja
function changeBuild(nr){
    document.getElementById('floor').src='../../../assets/tintyava/img/build/etaji-max1'+nr+'.png';
    $(".bigla3-new .two-screan .left ul li").css("color","#fff");
    $(".bigla3-new .two-screan .left ul li").eq(5-nr).css("color","#f7323a");
}// funckiqta smenq snimkata na sgradata v zavisimost ot izbraniq etaj, dinamichno smenq i cveta na elementite ot spisaka
function changeFloor(nr){
  for (var i = 0; i < 7; i++) {
    var j=i+1;
    if (document.getElementById('floor'+j)!==null) {
      document.getElementById('floor'+j).src= '../../../assets/tintyava/'+floor[i].normal +nr+'.png';
    }
  }
  $(".price-list tr").css("background","none");
  $(".price-list tr").eq(nr-1).css("background"," rgba(247,50,58,0.49)");
}
// prevod na statichnite dumi v saita
if ($('.language').length > 0) {
  var lang = $('.language').attr('id');
  if (lang == 'en') {
    var floorSlug = 'Floor';
    var suterenSlug = 'Basement';
    var apartmentSlug = 'Apartment';
    var freeSlug = 'Free';
    var soldSlug = 'Sold';
    var reserveSlug = 'Reserved';
    var meterSlug = ' sq. m.';
    var viewSlug = '3D';
  } else {
    var floorSlug = 'Етаж';
    var suterenSlug = 'Сутерен';
    var apartmentSlug = 'Апартамент';
    var freeSlug = 'Свободен';
    var soldSlug = 'Продаден';
    var reserveSlug = 'Резервиран';
    var meterSlug = ' кв. м.';
    var viewSlug = 'Виж 3D';
  }
}
// na click na nqkoi ot elementite na .price-list se zarejda razlichna snimka na apartament i v zavisimost ot tova dali e bil natisnat butona za zoom
function onClickAp(nr) {
  $("#apartment-number").empty();
  $('#etaj').empty();
   $(".down .apartment-full").show(300);
    $('.apartment-full').removeClass('none');
    for (var i = 0; i < 6; i++) {
           var j=i+1;
            if ($('.apartment'+j)!==null) {
               if (!$('.apartment-full').hasClass('active-zoom')) {
                           $('.apartment-full .floors').eq(1).addClass('zoom-big-pic');
                           $('.apartment-full .floors .apartment'+j).eq(0).attr({'src':'../../../assets/tintyava/'+ap[i].normal + nr+'.png'});         }
                           else {
                              $('.apartment-full .floors').eq(0).addClass('zoom-big-pic');
                            $('.apartment-full .floors .apartment'+j).eq(1).attr({'src':'../../../assets/tintyava/'+ap[i].zoom + nr+'.png'});         }       }     }
                             $("#apartment-number").append( apartmentSlug+" "+_outerArray[etajNr][nr-1].id);      $('#etaj').append('\            <td>'+_outerArray[etajNr][nr-1].sqm_light +' </td>\            <td>'+_outerArray[etajNr][nr-1].sqm_real +'  </td>\            <td>'+_outerArray[etajNr][nr-1].sqm +'</td>\            <td class="reserve">'+_outerArray[etajNr][nr-1].status +'</td>\     ');
                             if ($('#etaj .reserve').text()=="Продаден") {
                                          $('#etaj .reserve').css("color","#f7323a");        }
                                          else if ($('#etaj .reserve').text()=="Свободен") {
                                                           $('#etaj .reserve').css("color","#419d41");        }
}
// otdelna funkciq za click na suterena, beshe vavedeno kato uslovie v stranicata po kasno i e s otdelna finkciq
function onClickSuteren() {

  $('#Map-floor1').empty();
  $('#table').empty();
  $(".down .floor-full").show(300);
  $(".down .apartment-full").hide(300);
  $(".description").hide(300);
  $(".price-list").show(300);
  $('.apartment-full .floors').removeClass('zoom-big-pic');

  if ($('.floor-full').hasClass('active-zoom')) {

    $('.floor-full .floors').eq(0).addClass('zoom-big-pic');
    $('.floor-full .floors').eq(1).removeClass('zoom-big-pic');

  } else {

    $('.floor-full .floors').eq(1).addClass('zoom-big-pic');
    $('.floor-full .floors').eq(0).removeClass('zoom-big-pic');

  }

  $( ".apartment-full .floors" ).find("img").removeAttr("class");

  if ($('.floor-full').hasClass('none')) {

    $('.floor-full').removeClass('none');
    $('.apartment-full').removeClass('active-zoom');
    $( ".apartment-full .zoom" ).toggleClass( "active" );
    $( ".apartment-full .zoom-out" ).toggleClass( "active" );

  }

  document.getElementById("floor-full").innerHTML='<img src="../../../assets/tintyava/img/floors/Parking/parking.png" alt="">';
  document.getElementById("floor-zoom").innerHTML='<img src= "../../../assets/tintyava/img/floors/Parking/zoom/parking.png" alt="" class="zoom-pic">';
  document.getElementById("floor-number1").innerHTML = suterenSlug;
  document.getElementById("floor-number").innerHTML = suterenSlug;
  $('.price-list').addClass('scroll-table');

  for (var sut = 0; sut < Object.keys(_outerArray2).length; sut++) {

    if (_outerArray2[sut].status == 'Свободен') {
        _outerArray2[sut].status = freeSlug;
     } else if (_outerArray2[sut].status == 'Продаден') {
        _outerArray2[sut].status = soldSlug;
     } else if (_outerArray2[sut].status == 'Резервиран') {
        _outerArray2[sut].status = reserveSlug;
     }

    $('#table ').append('\ <tr> \ <td>'+suterenSlug+' '+_outerArray2[sut].id +'</td> \ <td>'+_outerArray2[sut].sqm +' '+meterSlug+'</td> \ <td class="reserve">'+_outerArray2[sut].status+'</td>\ </tr>\  ');

    if ($('.reserve').eq(sut).text()=="Продаден"  || $('.reserve').eq(sut).text()=="Sold") {

      $('.reserve').eq(sut).css("color","#f7323a");

    } else if ($('.reserve').eq(sut).text()=="Свободен" || $('.reserve').eq(sut).text()=="Free") {

      $('.reserve').eq(sut).css("color","#419d41");

    }

  }
}
// click na lista s etaji i na html map s etaji, pokazva se box s append html map za razpolojenieto na apartamentite na etaja
  function onClickFloor(nr) {

  etajNr = nr-1;
   $('#Map-floor1').empty();   $('#table').empty();   $(".down .floor-full").show(300);   $(".down .apartment-full").hide(300);   $(".description").hide(300);   $(".price-list").show(300);   $('.apartment-full .floors').removeClass('zoom-big-pic');   $('.price-list').removeClass('scroll-table');   if ($('.floor-full').hasClass('active-zoom'))   {      $('.floor-full .floors').eq(0).addClass('zoom-big-pic');      $('.floor-full .floors').eq(1).removeClass('zoom-big-pic');    }      else {        $('.floor-full .floors').eq(1).addClass('zoom-big-pic');        $('.floor-full .floors').eq(0).removeClass('zoom-big-pic');      }      $( ".apartment-full .floors" ).find("img").removeAttr("class");      if ($('.floor-full').hasClass('none')) {        $('.floor-full').removeClass('none');        $('.apartment-full').removeClass('active-zoom');        $( ".apartment-full .zoom" ).toggleClass( "active" );        $( ".apartment-full .zoom-out" ).toggleClass( "active" );      }      if ($('.apartment-full').hasClass('none')) {         $('.apartment-full').removeClass('none');   }         for (var i = 0; i < Object.keys(map).length; i++)       {         var j=i+1;         var array = map[i].small;         var br = Object.keys(array).length;         if (nr==j)         {            document.getElementById("floor-zoom").innerHTML='<img src= "../../../assets/tintyava/'+ floor[i].zoom+'.png" alt="" class="zoom-pic">';            document.getElementById("apartments-full").innerHTML='<img id="apartment" src="../../../assets/tintyava/'+ap[i].normal+'1.png" alt=""  class="apartment'+j+'">';            document.getElementById("floor-full").innerHTML='<img src="../../../assets/tintyava/'+ floor[i].normal+'.png" alt="" tppabs="../../../assets/tintyava/'+ floor[i].normal+'.png" usemap="#Map-floor1" id="floor'+j+'">';
       document.getElementById("floor-number").innerHTML = floorSlug+nr;
       document.getElementById("floor-number1").innerHTML = floorSlug+nr;
       for (var apr = 0; apr < Object.keys( _outerArray[i]).length; apr++) {
         var func = apr+1;

         if (_outerArray[i][apr].status == 'Свободен') {
            _outerArray[i][apr].status = freeSlug;
         } else if (_outerArray[i][apr].status == 'Продаден') {
            _outerArray[i][apr].status = soldSlug;
         } else if (_outerArray[i][apr].status == 'Резервиран') {
            _outerArray[i][apr].status = reserveSlug;
         }

         $('#table ').append('\         <tr onclick="onClickAp('+func+')" onmouseover="javascript:changeFloor('+func+');">\         <td>'+apartmentSlug+' '+_outerArray[i][apr].id +'</td>\         <td>'+_outerArray[i][apr].sqm + meterSlug +'</td>\         <td class="reserve">'+_outerArray[i][apr].status +'</td>\         </tr>\         ');
         if ($('.reserve').eq(apr).text()=="Продаден" || $('.reserve').eq(apr).text()=="Sold") {
            $('.reserve').eq(apr).css("color","#f7323a");
         }else if ($('.reserve').eq(apr).text()=="Свободен" || $('.reserve').eq(apr).text()=="Free") {
                 $('.reserve').eq(apr).css("color","#419d41");
         }

       }

         for (var p = 0; p < Object.keys(array).length; p++) {
          var d=p+1;
           if ($( window ).width()<500) {
            $('#Map-floor1').append('<area onmouseover="javascript:changeFloor('+d+');" onclick="onClickAp('+d+')" alt="" title="" shape="poly" coords="'+map[i].small[p]+'" />');
           }
           else {
              $('#Map-floor1').append('<area onmouseover="javascript:changeFloor('+d+');" onclick="onClickAp('+d+')" alt="" title="" shape="poly" coords="'+map[i].normal[p]+'" />');
            }
         }
      }
   }
}// snimkite na vsi4ki parvi apartamenti na saotvetniq etaj. Sledvashtite snimki se zarejdat kato se smenq cifrata nakraq na snimkata, sprqmo izbraniq apartament
ap = [
  {
    normal:'img/floors/Invert_parter/ap1',
    zoom: 'img/floors/Invert_parter/zoom/ap1'
  },
  {
    normal:'img/floors/Floor2/ap1',
    zoom: 'img/floors/Floor2/zoom/ap1'
  },
  {
    normal:'img/floors/Floor3/ap1',
    zoom: 'img/floors/Floor3/zoom/ap1'
  },
  {
    normal:'img/floors/Floor4/ap1',
    zoom: 'img/floors/Floor4/zoom/ap1'
  },
  {
    normal:'img/floors/Floor5/ap1',
    zoom: 'img/floors/Floor5/zoom/ap1'
  },
  {
    normal:'img/floors/Floor6/ap1',
    zoom: 'img/floors/Floor6/zoom/ap1'
  }
];
// snimkite na vsi4ki parvi etaji. Sledvashtite snimki se zarejdat kato se smenq cifrata nakraq na snimkata, sprqmo izbraniq apartament

  floor = [
    {

      normal:'img/floors/Invert_parter/kota0',

      zoom: 'img/floors/Invert_parter/zoom/kota0',

    },

    {
      normal:'img/floors/Floor2/kota1',
      zoom: 'img/floors/Floor2/zoom/kota1'
    },
    {
      normal:'img/floors/Floor3/kota2',
      zoom: 'img/floors/Floor3/zoom/kota2'
    },
    {
      normal:'img/floors/Floor4/kota4',
      zoom: 'img/floors/Floor4/zoom/kota4'
    },
    {
      normal:'img/floors/Floor5/kota5',
      zoom: 'img/floors/Floor5/zoom/kota5'
    },
    {
      normal:'img/floors/Floor6/kota6',
      zoom: 'img/floors/Floor6/zoom/kota6'
    }
  ];// html map koordinatite na snimkite s apartamenti na etaja, saotvetno malkiq i golemiq i variant, koito se zarejdat dinamichno sprqmo predhoden izbot
map = [   {
      small : [
    	"184,1,184,72,219,72,219,87,230,88,229,101,269,101,267,0",
    	"185,86,229,87,229,101,269,100,268,159,184,159",
    	"184,106,184,159,93,159,94,87,134,86,134,103",
    	"92,0,90,159,3,158,1,1",
    	"94,0,93,72,151,73,151,56,184,56,184,1"
    ],
    normal : [
    	"455,2,456,168,389,171,386,147,369,148,369,123,313,123,310,1",
    	"309,146,387,146,390,168,457,168,455,268,311,269",
    	"310,174,311,269,155,268,157,146,229,145,227,175" ,
    	"155,0,155,269,1,267,1,0" ,
    	"155,0,156,123,227,122,226,113,254,115,255,92,312,92,313,0"
    ]
  },
  {
      small : [
      "180,0,181,71,213,70,215,85,225,85,226,98,270,98,269,1",
      "181,83,180,155,270,154,270,98,225,98,224,86",
      "91,84,91,155,180,154,180,101,131,100,133,84",
      "91,0,91,155,2,155,2,0",
      "92,0,95,78,147,76,149,56,180,54,179,0"
    ],
    normal : [
        "305,21,304,120,341,120,378,119,379,165,427,165,426,99,446,99,448,14,425,15,428,1,381,2,381,18,381,20",
        "303,142,303,244,381,244,382,263,427,263,428,246,447,250,447,166,382,164,381,144",
        "303,173,303,243,277,244,277,263,230,263,230,244,154,245,154,142,222,143,223,172",
        "149,225,77,227,75,204,22,203,21,172,0,171,2,91,14,91,14,55,76,58,77,35,111,36,114,15,151,14,153,22,154,26,154,224",
        "153,20,155,120,223,122,223,112,248,113,250,93,303,93,304,19,278,3,231,2,231,18"
    ]
  },

   {
      small : [
      "184,0,186,81,218,80,219,87,231,87,231,100,270,100,270,2",
      "184,81,217,82,217,88,229,89,230,101,269,101,269,159,184,159",
      "183,104,184,159,92,159,94,86,135,86,136,104",
      "93,0,92,159,3,159,2,0",
      "94,0,94,73,151,73,152,56,183,56,183,0"
    ],
    normal : [
      "310,22,310,123,367,123,368,148,386,148,387,170,435,169,435,102,452,102,451,22,436,21,436,0,389,0,389,19",
      "309,145,310,248,387,249,390,268,437,267,438,250,453,248,452,204,457,203,455,168,390,168,389,147",
      "157,145,155,250,237,248,237,268,283,268,283,249,309,248,308,175,228,172,229,175,228,146",
      "154,17,157,231,77,232,78,206,21,207,21,174,1,174,0,92,20,94,21,62,77,62,81,36,114,36,114,14",
      "156,22,156,122,227,123,228,115,255,115,255,92,310,93,312,20,283,20,283,0,237,0,236,20"
    ]
  },

   {
      small : [
        "183,0,185,80,218,79,218,87,230,88,230,101,270,100,270,2",
        "270,100,269,157,127,159,126,104,183,104,184,86,230,86,231,100",
        "135,86,135,104,124,104,124,159,3,159,3,0,93,0,93,72,93,87",
        "94,0,93,73,136,72,135,66,151,68,152,54,185,55,185,0"
    ],
    normal : [
        "310,0,311,135,370,132,368,146,388,147,388,171,457,170,453,2",
        "455,169,455,269,212,268,212,175,307,176,310,146,388,146,390,168",
        "211,268,2,268,2,1,156,0,156,122,155,146,227,147,227,176,210,175",
        "154,0,159,135,253,136,254,94,311,94,309,0"
   ]
  },

  {
      small : [
        "184,1,184,72,219,72,219,87,230,88,229,101,269,101,267,0",
        "185,86,229,87,229,101,269,100,268,159,184,159",
        "184,106,184,159,93,159,94,87,134,86,134,103",
        "92,0,90,159,3,158,1,1",
        "94,0,93,72,151,73,151,56,184,56,184,1"
    ],
    normal : [
        "455,2,456,168,389,171,386,147,369,148,369,123,313,123,310,1",
        "309,146,387,146,390,168,457,168,455,268,311,269",
        "310,174,311,269,155,268,157,146,229,145,227,175",
        "155,0,155,269,1,267,1,0",
        "155,0,156,123,227,122,226,113,254,115,255,92,312,92,313,0"
    ]
  },

  {
      small : [
        "145,0,145,62,192,62,193,100,128,98,129,145,240,145,236,1",
        "146,2,146,63,129,62,130,144,39,145,39,1"
    ],
    normal : [
      "403,2,405,245,219,245,219,165,326,165,326,108,278,106,275,79,246,79,244,3",
      "246,1,246,104,218,102,219,244,45,246,46,4"
    ]
  }
]