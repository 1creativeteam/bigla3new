function initialize()
{
var mapProp = {
center: {lat: 42.7027018 , lng: 23.311127899999974},/*koardinata na centara na kartata za desctop*/
zoom:16,
scrollwheel: false,
draggable: true,
mapTypeId:google.maps.MapTypeId.ROADMAP
};
var mapProp768 = {
center: {lat: 42.7027018 , lng: 23.311127899999974},/*koardinata na centara na kartata za telefon*/

zoom:17,
scrollwheel: false,
draggable: true,
mapTypeId:google.maps.MapTypeId.ROADMAP
};

if (screen.width < 768) {
var map=new google.maps.Map(document.getElementById("googleMap"),mapProp768);

}
else {
var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
}
/*masiv - texa na info windowsa i koardinati na markera*/
var locations = [
['<div><p>гр. София 1303</br>ул. "Опълченска" № 46-48</p><a href="https://goo.gl/mjs3xy" target="_blank" >See in Google Maps</a></div>', 42.7027018,23.311127899999974]];
var infowindow = new google.maps.InfoWindow;
var marker, i;
var image = 'img/map-m.png';/*kartinkara za marker*/
for (i = 0; i < locations.length; i++) {
marker = new google.maps.Marker({
position: new google.maps.LatLng(locations[i][1], locations[i][2]),
map: map,
icon: image
});
/*info windows*/
google.maps.event.addListener(marker, 'click', (function(marker, i) {
    return function() {
        infowindow.setContent(locations[i][0]);
        infowindow.open(map, marker);

}
})(marker, i));
/*zatvarqne na info windows pri klik kadeto i da e na kartata*/
google.maps.event.addListener(map, 'click', function(event) {
if (infowindow) {
   infowindow.close();
};
})
}
}
google.maps.event.addDomListener(window, 'load', initialize);
