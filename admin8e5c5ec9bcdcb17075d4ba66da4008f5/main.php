<?php
ob_start();
ini_set("error_reporting", E_ALL);
ini_set("display_errors",1);

require("./configs/conf.inc.php");
require("function.php");
require_once("sec.inc.php");
require("login.php");
require_once(getcwd().'/lib/Smarty.class.php');
require_once(getcwd().'/lib/phpmailer/class.phpmailer.php');

//$locname=set_locale();
$locname="bg";
$language=$locname;

require_once './loc/loc_'.$locname.'.php';

if (@$dir=opendir('modules/') ){ 
    while ($file=readdir($dir)) {       
        if (is_file('modules/'.$file)) {   
	     require_once './modules/'.$file;
   	    }
    }
}
closedir($dir);                                                                                                                                                       

if(!check_login()) {
	header("Location:logon.php");
	exit();
}

$act=isset($_GET['act'])?trim($_GET['act']):"";
$act=sec_check($act,"str","");

$smarty = new Smarty();
$smarty->template_dir = './templates';
$smarty->compile_dir = './templates_c';
$smarty->cache_dir = './cache';
$smarty->config_dir = './configs';

$smarty->assign("loc",$vars["loc"]);

$design_templ=set_design();

$smarty->assign("left_body",main_left());
$smarty->assign("right_body",main_right());
$smarty->assign("main_body",main_main());
$smarty->assign("top_body",main_top());
$smarty->assign("bottom_body",main_bottom());
$smarty->assign("charset",$main_text["charset"]);
$smarty->assign("design",$design_templ);
$smarty->display("main.tpl"); 
// close dbase  
mysql_close();
ob_flush()
 ?>
