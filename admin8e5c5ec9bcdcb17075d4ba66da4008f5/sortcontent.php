<?php
ini_set('date.timezone', 'Europe/Sofia');
require("configs/conf.inc.php");
require("function.php");
require("login.php");
require("sec.inc.php");

$locname="bg";
$language=$locname;
require_once './loc/loc_'.$locname.'.php';
  if ( @$dir=opendir('modules/') ){ 
     while ($file=readdir($dir)) {       
        if (is_file('modules/'.$file)) {   
			require_once './modules/'.$file;
   	    }
    }
  }
 closedir($dir);                                                                                                                                                       

if(!check_login()) {
	header("Location:logon.php");	
	exit();
} 

require_once(getcwd().'/lib/Smarty.class.php');
$smarty = new Smarty();

$smarty->template_dir = './templates';
$smarty->compile_dir = './templates_c';
$smarty->cache_dir = './cache';
$smarty->config_dir = './configs';

$smarty->assign("main_body",main_main());

$smarty->display("main_sort.tpl"); 
// close dbase  
mysql_close();

?>