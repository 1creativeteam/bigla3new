<?php
ini_set('date.timezone', 'Europe/Sofia');

require_once("./configs/conf.inc.php");
require_once("sec.inc.php");
require("login.php");

$err=sec_check($_GET['err'],"str","");

if (isset($_POST["usrname"]) && isset($_POST["passwd"])){
    do_login($_POST["usrname"],$_POST["passwd"]);
}else{
    login($err);
}
mysql_close($dbh);
?>