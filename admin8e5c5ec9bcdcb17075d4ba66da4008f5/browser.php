<?php
ini_set('error_reporting', E_ALL);
ini_set('display_error', 1);
ini_set('date.timezone', 'Europe/Sofia');
require("configs/conf.inc.php");
require("function.php");
require_once("sec.inc.php");
//include("template.php");
require("login.php");
ini_set("display_errors",0);

//$locname=set_locale();
$locname="bg";
$language=$locname;

require_once './loc/loc_'.$locname.'.php';

if ( @$dir=opendir('modules/') ){ 
    while ($file=readdir($dir)) {       
        if (is_file('modules/'.$file)) {   
	     require_once './modules/'.$file;
   	    }
    }
}
closedir($dir);                                                                                                                                                       

if(!check_login()) {
	header("Location:logon.php");
	exit();
} 

require_once(getcwd().'/lib/Smarty.class.php');
$act=sec_check($_GET["act"],"str","");

$smarty = new Smarty();

$smarty->template_dir = './templates';
$smarty->compile_dir = './templates_c';
$smarty->cache_dir = './cache';
$smarty->config_dir = './configs';

$smarty->assign("loc",$vars["loc"]);
switch($_GET["type"]){
	case "image":
		$dirpic="../images/".$_GET["act"]."/".$_GET["rec"]."/";
		if($_GET["act"]=='news'){
			$imageinfo = $vars['newsmageinfo'][0]["enl"];
		}
		elseif($_GET["act"]=='homeselect'){
			$imageinfo = $vars['himage'][0]["enl"];
		}
		elseif($_GET["act"]=='recipes'){
			$imageinfo = $vars['rimage'][0]["enl"];
		}
		else{
			$imageinfo = $vars['imageinfo'][0]["enl"];
		}
		$urlpic=$vars['conf']['site_url']."/images/".$_GET["act"]."/".$_GET["rec"]."/";
		$smarty->assign("site_url",$vars['conf']['site_url']);
		if (file_exists($dirpic)){
			if ($myDirectory = opendir($dirpic)){
				while($entryName=readdir($myDirectory)){
					if (strpos($entryName, $imageinfo.".jpg")!==false) {
						$image["image"]=$urlpic.$entryName;
						$image["urlth"]=$urlpic.$entryName;
						$image["urlfl"]=$urlpic.str_replace($imageinfo.".jpg",".jpg",$entryName);
						$size=getimagesize($dirpic.$entryName);
						$image["size0"]=$size[0];
						$image["size1"]=$size[1];
						$image["sizeth"]=$size[0]."x".$size[1];
						$size=getimagesize($dirpic.str_replace($imageinfo.".jpg",".jpg",$entryName));
						$image["sizefl"]=$size[0]."x".$size[1];
						$images[]=$image;
					}
				}
				closedir($myDirectory);
			}
		}
		$smarty->assign("images",$images);
		
		$smarty->assign("charset",$main_text["charset"]);
		$smarty->display("image_browser.tpl"); 
		break;
	case "image1":
		/*
		 * browse content images
		 */
		$dirpic="../images/".$_GET["act"]."/".$_GET["rec"]."/";
		$urlpic=$vars['conf']['site_url']."/images/".$_GET["act"]."/".$_GET["rec"]."/";
		$smarty->assign("site_url",$vars['conf']['site_url']);
		if (file_exists($dirpic)){
			if ($myDirectory = opendir($dirpic)){
				while($entryName=readdir($myDirectory)){
					$pos = strpos($entryName, $vars['contentimage'][0]["enl"].".jpg");
					if ($pos!=0) {
						$image["image"]=$urlpic.$entryName;
						$image["urlth"]=$urlpic.$entryName;
						$image["urlfl"]=$urlpic.str_replace($vars['contentimage'][0]["enl"].".jpg",".jpg",$entryName);
						$size=getimagesize($dirpic.$entryName);
						$image["size0"]=$size[0];
						$image["size1"]=$size[1];
						$image["sizeth"]=$size[0]."x".$size[1];
						$size=getimagesize($dirpic.str_replace($vars['contentimage'][0]["enl"].".jpg",".jpg",$entryName));
						$image["sizefl"]=$size[0]."x".$size[1];
						$images[]=$image;
					}
				}
				closedir($myDirectory);
			}
		}
		$smarty->assign("images",$images);
		
		$smarty->assign("charset",$main_text["charset"]);
		$smarty->display("image_browser.tpl"); 
		break;	
	case "file":
		$dirpic="../images/".$_GET["act"]."/".$_GET["rec"]."/";
		if (file_exists($dirpic)){
			if ($myDirectory = opendir($dirpic)){
				while($entryName=readdir($myDirectory)){
					$pos = strpos($entryName, "_xs.jpg");
					if ($pos!=0) {
						$image["image"]=$dirpic.$entryName;
						$image["urlth"]=$dirpic.$entryName;
						$image["urlfl"]="./images/".$_GET["act"]."/".$_GET["rec"]."/".str_replace("_xs.jpg",".jpg",$entryName);
						$size=getimagesize($dirpic.$entryName);
						$image["size0"]=$size[0];
						$image["size1"]=$size[1];
						$image["sizeth"]=$size[0]."x".$size[1];
						$size=getimagesize($dirpic.str_replace("_xs.jpg",".jpg",$entryName));
						$image["sizefl"]=$size[0]."x".$size[1];
						$images[]=$image;
					}
				}
				closedir($myDirectory);
			}
		}
		$smarty->assign("images",$images);
		
		$smarty->assign("charset",$main_text["charset"]);
		$smarty->display("link_browser.tpl"); 
		break;
}
mysql_close();
?>
