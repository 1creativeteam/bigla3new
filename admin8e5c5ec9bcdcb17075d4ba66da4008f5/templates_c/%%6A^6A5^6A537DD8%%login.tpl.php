<?php /* Smarty version 2.6.13, created on 2014-08-18 18:41:10
         compiled from login.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'login.tpl', 45, false),)), $this); ?>
<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 7]>    <html class="ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html lang="bg" xml:lang="bg">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width" />
		<title>Администрация</title>
		<link rel="stylesheet" type="text/css" media="screen, projection" href="../design/styles.css" />
		<link rel="stylesheet" href="css/foundation.min.css">
	  	<link rel="stylesheet" href="css/app.css">
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="js/jquery-1.9.0.min.js"><\/script>');</script>
		<!--[if lt IE 9]>
			<script src="http://stexbar.googlecode.com/svn-history/r507/trunk/www/js/html5shiv.js"></script>
			<script>window.jQuery || document.write('<script src="../assets/js/html5shiv.js"><\/script>');</script>
		<![endif]-->
	</head>	
	<body>
		<div class="row">
			<div class="twelve columns">
				<div id="header">
					<div id="logo">
				  		<h1><a href="./"><img src="../assets/css/logo.png" alt="" /></a></h1>
					</div>
				</div>
				<div class="four columns centered login">
					<label for=""<?php if ($this->_tpl_vars['error']): ?> class="error"<?php endif; ?>><?php echo $this->_tpl_vars['error']; ?>
</label>
					<form action="logon.php" method="post">
						<div>
							<label for="usrname"></label>
							<input type="text" name="usrname" placeholder="потребител" value="" />
						</div>
						<div>
							<label for="password"></label>
							<input type="password" name="passwd" placeholder="парола" value="" />
						</div>
						<div class="eleven columns"></div>
						<div>
							<button type="submit" class="button expand">Влез</button>
						</div>
					</form>
					<div id="footer">&copy;Всички права запазени.<?php echo ((is_array($_tmp=time())) ? $this->_run_mod_handler('date_format', true, $_tmp, '%Y') : smarty_modifier_date_format($_tmp, '%Y')); ?>
</div>
				</div>
			</div>
		</div>
		<script src="js/foundation.min.js"></script>
	</body>
</html>