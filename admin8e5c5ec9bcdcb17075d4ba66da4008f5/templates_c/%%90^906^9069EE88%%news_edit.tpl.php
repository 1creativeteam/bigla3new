<?php /* Smarty version 2.6.13, created on 2016-12-21 16:29:37
         compiled from news_edit.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'news_edit.tpl', 19, false),)), $this); ?>
<script language="javascript" type="text/javascript" src="js/editor/ckeditor.js"></script>

<a href="#" class="langs" data-lang="bg">bg</a>

<a href="#" class="langs" data-lang="en">en</a>

<div class="form">

	<h1 class="sifr"><?php echo $this->_tpl_vars['maintitle']; ?>
</h1>

	<?php if ($this->_tpl_vars['error']): ?><div style="color:red;"><?php echo $this->_tpl_vars['error']; ?>
</div><?php endif; ?>

	<form action="<?php echo $this->_tpl_vars['action']; ?>
" method="post" enctype="multipart/form-data">

		<div class="row">

			<div class="forlangs titles_bg four columns">

				<label for="title_bg">Заглавие БГ</label><input type="text" name="title_bg" lang="<?php echo $this->_tpl_vars['lang']; ?>
"style="width:300px;" value="<?php if ($_POST['title_bg']):  echo ((is_array($_tmp=$_POST['title_bg'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html'));  else:  echo ((is_array($_tmp=$this->_tpl_vars['title_bg'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html'));  endif; ?>">

			</div>

			<div class="forlangs titles_en four columns end">

				<label for="title_en">Заглавие EN</label><input type="text" name="title_en" lang="<?php echo $this->_tpl_vars['lang']; ?>
"style="width:300px;" value="<?php if ($_POST['title_en']):  echo ((is_array($_tmp=$_POST['title_en'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html'));  else:  echo ((is_array($_tmp=$this->_tpl_vars['title_en'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html'));  endif; ?>">

			</div>

		</div>

		<div class="forlangs text_bg">

			<label for="text">Текст БГ</label>

			<textarea name="text_bg" id="text" ros="10" class="big"><?php if ($_POST['text_bg']):  echo ((is_array($_tmp=$_POST['text_bg'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html'));  else:  echo ((is_array($_tmp=$this->_tpl_vars['text_bg'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html'));  endif; ?></textarea>

		</div>

		<div class="forlangs text_en">

			<label for="text1">Текст EN</label>

			<textarea name="text_en" id="text1" ros="10" class="big"><?php if ($_POST['text_en']):  echo ((is_array($_tmp=$_POST['text_en'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html'));  else:  echo ((is_array($_tmp=$this->_tpl_vars['text_en'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html'));  endif; ?></textarea>

		</div>

		<?php if (count ( $this->_tpl_vars['image'] )): ?><h2 style="clear:both;margin-bottom:10px;">Снимка</h2><?php endif; ?>

		<?php unset($this->_sections['image']);
$this->_sections['image']['name'] = 'image';
$this->_sections['image']['loop'] = is_array($_loop=$this->_tpl_vars['image']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['image']['show'] = true;
$this->_sections['image']['max'] = $this->_sections['image']['loop'];
$this->_sections['image']['step'] = 1;
$this->_sections['image']['start'] = $this->_sections['image']['step'] > 0 ? 0 : $this->_sections['image']['loop']-1;
if ($this->_sections['image']['show']) {
    $this->_sections['image']['total'] = $this->_sections['image']['loop'];
    if ($this->_sections['image']['total'] == 0)
        $this->_sections['image']['show'] = false;
} else
    $this->_sections['image']['total'] = 0;
if ($this->_sections['image']['show']):

            for ($this->_sections['image']['index'] = $this->_sections['image']['start'], $this->_sections['image']['iteration'] = 1;
                 $this->_sections['image']['iteration'] <= $this->_sections['image']['total'];
                 $this->_sections['image']['index'] += $this->_sections['image']['step'], $this->_sections['image']['iteration']++):
$this->_sections['image']['rownum'] = $this->_sections['image']['iteration'];
$this->_sections['image']['index_prev'] = $this->_sections['image']['index'] - $this->_sections['image']['step'];
$this->_sections['image']['index_next'] = $this->_sections['image']['index'] + $this->_sections['image']['step'];
$this->_sections['image']['first']      = ($this->_sections['image']['iteration'] == 1);
$this->_sections['image']['last']       = ($this->_sections['image']['iteration'] == $this->_sections['image']['total']);
?>

		<div style="float:left;margin:5px;padding:0px;">

			<img src="../<?php echo $this->_tpl_vars['image'][$this->_sections['image']['index']]['path'];  echo $this->_tpl_vars['imgenl'];  echo $this->_tpl_vars['image'][$this->_sections['image']['index']]['ext']; ?>
?date=<?php echo time(); ?>
" alt="<?php echo $this->_tpl_vars['image'][$this->_sections['image']['index']]['name']; ?>
" style="border:none;" border="0" />

			<div style="margin:0px;padding:0px;height:auto;">

				<!--a style="float:left;" href="main.php?act=news&amp;act1=editpic&amp;id=<?php echo $_GET['rec']; ?>
&amp;lang=<?php echo $_GET['lang']; ?>
&amp;pic=<?php echo ((is_array($_tmp=$this->_tpl_vars['image'][$this->_sections['image']['index']]['path'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
&amp;func=<?php echo $_GET['act1']; ?>
" class="but edit" title="<?php echo $this->_tpl_vars['loc']['content']['11']; ?>
"></a-->

				<a style="float:left;" href="main.php?act=news&amp;act1=delpic&amp;id=<?php echo $this->_tpl_vars['image'][$this->_sections['image']['index']]['id']; ?>
" class="but delete" title="изтрий"></a>

			</div>

		</div>

		<?php endfor; endif; ?>

		<?php if (count ( $this->_tpl_vars['image'] ) == 0): ?>

		<div class="seven columns">

			<h2 style="clear:both;margin-bottom:10px;">Добави снимка</h2>

			<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['id']; ?>
" />

			<input type="file" name="image" />

			<!--input type="text" name="title" values="" />

			<input type="text" name="text" values="" /-->

			<p><strong>Добавените снимки трябва да са във формат .jpg, .png и .gif.</strong></p>

		</div>

		<?php endif; ?>

		<br>

		<div class="four columns">

			<label>Категория</label>

			<select name="categories_id">

				<option value="0">---изберете---</option>

				<?php $_from = $this->_tpl_vars['categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>

					<?php $this->assign('field', "name_".($this->_tpl_vars['lang'])); ?>

					<option value="<?php echo $this->_tpl_vars['label']['id']; ?>
" <?php if ($_POST['categories_id'] == $this->_tpl_vars['label']['id'] || $this->_tpl_vars['label']['id'] == $this->_tpl_vars['categories_id']): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['label'][$this->_tpl_vars['field']]; ?>


					</option>

				<?php endforeach; endif; unset($_from); ?>

			</select>

		</div>

		<div class="six columns" style="clear:both;margin-bottom:10px;">
			<h2 style="clear:both;margin-bottom:10px;">Добави Галерия</h2>
			<input type="file" name="gallery[]" multiple="true" />
		</div>

		<div class="twelve columns">
			<?php $_from = $this->_tpl_vars['gallery']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['picture']):
?>
				<div style="float:left;margin:5px;padding:0px;">
					<img src="../<?php echo $this->_tpl_vars['picture']['picture']; ?>
" alt="" style="border:none;" border="0" width="200" height="136" />
					<div style="margin:0px;padding:0px;height:auto;">
						<a style="float:left;" href="main.php?act=news&amp;act1=delgallerypic&amp;id=<?php echo $this->_tpl_vars['picture']['id']; ?>
" class="but delete" title="изтрий"></a>
					</div>
				</div>
			<?php endforeach; else: ?>
				<p>Няма въведени</p>
			<?php endif; unset($_from); ?>
		</div>

		<div class="row clear">

			<div class="two columns">

				<input class="button expand postfix" type="submit" style="margin-left:10px" value="Запази" />

			</div>

		</div>

	</form>

</div>

<script type="text/javascript">

	<?php echo '

	CKEDITOR.replace( \'text1\' , { });

	CKEDITOR.replace( \'text\' , { });

	'; ?>


</script>
