<?php /* Smarty version 2.6.13, created on 2014-09-08 13:47:43
         compiled from home_edit.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'home_edit.tpl', 36, false),)), $this); ?>
<script language="javascript" type="text/javascript" src="js/editor/ckeditor.js"></script>

<div class="form">
	<h1 class="sifr">Home</h1>
	<?php if ($this->_tpl_vars['error']): ?><div style="color:red;"><?php echo $this->_tpl_vars['error']; ?>
</div><?php endif; ?>
	<form action="<?php echo $this->_tpl_vars['action']; ?>
" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="four columns">
				<input type="text" name="title_bg" lang="<?php echo $this->_tpl_vars['lang']; ?>
" style="width:300px;" value="<?php if ($_POST['title_bg']):  echo $_POST['title_bg'];  else:  echo $this->_tpl_vars['title_bg'];  endif; ?>" placeholder="Заглавие BG">
			</div>
			<div class="four columns end">
				<input type="text" name="title_en" lang="<?php echo $this->_tpl_vars['lang']; ?>
" style="width:300px;" value="<?php if ($_POST['title_en']):  echo $_POST['title_en'];  else:  echo $this->_tpl_vars['title_en'];  endif; ?>" placeholder="Заглавие EN">
			</div>
		</div>
		<div class="row">
			<div class="four columns">
				<input class="link" name="link_bg" type="text" value="<?php if ($_POST['link_bg']):  echo $_POST['link_bg'];  else:  echo $this->_tpl_vars['link_bg'];  endif; ?>" style="width:300px;" placeholder="Линк БГ" />
			</div>
			<div class="four columns end">
				<input class="link" name="link_en" type="text" value="<?php if ($_POST['link_en']):  echo $_POST['link_en'];  else:  echo $this->_tpl_vars['link_en'];  endif; ?>" style="width:300px;" placeholder="Линк EN" />
			</div>
		</div>
		<div class="row">
			<div class="six columns">
				<input name="video" type="text" value="<?php if ($_POST['video']):  echo $_POST['video'];  else:  echo $this->_tpl_vars['video_url'];  endif; ?>" placeholder="Качи видео от youtube или vimeo" />
			</div>
			<div class="end">
				<a href="<?php echo $this->_tpl_vars['video_path']; ?>
" target="_blank"><?php echo $this->_tpl_vars['video_path']; ?>
</a>
			</div>
		</div>
     	<?php if (count ( $this->_tpl_vars['image'] )): ?><h2 style="clear:both;margin-bottom:10px;">Снимка</h2><?php endif; ?>
		<?php unset($this->_sections['image']);
$this->_sections['image']['name'] = 'image';
$this->_sections['image']['loop'] = is_array($_loop=$this->_tpl_vars['image']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['image']['show'] = true;
$this->_sections['image']['max'] = $this->_sections['image']['loop'];
$this->_sections['image']['step'] = 1;
$this->_sections['image']['start'] = $this->_sections['image']['step'] > 0 ? 0 : $this->_sections['image']['loop']-1;
if ($this->_sections['image']['show']) {
    $this->_sections['image']['total'] = $this->_sections['image']['loop'];
    if ($this->_sections['image']['total'] == 0)
        $this->_sections['image']['show'] = false;
} else
    $this->_sections['image']['total'] = 0;
if ($this->_sections['image']['show']):

            for ($this->_sections['image']['index'] = $this->_sections['image']['start'], $this->_sections['image']['iteration'] = 1;
                 $this->_sections['image']['iteration'] <= $this->_sections['image']['total'];
                 $this->_sections['image']['index'] += $this->_sections['image']['step'], $this->_sections['image']['iteration']++):
$this->_sections['image']['rownum'] = $this->_sections['image']['iteration'];
$this->_sections['image']['index_prev'] = $this->_sections['image']['index'] - $this->_sections['image']['step'];
$this->_sections['image']['index_next'] = $this->_sections['image']['index'] + $this->_sections['image']['step'];
$this->_sections['image']['first']      = ($this->_sections['image']['iteration'] == 1);
$this->_sections['image']['last']       = ($this->_sections['image']['iteration'] == $this->_sections['image']['total']);
?>
		<div style="float:left;margin:5px;padding:0px;">
			<img src="../<?php echo $this->_tpl_vars['image'][$this->_sections['image']['index']]['path'];  echo $this->_tpl_vars['imgenl'];  echo $this->_tpl_vars['image'][$this->_sections['image']['index']]['ext']; ?>
?date=<?php echo time(); ?>
" alt="<?php echo $this->_tpl_vars['image'][$this->_sections['image']['index']]['name']; ?>
" style="border:none;" border="0" />
			<div style="margin:0px;padding:0px;height:auto;">
				<!--a style="float:left;" href="main.php?act=news&amp;act1=editpic&amp;id=<?php echo $_GET['rec']; ?>
&amp;lang=<?php echo $_GET['lang']; ?>
&amp;pic=<?php echo ((is_array($_tmp=$this->_tpl_vars['image'][$this->_sections['image']['index']]['path'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
&amp;func=<?php echo $_GET['act1']; ?>
" class="but edit" title="<?php echo $this->_tpl_vars['loc']['content']['11']; ?>
"></a-->
				<a style="float:left;" href="main.php?act=home&amp;act1=delpic&amp;id=<?php echo $this->_tpl_vars['image'][$this->_sections['image']['index']]['id']; ?>
" class="but delete" title="изтрий"></a>
			</div>
		</div>
		<?php endfor; endif; ?>
		<?php if (count ( $this->_tpl_vars['image'] ) == 0): ?>
		<h2 style="clear:both;margin-bottom:10px;">Добави снимка</h2>
		<input type="file" name="image" />
		<input type="hidden" name="id" values="<?php echo $this->_tpl_vars['id']; ?>
">
		<!--input type="text" name="title" values="" />
		<input type="text" name="text" values="" /-->
		<?php endif; ?>
		<p style="clear: both;"><strong>Добавените снимки трябва да са във формат .jpg, .png и .gif. <br />Ако желаете да качвате снимка трябва полето за видео да е празно!</strong></p>
		<div class="row" style="clear: both; margin-top:10px;">
			<div class="two columns">
				<input class="button expand postfix" type="submit" value="Запази" />
			</div>
		</div>
	</form>
</div>
<script type="text/javascript"><?php echo 'CKEDITOR.replace( \'text1\' , { });'; ?>
</script>