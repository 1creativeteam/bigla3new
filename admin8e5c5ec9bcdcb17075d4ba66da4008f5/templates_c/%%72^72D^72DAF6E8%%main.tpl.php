<?php /* Smarty version 2.6.13, created on 2016-12-07 17:08:26
         compiled from main.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'main.tpl', 87, false),)), $this); ?>
<!DOCTYPE html>

<html lang="bg" xml:lang="bg">

<head>

	<meta charset="utf-8" />

	<meta name="viewport" content="width=device-width" />

	<title>Администрация</title>

	<link rel="stylesheet" href="css/foundation.min.css">

  	<link rel="stylesheet" href="css/app.css">

  	<link rel="stylesheet" href="css/smoothness/jquery-ui-1.10.0.custom.css">

  	<link href="css/modal.css" rel="stylesheet" type="text/css" />

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>

	<script>window.jQuery || document.write('<script src="js/jquery-1.9.0.min.js"><\/script>');</script>

	<script src="js/jquery-ui-1.10.0.custom.min.js"></script>

	<script src="js/jquery.ui.datepicker-bg.js"></script>
	
	<script src="js/jquery.modal.js"></script>

	<script src="js/site.js"></script>

</head>	

<body>

	<div class="row">

		<div class="twelve columns">

		<!-- header begins -->

			<div id="header">

				<div id="logo">

			    	<h1><a href="./"><img src="../assets/css/logo.png" alt="" /></a></h1>

				</div>

				<?php echo $this->_tpl_vars['left_body']; ?>


				<?php if ($this->_tpl_vars['loc']['loc']['languagebar'] == 'y'): ?>

				<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "language.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

				<?php endif; ?>

			</div>

			<!-- header ends -->

			<!-- content begins -->       

			<div id="content">

				<p class="legend"><img src="images/note.gif" alt="" align="absbottom" border="0" /> - Нов запис

				<img src="images/edit.gif" alt="" align="absbottom" border="0" /> - Редакция

				<img src="images/arrange.gif" alt="" align="absbottom" border="0" /> - Сортиране

				<img src="images/locked.gif" alt="" align="absbottom" border="0" /> - Скрий

				<img src="images/unlocked.gif" alt="" align="absbottom" border="0" /> - Покажи</p>

				<div class="clearfix"></div>

				<?php echo $this->_tpl_vars['main_body']; ?>


			</div>

			<!-- bottom end --> 

			<!-- footer begins -->

			<div id="footer">&copy;Всички права запазени. <?php echo ((is_array($_tmp=time())) ? $this->_run_mod_handler('date_format', true, $_tmp, '%Y') : smarty_modifier_date_format($_tmp, '%Y')); ?>
</div>

			<!-- footer ends -->

		</div>

	</div>

	<!--[if lt IE 9]>

		<script src="http://stexbar.googlecode.com/svn-history/r507/trunk/www/js/html5shiv.js"></script>

		<script>window.jQuery || document.write('<script src="../assets/js/html5shiv.js"><\/script>');</script>

	<![endif]-->

	<script type="text/javascript" src="js/functions.js"></script>

	<script src="js/foundation.min.js">/script>

</body>

</html>