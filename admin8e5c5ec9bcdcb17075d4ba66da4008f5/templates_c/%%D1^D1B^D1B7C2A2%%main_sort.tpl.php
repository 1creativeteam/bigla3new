<?php /* Smarty version 2.6.13, created on 2014-08-21 12:04:27
         compiled from main_sort.tpl */ ?>
<!DOCTYPE html>
<html lang="bg" xml:lang="bg">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Администрация</title>
		<link rel="stylesheet" type="text/css" media="screen, projection" href="site.css" />
		<link rel="stylesheet" type="text/css" href="site.css" />
		<script src="js/jquery.js" type="text/javascript"></script>
		<script type="text/javascript" src="js/interface.js"></script>

		<script type="text/javascript">
		<?php echo '
		$(document).ready(
			function () {
				$(\'div#sort\').Sortable(
					{
						accept : 		\'sortableitem\',
						helperclass : 	\'sorthelper\',
						activeclass : 	\'sortableactive\',
						hoverclass : 	\'sortablehover\',
						opacity: 		0.8,
						fx:				200,
						axis:			\'vertically\',
						opacity:		0.4,
						revert:			true
					}
				)
			}
		);
		'; ?>

		</script>

		<style type="text/css" media="all">
		<?php echo '
		.sortableitem {
			cursor:move;
			border: 1px solid black;
			margin:10px 5px;
			clear: both;
		}
		.image {
			float:left;
			width: 50px;
			margin:5px;
		}
		h3 {
			margin:5px 0px;
		}
		'; ?>

		</style>
	</head>


	<body>
		<div id="middle">
		<?php echo $this->_tpl_vars['main_body']; ?>

		</div>
	</body>
</html>