<?php /* Smarty version 2.6.13, created on 2017-01-31 10:44:24
         compiled from tintyava_add.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'tintyava_add.tpl', 37, false),)), $this); ?>
<script language="javascript" type="text/javascript" src="js/editor/ckeditor.js"></script>



<a href="#" class="langs" data-lang="bg">bg</a>



<a href="#" class="langs" data-lang="en">en</a>



<div class="form">



	<h1 class="sifr"><?php echo $this->_tpl_vars['maintitle']; ?>
</h1>



	<?php if ($this->_tpl_vars['error']): ?><div style="color:red;"><?php echo $this->_tpl_vars['error']; ?>
</div><?php endif; ?>



	<form action="<?php echo $this->_tpl_vars['action']; ?>
" method="post" enctype="multipart/form-data">



		<div class="row clear">



			<div class="forlangs free_text_bg five columns">



				<label for="title_bg"><strong>Заглавие  БГ</strong></label><input type="text" name="free_text_bg" lang="<?php echo $this->_tpl_vars['lang']; ?>
"style="width:300px;" value="<?php if (((is_array($_tmp=$_POST['free_text_bg'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html'))):  echo $_POST['free_text_bg'];  else:  echo ((is_array($_tmp=$this->_tpl_vars['data']['free_text_bg'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html'));  endif; ?>">



			</div>



			<div class="forlangs free_text_en five columns end">



				<label for="free_text_en"><strong>Заглавие EN</strong></label><input type="text" name="free_text_en" lang="<?php echo $this->_tpl_vars['lang']; ?>
"style="width:300px;" value="<?php if ($_POST['title_en']):  echo ((is_array($_tmp=$_POST['free_text_en'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html'));  else:  echo ((is_array($_tmp=$this->_tpl_vars['data']['free_text_en'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html'));  endif; ?>">



			</div>



		</div>



		<div class="row clear">



			<div class="forlangs text_bg">



				<label for="text"><strong>Описание БГ</strong></label>



				<textarea name="text_bg" id="text" ros="10" class="big"><?php if ($_POST['text_bg']):  echo $_POST['text_bg'];  else:  echo $this->_tpl_vars['data']['text_bg'];  endif; ?></textarea>



			</div>



			<div class="forlangs text_en">



				<label for="text1"><strong>Описание EN</strong></label>



				<textarea name="text_en" id="text1" ros="10" class="big"><?php if ($_POST['text_en']):  echo $_POST['text_en'];  else:  echo $this->_tpl_vars['data']['text_en'];  endif; ?></textarea>



			</div>


		

		</div>


		<div id="tabs" class="clear tintyavaTabs">



			<ul>



				<li><a href="#etaj-1">Етаж 1</a></li>



				<li><a href="#etaj-2">Етаж 2</a></li>



				<li><a href="#etaj-3">Етаж 3</a></li>



				<li><a href="#etaj-4">Етаж 4</a></li>



				<li><a href="#etaj-5">Етаж 5</a></li>



				<li><a href="#etaj-6">Етаж 6</a></li>



				<li><a href="#etaj-7">Сутерен</a></li>



			</ul>



			<div id="etaj-1">



				<div class="row">



					<div class="two columns">



						<label>Апартамент 1</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[1][sqm]" value="<?php if ($_POST['apartments']['1']['sqm']):  echo $_POST['apartments']['1']['sqm'];  else:  echo $this->_tpl_vars['apartments']['1']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[1][sqm_real]" value="<?php if ($_POST['apartments']['1']['sqm_real']):  echo $_POST['apartments']['1']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['1']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[1][sqm_light]" value="<?php if ($_POST['apartments']['1']['sqm_light']):  echo $_POST['apartments']['1']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['1']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[1][floor]" value="1">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[1][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['1']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['1']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> >	<?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 2</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[2][sqm]" value="<?php if ($_POST['apartments']['2']['sqm']):  echo $_POST['apartments']['2']['sqm'];  else:  echo $this->_tpl_vars['apartments']['2']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[2][sqm_real]" value="<?php if ($_POST['apartments']['2']['sqm_real']):  echo $_POST['apartments']['2']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['2']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[2][sqm_light]" value="<?php if ($_POST['apartments']['2']['sqm_light']):  echo $_POST['apartments']['2']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['2']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[2][floor]" value="1">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[2][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['2']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['2']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> >	<?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 3</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[3][sqm]" value="<?php if ($_POST['apartments']['3']['sqm']):  echo $_POST['apartments']['3']['sqm'];  else:  echo $this->_tpl_vars['apartments']['3']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[3][sqm_real]" value="<?php if ($_POST['apartments']['3']['sqm_real']):  echo $_POST['apartments']['3']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['3']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[3][sqm_light]" value="<?php if ($_POST['apartments']['3']['sqm_light']):  echo $_POST['apartments']['3']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['3']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[3][floor]" value="1">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[3][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['3']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['3']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> >	<?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 4</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[4][sqm]" value="<?php if ($_POST['apartments']['4']['sqm']):  echo $_POST['apartments']['4']['sqm'];  else:  echo $this->_tpl_vars['apartments']['4']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[4][sqm_real]" value="<?php if ($_POST['apartments']['4']['sqm_real']):  echo $_POST['apartments']['4']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['4']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[4][sqm_light]" value="<?php if ($_POST['apartments']['4']['sqm_light']):  echo $_POST['apartments']['4']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['4']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[4][floor]" value="1">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[4][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['4']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['4']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> >	<?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 5</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[5][sqm]" value="<?php if ($_POST['apartments']['5']['sqm']):  echo $_POST['apartments']['5']['sqm'];  else:  echo $this->_tpl_vars['apartments']['5']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[5][sqm_real]" value="<?php if ($_POST['apartments']['5']['sqm_real']):  echo $_POST['apartments']['5']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['5']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[5][sqm_light]" value="<?php if ($_POST['apartments']['5']['sqm_light']):  echo $_POST['apartments']['5']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['5']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[5][floor]" value="1">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[5][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['5']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['5']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> >	<?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



			</div>



			<div id="etaj-2">



				<div class="row">



					<div class="two columns">



						<label>Апартамент 6</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[6][sqm]" value="<?php if ($_POST['apartments']['6']['sqm']):  echo $_POST['apartments']['6']['sqm'];  else:  echo $this->_tpl_vars['apartments']['6']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[6][sqm_real]" value="<?php if ($_POST['apartments']['6']['sqm_real']):  echo $_POST['apartments']['6']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['6']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[6][sqm_light]" value="<?php if ($_POST['apartments']['6']['sqm_light']):  echo $_POST['apartments']['6']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['6']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[6][floor]" value="2">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[6][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['6']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['6']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> >	<?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 7</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[7][sqm]" value="<?php if ($_POST['apartments']['7']['sqm']):  echo $_POST['apartments']['7']['sqm'];  else:  echo $this->_tpl_vars['apartments']['7']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[7][sqm_real]" value="<?php if ($_POST['apartments']['7']['sqm_real']):  echo $_POST['apartments']['7']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['7']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[7][sqm_light]" value="<?php if ($_POST['apartments']['7']['sqm_light']):  echo $_POST['apartments']['7']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['7']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[7][floor]" value="2">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[7][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['7']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['7']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> >	<?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 8</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[8][sqm]" value="<?php if ($_POST['apartments']['8']['sqm']):  echo $_POST['apartments']['8']['sqm'];  else:  echo $this->_tpl_vars['apartments']['8']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[8][sqm_real]" value="<?php if ($_POST['apartments']['8']['sqm_real']):  echo $_POST['apartments']['8']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['8']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[8][sqm_light]" value="<?php if ($_POST['apartments']['8']['sqm_light']):  echo $_POST['apartments']['8']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['8']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[8][floor]" value="2">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[8][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['8']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['8']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> >	<?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 9</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[9][sqm]" value="<?php if ($_POST['apartments']['9']['sqm']):  echo $_POST['apartments']['9']['sqm'];  else:  echo $this->_tpl_vars['apartments']['9']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[9][sqm_real]" value="<?php if ($_POST['apartments']['9']['sqm_real']):  echo $_POST['apartments']['9']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['9']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[9][sqm_light]" value="<?php if ($_POST['apartments']['9']['sqm_light']):  echo $_POST['apartments']['9']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['9']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[9][floor]" value="2">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[9][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['9']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['9']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> >	<?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 10</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[10][sqm]" value="<?php if ($_POST['apartments']['10']['sqm']):  echo $_POST['apartments']['10']['sqm'];  else:  echo $this->_tpl_vars['apartments']['10']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[10][sqm_real]" value="<?php if ($_POST['apartments']['10']['sqm_real']):  echo $_POST['apartments']['10']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['10']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[10][sqm_light]" value="<?php if ($_POST['apartments']['10']['sqm_light']):  echo $_POST['apartments']['10']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['10']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[10][floor]" value="2">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[10][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['10']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['10']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> >



									<?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



			</div>



			<div id="etaj-3">



				<div class="row">



					<div class="two columns">



						<label>Апартамент 11</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[11][sqm]" value="<?php if ($_POST['apartments']['11']['sqm']):  echo $_POST['apartments']['11']['sqm'];  else:  echo $this->_tpl_vars['apartments']['11']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[11][sqm_real]" value="<?php if ($_POST['apartments']['11']['sqm_real']):  echo $_POST['apartments']['11']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['11']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[11][sqm_light]" value="<?php if ($_POST['apartments']['11']['sqm_light']):  echo $_POST['apartments']['11']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['11']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[11][floor]" value="3">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[11][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['11']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['11']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 12</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[12][sqm]" value="<?php if ($_POST['apartments']['12']['sqm']):  echo $_POST['apartments']['12']['sqm'];  else:  echo $this->_tpl_vars['apartments']['12']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[12][sqm_real]" value="<?php if ($_POST['apartments']['12']['sqm_real']):  echo $_POST['apartments']['12']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['12']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[12][sqm_light]" value="<?php if ($_POST['apartments']['12']['sqm_light']):  echo $_POST['apartments']['12']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['12']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[12][floor]" value="3">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[12][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['12']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['12']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 13</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[13][sqm]" value="<?php if ($_POST['apartments']['13']['sqm']):  echo $_POST['apartments']['13']['sqm'];  else:  echo $this->_tpl_vars['apartments']['13']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[13][sqm_real]" value="<?php if ($_POST['apartments']['13']['sqm_real']):  echo $_POST['apartments']['13']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['13']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[13][sqm_light]" value="<?php if ($_POST['apartments']['13']['sqm_light']):  echo $_POST['apartments']['13']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['13']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[13][floor]" value="3">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[13][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['13']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['13']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 14</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[14][sqm]" value="<?php if ($_POST['apartments']['14']['sqm']):  echo $_POST['apartments']['14']['sqm'];  else:  echo $this->_tpl_vars['apartments']['14']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[14][sqm_real]" value="<?php if ($_POST['apartments']['14']['sqm_real']):  echo $_POST['apartments']['14']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['14']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[14][sqm_light]" value="<?php if ($_POST['apartments']['14']['sqm_light']):  echo $_POST['apartments']['14']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['14']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[14][floor]" value="3">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[14][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['14']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['14']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 15</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[15][sqm]" value="<?php if ($_POST['apartments']['15']['sqm']):  echo $_POST['apartments']['15']['sqm'];  else:  echo $this->_tpl_vars['apartments']['15']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[15][sqm_real]" value="<?php if ($_POST['apartments']['15']['sqm_real']):  echo $_POST['apartments']['15']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['15']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[15][sqm_light]" value="<?php if ($_POST['apartments']['15']['sqm_light']):  echo $_POST['apartments']['15']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['15']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[15][floor]" value="3">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[15][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['15']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['15']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



			</div>



			<div id="etaj-4">



				<div class="row">



					<div class="two columns">



						<label>Апартамент 16</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[16][sqm]" value="<?php if ($_POST['apartments']['16']['sqm']):  echo $_POST['apartments']['16']['sqm'];  else:  echo $this->_tpl_vars['apartments']['16']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[16][sqm_real]" value="<?php if ($_POST['apartments']['16']['sqm_real']):  echo $_POST['apartments']['16']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['16']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[16][sqm_light]" value="<?php if ($_POST['apartments']['16']['sqm_light']):  echo $_POST['apartments']['16']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['16']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[16][floor]" value="4">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[16][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['16']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['16']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 17</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[17][sqm]" value="<?php if ($_POST['apartments']['17']['sqm']):  echo $_POST['apartments']['17']['sqm'];  else:  echo $this->_tpl_vars['apartments']['17']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[17][sqm_real]" value="<?php if ($_POST['apartments']['17']['sqm_real']):  echo $_POST['apartments']['17']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['17']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[17][sqm_light]" value="<?php if ($_POST['apartments']['17']['sqm_light']):  echo $_POST['apartments']['17']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['17']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[17][floor]" value="4">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[17][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['17']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['17']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 18</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[18][sqm]" value="<?php if ($_POST['apartments']['18']['sqm']):  echo $_POST['apartments']['18']['sqm'];  else:  echo $this->_tpl_vars['apartments']['18']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[18][sqm_real]" value="<?php if ($_POST['apartments']['18']['sqm_real']):  echo $_POST['apartments']['18']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['18']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[18][sqm_light]" value="<?php if ($_POST['apartments']['18']['sqm_light']):  echo $_POST['apartments']['18']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['18']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[18][floor]" value="4">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[18][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['18']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['18']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 19</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[19][sqm]" value="<?php if ($_POST['apartments']['19']['sqm']):  echo $_POST['apartments']['19']['sqm'];  else:  echo $this->_tpl_vars['apartments']['19']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[19][sqm_real]" value="<?php if ($_POST['apartments']['19']['sqm_real']):  echo $_POST['apartments']['19']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['19']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[19][sqm_light]" value="<?php if ($_POST['apartments']['19']['sqm_light']):  echo $_POST['apartments']['19']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['19']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[19][floor]" value="4">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[19][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['19']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['19']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



			</div>



			<div id="etaj-5">



				<div class="row">



					<div class="two columns">



						<label>Апартамент 20</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[20][sqm]" value="<?php if ($_POST['apartments']['20']['sqm']):  echo $_POST['apartments']['20']['sqm'];  else:  echo $this->_tpl_vars['apartments']['20']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[20][sqm_real]" value="<?php if ($_POST['apartments']['20']['sqm_real']):  echo $_POST['apartments']['20']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['20']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[20][sqm_light]" value="<?php if ($_POST['apartments']['20']['sqm_light']):  echo $_POST['apartments']['20']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['20']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[20][floor]" value="5">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[20][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['20']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['20']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 21</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[21][sqm]" value="<?php if ($_POST['apartments']['21']['sqm']):  echo $_POST['apartments']['21']['sqm'];  else:  echo $this->_tpl_vars['apartments']['21']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[21][sqm_real]" value="<?php if ($_POST['apartments']['21']['sqm_real']):  echo $_POST['apartments']['21']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['21']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[21][sqm_light]" value="<?php if ($_POST['apartments']['21']['sqm_light']):  echo $_POST['apartments']['21']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['21']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[21][floor]" value="5">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[21][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['21']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['21']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 22</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[22][sqm]" value="<?php if ($_POST['apartments']['22']['sqm']):  echo $_POST['apartments']['22']['sqm'];  else:  echo $this->_tpl_vars['apartments']['22']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[22][sqm_real]" value="<?php if ($_POST['apartments']['22']['sqm_real']):  echo $_POST['apartments']['22']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['22']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[22][sqm_light]" value="<?php if ($_POST['apartments']['22']['sqm_light']):  echo $_POST['apartments']['22']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['22']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[22][floor]" value="5">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[22][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['22']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['22']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 23</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[23][sqm]" value="<?php if ($_POST['apartments']['23']['sqm']):  echo $_POST['apartments']['23']['sqm'];  else:  echo $this->_tpl_vars['apartments']['23']['sqm']; ?>
 <?php endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[23][sqm_real]" value="<?php if ($_POST['apartments']['23']['sqm_real']):  echo $_POST['apartments']['23']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['23']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[23][sqm_light]" value="<?php if ($_POST['apartments']['23']['sqm_light']):  echo $_POST['apartments']['23']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['23']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[23][floor]" value="5">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[23][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['23']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['23']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 24</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[24][sqm]" value="<?php if ($_POST['apartments']['24']['sqm']):  echo $_POST['apartments']['24']['sqm'];  else:  echo $this->_tpl_vars['apartments']['24']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[24][sqm_real]" value="<?php if ($_POST['apartments']['24']['sqm_real']):  echo $_POST['apartments']['24']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['24']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[24][sqm_light]" value="<?php if ($_POST['apartments']['24']['sqm_light']):  echo $_POST['apartments']['24']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['24']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[24][floor]" value="5">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[24][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['24']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['24']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



			</div>



			<div id="etaj-6">



				<div class="row">



					<div class="two columns">



						<label>Апартамент 25</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[25][sqm]" value="<?php if ($_POST['apartments']['25']['sqm']):  echo $_POST['apartments']['25']['sqm'];  else:  echo $this->_tpl_vars['apartments']['25']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[25][sqm_real]" value="<?php if ($_POST['apartments']['25']['sqm_real']):  echo $_POST['apartments']['25']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['25']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[25][sqm_light]" value="<?php if ($_POST['apartments']['25']['sqm_light']):  echo $_POST['apartments']['25']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['25']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[25][floor]" value="6">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[25][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['25']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['25']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 26</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[26][sqm]" value="<?php if ($_POST['apartments']['26']['sqm']):  echo $_POST['apartments']['26']['sqm'];  else:  echo $this->_tpl_vars['apartments']['26']['sqm'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[26][sqm_real]" value="<?php if ($_POST['apartments']['26']['sqm_real']):  echo $_POST['apartments']['26']['sqm_real'];  else:  echo $this->_tpl_vars['apartments']['26']['sqm_real'];  endif; ?>">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[26][sqm_light]" value="<?php if ($_POST['apartments']['26']['sqm_light']):  echo $_POST['apartments']['26']['sqm_light'];  else:  echo $this->_tpl_vars['apartments']['26']['sqm_light'];  endif; ?>">



						<input type="hidden" name="apartments[26][floor]" value="6">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[26][status]">



							<option value="0">---изберете---</option>



							<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>



								<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($_POST['apartments']['26']['status'] == $this->_tpl_vars['label'] || $this->_tpl_vars['apartments']['26']['status'] == $this->_tpl_vars['label']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['label']; ?>




								</option>



							<?php endforeach; endif; unset($_from); ?>



						</select>



					</div>



				</div>



			</div>



			<div id="etaj-7">



				<div class="row">

					<?php unset($this->_sections['p']);
$this->_sections['p']['start'] = (int)1;
$this->_sections['p']['loop'] = is_array($_loop=32) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['p']['name'] = 'p';
$this->_sections['p']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['p']['show'] = true;
$this->_sections['p']['max'] = $this->_sections['p']['loop'];
if ($this->_sections['p']['start'] < 0)
    $this->_sections['p']['start'] = max($this->_sections['p']['step'] > 0 ? 0 : -1, $this->_sections['p']['loop'] + $this->_sections['p']['start']);
else
    $this->_sections['p']['start'] = min($this->_sections['p']['start'], $this->_sections['p']['step'] > 0 ? $this->_sections['p']['loop'] : $this->_sections['p']['loop']-1);
if ($this->_sections['p']['show']) {
    $this->_sections['p']['total'] = min(ceil(($this->_sections['p']['step'] > 0 ? $this->_sections['p']['loop'] - $this->_sections['p']['start'] : $this->_sections['p']['start']+1)/abs($this->_sections['p']['step'])), $this->_sections['p']['max']);
    if ($this->_sections['p']['total'] == 0)
        $this->_sections['p']['show'] = false;
} else
    $this->_sections['p']['total'] = 0;
if ($this->_sections['p']['show']):

            for ($this->_sections['p']['index'] = $this->_sections['p']['start'], $this->_sections['p']['iteration'] = 1;
                 $this->_sections['p']['iteration'] <= $this->_sections['p']['total'];
                 $this->_sections['p']['index'] += $this->_sections['p']['step'], $this->_sections['p']['iteration']++):
$this->_sections['p']['rownum'] = $this->_sections['p']['iteration'];
$this->_sections['p']['index_prev'] = $this->_sections['p']['index'] - $this->_sections['p']['step'];
$this->_sections['p']['index_next'] = $this->_sections['p']['index'] + $this->_sections['p']['step'];
$this->_sections['p']['first']      = ($this->_sections['p']['iteration'] == 1);
$this->_sections['p']['last']       = ($this->_sections['p']['iteration'] == $this->_sections['p']['total']);
?>

					<?php $this->assign('ident', $this->_sections['p']['iteration']); ?>

					<div class="six columns">

						<div class="two columns">

							<label><strong>място <?php echo $this->_tpl_vars['ident']; ?>
</strong></label>

						</div>

						<div class="five columns">

							<label>кв.м.</label>

							<input type="text" name="parking[<?php echo $this->_tpl_vars['ident']; ?>
][sqm]" value="<?php if ($this->_tpl_vars['parking'][$this->_tpl_vars['ident']]['sqm']):  echo $this->_tpl_vars['parking'][$this->_tpl_vars['ident']]['sqm'];  endif; ?>">

						</div>

						<div class="five columns">

							<label>статус</label>

							<select name="parking[<?php echo $this->_tpl_vars['ident']; ?>
][status]">

								<option value="0">---изберете---</option>

								<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>

									<option value="<?php echo $this->_tpl_vars['label']; ?>
" <?php if ($this->_tpl_vars['parking'][$this->_tpl_vars['ident']]['status'] == $this->_tpl_vars['label']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['label']; ?>


									</option>

								<?php endforeach; endif; unset($_from); ?>

							</select>

						</div>

					</div>

					<?php endfor; endif; ?>

				</div>



			</div>





		</div>



		<div class="row clear clearfix">

			<div class="two columns">

				<button type="button" class="button success" id="galleryAdd">Добави към галерия</button>

			</div>

			<div class="ten columns">
				<label for="text1" style="text-align:right"><strong>Главна Снимка</strong></label>
				<input type="file" style="float:right" name="main_image">
			</div>

		</div>

		<div class="row clear row-image" id="galleryHolder" <?php if ($this->_tpl_vars['galleryItems']): ?> style="display:block"<?php endif; ?>>

			<?php $_from = $this->_tpl_vars['galleryItems']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>

				<div class="twelve columns galleryRow" data-object="<?php echo $this->_tpl_vars['item']['id']; ?>
">

					<div class="one column galleryPreview"><a href="#<?php echo $this->_tpl_vars['item']['gallery_group'];  echo $this->_tpl_vars['item']['ordered']; ?>
" class="modal" rel="modal:open">view</a></div>

					<div class="three columns"><input type="file" name="image[]"></div>

					<div class="four columns"><input type="text" name="image_caption[]" value="<?php echo $this->_tpl_vars['item']['title']; ?>
"></div>

					<div class="one column"><input type="button" value="up" onclick="updateOrder(event)" class="button default small"></div>

					<div class="one column"><input type="button" value="down" onclick="updateOrder(event)" class="button default small"></div>

					<div class="one column"><input type="button" value="X" onclick="removeGalleryRow(event)" class="button alert small"></div>

					<div class="one column">

						<input type="button" data-action="<?php echo $this->_tpl_vars['actionUpdateGallery']; ?>
" onclick="callAjax(event);" value="Update" class="button default small">

					</div>

					<div id="<?php echo $this->_tpl_vars['item']['gallery_group'];  echo $this->_tpl_vars['item']['ordered']; ?>
" style="display:none;max-height:400px;">

    					<img src="../<?php echo $this->_tpl_vars['item']['picture_path']; ?>
" alt="image" style="width:378px;height:250px">

  					</div>

			    </div>

			<?php endforeach; endif; unset($_from); ?>

		</div>



		<div class="row clear">



			<div class="two columns">



				<input class="button expand postfix" type="submit" value="Запази" />



			</div>



		</div>



	</form>



</div>



<script type="text/javascript">

	<?php echo '



		$( function() {

			$(\'.modal\').modal();

	    	$( "#tabs" ).tabs();

	    	$(\'.galleryRow\').removeClass(\'lastChild\');

	    	$(\'.galleryRow:last\').addClass(\'lastChild\');

	    	$(\'.galleryRow\').removeClass(\'firstChild\');

	    	$(\'.galleryRow:first\').addClass(\'firstChild\');



	    	$(\'#galleryAdd\').click(function() {

	    		$(\'#galleryHolder\').css(\'display\', \'block\');

	    		$(\'.galleryRow\').removeClass(\'lastChild\');

	    		

	    		$(\'#galleryHolder\').append(

	    			\'<div class="twelve columns newItem galleryRow firstChild lastChild">\' +

	    				\'<div class="one column galleryPreview"></div>\' +

						\'<div class="three columns"><input type="file" name="new_image[]"></div>\' +

						\'<div class="four columns"><input type="text" name="new_image_caption[]"></div>\' +

						\'<div class="one column"><input type="button" value="up" onclick="updateOrder(event)" class="button default small"></div>\' +

						\'<div class="one column"><input type="button" value="down" onclick="updateOrder(event)" class="button default small"></div>\' +

						\'<div class="one column"><input type="button" value="X" onclick="removeGalleryRow(event)" class="button alert small"></div>\' +

						\'<div class="one column"></div>\' +

					\'</div>\'

	    		);

	    		$(\'.galleryRow\').removeClass(\'firstChild\');

	    		$(\'.galleryRow:first\').addClass(\'firstChild\');

	    	});



		});



		function callAjax(event) {

			event.preventDefault();

			var bttn = $(event.target);

			var fileInput = bttn.closest(\'div\').parent().find(\'.three input[type="file"]\');

			var textInput = bttn.closest(\'div\').parent().find(\'.four input[type="text"]\');

			var file_data = fileInput.prop(\'files\')[0];

			var text_data = textInput.val();

			var itemId = bttn.parent().closest(\'.galleryRow\').attr(\'data-object\');

			var action = \'ajax/galleryItemsAjax.php\';



			var formdata = new FormData();

			formdata.append(\'file\', file_data);

			formdata.append(\'text\', text_data);

			if (typeof itemId !== "undefined") {

				formdata.append(\'id\', itemId);

			}

			console.log(formdata.id);



		    $.ajax({

				url: action,

				data: formdata,

				cache: false,

			    contentType: false,

			    processData: false,

				type: \'POST\',

				success: function(response) {

				    location.reload(true);

				},

				error: function (error) {

                	alert(\'Възникна грешка при редакция на елемент от галерията.\');

              	}

			});

		}



		function removeGalleryRow(event) {

			event.preventDefault();

			var domElement = $(event.target);

			var itemId = domElement.parent().closest(\'.galleryRow\').attr(\'data-object\');

			var action = \'ajax/galleryDeleteAjax.php\';



			var formdata = new FormData();

			formdata.append(\'id\', itemId);



			if (typeof itemId !== "undefined") {

				

				$.ajax({

					url: action,

					data: formdata,

					cache: false,

				    contentType: false,

				    processData: false,

					type: \'POST\',

					success: function(response) {

					    location.reload(true);

					},

					error: function (error) {

	                	alert(\'Възникна грешка при изтриването на ресурса.\');

	              	}

				});

				

			} else {

				$(\'.galleryRow:first\').addClass(\'firstChild\');

				$(\'.galleryRow\').removeClass(\'lastChild\');

				if (domElement.parent().closest(\'.galleryRow\').hasClass(\'lastChild\')) {

					domElement.parent().closest(\'.galleryRow\').prev(\'.galleryRow\').addClass(\'lastChild\');

				}

				domElement.parent().closest(\'.galleryRow\').remove();

				$(\'.galleryRow:last\').addClass(\'lastChild\');

			}

		}



		function updateOrder(event) {

			event.preventDefault();

			var domElement = $(event.target);

			var direction = domElement.val();

			var countHolders = $(\'.galleryRow\').length;

			var newItems = $(\'.newItem\').length;

			var action = \'ajax/galleryOrderAjax.php\';



			if (countHolders == 1) {

				alert(\'Имате 1 елемент. Невъзможна е смяна на подредбата.\');

				return false;

			} else if (newItems > 0) {

				alert(\'Имате нови елементи в галерията. Запазете промени преди да промените подредбата.\');

				return false;

			} else if (domElement.parent().closest(\'.galleryRow\').hasClass(\'lastChild\') && direction == \'down\') {

				alert(\'Allowed direction : "up"\');

				return false;

			} else if (domElement.parent().closest(\'.galleryRow\').hasClass(\'firstChild\') && direction == \'up\') {

				alert(\'Allowed direction : "down"\');

				return false;

			}



			var itemId = domElement.parent().closest(\'.galleryRow\').attr(\'data-object\');



			if (direction == \'up\') {

				var changeItemId = domElement.parent().closest(\'.galleryRow\').prev(\'.galleryRow\').attr(\'data-object\');

			} else if (direction == \'down\') {

				var changeItemId = domElement.parent().closest(\'.galleryRow\').next(\'.galleryRow\').attr(\'data-object\');

			}



			var formdata = new FormData();

			formdata.append(\'mainID\', itemId);

			formdata.append(\'changeID\', changeItemId);



		    $.ajax({

				url: action,

				data: formdata,

				cache: false,

			    contentType: false,

			    processData: false,

				type: \'POST\',

				success: function(response) {

				    location.reload(true);

				},

				error: function (error) {

                	alert(\'Грешка при промяна на подредбата.\');

              	}

			});



			// if (direction == \'up\') {

			// 	domElement.parent().closest(\'.galleryRow\').insertBefore(domElement.parent().closest(\'.galleryRow\').prev(\'.galleryRow\'));

			// } else if (direction == \'down\') {

			// 	domElement.parent().closest(\'.galleryRow\').next(\'.galleryRow\').insertBefore(domElement.parent().closest(\'.galleryRow\'));

			// }



			// $(\'.galleryRow\').removeClass(\'lastChild firstChild\');

   			// $(\'.galleryRow:last\').addClass(\'lastChild\');

   			// $(\'.galleryRow:first\').addClass(\'firstChild\');



		}



		CKEDITOR.replace( \'text1\' , { });



		CKEDITOR.replace( \'text\' , { });



   	'; ?>




</script>