<?php /* Smarty version 2.6.13, created on 2014-09-10 12:47:03
         compiled from content_newsedit.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'math', 'content_newsedit.tpl', 25, false),array('modifier', 'escape', 'content_newsedit.tpl', 48, false),)), $this); ?>
<script language="javascript" type="text/javascript" src="js/editor/ckeditor.js"></script>
<a href="#" class="langs" data-lang="bg">bg</a>
<a href="#" class="langs" data-lang="en">en</a>
<div class="form">
	<h1 class="sifr"><?php echo $this->_tpl_vars['maintitle']; ?>
</h1>
	<?php if ($this->_tpl_vars['error']): ?><div style="color:red;"><?php echo $this->_tpl_vars['error']; ?>
</div><?php endif; ?>
	<form action="<?php echo $this->_tpl_vars['action']; ?>
" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="forlangs titles_bg four columns">
				<label for="title_bg">Заглавие БГ</label><input type="text" name="title_bg" lang="<?php echo $this->_tpl_vars['lang']; ?>
"style="width:300px;" value="<?php if ($_POST['title_bg']):  echo $_POST['title_bg'];  else:  echo $this->_tpl_vars['title_bg'];  endif; ?>">
			</div>
			<div class="forlangs titles_en four columns end">
				<label for="title_en">Заглавие EN</label><input type="text" name="title_en" lang="<?php echo $this->_tpl_vars['lang']; ?>
"style="width:300px;" value="<?php if ($_POST['title_en']):  echo $_POST['title_en'];  else:  echo $this->_tpl_vars['title_en'];  endif; ?>">
			</div>
		</div>
		<div>
			<label for="link">Link</label><input type="text" name="link" lang="<?php echo $this->_tpl_vars['lang']; ?>
"style="width:300px;" value="<?php if ($_POST['link']):  echo $_POST['link'];  else:  echo $this->_tpl_vars['link'];  endif; ?>">
		</div>
		<div class="row">
			<div class="two columns">
			 	<label for="cat_id">Категории:</label>
			 	<input type="hidden" name="old_cat_id" value="<?php echo $this->_tpl_vars['id1']; ?>
">
				<select id="cat_id" name="cat_id">
			    	<option value="0">-- без категогория --</option>
			    	<?php echo smarty_function_math(array('equation' => '1','assign' => 'level'), $this);?>
 
					<?php $_from = $this->_tpl_vars['cat']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['c']):
?>
					<?php if ($_GET['rec'] != $this->_tpl_vars['c']['id']): ?>
					<option style="font-weight:bold;" value="<?php echo $this->_tpl_vars['c']['id']; ?>
"<?php if ($_GET['catid'] == $this->_tpl_vars['c']['id'] || $this->_tpl_vars['id1'] == $this->_tpl_vars['c']['id']): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['c']['title_bg']; ?>
</option>
					<?php endif; ?>
					<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "subcats.tpl", 'smarty_include_vars' => array('cat' => $this->_tpl_vars['c']['children'],'level' => $this->_tpl_vars['level'],'fcat' => $_GET['catid'],'fcat1' => $this->_tpl_vars['id1'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
					<?php endforeach; endif; unset($_from); ?>
				</select>
			</div>
		</div>
		<div class="forlangs text_bg">
			<label for="text">Текст БГ</label>
			<textarea name="text_bg" id="text" ros="10" class="big"><?php if ($_POST['text_bg']):  echo $_POST['text_bg'];  else:  echo $this->_tpl_vars['text_bg'];  endif; ?></textarea>
		</div>
		<div class="forlangs text_en">
			<label for="text1">Текст EN</label>
			<textarea name="text_en" id="text1" ros="10" class="big"><?php if ($_POST['text_en']):  echo $_POST['text_en'];  else:  echo $this->_tpl_vars['text_en'];  endif; ?></textarea>
		</div>
		<?php if (count ( $this->_tpl_vars['image'] )): ?><h2 style="clear:both;margin-bottom:10px;">Снимка</h2><?php endif; ?>
		<?php unset($this->_sections['image']);
$this->_sections['image']['name'] = 'image';
$this->_sections['image']['loop'] = is_array($_loop=$this->_tpl_vars['image']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['image']['show'] = true;
$this->_sections['image']['max'] = $this->_sections['image']['loop'];
$this->_sections['image']['step'] = 1;
$this->_sections['image']['start'] = $this->_sections['image']['step'] > 0 ? 0 : $this->_sections['image']['loop']-1;
if ($this->_sections['image']['show']) {
    $this->_sections['image']['total'] = $this->_sections['image']['loop'];
    if ($this->_sections['image']['total'] == 0)
        $this->_sections['image']['show'] = false;
} else
    $this->_sections['image']['total'] = 0;
if ($this->_sections['image']['show']):

            for ($this->_sections['image']['index'] = $this->_sections['image']['start'], $this->_sections['image']['iteration'] = 1;
                 $this->_sections['image']['iteration'] <= $this->_sections['image']['total'];
                 $this->_sections['image']['index'] += $this->_sections['image']['step'], $this->_sections['image']['iteration']++):
$this->_sections['image']['rownum'] = $this->_sections['image']['iteration'];
$this->_sections['image']['index_prev'] = $this->_sections['image']['index'] - $this->_sections['image']['step'];
$this->_sections['image']['index_next'] = $this->_sections['image']['index'] + $this->_sections['image']['step'];
$this->_sections['image']['first']      = ($this->_sections['image']['iteration'] == 1);
$this->_sections['image']['last']       = ($this->_sections['image']['iteration'] == $this->_sections['image']['total']);
?>
		<div style="float:left;margin:5px;padding:0px;">
			<img src="../<?php echo $this->_tpl_vars['image'][$this->_sections['image']['index']]['path'];  echo $this->_tpl_vars['imgenl'];  echo $this->_tpl_vars['image'][$this->_sections['image']['index']]['ext']; ?>
?date=<?php echo time(); ?>
" alt="<?php echo $this->_tpl_vars['image'][$this->_sections['image']['index']]['name']; ?>
" style="border:none;" border="0" />
			<div style="margin:0px;padding:0px;height:auto;">
				<!--a style="float:left;" href="main.php?act=content&amp;act1=editpic&amp;id=<?php echo $_GET['rec']; ?>
&amp;lang=<?php echo $_GET['lang']; ?>
&amp;pic=<?php echo ((is_array($_tmp=$this->_tpl_vars['image'][$this->_sections['image']['index']]['path'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
&amp;func=<?php echo $_GET['act1']; ?>
" class="but edit" title="<?php echo $this->_tpl_vars['loc']['content']['11']; ?>
"></a-->
				<a style="float:left;" href="main.php?act=content&amp;act1=delpic&amp;id=<?php echo $this->_tpl_vars['image'][$this->_sections['image']['index']]['id']; ?>
" class="but delete" title="изтрий"></a>
			</div>
		</div>
		<?php endfor; endif; ?>
		<?php if (count ( $this->_tpl_vars['image'] ) < $this->_tpl_vars['loc']['limitpicture']): ?>
		<h2 style="clear:both;margin-bottom:10px;">Добави снимка</h2>
		<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['rec']; ?>
">
		<input type="file" name="image">
		<p><strong>Добавените снимки трябва да са във формат .jpg, .png и .gif.</strong></p>
		<?php endif; ?>
		<div class="row">
			<div class="two columns">
				<input class="button expand postfix" type="submit" value="Запази" />
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
	<?php echo '
	CKEDITOR.replace( \'text1\' , { });
	CKEDITOR.replace( \'text\' , { });
	'; ?>

</script>