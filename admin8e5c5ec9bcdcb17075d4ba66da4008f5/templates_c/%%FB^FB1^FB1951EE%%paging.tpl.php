<?php /* Smarty version 2.6.13, created on 2014-08-18 15:21:28
         compiled from paging.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'math', 'paging.tpl', 7, false),)), $this); ?>
<?php if ($this->_tpl_vars['pages'] > 1): ?>
<div class="pages">
	<?php if ($this->_tpl_vars['page'] > 0): ?><a href="<?php echo $this->_tpl_vars['url']; ?>
page=0"><?php echo $this->_tpl_vars['loc']['loc']['1']; ?>
</a><?php else: ?><span id="empty"><?php echo $this->_tpl_vars['loc']['loc']['1']; ?>
</span><?php endif; ?>
	<?php if ($_GET['page'] > 0): ?><a href="<?php echo $this->_tpl_vars['url']; ?>
page=<?php echo $_GET['page']-$this->_tpl_vars['perpage']; ?>
"><?php echo $this->_tpl_vars['loc']['loc']['3']; ?>
</a><?php else: ?><span id="empty"><?php echo $this->_tpl_vars['loc']['loc']['3']; ?>
</span><?php endif; ?>
	<ul>
	<?php unset($this->_sections['p']);
$this->_sections['p']['start'] = (int)$this->_tpl_vars['start'];
$this->_sections['p']['loop'] = is_array($_loop=$this->_tpl_vars['end']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['p']['name'] = 'p';
$this->_sections['p']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['p']['show'] = true;
$this->_sections['p']['max'] = $this->_sections['p']['loop'];
if ($this->_sections['p']['start'] < 0)
    $this->_sections['p']['start'] = max($this->_sections['p']['step'] > 0 ? 0 : -1, $this->_sections['p']['loop'] + $this->_sections['p']['start']);
else
    $this->_sections['p']['start'] = min($this->_sections['p']['start'], $this->_sections['p']['step'] > 0 ? $this->_sections['p']['loop'] : $this->_sections['p']['loop']-1);
if ($this->_sections['p']['show']) {
    $this->_sections['p']['total'] = min(ceil(($this->_sections['p']['step'] > 0 ? $this->_sections['p']['loop'] - $this->_sections['p']['start'] : $this->_sections['p']['start']+1)/abs($this->_sections['p']['step'])), $this->_sections['p']['max']);
    if ($this->_sections['p']['total'] == 0)
        $this->_sections['p']['show'] = false;
} else
    $this->_sections['p']['total'] = 0;
if ($this->_sections['p']['show']):

            for ($this->_sections['p']['index'] = $this->_sections['p']['start'], $this->_sections['p']['iteration'] = 1;
                 $this->_sections['p']['iteration'] <= $this->_sections['p']['total'];
                 $this->_sections['p']['index'] += $this->_sections['p']['step'], $this->_sections['p']['iteration']++):
$this->_sections['p']['rownum'] = $this->_sections['p']['iteration'];
$this->_sections['p']['index_prev'] = $this->_sections['p']['index'] - $this->_sections['p']['step'];
$this->_sections['p']['index_next'] = $this->_sections['p']['index'] + $this->_sections['p']['step'];
$this->_sections['p']['first']      = ($this->_sections['p']['iteration'] == 1);
$this->_sections['p']['last']       = ($this->_sections['p']['iteration'] == $this->_sections['p']['total']);
?>
		<li><a href="<?php echo $this->_tpl_vars['url']; ?>
page=<?php echo smarty_function_math(array('equation' => "p*(i+s-1)",'p' => $this->_tpl_vars['perpage'],'i' => $this->_sections['p']['iteration'],'s' => $this->_tpl_vars['start']), $this);?>
" <?php if ($this->_tpl_vars['page']+1 == ( $this->_sections['p']['iteration']+$this->_tpl_vars['start'] )): ?>class="on"<?php endif; ?>><?php echo $this->_sections['p']['iteration']+$this->_tpl_vars['start']; ?>
</a></li>
	<?php endfor; endif; ?>
	</ul>
	<?php if ($this->_tpl_vars['pages'] > $this->_tpl_vars['page']+1): ?><a href="<?php echo $this->_tpl_vars['url']; ?>
page=<?php echo $_GET['page']+$this->_tpl_vars['perpage']; ?>
"><?php echo $this->_tpl_vars['loc']['loc']['4']; ?>
</a><?php else: ?><span id="empty"><?php echo $this->_tpl_vars['loc']['loc']['4']; ?>
</span><?php endif; ?>
	<?php if ($_GET['page'] != $this->_tpl_vars['pages']*$this->_tpl_vars['perpage']-$this->_tpl_vars['perpage']): ?><a href="<?php echo $this->_tpl_vars['url']; ?>
page=<?php echo $this->_tpl_vars['pages']*$this->_tpl_vars['perpage']-$this->_tpl_vars['perpage']; ?>
"><?php echo $this->_tpl_vars['loc']['loc']['2']; ?>
</a><?php else: ?><span id="empty"><?php echo $this->_tpl_vars['loc']['loc']['2']; ?>
</span><?php endif; ?>
</div>
<?php endif; ?>

	