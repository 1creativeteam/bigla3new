<?php /* Smarty version 2.6.13, created on 2016-12-13 19:06:23
         compiled from categories_first.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'categories_first.tpl', 11, false),)), $this); ?>
<div class="form">
	<h1>
		<div style="float:right;font-size:12px;">Добави нова категория
			<a href="main.php?act=categories&amp;act1=create&amp;lang=<?php echo $this->_tpl_vars['lang']; ?>
" class="but new" title="Нова категоия"></a>
		</div>
		<?php echo $this->_tpl_vars['loc']['categories']['1']; ?>

	</h1>
	<?php if ($this->_tpl_vars['error']): ?><div style="color:red;"><?php echo $this->_tpl_vars['error']; ?>
</div><br><?php endif; ?>
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<?php $_from = $this->_tpl_vars['categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['c']):
?>
			<tr class="<?php echo smarty_function_cycle(array('values' => "odd,even"), $this);?>
">
				<td width="*"><?php echo $this->_tpl_vars['c']['name_bg']; ?>
</td>
				<td width="*"><?php echo $this->_tpl_vars['c']['name_en']; ?>
</td>

				<td width="1%"><a href="main.php?act=<?php echo $_GET['act']; ?>
&amp;act1=edit&amp;rec=<?php echo $this->_tpl_vars['c']['id']; ?>
&amp;lang=<?php echo $this->_tpl_vars['lang']; ?>
" class="but edit" title="редактирай"></a></td>
				<td width="1%"><a href="main.php?act=<?php echo $_GET['act']; ?>
&amp;act1=delete&amp;rec=<?php echo $this->_tpl_vars['c']['id']; ?>
&amp;lang=<?php echo $this->_tpl_vars['lang']; ?>
" class="but delete" title="изтрий"></a></td>
			</tr>
			<?php endforeach; else: ?>
			<tr>
				<td>Няма въведени</td>
			</tr>	
		<?php endif; unset($_from); ?>
	</table>
	<?php echo $this->_tpl_vars['paging']; ?>

</div>
