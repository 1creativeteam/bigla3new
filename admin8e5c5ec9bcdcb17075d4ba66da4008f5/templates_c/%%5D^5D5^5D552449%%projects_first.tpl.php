<?php /* Smarty version 2.6.13, created on 2014-08-18 15:24:05
         compiled from projects_first.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'math', 'projects_first.tpl', 22, false),array('function', 'cycle', 'projects_first.tpl', 24, false),)), $this); ?>
<div class="form">
	<h1>
		<div style="float:right;font-size:12px;"><a href="main.php?act=projects&amp;act1=newcateg&amp;lang=<?php echo $this->_tpl_vars['lang']; ?>
" class="but new" title="Нов"></a></div>
		<?php echo $this->_tpl_vars['loc']['content']['1']; ?>

	</h1>
	<?php if (count ( $this->_tpl_vars['subs'] ) > 0): ?>
	<div class="row">
	 	<div class="five columns" style="margin-bottom:30px;">
		 	<label for="">Избери категория в която да създадеш страница</label>
		 	<select id="cat_id" name="cat_id" onchange="window.location.href = this.value">
		 		<option value=""></option>
		    	<?php $_from = $this->_tpl_vars['subs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['c']):
?>
					<option style="font-weight:bold;" value="main.php?act=projects&amp;act1=newcateg&amp;lang=<?php echo $this->_tpl_vars['lang']; ?>
&amp;catid=<?php echo $this->_tpl_vars['c']['id']; ?>
"><?php echo $this->_tpl_vars['c']['title_bg']; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
			</select>
		</div>
	</div>
	<?php endif; ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "search_form.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<?php if (count ( $this->_tpl_vars['content'] ) > 0): ?><div style="float:right;font-size:12px;"><a href="javascript:new_window('sortcontent.php?act=projects&amp;act1=sortui&amp;id1=<?php echo $_GET['id1']; ?>
&amp;lang=<?php echo $this->_tpl_vars['lang']; ?>
','sort');" class="but sort" title="подреди по меню"></a></div><?php endif; ?>
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<?php echo smarty_function_math(array('equation' => '1','assign' => 'level'), $this);?>
 
	<?php $_from = $this->_tpl_vars['content']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['c']):
?>
		<tr class="<?php echo smarty_function_cycle(array('values' => "odd,even"), $this);?>
">
			<td width="*"><?php echo $this->_tpl_vars['c']['title_bg']; ?>
</td>
			<?php if ($_SESSION['client_level'] == 1): ?>
			<?php if ($this->_tpl_vars['c']['show'] == y): ?>
				<td width="1%"><a href="main.php?act=projects&amp;act1=hide&amp;rec=<?php echo $this->_tpl_vars['c']['id']; ?>
&amp;lang=<?php echo $this->_tpl_vars['lang']; ?>
" class="but hide" title="не се вижда"></a></td>
			<?php else: ?>
				<td width="1%"><a href="main.php?act=projects&amp;act1=show&amp;rec=<?php echo $this->_tpl_vars['c']['id']; ?>
&amp;lang=<?php echo $this->_tpl_vars['lang']; ?>
" class="but show" title="вижда се"></a></td>
			<?php endif; ?>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['c']['image'] == y): ?>
				<td width="1%"><img src="images/photo.png" alt="снимка" title="снимка" /></td>
			<?php else: ?>
				<td width="1%"><img src="images/no_photo.png" alt="без снимка" title="без снимка" /></td>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['c']['menu'] == 0): ?>
				<td width="1%"><a href="main.php?act=projects&amp;act1=showmenu&amp;rec=<?php echo $this->_tpl_vars['c']['id']; ?>
&amp;lang=<?php echo $this->_tpl_vars['lang']; ?>
" class="but show" title="добави за подредба"></a></td>
			<?php else: ?>
				<td width="1%"><a href="main.php?act=projects&amp;act1=hidemenu&amp;rec=<?php echo $this->_tpl_vars['c']['id']; ?>
&amp;lang=<?php echo $this->_tpl_vars['lang']; ?>
" class="but hide" title="махни от подредба"></a></td>
			<?php endif; ?>
			<td width="1%"><a href="main.php?act=projects&amp;act1=edit&amp;rec=<?php echo $this->_tpl_vars['c']['id']; ?>
&amp;lang=<?php echo $this->_tpl_vars['lang']; ?>
" class="but edit" title="редактирай"></a></td>
			<td width="1%"><a href="main.php?act=projects&amp;act1=delmessage&amp;rec=<?php echo $this->_tpl_vars['c']['id']; ?>
&amp;lang=<?php echo $this->_tpl_vars['lang']; ?>
" class="but delete" title="изтрий"></a></td>
		</tr>
		<?php if (count ( $this->_tpl_vars['c']['children'] ) > 0): ?>
		<?php if ($_GET['act'] == ""):  $this->assign('act', 'content');  else:  $this->assign('act', $_GET['act']);  endif; ?>
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "page_subcats.tpl", 'smarty_include_vars' => array('act' => $this->_tpl_vars['act'],'content' => $this->_tpl_vars['c']['children'],'level' => $this->_tpl_vars['level'],'fcat' => $this->_tpl_vars['fcat'],'home' => 'n')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<?php endif; ?>
	<?php endforeach; else: ?>
		<tr>
			<td>Няма въведени</td>
		</tr>	
	<?php endif; unset($_from); ?>
	</table>
	<?php echo $this->_tpl_vars['paging']; ?>

</div>