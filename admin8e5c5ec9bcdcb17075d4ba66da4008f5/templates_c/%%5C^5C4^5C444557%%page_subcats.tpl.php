<?php /* Smarty version 2.6.13, created on 2014-08-18 15:17:32
         compiled from page_subcats.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'math', 'page_subcats.tpl', 1, false),array('function', 'cycle', 'page_subcats.tpl', 4, false),)), $this); ?>
<?php echo smarty_function_math(array('equation' => "x+1",'x' => $this->_tpl_vars['level'],'assign' => 'level'), $this);?>

<?php if ($this->_tpl_vars['act1'] != 'do'):  $this->assign('act1', 'act1');  endif; ?>
<?php $_from = $this->_tpl_vars['content']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['c']):
?>
	<tr class="<?php echo smarty_function_cycle(array('values' => "odd,even"), $this);?>
">
		<td width="*"><?php if ($this->_tpl_vars['level'] == 3): ?> &mdash; &mdash; <?php elseif ($this->_tpl_vars['level'] == 4): ?> &mdash; &mdash; &mdash;  <?php elseif ($this->_tpl_vars['level'] == 5): ?> &mdash; &mdash; &mdash; &mdash; <?php else: ?> &mdash; <?php endif;  echo $this->_tpl_vars['c']['title_bg']; ?>
</td>
		<?php if ($_SESSION['client_level'] == 1): ?>
			<?php if ($this->_tpl_vars['c']['show'] == y): ?>
				<td width="1%"><a href="main.php?act=<?php echo $this->_tpl_vars['act']; ?>
&amp;<?php echo $this->_tpl_vars['act1']; ?>
=hide&amp;rec=<?php echo $this->_tpl_vars['c']['id']; ?>
&amp;lang=<?php echo $this->_tpl_vars['lang']; ?>
" class="but hide" title="не се вижда"></a></td>
			<?php else: ?>
				<td width="1%"><a href="main.php?act=<?php echo $this->_tpl_vars['act']; ?>
&amp;<?php echo $this->_tpl_vars['act1']; ?>
=show&amp;rec=<?php echo $this->_tpl_vars['c']['id']; ?>
&amp;lang=<?php echo $this->_tpl_vars['lang']; ?>
" class="but show" title="вижда се"></a></td>
			<?php endif; ?>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['c']['image'] != ""): ?>
		<?php if ($this->_tpl_vars['c']['image'] == y): ?>
			<td width="1%"><img src="images/photo.png" alt="снимка" title="снимка" /></td>
		<?php else: ?>
			<td width="1%"><img src="images/no_photo.png" alt="без снимка" title="без снимка" /></td>
		<?php endif; ?>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['home'] == 'y'): ?>
		<?php if ($this->_tpl_vars['c']['home'] == 0): ?>
			<td width="1%"><a href="main.php?act=<?php echo $this->_tpl_vars['act']; ?>
&amp;<?php echo $this->_tpl_vars['act1']; ?>
=showhome&amp;rec=<?php echo $this->_tpl_vars['c']['id']; ?>
&amp;lang=<?php echo $this->_tpl_vars['lang']; ?>
" class="but nohome" title="покажи на първа страница"></a></td>
		<?php else: ?>
			<td width="1%"><a href="main.php?act=<?php echo $this->_tpl_vars['act']; ?>
&amp;<?php echo $this->_tpl_vars['act1']; ?>
=hidehome&amp;rec=<?php echo $this->_tpl_vars['c']['id']; ?>
&amp;lang=<?php echo $this->_tpl_vars['lang']; ?>
" class="but home" title="махни от първа страница"></a></td>
		<?php endif; ?>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['c']['menu'] == 0): ?>
			<td width="1%"><a href="main.php?act=<?php echo $this->_tpl_vars['act']; ?>
&amp;<?php echo $this->_tpl_vars['act1']; ?>
=showmenu&amp;rec=<?php echo $this->_tpl_vars['c']['id']; ?>
&amp;lang=<?php echo $this->_tpl_vars['lang']; ?>
" class="but show" title="добави в менюто"></a></td>
		<?php else: ?>
			<td width="1%"><a href="main.php?act=<?php echo $this->_tpl_vars['act']; ?>
&amp;<?php echo $this->_tpl_vars['act1']; ?>
=hidemenu&amp;rec=<?php echo $this->_tpl_vars['c']['id']; ?>
&amp;lang=<?php echo $this->_tpl_vars['lang']; ?>
" class="but hide" title="махни от менюто"></a></td>
		<?php endif; ?>
		<td width="1%"><a href="main.php?act=<?php echo $this->_tpl_vars['act']; ?>
&amp;<?php echo $this->_tpl_vars['act1']; ?>
=edit&amp;rec=<?php echo $this->_tpl_vars['c']['id']; ?>
&amp;lang=<?php echo $this->_tpl_vars['lang']; ?>
" class="but edit" title="редактирай"></a></td>
		<td width="1%"><a href="main.php?act=<?php echo $this->_tpl_vars['act']; ?>
&amp;<?php echo $this->_tpl_vars['act1']; ?>
=delmessage&amp;rec=<?php echo $this->_tpl_vars['c']['id']; ?>
&amp;lang=<?php echo $this->_tpl_vars['lang']; ?>
" class="but delete" title="изтрий"></a></td>
	</tr>
	<?php if (count ( $this->_tpl_vars['c']['children'] ) > 0): ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "page_subcats.tpl", 'smarty_include_vars' => array('act' => $this->_tpl_vars['act'],'act1' => $this->_tpl_vars['act1'],'content' => $this->_tpl_vars['c']['children'],'level' => $this->_tpl_vars['level'],'fcat' => $this->_tpl_vars['fcat'],'home' => $this->_tpl_vars['home'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<?php endif; ?>
<?php endforeach; endif; unset($_from); ?>