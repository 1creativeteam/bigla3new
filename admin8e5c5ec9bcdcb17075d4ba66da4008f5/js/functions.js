var d = document;

function new_window(url, win_name){
	win_name=window.open(url,win_name,'toolbar=no,scrollbars=yes,resizable=yes,width=600,height=480,status=yes,left=100,top=100,screenX=100,screenY=100')
	win_name.focus()
}

function pic_win(url,wintitle){
	picwin=window.open('image.php?pic='+url+'&name='+wintitle,'picwin','toolbar=no,scrollbars=no,width=620,height=485,left=0,top=0,screenX=0,screenY=0,status=no');
	picwin.focus();
}

function print_window(url){
	printwin=window.open(url,'printwin','toolbar=no,scrollbars=yes,width=600,height=500')
	printwin.focus()
}

function addPLink(id,name){
	var sel = document.getElementById('plinks');
	var opt = new Option(name, id, true);
	sel.options[sel.options.length] = opt;
}

function delPLink(){
	var sel = document.getElementById('plinks');
	sel.options[sel.selectedIndex] = null;
}

function selectPLinks(){
	var sel = document.getElementById('plinks');
	for (i=0;i<sel.options.length;i++){
		sel.options[i].selected = true;
	}
}

function addGLink(id,name){
	var sel = document.getElementById('glinks');
	var opt = new Option(name, id, true);
	sel.options[sel.options.length] = opt;
}

function delGLink(){
	var sel = document.getElementById('glinks');
	sel.options[sel.selectedIndex] = null;
}

function selectGLinks(){
	var sel = document.getElementById('glinks');
	for (i=0;i<sel.options.length;i++){
		sel.options[i].selected = true;
	}
}

function addVLink(id,name){
	var sel = document.getElementById('vlinks');
	var opt = new Option(name, id, true);
	sel.options[sel.options.length] = opt;
}

function delVLink(){
	var sel = document.getElementById('vlinks');
	sel.options[sel.selectedIndex] = null;
}

function selectVLinks(){
	var sel = document.getElementById('vlinks');
	for (i=0;i<sel.options.length;i++){
		sel.options[i].selected = true;
	}
}