/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config
	config.language = 'en';

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'about' }
	];

	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	config.removeButtons = 'About';

	// Se the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre;span;div';

	// Make dialogs simpler.
	config.removeDialogTabs = 'image:advanced;link:advanced';
	
	config.filebrowserBrowseUrl = CKEDITOR.basePath + '../kcfinder/browse.php?type=files';
	config.filebrowserImageBrowseUrl = CKEDITOR.basePath + '../kcfinder/browse.php?type=images';
	config.filebrowserFlashBrowseUrl = CKEDITOR.basePath + '../kcfinder/browse.php?type=flash';
	config.filebrowserUploadUrl = CKEDITOR.basePath + '../kcfinder/upload.php?type=files';
	config.filebrowserImageUploadUrl = CKEDITOR.basePath + '../kcfinder/upload.php?type=images';
	config.filebrowserFlashUploadUrl = CKEDITOR.basePath + '../kcfinder/upload.php?type=flash';

	// remove prevent first p tags
    config.enterMode = 2;

    config.allowedContent = true;
    
    // allow empty span
	config.protectedSource.push( /<span[\s\S]*?\>/g ); //allows beginning tag
	config.protectedSource.push( /<\/span[\s\S]*?\>/g ); //allows ending </i> tag

	config.format_span = {name: 'SPAN', element : 'span', attributes : { 'class' : '' } };
	config.format_div = {name: 'DIV', element : 'div', attributes : { 'class' : '' } };

	config.stylesSet = 'my_styles';


};

CKEDITOR.stylesSet.add( 'my_styles', [
	   
    // Inline styles.
    { name: 'city-c', element: 'span', attributes: { 'class': 'city-c' } },

    {
        name: 'img-left',
        element: 'img',
        attributes: { 'class': 'img-left' }
	},

	{
        name: 'img clear',
        element: 'img',
        attributes: { 'class': '' }
	},

]);
