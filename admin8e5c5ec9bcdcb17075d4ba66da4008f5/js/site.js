﻿$(function() {

    $('.langs[data-lang="bg"]').css('color','red');
    $('.titles_en, .text_en, .free_text_en, .floors_en').hide();

    $('.langs').click(function() {
        $('.langs').css('color','');
        var lang = $(this).data('lang');
        $('.forlangs').hide();
        $(this).css('color','red');
        $('.titles_' + lang +', .text_' + lang +', .floors_'+ lang +', .free_text_' + lang +'').show();
        return false;
    });

});