﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/
CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	config.language = 'en';
	config.enterMode = CKEDITOR.ENTER_P;
	config.disableNativeSpellChecker = false;
	config.forcePasteAsPlainText = true;
	config.format_tags = 'p;h1;h2;h3;h4';
	
	config.filebrowserBrowseUrl = CKEDITOR.basePath + 'filemanager/browser/default/browser.html?Connector=' + encodeURIComponent(CKEDITOR.basePath + 'filemanager/connectors/php/connector.php'),
	config.filebrowserImageBrowseUrl = CKEDITOR.basePath +'filemanager/browser/default/browser.html?Type=Image&Connector=' + encodeURIComponent(CKEDITOR.basePath + 'filemanager/connectors/php/connector.php'),
    config.filebrowserFlashBrowseUrl = CKEDITOR.basePath +'filemanager/browser/default/browser.html?Type=Flash&Connector=' + encodeURIComponent(CKEDITOR.basePath + 'filemanager/connectors/php/connector.php'),
	config.filebrowserUploadUrl  = CKEDITOR.basePath + 'filemanager/connectors/php/upload.php?Type=File',
	config.filebrowserImageUploadUrl = CKEDITOR.basePath + 'filemanager/connectors/php/upload.php?Type=Image',
	config.filebrowserFlashUploadUrl = CKEDITOR.basePath +'filemanager/connectors/php/upload.php?Type=Flash'
	
	config.toolbar = 'Editor';
	config.toolbar_Editor =
	[
		['Source'],
		['Cut','Copy','Paste','PasteText','PasteFromWord'],
		['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
		['Link','Unlink','Anchor'],
		'/',
		['Bold','Italic','Underline','Strike','-','Subscript','Superscript','-','TextColor'],
		['NumberedList','BulletedList','-','Outdent','Indent'],
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		['Image', 'Table','SpecialChar', 'Format']
	];
	
	config.toolbar_Basic =
	[
		['Source'],
		['Cut','Copy','Paste','PasteText'],
		['Link','Unlink'],
		['Bold','Italic','Underline'],
		['NumberedList','BulletedList','-','Outdent','Indent'],
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock']
	];
	// remove prevent first p tags
    config.enterMode = 2
    // allow empty span
	config.protectedSource.push( /<span[\s\S]*?\>/g ); //allows beginning <i> tag
	config.protectedSource.push( /<\/span[\s\S]*?\>/g ); //allows ending </i> tag

};