<script language="javascript" type="text/javascript" src="js/editor/ckeditor.js"></script>

<div class="form">
	<h1 class="sifr">Home</h1>
	{if $error}<div style="color:red;">{$error}</div>{/if}
	<form action="{$action}" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="four columns">
				<input type="text" name="title_bg" lang="{$lang}" style="width:300px;" value="{if $smarty.post.title_bg}{$smarty.post.title_bg}{else}{$title_bg}{/if}" placeholder="Заглавие BG">
			</div>
			<div class="four columns end">
				<input type="text" name="title_en" lang="{$lang}" style="width:300px;" value="{if $smarty.post.title_en}{$smarty.post.title_en}{else}{$title_en}{/if}" placeholder="Заглавие EN">
			</div>
		</div>
		<div class="row">
			<div class="four columns">
				<input class="link" name="link_bg" type="text" value="{if $smarty.post.link_bg}{$smarty.post.link_bg}{else}{$link_bg}{/if}" style="width:300px;" placeholder="Линк БГ" />
			</div>
			<div class="four columns end">
				<input class="link" name="link_en" type="text" value="{if $smarty.post.link_en}{$smarty.post.link_en}{else}{$link_en}{/if}" style="width:300px;" placeholder="Линк EN" />
			</div>
		</div>
		<div class="row">
			<div class="six columns">
				<input name="video" type="text" value="{if $smarty.post.video}{$smarty.post.video}{else}{$video_url}{/if}" placeholder="Качи видео от youtube или vimeo" />
			</div>
			<div class="end">
				<a href="{$video_path}" target="_blank">{$video_path}</a>
			</div>
		</div>
     	{if count($image)}<h2 style="clear:both;margin-bottom:10px;">Снимка</h2>{/if}
		{section name=image loop=$image}
		<div style="float:left;margin:5px;padding:0px;">
			<img src="../{$image[image].path}{$imgenl}{$image[image].ext}?date={$smarty.now}" alt="{$image[image].name}" style="border:none;" border="0" />
			<div style="margin:0px;padding:0px;height:auto;">
				<!--a style="float:left;" href="main.php?act=news&amp;act1=editpic&amp;id={$smarty.get.rec}&amp;lang={$smarty.get.lang}&amp;pic={$image[image].path|escape:'url'}&amp;func={$smarty.get.act1}" class="but edit" title="{$loc.content.11}"></a-->
				<a style="float:left;" href="main.php?act=home&amp;act1=delpic&amp;id={$image[image].id}" class="but delete" title="изтрий"></a>
			</div>
		</div>
		{/section}
		{if count($image)==0}
		<h2 style="clear:both;margin-bottom:10px;">Добави снимка</h2>
		<input type="file" name="image" />
		<input type="hidden" name="id" values="{$id}">
		<!--input type="text" name="title" values="" />
		<input type="text" name="text" values="" /-->
		{/if}
		<p style="clear: both;"><strong>Добавените снимки трябва да са във формат .jpg, .png и .gif. <br />Ако желаете да качвате снимка трябва полето за видео да е празно!</strong></p>
		<div class="row" style="clear: both; margin-top:10px;">
			<div class="two columns">
				<input class="button expand postfix" type="submit" value="Запази" />
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">{literal}CKEDITOR.replace( 'text1' , { });{/literal}</script>
