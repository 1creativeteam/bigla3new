<html>
<head>
	<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset={$charset}">
	<title>Image Browser</title>
	<link rel="stylesheet" type="text/css" href="site.css">
	<script src="js/functions.js"></script>
	<script src="../js/ajax.js"></script>
	<script src="../js/debug.js"></script>
	<script language="javascript" type="text/javascript" src="js/tiny_mce/tiny_mce_popup.js"></script>
	<script type="text/javascript">
	{literal}
		function mySubmit(imageURL) {
		  //call this function only after page has loaded
		  //otherwise tinyMCEPopup.close will close the
		  //"Insert/Edit Image" or "Insert/Edit Link" window instead
		
		  var URL = imageURL;
		  var win = tinyMCEPopup.getWindowArg("window");
		
		  // insert information now
		  win.document.getElementById(tinyMCEPopup.getWindowArg("input")).value = URL;
		
		  // for image browsers: update image dimensions
		  if (win.getImageData) win.getImageData();
		
		  // close popup window
		  tinyMCEPopup.close();
		}
	{/literal}
	</script>
</head>
<body>
	<div style="height:390px;overflow:auto;">
	{section name=i loop=$images}
	<div style="width:{$images[i].size0}px;height:auto;margin:5px;">
		<img src="{$images[i].image}" alt="" /><br />
		<a href="javascript:mySubmit('{$images[i].urlth}');">{$images[i].sizeth}</a> |
		<a href="javascript:mySubmit('{$images[i].urlfl}');">{$images[i].sizefl}</a>
	</div>
	{/section}
	</div>
</body>
</html>