<script language="javascript" type="text/javascript" src="js/editor/ckeditor.js"></script>

<a href="#" class="langs" data-lang="bg">bg</a>

<a href="#" class="langs" data-lang="en">en</a>

<div class="form">

	<h1 class="sifr">{$maintitle}</h1>

	{if $error}<div style="color:red;">{$error}</div>{/if}

	<form action="{$action}" method="post" enctype="multipart/form-data">

		<div class="row">

			<div class="forlangs titles_bg four columns">

				<label for="title_bg">Заглавие БГ</label><input type="text" name="title_bg" lang="{$lang}"style="width:300px;" value="{if $smarty.post.title_bg}{$smarty.post.title_bg|escape:'html'}{else}{$title_bg|escape:'html'}{/if}">

			</div>

			<div class="forlangs titles_en four columns end">

				<label for="title_en">Заглавие EN</label><input type="text" name="title_en" lang="{$lang}"style="width:300px;" value="{if $smarty.post.title_en}{$smarty.post.title_en|escape:'html'}{else}{$title_en|escape:'html'}{/if}">

			</div>

		</div>

		<div class="forlangs text_bg">

			<label for="text">Текст БГ</label>

			<textarea name="text_bg" id="text" ros="10" class="big">{if $smarty.post.text_bg}{$smarty.post.text_bg|escape:'html'}{else}{$text_bg|escape:'html'}{/if}</textarea>

		</div>

		<div class="forlangs text_en">

			<label for="text1">Текст EN</label>

			<textarea name="text_en" id="text1" ros="10" class="big">{if $smarty.post.text_en}{$smarty.post.text_en|escape:'html'}{else}{$text_en|escape:'html'}{/if}</textarea>

		</div>

		{if count($image)}<h2 style="clear:both;margin-bottom:10px;">Снимка</h2>{/if}

		{section name=image loop=$image}

		<div style="float:left;margin:5px;padding:0px;">

			<img src="../{$image[image].path}{$imgenl}{$image[image].ext}?date={$smarty.now}" alt="{$image[image].name}" style="border:none;" border="0" />

			<div style="margin:0px;padding:0px;height:auto;">

				<!--a style="float:left;" href="main.php?act=news&amp;act1=editpic&amp;id={$smarty.get.rec}&amp;lang={$smarty.get.lang}&amp;pic={$image[image].path|escape:'url'}&amp;func={$smarty.get.act1}" class="but edit" title="{$loc.content.11}"></a-->

				<a style="float:left;" href="main.php?act=news&amp;act1=delpic&amp;id={$image[image].id}" class="but delete" title="изтрий"></a>

			</div>

		</div>

		{/section}

		{if count($image)==0}

		<div class="seven columns">

			<h2 style="clear:both;margin-bottom:10px;">Добави снимка</h2>

			<input type="hidden" name="id" value="{$id}" />

			<input type="file" name="image" />

			<!--input type="text" name="title" values="" />

			<input type="text" name="text" values="" /-->

			<p><strong>Добавените снимки трябва да са във формат .jpg, .png и .gif.</strong></p>

		</div>

		{/if}

		<br>

		<div class="four columns">

			<label>Категория</label>

			<select name="categories_id">

				<option value="0">---изберете---</option>

				{foreach from=$categories item=label key=key}

					{assign var=field value=name_$lang}

					<option value="{$label.id}" {if $smarty.post.categories_id==$label.id || $label.id==$categories_id} selected="selected"{/if}>{$label.$field}

					</option>

				{/foreach}

			</select>

		</div>

		<div class="six columns" style="clear:both;margin-bottom:10px;">
			<h2 style="clear:both;margin-bottom:10px;">Добави Галерия</h2>
			<input type="file" name="gallery[]" multiple="true" />
		</div>

		<div class="twelve columns">
			{foreach from=$gallery item=picture}
				<div style="float:left;margin:5px;padding:0px;">
					<img src="../{$picture.picture}" alt="" style="border:none;" border="0" width="200" height="136" />
					<div style="margin:0px;padding:0px;height:auto;">
						<a style="float:left;" href="main.php?act=news&amp;act1=delgallerypic&amp;id={$picture.id}" class="but delete" title="изтрий"></a>
					</div>
				</div>
			{foreachelse}
				<p>Няма въведени</p>
			{/foreach}
		</div>

		<div class="row clear">

			<div class="two columns">

				<input class="button expand postfix" type="submit" style="margin-left:10px" value="Запази" />

			</div>

		</div>

	</form>

</div>

<script type="text/javascript">

	{literal}

	CKEDITOR.replace( 'text1' , { });

	CKEDITOR.replace( 'text' , { });

	{/literal}

</script>

