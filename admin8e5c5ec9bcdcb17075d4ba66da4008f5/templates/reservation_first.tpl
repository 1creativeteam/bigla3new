<div class="form">
	<h1>{$loc.reservation.1}</h1>
	{include file="search_form.tpl"}
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr class="{cycle values="odd,even"}">
			<th>корт/час</th>
			<th width="*">регистрация на</th>
			<th>тел</th>
			<th>email</th>
			<th>&nbsp;</th>
		</tr>
	{foreach from=$events item=c}
		<tr class="{cycle values="odd,even"}">
			<td>{$c.start|date_format:'%d-%m-%Y %H:%M'} {if $c.court_type==1}корт {$c.playground}{elseif $c.court_type==2}скуош{/if}</td>
			<td>{$c.first_name} {$c.last_name}</td>
			<td>{$c.phone}</td>
			<td><a href="mailto:{$c.email}">{$c.email}</a></td>
			<td width="1%"><a href="main.php?act=reservation&amp;act1=delmessage&amp;rec={$c.id}" class="but delete" title="изтрий"></a></td>
		</tr>
	{foreachelse}
	<tr>
		<td>Няма въведени</td>
	</tr>	
	{/foreach}
	</table>
	{$paging}
</div>
