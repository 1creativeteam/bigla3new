<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Resize image</title>

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/interface.js"></script>

	<link rel="stylesheet" type="text/css" href="site.css">
	<style type="text/css" media="all">
	{literal}
	body
	{
		margin: 0;
		padding: 50;
		/*height: 100%;*/
	}
	#resizeMe
	{
		position: absolute;
		width: 200px;
		height: 300px;
		left: 50px;
		top: 50px;
		cursor: move;
		border: 1px dashed #eee;
		background-repeat: no-repeat;
	}
	#resizeSE,
	#resizeE,
	#resizeNE,
	#resizeN,
	#resizeNW,
	#resizeW,
	#resizeSW,
	#resizeS
	{
		position: absolute;
		width: 8px;
		height: 8px;
		background-color: #333;
		border: 1px solid #fff;
		overflow: hidden;
	}
	#resizeSE{
		bottom: -10px;
		right: -10px;
		cursor: se-resize;
	}
	#resizeNE
	{
		top: -10px;
		right: -10px;
		cursor: ne-resize;
	}
	#resizeNW{
		top: -10px;
		left: -10px;
		cursor: nw-resize;
	}
	#resizeSW
	{
		left: -10px;
		bottom: -10px;
		cursor: sw-resize;
	}
	#container
	{
		position: relative;
		background-color: #ccc;
	    filter:alpha(opacity=75); 
	    -moz-opacity:0.75;
	    opacity:0.75; 
	}
	{/literal}
	</style>
	<script type="text/javascript">
	var x={$size.x};
	var y={$size.y};
	var th_x={$size.th_x};
	var th_y={$size.th_y};
	{literal}
	$(document).ready(
		function()
		{
			$('#resizeMe').Resizable(
				{
					//minWidth: th_x,
					//minHeight: th_y,
					minTop: 50,
					minLeft: 50,
					maxWidth: x,
					maxHeight: y,
					dragHandle: true,
					ratio: th_y/th_x,
					onDrag: function(px, py)
					{
						this.style.backgroundPosition = '-' + (px-50) + 'px -' + (py-50) + 'px';

						if(px>x-$(this).css("width")+50){
							$(this).css("left:"+(x-$(this).css("width")+50));
							px=x-$(this).css("width")+50;
						}
						if(py>y-$(this).css("height")+50){
							$(this).css("top:"+(y-$(this).css("height")+50));
							py=y-$(this).css("height")+50;
						}

						$('#sx').val(px-50);
						$('#sy').val(py-50);
					},
					handlers: {
						se: '#resizeSE',
						ne: '#resizeNE',
						nw: '#resizeNW',
						sw: '#resizeSW'
					},
					onResize : function(size, position) {
						this.style.backgroundPosition = '-' + (position.left-50) + 'px -' + (position.top-50) + 'px';
						
						if(position.left<50){
							this.style.left='50px';
						}
						if(position.top<50){
							this.style.top='50px';
						}

						if(size.width>x-position.left+50){
							this.style.width=(x-position.left+50)+'px';
						}
						if(size.height>y-position.top+50){
							this.style.height=(y-position.top+50)+'px';
						}

						$('#sx').val(position.left-50);
						$('#sy').val(position.top-50);
						$('#wx').val(size.width);
						$('#wy').val(size.height);
					}
				}
			)
		}
	);
	{/literal}
	</script>
</head>
<body>
	<div id="container" style="width:{$size.x}px;height:{$size.y}px;">
		<img src="{$image}" alt="" width="{$size.x}" height="{$size.y}" />
	</div>
	<div style="width:{$size.th_x}px; height:{$size.th_y}px;background-image:URL({$image});" id="resizeMe">
		<div id="resizeSE"></div>
		<div id="resizeNE"></div>
		<div id="resizeNW"></div>
		<div id="resizeSW"></div>
	</div>
	<div id="form">
	<p style="margin-bottom:10px;font-weight:bold;">С местене на черните квадратчета оразмерявате картинка с размер {$size.th_x}px X {$size.th_y}px</p>
	<form action="{$smarty.session.action}" method="post">
		<input type="hidden" id="sx" name="sx" value="0" />
		<input type="hidden" id="sy" name="sy" value="0" />
		<input type="hidden" id="wx" name="wx" value="{$size.th_x}" />
		<input type="hidden" id="wy" name="wy" value="{$size.th_y}" />
		<button type="submit">Запази</button>
	</form>
	</div>
</body>
</html>