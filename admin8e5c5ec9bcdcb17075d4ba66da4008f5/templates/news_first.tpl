<div class="form">
	<h1>
		<div style="float:right;font-size:12px;"><a href="main.php?act={$smarty.get.act}&amp;act1=newcateg&amp;lang={$lang}" class="but new" title="Нов"></a></div>
		{if $smarty.get.act=='careers'}{$loc.careers.1}{else}{$loc.news.1}{/if}
	</h1>
	{include file="search_form.tpl"}
	{if count($news)>0}<div style="float:right;font-size:12px;"><a href="javascript:new_window('sortcontent.php?act={$smarty.get.act}&amp;act1=sortui&amp;id1={$smarty.get.id1}&amp;lang={$lang}','sort');" class="but sort" title="подреди по меню"></a></div>{/if}
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
	{foreach from=$news item=c}
		<tr class="{cycle values="odd,even"}">
			<td width="*">{$c.title_bg}</td>
			{if $smarty.session.client_level==1}
				{if $c.show==y}
					<td width="1%"><a href="main.php?act={$smarty.get.act}&amp;act1=hide&amp;rec={$c.id}&amp;lang={$lang}" class="but hide" title="не се вижда"></a></td>
				{else}
					<td width="1%"><a href="main.php?act={$smarty.get.act}&amp;act1=show&amp;rec={$c.id}&amp;lang={$lang}" class="but show" title="вижда се"></a></td>
				{/if}
			{/if}
			{if	$c.image==y}
				<td width="1%"><img src="images/photo.png" alt="снимка" title="снимка" /></td>
			{else}
				<td width="1%"><img src="images/no_photo.png" alt="без снимка" title="без снимка" /></td>
			{/if}
			{if $c.menu==0}
				<td width="1%"><a href="main.php?act={$smarty.get.act}&amp;act1=showmenu&amp;rec={$c.id}&amp;lang={$lang}" class="but show" title="добави за подредба"></a></td>
			{else}
				<td width="1%"><a href="main.php?act={$smarty.get.act}&amp;act1=hidemenu&amp;rec={$c.id}&amp;lang={$lang}" class="but hide" title="махни от подредба"></a></td>
			{/if}
			<td width="1%"><a href="main.php?act={$smarty.get.act}&amp;act1=edit&amp;rec={$c.id}&amp;lang={$lang}" class="but edit" title="редактирай"></a></td>
			<td width="1%"><a href="main.php?act={$smarty.get.act}&amp;act1=delmessage&amp;rec={$c.id}&amp;lang={$lang}" class="but delete" title="изтрий"></a></td>
		</tr>
	{foreachelse}
	<tr>
		<td>Няма въведени</td>
	</tr>	
	{/foreach}
	</table>
	{$paging}
</div>
