<div class="form">
	<h1>
		<div style="float:right;font-size:12px;"><a href="main.php?act=content&amp;act1=newcateg&amp;lang={$lang}" class="but new" title="Нов"></a></div>
		{$loc.content.1}
	</h1>
	{if count($subs)>0}
	<div class="row">
	 	<div class="five columns" style="margin-bottom:30px;">
		 	<label for="">Избери категория в която да създадеш страница</label>
		 	<select id="cat_id" name="cat_id" onchange="window.location.href = this.value">
		 		<option value=""></option>
		    	{foreach from=$subs item=c}
					<option style="font-weight:bold;" value="main.php?act=content&amp;act1=newcateg&amp;lang={$lang}&amp;catid={$c.id}">{$c.title_bg}</option>
				{/foreach}
			</select>
		</div>
	</div>
	{/if}
	{include file="search_form.tpl"}
	{if count($content)>0}<div style="float:right;font-size:12px;"><a href="javascript:new_window('sortcontent.php?act=content&amp;act1=sortui&amp;id1={$smarty.get.id1}&amp;lang={$lang}','sort');" class="but sort" title="подреди по меню"></a></div>{/if}
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
	{math equation="1" assign=level} 
	{foreach from=$content item=c}
		<tr class="{cycle values="odd,even"}">
			<td width="*">{$c.title_bg}</td>
			{if $smarty.session.client_level==1}
			{if $c.show==y}
				<td width="1%"><a href="main.php?act=content&amp;act1=hide&amp;rec={$c.id}&amp;lang={$lang}" class="but hide" title="не се вижда"></a></td>
			{else}
				<td width="1%"><a href="main.php?act=content&amp;act1=show&amp;rec={$c.id}&amp;lang={$lang}" class="but show" title="вижда се"></a></td>
			{/if}
			{/if}
			{if	$c.image==y}
				<td width="1%"><img src="images/photo.png" alt="снимка" title="снимка" /></td>
			{else}
				<td width="1%"><img src="images/no_photo.png" alt="без снимка" title="без снимка" /></td>
			{/if}
			{*if $c.home==0}
				<td width="1%"><a href="main.php?act=content&amp;act1=showhome&amp;rec={$c.id}&amp;lang={$lang}" class="but nohome" title="покажи на първа страница"></a></td>
			{else}
				<td width="1%"><a href="main.php?act=content&amp;act1=hidehome&amp;rec={$c.id}&amp;lang={$lang}" class="but home" title="махни от първа страница"></a></td>
			{/if*}
			{if $c.menu==0}
				<td width="1%"><a href="main.php?act=content&amp;act1=showmenu&amp;rec={$c.id}&amp;lang={$lang}" class="but show" title="добави за подредба"></a></td>
			{else}
				<td width="1%"><a href="main.php?act=content&amp;act1=hidemenu&amp;rec={$c.id}&amp;lang={$lang}" class="but hide" title="махни от подредба"></a></td>
			{/if}
			<td width="1%"><a href="main.php?act=content&amp;act1=edit&amp;rec={$c.id}&amp;lang={$lang}" class="but edit" title="редактирай"></a></td>
			<td width="1%"><a href="main.php?act=content&amp;act1=delmessage&amp;rec={$c.id}&amp;lang={$lang}" class="but delete" title="изтрий"></a></td>
		</tr>
		{if count($c.children)>0}
		{if $smarty.get.act==""}{assign var='act' value='content'}{else}{assign var='act' value=$smarty.get.act}{/if}
		{include file="page_subcats.tpl" act=$act content=$c.children level=$level fcat=$fcat home=n}
		{/if}
	{foreachelse}
		<tr>
			<td>Няма въведени</td>
		</tr>	
	{/foreach}
	</table>
	{$paging}
</div>
