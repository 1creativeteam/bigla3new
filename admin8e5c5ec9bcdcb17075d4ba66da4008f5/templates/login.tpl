<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 7]>    <html class="ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html lang="bg" xml:lang="bg">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width" />
		<title>Администрация</title>
		<link rel="stylesheet" type="text/css" media="screen, projection" href="../design/styles.css" />
		<link rel="stylesheet" href="css/foundation.min.css">
	  	<link rel="stylesheet" href="css/app.css">
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="js/jquery-1.9.0.min.js"><\/script>');</script>
		<!--[if lt IE 9]>
			<script src="http://stexbar.googlecode.com/svn-history/r507/trunk/www/js/html5shiv.js"></script>
			<script>window.jQuery || document.write('<script src="../assets/js/html5shiv.js"><\/script>');</script>
		<![endif]-->
	</head>	
	<body>
		<div class="row">
			<div class="twelve columns">
				<div id="header">
					<div id="logo">
				  		<h1><a href="./"><img src="../assets/css/logo.png" alt="" /></a></h1>
					</div>
				</div>
				<div class="four columns centered login">
					<label for=""{if $error} class="error"{/if}>{$error}</label>
					<form action="logon.php" method="post">
						<div>
							<label for="usrname"></label>
							<input type="text" name="usrname" placeholder="потребител" value="" />
						</div>
						<div>
							<label for="password"></label>
							<input type="password" name="passwd" placeholder="парола" value="" />
						</div>
						<div class="eleven columns"></div>
						<div>
							<button type="submit" class="button expand">Влез</button>
						</div>
					</form>
					<div id="footer">&copy;Всички права запазени.{$smarty.now|date_format:'%Y'}</div>
				</div>
			</div>
		</div>
		<script src="js/foundation.min.js"></script>
	</body>
</html>