<!DOCTYPE html>

<html lang="bg" xml:lang="bg">

<head>

	<meta charset="utf-8" />

	<meta name="viewport" content="width=device-width" />

	<title>Администрация</title>

	<link rel="stylesheet" href="css/foundation.min.css">

  	<link rel="stylesheet" href="css/app.css">

  	<link rel="stylesheet" href="css/smoothness/jquery-ui-1.10.0.custom.css">

  	<link href="css/modal.css" rel="stylesheet" type="text/css" />

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>

	<script>window.jQuery || document.write('<script src="js/jquery-1.9.0.min.js"><\/script>');</script>

	<script src="js/jquery-ui-1.10.0.custom.min.js"></script>

	<script src="js/jquery.ui.datepicker-bg.js"></script>
	
	<script src="js/jquery.modal.js"></script>

	<script src="js/site.js"></script>

</head>	

<body>

	<div class="row">

		<div class="twelve columns">

		<!-- header begins -->

			<div id="header">

				<div id="logo">

			    	<h1><a href="./"><img src="../assets/css/logo.png" alt="" /></a></h1>

				</div>

				{$left_body}

				{if $loc.loc.languagebar=='y'}

				{include file="language.tpl"}

				{/if}

			</div>

			<!-- header ends -->

			<!-- content begins -->       

			<div id="content">

				<p class="legend"><img src="images/note.gif" alt="" align="absbottom" border="0" /> - Нов запис

				<img src="images/edit.gif" alt="" align="absbottom" border="0" /> - Редакция

				<img src="images/arrange.gif" alt="" align="absbottom" border="0" /> - Сортиране

				<img src="images/locked.gif" alt="" align="absbottom" border="0" /> - Скрий

				<img src="images/unlocked.gif" alt="" align="absbottom" border="0" /> - Покажи</p>

				<div class="clearfix"></div>

				{$main_body}

			</div>

			<!-- bottom end --> 

			<!-- footer begins -->

			<div id="footer">&copy;Всички права запазени. {$smarty.now|date_format:'%Y'}</div>

			<!-- footer ends -->

		</div>

	</div>

	<!--[if lt IE 9]>

		<script src="http://stexbar.googlecode.com/svn-history/r507/trunk/www/js/html5shiv.js"></script>

		<script>window.jQuery || document.write('<script src="../assets/js/html5shiv.js"><\/script>');</script>

	<![endif]-->

	<script type="text/javascript" src="js/functions.js"></script>

	<script src="js/foundation.min.js">/script>

</body>

</html>