<script language="javascript" type="text/javascript" src="js/editor/ckeditor.js"></script>

<div class="form">
	<h1 class="sifr">{$maintitle}</h1>
	{if $error}<div style="color:red;">{$error}</div><br>{/if}
	<form action="{$action}" method="post" enctype="multipart/form-data">

		<div class="row clear">
			<div class="name_bg six columns">
				<label for="name_bg"><strong>Заглавие  БГ</strong></label><input type="text" name="name_bg" style="width:445px;" value="{if $smarty.post.name_bg|escape:'html'}{$smarty.post.name_bg}{else}{$data.name_bg|escape:'html'}{/if}">
			</div>
			<div class="name_en six columns">
				<label for="name_en"><strong>Заглавие EN</strong></label><input type="text" name="name_en" style="width:445px;" value="{if $smarty.post.name_en}{$smarty.post.name_en|escape:'html'}{else}{$data.name_en|escape:'html'}{/if}">
			</div>
		</div>

		<div class="row clear">
			<div class="two columns">
				<input class="button expand postfix" type="submit" value="Запази" />
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
	{literal}

   	{/literal}
</script>