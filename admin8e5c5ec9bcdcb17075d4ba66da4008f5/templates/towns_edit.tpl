<div class="form">
	<h1 class="sifr">Град</h1>
	{if $error}<div style="color:red;">{$error}</div>{/if}
	<form action="{$action}" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="four columns">
				<input type="text" name="town_bg" lang="{$lang}" style="width:300px;" value="{if $smarty.post.town_bg}{$smarty.post.town_bg}{else}{$town_bg}{/if}" placeholder="Град BG">
			</div>
			<div class="four columns end">
				<input type="text" name="town_en" lang="{$lang}" style="width:300px;" value="{if $smarty.post.town_en}{$smarty.post.town_en}{else}{$town_en}{/if}" placeholder="Град EN">
			</div>
		</div>
		<div class="row" style="clear: both; margin-top:10px;">
			<div class="two columns">
				<input class="button expand postfix" type="submit" value="Запази" />
			</div>
		</div>
	</form>
</div>
