<HTML>
<HEAD>
	<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset={$charset}">
	<TITLE>Image Browser</TITLE>
	<link rel="stylesheet" type="text/css" href="site.css">
	<script src="js/functions.js"></script>
	<script src="../js/ajax.js"></script>
	<script src="../js/debug.js"></script>
	<script language="javascript" type="text/javascript" src="js/tiny_mce/tiny_mce_popup.js"></script>
	<script type="text/javascript">
	{literal}
		function mySubmit(imageURL) {
		  //call this function only after page has loaded
		  //otherwise tinyMCEPopup.close will close the
		  //"Insert/Edit Image" or "Insert/Edit Link" window instead
		
		  var URL = imageURL;
		  var win = tinyMCEPopup.getWindowArg("window");
		
		  // insert information now
		  win.document.getElementById(tinyMCEPopup.getWindowArg("input")).value = URL;
		
		  // for image browsers: update image dimensions
		  if (win.getImageData) win.getImageData();
		
		  // close popup window
		  tinyMCEPopup.close();
		}
	{/literal}
	</script>
</HEAD>


<body>

<div>
{section name=i loop=$images}
<div style="float:left;width:80px;height:80px;margin:5px;padding:0px;">
	<a href="javascript:mySubmit('javascript:pic_win1(\'{$images[i].urlfl}\')');">
		<img src="{$images[i].image}" alt="" />
	</a>
</div>
{/section}
</div>

</body>

</html>