<div class="form">
	<h1>
		<div style="float:right;font-size:12px;">Добави нова категория
			<a href="main.php?act=categories&amp;act1=create&amp;lang={$lang}" class="but new" title="Нова категоия"></a>
		</div>
		{$loc.categories.1}
	</h1>
	{if $error}<div style="color:red;">{$error}</div><br>{/if}
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
		{foreach from=$categories item=c}
			<tr class="{cycle values="odd,even"}">
				<td width="*">{$c.name_bg}</td>
				<td width="*">{$c.name_en}</td>

				<td width="1%"><a href="main.php?act={$smarty.get.act}&amp;act1=edit&amp;rec={$c.id}&amp;lang={$lang}" class="but edit" title="редактирай"></a></td>
				<td width="1%"><a href="main.php?act={$smarty.get.act}&amp;act1=delete&amp;rec={$c.id}&amp;lang={$lang}" class="but delete" title="изтрий"></a></td>
			</tr>
			{foreachelse}
			<tr>
				<td>Няма въведени</td>
			</tr>	
		{/foreach}
	</table>
	{$paging}
</div>

