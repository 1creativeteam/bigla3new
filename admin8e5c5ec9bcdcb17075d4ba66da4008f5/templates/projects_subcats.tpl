{math equation="x+1" x=$level assign=level}
{foreach item=c from=$cat name=cs}
	{if $smarty.get.rec!=$c.id}
	<option style="{if count($c.children)>0}margin-left:{$level+2}px;{else}margin-left:{$level+5}px;{/if}" value="{$c.id}|{$level}"{if $fcat==$c.id || $fcat1==$c.id} selected="selected"{/if}>{if $level==3} &mdash; &mdash; {elseif $level==4} &mdash; &mdash; &mdash;  {elseif $level==5} &mdash; &mdash; &mdash; &mdash; {else} &mdash; {/if}{$c.title_bg}</option>
	{/if}
	{include file="projects_subcats.tpl" cat=$c.children level=$level fcat=$fcat fcat1=$id1}
{/foreach}
