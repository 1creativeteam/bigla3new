{if $pages>1}
<div class="pages">
	{if $page>0}<a href="{$url}page=0">{$loc.loc.1}</a>{else}<span id="empty">{$loc.loc.1}</span>{/if}
	{if $smarty.get.page>0}<a href="{$url}page={$smarty.get.page-$perpage}">{$loc.loc.3}</a>{else}<span id="empty">{$loc.loc.3}</span>{/if}
	<ul>
	{section start=$start loop=$end name=p step=1}
		<li><a href="{$url}page={math equation="p*(i+s-1)" p=$perpage i=$smarty.section.p.iteration s=$start}" {if $page+1==($smarty.section.p.iteration+$start)}class="on"{/if}>{$smarty.section.p.iteration+$start}</a></li>
	{/section}
	</ul>
	{if $pages>$page+1}<a href="{$url}page={$smarty.get.page+$perpage}">{$loc.loc.4}</a>{else}<span id="empty">{$loc.loc.4}</span>{/if}
	{if $smarty.get.page!=$pages*$perpage-$perpage}<a href="{$url}page={$pages*$perpage-$perpage}">{$loc.loc.2}</a>{else}<span id="empty">{$loc.loc.2}</span>{/if}
</div>
{/if}

	