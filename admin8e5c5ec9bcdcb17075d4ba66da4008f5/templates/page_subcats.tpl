{math equation="x+1" x=$level assign=level}
{if $act1!="do"}{assign var='act1' value='act1'}{/if}
{foreach item=c from=$content}
	<tr class="{cycle values="odd,even"}">
		<td width="*">{if $level==3} &mdash; &mdash; {elseif $level==4} &mdash; &mdash; &mdash;  {elseif $level==5} &mdash; &mdash; &mdash; &mdash; {else} &mdash; {/if}{$c.title_bg}</td>
		{if $smarty.session.client_level==1}
			{if $c.show==y}
				<td width="1%"><a href="main.php?act={$act}&amp;{$act1}=hide&amp;rec={$c.id}&amp;lang={$lang}" class="but hide" title="не се вижда"></a></td>
			{else}
				<td width="1%"><a href="main.php?act={$act}&amp;{$act1}=show&amp;rec={$c.id}&amp;lang={$lang}" class="but show" title="вижда се"></a></td>
			{/if}
		{/if}
		{if	$c.image!=""}
		{if	$c.image==y}
			<td width="1%"><img src="images/photo.png" alt="снимка" title="снимка" /></td>
		{else}
			<td width="1%"><img src="images/no_photo.png" alt="без снимка" title="без снимка" /></td>
		{/if}
		{/if}
		{if $home=='y'}
		{if	$c.home==0}
			<td width="1%"><a href="main.php?act={$act}&amp;{$act1}=showhome&amp;rec={$c.id}&amp;lang={$lang}" class="but nohome" title="покажи на първа страница"></a></td>
		{else}
			<td width="1%"><a href="main.php?act={$act}&amp;{$act1}=hidehome&amp;rec={$c.id}&amp;lang={$lang}" class="but home" title="махни от първа страница"></a></td>
		{/if}
		{/if}
		{if $c.menu==0}
			<td width="1%"><a href="main.php?act={$act}&amp;{$act1}=showmenu&amp;rec={$c.id}&amp;lang={$lang}" class="but show" title="добави в менюто"></a></td>
		{else}
			<td width="1%"><a href="main.php?act={$act}&amp;{$act1}=hidemenu&amp;rec={$c.id}&amp;lang={$lang}" class="but hide" title="махни от менюто"></a></td>
		{/if}
		<td width="1%"><a href="main.php?act={$act}&amp;{$act1}=edit&amp;rec={$c.id}&amp;lang={$lang}" class="but edit" title="редактирай"></a></td>
		<td width="1%"><a href="main.php?act={$act}&amp;{$act1}=delmessage&amp;rec={$c.id}&amp;lang={$lang}" class="but delete" title="изтрий"></a></td>
	</tr>
	{if count($c.children)>0}
	{include file="page_subcats.tpl" act=$act act1=$act1 content=$c.children level=$level fcat=$fcat home=$home}
	{/if}
{/foreach}
