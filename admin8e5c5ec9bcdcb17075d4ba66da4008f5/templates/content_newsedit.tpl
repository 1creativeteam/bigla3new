<script language="javascript" type="text/javascript" src="js/editor/ckeditor.js"></script>
<a href="#" class="langs" data-lang="bg">bg</a>
<a href="#" class="langs" data-lang="en">en</a>
<div class="form">
	<h1 class="sifr">{$maintitle}</h1>
	{if $error}<div style="color:red;">{$error}</div>{/if}
	<form action="{$action}" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="forlangs titles_bg four columns">
				<label for="title_bg">Заглавие БГ</label><input type="text" name="title_bg" lang="{$lang}"style="width:300px;" value="{if $smarty.post.title_bg}{$smarty.post.title_bg}{else}{$title_bg}{/if}">
			</div>
			<div class="forlangs titles_en four columns end">
				<label for="title_en">Заглавие EN</label><input type="text" name="title_en" lang="{$lang}"style="width:300px;" value="{if $smarty.post.title_en}{$smarty.post.title_en}{else}{$title_en}{/if}">
			</div>
		</div>
		<div>
			<label for="link">Link</label><input type="text" name="link" lang="{$lang}"style="width:300px;" value="{if $smarty.post.link}{$smarty.post.link}{else}{$link}{/if}">
		</div>
		<div class="row">
			<div class="two columns">
			 	<label for="cat_id">Категории:</label>
			 	<input type="hidden" name="old_cat_id" value="{$id1}">
				<select id="cat_id" name="cat_id">
			    	<option value="0">-- без категогория --</option>
			    	{math equation="1" assign=level} 
					{foreach from=$cat item=c}
					{if $smarty.get.rec!=$c.id}
					<option style="font-weight:bold;" value="{$c.id}"{if $smarty.get.catid==$c.id || $id1==$c.id} selected="selected"{/if}>{$c.title_bg}</option>
					{/if}
					{include file="subcats.tpl" cat=$c.children level=$level fcat=$smarty.get.catid fcat1=$id1}
					{/foreach}
				</select>
			</div>
		</div>
		<div class="forlangs text_bg">
			<label for="text">Текст БГ</label>
			<textarea name="text_bg" id="text" ros="10" class="big">{if $smarty.post.text_bg}{$smarty.post.text_bg}{else}{$text_bg}{/if}</textarea>
		</div>
		<div class="forlangs text_en">
			<label for="text1">Текст EN</label>
			<textarea name="text_en" id="text1" ros="10" class="big">{if $smarty.post.text_en}{$smarty.post.text_en}{else}{$text_en}{/if}</textarea>
		</div>
		{if count($image)}<h2 style="clear:both;margin-bottom:10px;">Снимка</h2>{/if}
		{section name=image loop=$image}
		<div style="float:left;margin:5px;padding:0px;">
			<img src="../{$image[image].path}{$imgenl}{$image[image].ext}?date={$smarty.now}" alt="{$image[image].name}" style="border:none;" border="0" />
			<div style="margin:0px;padding:0px;height:auto;">
				<!--a style="float:left;" href="main.php?act=content&amp;act1=editpic&amp;id={$smarty.get.rec}&amp;lang={$smarty.get.lang}&amp;pic={$image[image].path|escape:'url'}&amp;func={$smarty.get.act1}" class="but edit" title="{$loc.content.11}"></a-->
				<a style="float:left;" href="main.php?act=content&amp;act1=delpic&amp;id={$image[image].id}" class="but delete" title="изтрий"></a>
			</div>
		</div>
		{/section}
		{if count($image) < $loc.limitpicture}
		<h2 style="clear:both;margin-bottom:10px;">Добави снимка</h2>
		<input type="hidden" name="id" value="{$rec}">
		<input type="file" name="image">
		<p><strong>Добавените снимки трябва да са във формат .jpg, .png и .gif.</strong></p>
		{/if}
		<div class="row">
			<div class="two columns">
				<input class="button expand postfix" type="submit" value="Запази" />
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
	{literal}
	CKEDITOR.replace( 'text1' , { });
	CKEDITOR.replace( 'text' , { });
	{/literal}
</script>
