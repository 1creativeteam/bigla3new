<script language="javascript" type="text/javascript" src="js/editor/ckeditor.js"></script>



<a href="#" class="langs" data-lang="bg">bg</a>



<a href="#" class="langs" data-lang="en">en</a>



<div class="form">



	<h1 class="sifr">{$maintitle}</h1>



	{if $error}<div style="color:red;">{$error}</div>{/if}



	<form action="{$action}" method="post" enctype="multipart/form-data">



		<div class="row clear">



			<div class="forlangs free_text_bg five columns">



				<label for="title_bg"><strong>Заглавие  БГ</strong></label><input type="text" name="free_text_bg" lang="{$lang}"style="width:300px;" value="{if $smarty.post.free_text_bg|escape:'html'}{$smarty.post.free_text_bg}{else}{$data.free_text_bg|escape:'html'}{/if}">



			</div>



			<div class="forlangs free_text_en five columns end">



				<label for="free_text_en"><strong>Заглавие EN</strong></label><input type="text" name="free_text_en" lang="{$lang}"style="width:300px;" value="{if $smarty.post.title_en}{$smarty.post.free_text_en|escape:'html'}{else}{$data.free_text_en|escape:'html'}{/if}">



			</div>



		</div>



		<div class="row clear">



			<div class="forlangs text_bg">



				<label for="text"><strong>Описание БГ</strong></label>



				<textarea name="text_bg" id="text" ros="10" class="big">{if $smarty.post.text_bg}{$smarty.post.text_bg}{else}{$data.text_bg}{/if}</textarea>



			</div>



			<div class="forlangs text_en">



				<label for="text1"><strong>Описание EN</strong></label>



				<textarea name="text_en" id="text1" ros="10" class="big">{if $smarty.post.text_en}{$smarty.post.text_en}{else}{$data.text_en}{/if}</textarea>



			</div>


		

		</div>


		<div id="tabs" class="clear tintyavaTabs">



			<ul>



				<li><a href="#etaj-1">Етаж 1</a></li>



				<li><a href="#etaj-2">Етаж 2</a></li>



				<li><a href="#etaj-3">Етаж 3</a></li>



				<li><a href="#etaj-4">Етаж 4</a></li>



				<li><a href="#etaj-5">Етаж 5</a></li>



				<li><a href="#etaj-6">Етаж 6</a></li>



				<li><a href="#etaj-7">Сутерен</a></li>



			</ul>



			<div id="etaj-1">



				<div class="row">



					<div class="two columns">



						<label>Апартамент 1</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[1][sqm]" value="{if $smarty.post.apartments.1.sqm}{$smarty.post.apartments.1.sqm}{else}{$apartments.1.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[1][sqm_real]" value="{if $smarty.post.apartments.1.sqm_real}{$smarty.post.apartments.1.sqm_real}{else}{$apartments.1.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[1][sqm_light]" value="{if $smarty.post.apartments.1.sqm_light}{$smarty.post.apartments.1.sqm_light}{else}{$apartments.1.sqm_light}{/if}">



						<input type="hidden" name="apartments[1][floor]" value="1">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[1][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.1.status==$label || $apartments.1.status==$label} selected="selected"{/if} >	{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 2</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[2][sqm]" value="{if $smarty.post.apartments.2.sqm}{$smarty.post.apartments.2.sqm}{else}{$apartments.2.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[2][sqm_real]" value="{if $smarty.post.apartments.2.sqm_real}{$smarty.post.apartments.2.sqm_real}{else}{$apartments.2.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[2][sqm_light]" value="{if $smarty.post.apartments.2.sqm_light}{$smarty.post.apartments.2.sqm_light}{else}{$apartments.2.sqm_light}{/if}">



						<input type="hidden" name="apartments[2][floor]" value="1">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[2][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.2.status==$label || $apartments.2.status==$label} selected="selected"{/if} >	{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 3</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[3][sqm]" value="{if $smarty.post.apartments.3.sqm}{$smarty.post.apartments.3.sqm}{else}{$apartments.3.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[3][sqm_real]" value="{if $smarty.post.apartments.3.sqm_real}{$smarty.post.apartments.3.sqm_real}{else}{$apartments.3.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[3][sqm_light]" value="{if $smarty.post.apartments.3.sqm_light}{$smarty.post.apartments.3.sqm_light}{else}{$apartments.3.sqm_light}{/if}">



						<input type="hidden" name="apartments[3][floor]" value="1">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[3][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.3.status==$label || $apartments.3.status==$label} selected="selected"{/if} >	{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 4</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[4][sqm]" value="{if $smarty.post.apartments.4.sqm}{$smarty.post.apartments.4.sqm}{else}{$apartments.4.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[4][sqm_real]" value="{if $smarty.post.apartments.4.sqm_real}{$smarty.post.apartments.4.sqm_real}{else}{$apartments.4.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[4][sqm_light]" value="{if $smarty.post.apartments.4.sqm_light}{$smarty.post.apartments.4.sqm_light}{else}{$apartments.4.sqm_light}{/if}">



						<input type="hidden" name="apartments[4][floor]" value="1">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[4][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.4.status==$label || $apartments.4.status==$label} selected="selected"{/if} >	{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 5</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[5][sqm]" value="{if $smarty.post.apartments.5.sqm}{$smarty.post.apartments.5.sqm}{else}{$apartments.5.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[5][sqm_real]" value="{if $smarty.post.apartments.5.sqm_real}{$smarty.post.apartments.5.sqm_real}{else}{$apartments.5.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[5][sqm_light]" value="{if $smarty.post.apartments.5.sqm_light}{$smarty.post.apartments.5.sqm_light}{else}{$apartments.5.sqm_light}{/if}">



						<input type="hidden" name="apartments[5][floor]" value="1">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[5][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.5.status==$label || $apartments.5.status==$label} selected="selected"{/if} >	{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



			</div>



			<div id="etaj-2">



				<div class="row">



					<div class="two columns">



						<label>Апартамент 6</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[6][sqm]" value="{if $smarty.post.apartments.6.sqm}{$smarty.post.apartments.6.sqm}{else}{$apartments.6.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[6][sqm_real]" value="{if $smarty.post.apartments.6.sqm_real}{$smarty.post.apartments.6.sqm_real}{else}{$apartments.6.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[6][sqm_light]" value="{if $smarty.post.apartments.6.sqm_light}{$smarty.post.apartments.6.sqm_light}{else}{$apartments.6.sqm_light}{/if}">



						<input type="hidden" name="apartments[6][floor]" value="2">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[6][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.6.status==$label || $apartments.6.status==$label} selected="selected"{/if} >	{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 7</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[7][sqm]" value="{if $smarty.post.apartments.7.sqm}{$smarty.post.apartments.7.sqm}{else}{$apartments.7.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[7][sqm_real]" value="{if $smarty.post.apartments.7.sqm_real}{$smarty.post.apartments.7.sqm_real}{else}{$apartments.7.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[7][sqm_light]" value="{if $smarty.post.apartments.7.sqm_light}{$smarty.post.apartments.7.sqm_light}{else}{$apartments.7.sqm_light}{/if}">



						<input type="hidden" name="apartments[7][floor]" value="2">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[7][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.7.status==$label || $apartments.7.status==$label} selected="selected"{/if} >	{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 8</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[8][sqm]" value="{if $smarty.post.apartments.8.sqm}{$smarty.post.apartments.8.sqm}{else}{$apartments.8.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[8][sqm_real]" value="{if $smarty.post.apartments.8.sqm_real}{$smarty.post.apartments.8.sqm_real}{else}{$apartments.8.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[8][sqm_light]" value="{if $smarty.post.apartments.8.sqm_light}{$smarty.post.apartments.8.sqm_light}{else}{$apartments.8.sqm_light}{/if}">



						<input type="hidden" name="apartments[8][floor]" value="2">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[8][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.8.status==$label || $apartments.8.status==$label} selected="selected"{/if} >	{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 9</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[9][sqm]" value="{if $smarty.post.apartments.9.sqm}{$smarty.post.apartments.9.sqm}{else}{$apartments.9.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[9][sqm_real]" value="{if $smarty.post.apartments.9.sqm_real}{$smarty.post.apartments.9.sqm_real}{else}{$apartments.9.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[9][sqm_light]" value="{if $smarty.post.apartments.9.sqm_light}{$smarty.post.apartments.9.sqm_light}{else}{$apartments.9.sqm_light}{/if}">



						<input type="hidden" name="apartments[9][floor]" value="2">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[9][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.9.status==$label || $apartments.9.status==$label} selected="selected"{/if} >	{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 10</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[10][sqm]" value="{if $smarty.post.apartments.10.sqm}{$smarty.post.apartments.10.sqm}{else}{$apartments.10.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[10][sqm_real]" value="{if $smarty.post.apartments.10.sqm_real}{$smarty.post.apartments.10.sqm_real}{else}{$apartments.10.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[10][sqm_light]" value="{if $smarty.post.apartments.10.sqm_light}{$smarty.post.apartments.10.sqm_light}{else}{$apartments.10.sqm_light}{/if}">



						<input type="hidden" name="apartments[10][floor]" value="2">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[10][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.10.status==$label || $apartments.10.status==$label} selected="selected"{/if} >



									{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



			</div>



			<div id="etaj-3">



				<div class="row">



					<div class="two columns">



						<label>Апартамент 11</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[11][sqm]" value="{if $smarty.post.apartments.11.sqm}{$smarty.post.apartments.11.sqm}{else}{$apartments.11.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[11][sqm_real]" value="{if $smarty.post.apartments.11.sqm_real}{$smarty.post.apartments.11.sqm_real}{else}{$apartments.11.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[11][sqm_light]" value="{if $smarty.post.apartments.11.sqm_light}{$smarty.post.apartments.11.sqm_light}{else}{$apartments.11.sqm_light}{/if}">



						<input type="hidden" name="apartments[11][floor]" value="3">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[11][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.11.status==$label || $apartments.11.status==$label} selected="selected"{/if}>{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 12</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[12][sqm]" value="{if $smarty.post.apartments.12.sqm}{$smarty.post.apartments.12.sqm}{else}{$apartments.12.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[12][sqm_real]" value="{if $smarty.post.apartments.12.sqm_real}{$smarty.post.apartments.12.sqm_real}{else}{$apartments.12.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[12][sqm_light]" value="{if $smarty.post.apartments.12.sqm_light}{$smarty.post.apartments.12.sqm_light}{else}{$apartments.12.sqm_light}{/if}">



						<input type="hidden" name="apartments[12][floor]" value="3">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[12][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.12.status==$label || $apartments.12.status==$label} selected="selected"{/if} >{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 13</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[13][sqm]" value="{if $smarty.post.apartments.13.sqm}{$smarty.post.apartments.13.sqm}{else}{$apartments.13.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[13][sqm_real]" value="{if $smarty.post.apartments.13.sqm_real}{$smarty.post.apartments.13.sqm_real}{else}{$apartments.13.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[13][sqm_light]" value="{if $smarty.post.apartments.13.sqm_light}{$smarty.post.apartments.13.sqm_light}{else}{$apartments.13.sqm_light}{/if}">



						<input type="hidden" name="apartments[13][floor]" value="3">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[13][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.13.status==$label || $apartments.13.status==$label} selected="selected"{/if} >{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 14</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[14][sqm]" value="{if $smarty.post.apartments.14.sqm}{$smarty.post.apartments.14.sqm}{else}{$apartments.14.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[14][sqm_real]" value="{if $smarty.post.apartments.14.sqm_real}{$smarty.post.apartments.14.sqm_real}{else}{$apartments.14.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[14][sqm_light]" value="{if $smarty.post.apartments.14.sqm_light}{$smarty.post.apartments.14.sqm_light}{else}{$apartments.14.sqm_light}{/if}">



						<input type="hidden" name="apartments[14][floor]" value="3">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[14][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.14.status==$label || $apartments.14.status==$label} selected="selected"{/if} >{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 15</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[15][sqm]" value="{if $smarty.post.apartments.15.sqm}{$smarty.post.apartments.15.sqm}{else}{$apartments.15.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[15][sqm_real]" value="{if $smarty.post.apartments.15.sqm_real}{$smarty.post.apartments.15.sqm_real}{else}{$apartments.15.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[15][sqm_light]" value="{if $smarty.post.apartments.15.sqm_light}{$smarty.post.apartments.15.sqm_light}{else}{$apartments.15.sqm_light}{/if}">



						<input type="hidden" name="apartments[15][floor]" value="3">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[15][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.15.status==$label || $apartments.15.status==$label} selected="selected"{/if} >{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



			</div>



			<div id="etaj-4">



				<div class="row">



					<div class="two columns">



						<label>Апартамент 16</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[16][sqm]" value="{if $smarty.post.apartments.16.sqm}{$smarty.post.apartments.16.sqm}{else}{$apartments.16.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[16][sqm_real]" value="{if $smarty.post.apartments.16.sqm_real}{$smarty.post.apartments.16.sqm_real}{else}{$apartments.16.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[16][sqm_light]" value="{if $smarty.post.apartments.16.sqm_light}{$smarty.post.apartments.16.sqm_light}{else}{$apartments.16.sqm_light}{/if}">



						<input type="hidden" name="apartments[16][floor]" value="4">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[16][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.16.status==$label || $apartments.16.status==$label} selected="selected"{/if} >{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 17</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[17][sqm]" value="{if $smarty.post.apartments.17.sqm}{$smarty.post.apartments.17.sqm}{else}{$apartments.17.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[17][sqm_real]" value="{if $smarty.post.apartments.17.sqm_real}{$smarty.post.apartments.17.sqm_real}{else}{$apartments.17.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[17][sqm_light]" value="{if $smarty.post.apartments.17.sqm_light}{$smarty.post.apartments.17.sqm_light}{else}{$apartments.17.sqm_light}{/if}">



						<input type="hidden" name="apartments[17][floor]" value="4">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[17][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.17.status==$label || $apartments.17.status==$label} selected="selected"{/if} >{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 18</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[18][sqm]" value="{if $smarty.post.apartments.18.sqm}{$smarty.post.apartments.18.sqm}{else}{$apartments.18.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[18][sqm_real]" value="{if $smarty.post.apartments.18.sqm_real}{$smarty.post.apartments.18.sqm_real}{else}{$apartments.18.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[18][sqm_light]" value="{if $smarty.post.apartments.18.sqm_light}{$smarty.post.apartments.18.sqm_light}{else}{$apartments.18.sqm_light}{/if}">



						<input type="hidden" name="apartments[18][floor]" value="4">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[18][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.18.status==$label || $apartments.18.status==$label} selected="selected"{/if} >{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 19</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[19][sqm]" value="{if $smarty.post.apartments.19.sqm}{$smarty.post.apartments.19.sqm}{else}{$apartments.19.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[19][sqm_real]" value="{if $smarty.post.apartments.19.sqm_real}{$smarty.post.apartments.19.sqm_real}{else}{$apartments.19.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[19][sqm_light]" value="{if $smarty.post.apartments.19.sqm_light}{$smarty.post.apartments.19.sqm_light}{else}{$apartments.19.sqm_light}{/if}">



						<input type="hidden" name="apartments[19][floor]" value="4">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[19][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.19.status==$label || $apartments.19.status==$label} selected="selected"{/if} >{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



			</div>



			<div id="etaj-5">



				<div class="row">



					<div class="two columns">



						<label>Апартамент 20</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[20][sqm]" value="{if $smarty.post.apartments.20.sqm}{$smarty.post.apartments.20.sqm}{else}{$apartments.20.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[20][sqm_real]" value="{if $smarty.post.apartments.20.sqm_real}{$smarty.post.apartments.20.sqm_real}{else}{$apartments.20.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[20][sqm_light]" value="{if $smarty.post.apartments.20.sqm_light}{$smarty.post.apartments.20.sqm_light}{else}{$apartments.20.sqm_light}{/if}">



						<input type="hidden" name="apartments[20][floor]" value="5">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[20][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.20.status==$label || $apartments.20.status==$label} selected="selected"{/if} >{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 21</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[21][sqm]" value="{if $smarty.post.apartments.21.sqm}{$smarty.post.apartments.21.sqm}{else}{$apartments.21.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[21][sqm_real]" value="{if $smarty.post.apartments.21.sqm_real}{$smarty.post.apartments.21.sqm_real}{else}{$apartments.21.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[21][sqm_light]" value="{if $smarty.post.apartments.21.sqm_light}{$smarty.post.apartments.21.sqm_light}{else}{$apartments.21.sqm_light}{/if}">



						<input type="hidden" name="apartments[21][floor]" value="5">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[21][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.21.status==$label || $apartments.21.status==$label} selected="selected"{/if} >{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 22</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[22][sqm]" value="{if $smarty.post.apartments.22.sqm}{$smarty.post.apartments.22.sqm}{else}{$apartments.22.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[22][sqm_real]" value="{if $smarty.post.apartments.22.sqm_real}{$smarty.post.apartments.22.sqm_real}{else}{$apartments.22.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[22][sqm_light]" value="{if $smarty.post.apartments.22.sqm_light}{$smarty.post.apartments.22.sqm_light}{else}{$apartments.22.sqm_light}{/if}">



						<input type="hidden" name="apartments[22][floor]" value="5">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[22][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.22.status==$label || $apartments.22.status==$label} selected="selected"{/if} >{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 23</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[23][sqm]" value="{if $smarty.post.apartments.23.sqm}{$smarty.post.apartments.23.sqm}{else}{$apartments.23.sqm} {/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[23][sqm_real]" value="{if $smarty.post.apartments.23.sqm_real}{$smarty.post.apartments.23.sqm_real}{else}{$apartments.23.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[23][sqm_light]" value="{if $smarty.post.apartments.23.sqm_light}{$smarty.post.apartments.23.sqm_light}{else}{$apartments.23.sqm_light}{/if}">



						<input type="hidden" name="apartments[23][floor]" value="5">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[23][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.23.status==$label || $apartments.23.status==$label} selected="selected"{/if} >{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 24</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[24][sqm]" value="{if $smarty.post.apartments.24.sqm}{$smarty.post.apartments.24.sqm}{else}{$apartments.24.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[24][sqm_real]" value="{if $smarty.post.apartments.24.sqm_real}{$smarty.post.apartments.24.sqm_real}{else}{$apartments.24.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[24][sqm_light]" value="{if $smarty.post.apartments.24.sqm_light}{$smarty.post.apartments.24.sqm_light}{else}{$apartments.24.sqm_light}{/if}">



						<input type="hidden" name="apartments[24][floor]" value="5">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[24][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.24.status==$label || $apartments.24.status==$label} selected="selected"{/if} >{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



			</div>



			<div id="etaj-6">



				<div class="row">



					<div class="two columns">



						<label>Апартамент 25</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[25][sqm]" value="{if $smarty.post.apartments.25.sqm}{$smarty.post.apartments.25.sqm}{else}{$apartments.25.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[25][sqm_real]" value="{if $smarty.post.apartments.25.sqm_real}{$smarty.post.apartments.25.sqm_real}{else}{$apartments.25.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[25][sqm_light]" value="{if $smarty.post.apartments.25.sqm_light}{$smarty.post.apartments.25.sqm_light}{else}{$apartments.25.sqm_light}{/if}">



						<input type="hidden" name="apartments[25][floor]" value="6">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[25][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.25.status==$label || $apartments.25.status==$label} selected="selected"{/if} >{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



				<div class="row">



					<div class="two columns">



						<label>Апартамент 26</label>



					</div>



					<div class="two columns">



						<label>ОЗП</label>



						<input type="text" name="apartments[26][sqm]" value="{if $smarty.post.apartments.26.sqm}{$smarty.post.apartments.26.sqm}{else}{$apartments.26.sqm}{/if}">



					</div>



					<div class="two columns">



						<label>Застроена площ</label>



						<input type="text" name="apartments[26][sqm_real]" value="{if $smarty.post.apartments.26.sqm_real}{$smarty.post.apartments.26.sqm_real}{else}{$apartments.26.sqm_real}{/if}">



					</div>



					<div class="two columns">



						<label>св. квадратура</label>



						<input type="text" name="apartments[26][sqm_light]" value="{if $smarty.post.apartments.26.sqm_light}{$smarty.post.apartments.26.sqm_light}{else}{$apartments.26.sqm_light}{/if}">



						<input type="hidden" name="apartments[26][floor]" value="6">



					</div>



					<div class="two columns">



						<label>Статус</label>



						<select name="apartments[26][status]">



							<option value="0">---изберете---</option>



							{foreach from=$statuses item=label key=key}



								<option value="{$label}" {if $smarty.post.apartments.26.status==$label || $apartments.26.status==$label} selected="selected"{/if} >{$label}



								</option>



							{/foreach}



						</select>



					</div>



				</div>



			</div>



			<div id="etaj-7">



				<div class="row">

					{section start=1 loop=32 name=p step=1}

					{assign var="ident" value=$smarty.section.p.iteration}

					<div class="six columns">

						<div class="two columns">

							<label><strong>място {$ident}</strong></label>

						</div>

						<div class="five columns">

							<label>кв.м.</label>

							<input type="text" name="parking[{$ident}][sqm]" value="{if $parking.$ident.sqm}{$parking.$ident.sqm}{/if}">

						</div>

						<div class="five columns">

							<label>статус</label>

							<select name="parking[{$ident}][status]">

								<option value="0">---изберете---</option>

								{foreach from=$statuses item=label key=key}

									<option value="{$label}" {if $parking.$ident.status == $label} selected="selected" {/if} >{$label}

									</option>

								{/foreach}

							</select>

						</div>

					</div>

					{/section}

				</div>



			</div>





		</div>



		<div class="row clear clearfix">

			<div class="two columns">

				<button type="button" class="button success" id="galleryAdd">Добави към галерия</button>

			</div>

			<div class="ten columns">
				<label for="text1" style="text-align:right"><strong>Главна Снимка</strong></label>
				<input type="file" style="float:right" name="main_image">
			</div>

		</div>

		<div class="row clear row-image" id="galleryHolder" {if $galleryItems} style="display:block"{/if}>

			{foreach from=$galleryItems key=key item=item}

				<div class="twelve columns galleryRow" data-object="{$item.id}">

					<div class="one column galleryPreview"><a href="#{$item.gallery_group}{$item.ordered}" class="modal" rel="modal:open">view</a></div>

					<div class="three columns"><input type="file" name="image[]"></div>

					<div class="four columns"><input type="text" name="image_caption[]" value="{$item.title}"></div>

					<div class="one column"><input type="button" value="up" onclick="updateOrder(event)" class="button default small"></div>

					<div class="one column"><input type="button" value="down" onclick="updateOrder(event)" class="button default small"></div>

					<div class="one column"><input type="button" value="X" onclick="removeGalleryRow(event)" class="button alert small"></div>

					<div class="one column">

						<input type="button" data-action="{$actionUpdateGallery}" onclick="callAjax(event);" value="Update" class="button default small">

					</div>

					<div id="{$item.gallery_group}{$item.ordered}" style="display:none;max-height:400px;">

    					<img src="../{$item.picture_path}" alt="image" style="width:378px;height:250px">

  					</div>

			    </div>

			{/foreach}

		</div>



		<div class="row clear">



			<div class="two columns">



				<input class="button expand postfix" type="submit" value="Запази" />



			</div>



		</div>



	</form>



</div>



<script type="text/javascript">

	{literal}



		$( function() {

			$('.modal').modal();

	    	$( "#tabs" ).tabs();

	    	$('.galleryRow').removeClass('lastChild');

	    	$('.galleryRow:last').addClass('lastChild');

	    	$('.galleryRow').removeClass('firstChild');

	    	$('.galleryRow:first').addClass('firstChild');



	    	$('#galleryAdd').click(function() {

	    		$('#galleryHolder').css('display', 'block');

	    		$('.galleryRow').removeClass('lastChild');

	    		

	    		$('#galleryHolder').append(

	    			'<div class="twelve columns newItem galleryRow firstChild lastChild">' +

	    				'<div class="one column galleryPreview"></div>' +

						'<div class="three columns"><input type="file" name="new_image[]"></div>' +

						'<div class="four columns"><input type="text" name="new_image_caption[]"></div>' +

						'<div class="one column"><input type="button" value="up" onclick="updateOrder(event)" class="button default small"></div>' +

						'<div class="one column"><input type="button" value="down" onclick="updateOrder(event)" class="button default small"></div>' +

						'<div class="one column"><input type="button" value="X" onclick="removeGalleryRow(event)" class="button alert small"></div>' +

						'<div class="one column"></div>' +

					'</div>'

	    		);

	    		$('.galleryRow').removeClass('firstChild');

	    		$('.galleryRow:first').addClass('firstChild');

	    	});



		});



		function callAjax(event) {

			event.preventDefault();

			var bttn = $(event.target);

			var fileInput = bttn.closest('div').parent().find('.three input[type="file"]');

			var textInput = bttn.closest('div').parent().find('.four input[type="text"]');

			var file_data = fileInput.prop('files')[0];

			var text_data = textInput.val();

			var itemId = bttn.parent().closest('.galleryRow').attr('data-object');

			var action = 'ajax/galleryItemsAjax.php';



			var formdata = new FormData();

			formdata.append('file', file_data);

			formdata.append('text', text_data);

			if (typeof itemId !== "undefined") {

				formdata.append('id', itemId);

			}

			console.log(formdata.id);



		    $.ajax({

				url: action,

				data: formdata,

				cache: false,

			    contentType: false,

			    processData: false,

				type: 'POST',

				success: function(response) {

				    location.reload(true);

				},

				error: function (error) {

                	alert('Възникна грешка при редакция на елемент от галерията.');

              	}

			});

		}



		function removeGalleryRow(event) {

			event.preventDefault();

			var domElement = $(event.target);

			var itemId = domElement.parent().closest('.galleryRow').attr('data-object');

			var action = 'ajax/galleryDeleteAjax.php';



			var formdata = new FormData();

			formdata.append('id', itemId);



			if (typeof itemId !== "undefined") {

				

				$.ajax({

					url: action,

					data: formdata,

					cache: false,

				    contentType: false,

				    processData: false,

					type: 'POST',

					success: function(response) {

					    location.reload(true);

					},

					error: function (error) {

	                	alert('Възникна грешка при изтриването на ресурса.');

	              	}

				});

				

			} else {

				$('.galleryRow:first').addClass('firstChild');

				$('.galleryRow').removeClass('lastChild');

				if (domElement.parent().closest('.galleryRow').hasClass('lastChild')) {

					domElement.parent().closest('.galleryRow').prev('.galleryRow').addClass('lastChild');

				}

				domElement.parent().closest('.galleryRow').remove();

				$('.galleryRow:last').addClass('lastChild');

			}

		}



		function updateOrder(event) {

			event.preventDefault();

			var domElement = $(event.target);

			var direction = domElement.val();

			var countHolders = $('.galleryRow').length;

			var newItems = $('.newItem').length;

			var action = 'ajax/galleryOrderAjax.php';



			if (countHolders == 1) {

				alert('Имате 1 елемент. Невъзможна е смяна на подредбата.');

				return false;

			} else if (newItems > 0) {

				alert('Имате нови елементи в галерията. Запазете промени преди да промените подредбата.');

				return false;

			} else if (domElement.parent().closest('.galleryRow').hasClass('lastChild') && direction == 'down') {

				alert('Allowed direction : "up"');

				return false;

			} else if (domElement.parent().closest('.galleryRow').hasClass('firstChild') && direction == 'up') {

				alert('Allowed direction : "down"');

				return false;

			}



			var itemId = domElement.parent().closest('.galleryRow').attr('data-object');



			if (direction == 'up') {

				var changeItemId = domElement.parent().closest('.galleryRow').prev('.galleryRow').attr('data-object');

			} else if (direction == 'down') {

				var changeItemId = domElement.parent().closest('.galleryRow').next('.galleryRow').attr('data-object');

			}



			var formdata = new FormData();

			formdata.append('mainID', itemId);

			formdata.append('changeID', changeItemId);



		    $.ajax({

				url: action,

				data: formdata,

				cache: false,

			    contentType: false,

			    processData: false,

				type: 'POST',

				success: function(response) {

				    location.reload(true);

				},

				error: function (error) {

                	alert('Грешка при промяна на подредбата.');

              	}

			});



			// if (direction == 'up') {

			// 	domElement.parent().closest('.galleryRow').insertBefore(domElement.parent().closest('.galleryRow').prev('.galleryRow'));

			// } else if (direction == 'down') {

			// 	domElement.parent().closest('.galleryRow').next('.galleryRow').insertBefore(domElement.parent().closest('.galleryRow'));

			// }



			// $('.galleryRow').removeClass('lastChild firstChild');

   			// $('.galleryRow:last').addClass('lastChild');

   			// $('.galleryRow:first').addClass('firstChild');



		}



		CKEDITOR.replace( 'text1' , { });



		CKEDITOR.replace( 'text' , { });



   	{/literal}



</script>