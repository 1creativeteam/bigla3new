<?php
require("./loc/categories_$language.php");

function categories_main() {

 	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");
	$act1=isset($_GET['act1'])?trim($_GET['act1']):"";
	$act1=sec_check($act1,"str","");
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rez = "";

	switch($act1){
		case "":
			$rez.=categories_first();
	        break;
	    default:
			$funcname=$act."_".$act1;
			$rez.=$funcname();
		}

	return $rez;
}

function categories_first(){

	global $dbh,$vars;

	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$error=isset($_GET['error'])?trim($_GET['error']):"";
	// $page=isset($_GET['page'])?trim($_GET['page']):"";
	// $page=sec_check($page,"int", 0);
	// $perpage = 5;
	$where=isset($_GET['where'])?trim($_GET['where']):"";

	$sqlCategories = "SELECT * FROM categories ORDER BY id";
	$resultCategories = mysql_query($sqlCategories) or fail(mysql_error().$sqlCategories);

	if (mysql_num_rows($resultCategories) > 0) {
		while ($row = mysql_fetch_assoc($resultCategories)) {
		    $categories[] = $row;
		}
	}

	$smarty = new Smarty();
	$smarty->caching = false;
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	$smarty->assign("loc",$vars["loc"]);
	if ($lang=="") {
	    $lang="bg";
	}
	$smarty->assign("lang",$lang);
	$smarty->assign("error",$error);
	$smarty->assign("categories",$categories);
	// if ($categories != 0){
	// 	$smarty->assign("paging",paging(mysql_num_rows($resultCategories), $perpage));
	// }
	return $smarty->fetch("categories_first.tpl");
}

function categories_create() {

	global $dbh,$vars,$config;

	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$error=isset($_GET['error'])?trim($_GET['error']):"";
	//$error=sec_check(base64_decode($error),"str","");

	$smarty = new Smarty();
	$smarty->caching = false;
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	$smarty->assign("loc", $vars["loc"]);
	$smarty->assign("error", $error);
	$smarty->assign("action", "main.php?act=categories&act1=newadd&lang=$lang");
	$smarty->assign("maintitle", $vars["loc"]["categories"][12]);

	return $smarty->fetch("categories_add.tpl");
}

function categories_edit() {

	global $dbh,$vars,$config;

	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$error=isset($_GET['error'])?trim($_GET['error']):"";
	$rec = isset($_GET['rec'])?trim($_GET['rec']):"";

	if ($rec == '') {
		$error = $vars["loc"]["categories"][1001];

		header("Location: main.php?act=categories&error=".$error);
		exit();
	}

	$sqlCategories = "SELECT * FROM categories WHERE id=".$rec;
	$resultCategories = mysql_query($sqlCategories) or fail(mysql_error().$sqlCategories);
	$category = mysql_fetch_assoc($resultCategories);

	$smarty = new Smarty();
	$smarty->caching = false;
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	$smarty->assign("loc", $vars["loc"]);
	$smarty->assign("error", $error);
	$smarty->assign("category", $category);
	$smarty->assign("action", "main.php?act=categories&act1=update&lang=$lang&rec=$rec");
	$smarty->assign("maintitle", $vars["loc"]["categories"][9]);

	return $smarty->fetch("categories_edit.tpl");
}

function categories_newadd() {
	
	global $dbh, $vars, $config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($_GET['lang'],"str","");

  	if (($_POST["name_bg"] != '') && ($_POST["name_en"] != '')) {

	 	$sql = "INSERT INTO categories (" .
						" `name_bg`,".
						" `name_en`,".
						" `slug_bg`,".
						" `slug_en`".
						")".
			    		" VALUES (" .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s" .
			    		")";

		$sql = sprintf($sql,
				quote_smart($_POST["name_bg"]),
	  			quote_smart($_POST["name_en"]),
	  			quote_smart(url_slug($_POST["name_bg"])),
	  			quote_smart(url_slug($_POST["name_en"]))
		);

		$result = mysql_query($sql) or fail(mysql_error());
		mysql_insert_id();

  	} else {

  		$error = $vars["loc"]["categories"][1000];

		header("Location: main.php?act=categories&act1=create&lang=".$lang."&error=".$error);
		exit();

   	}

   	header("Location: main.php?act=categories&lang=".$lang);
	exit();

}

function categories_update() {
	
	global $dbh, $vars, $config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($_GET['lang'],"str","");
	$rec = isset($_GET['rec'])?trim($_GET['rec']):"";

	if ($rec == '') {
		$error = $vars["loc"]["categories"][1001];

		header("Location: main.php?act=categories&error=".$error);
		exit();
	}

  	if (($_POST["name_bg"] != '') && ($_POST["name_en"] != '')) {

  		$name_bg = $_POST["name_bg"];
  		$name_en = $_POST["name_en"];
  		$slug_bg = url_slug($_POST["name_bg"]);
  		$slug_en = url_slug($_POST["name_en"]);
  		
		$sql = "UPDATE categories SET name_bg = '".$name_bg."', name_en = '".$name_en."', slug_bg = '".$slug_bg."', slug_en = '".$slug_en."' WHERE ID = ".$rec;
		$result = mysql_query($sql) or fail(mysql_error());

  	} else {

  		$error = $vars["loc"]["categories"][1000];

		header("Location: main.php?act=categories&act1=edit&lang=".$lang."&error=".$error."&rec=".$rec);
		exit();

   	}

   	header("Location: main.php?act=categories&lang=".$lang);
	exit();

}

function categories_delete() {

	global $dbh,$vars,$config;

	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$error=isset($_GET['error'])?trim($_GET['error']):"";
	$rec = isset($_GET['rec'])?trim($_GET['rec']):"";

	if ($rec == '') {
		$error = $vars["loc"]["categories"][1001];

		header("Location: main.php?act=categories&error=".$error);
		exit();
	}

	$sql = "DELETE FROM categories WHERE id=".$rec;
	$result = mysql_query($sql) or fail(mysql_error().$sql);

	if ($result === true) {
		header("Location: main.php?act=categories&lang=".$lang);
		exit();
	}

	$error = $vars["loc"]["categories"][1002];
	header("Location: main.php?act=categories&error=".$error);
	exit();
}


?>