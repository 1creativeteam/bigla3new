<?php
function paging($all,$perpage){
	global $vars, $config;
	$sql_which=isset($_GET["page"])?trim($_GET["page"]):"";
	$sql_which=sec_check($sql_which,"int","0");
	
	$smarty = new Smarty();
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	$smarty->config_overwrite = false;
	
	$smarty->assign("loc",$vars["loc"]);
	
	$url=preg_replace('/((\?|&)page=\d+)/','',$config['conf']['site_url'].$_SERVER['REQUEST_URI']);
	if(preg_match('/\?[^?]+$/',$url)){
		$url=$url."&";
	}
	else{
		$url=$url."?";
	}
	
	$pages=ceil($all/$perpage);
	$page = (int)($sql_which/$perpage);
	
	$start = ($page<10)?0:max(0,$page-3);
	$end = min(max($start + 7,11),$pages); 
	
	if($page>max($pages-10,0)){
		$start = max($pages-10,0);
		$end = $pages;
	}
	$smarty->assign("page",$page);
	$smarty->assign("pages",$pages);
	$smarty->assign("perpage",$perpage);
	$smarty->assign("start",$start);
	$smarty->assign("end",$end);
	$smarty->assign("url",$url);
	
	return $smarty->fetch("paging.tpl");
}
?>