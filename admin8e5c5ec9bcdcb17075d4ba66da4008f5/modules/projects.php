<?php
require("./loc/projects_$language.php");

function projects_main(){
 
 	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");
	$act1=isset($_GET['act1'])?trim($_GET['act1']):"";
	$act1=sec_check($act1,"str","");
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rez = "";
    //$menu = "";
	switch($act1){
		case "":
			$rez.=projects_first();
	        break;
		case "sortui":
			$rez.=projects_sortui();
	        break;
	    case "sorthome":
			$rez.=projects_sorthome();
	        break;    
		default:
			$funcname=$act."_".$act1;
			$rez.=$funcname();
		}
	return $rez;
}

function projects_first(){
	global $dbh,$vars;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$where=isset($_GET['where'])?trim($_GET['where']):"";
	
	$smarty = new Smarty();
	$smarty->caching = false;
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	$smarty->assign("loc",$vars["loc"]);
	
	if ($lang=="") {
	    $lang="bg";
	}

	if($where){
		if(is_numeric($where)){
			$and = "id='$where'";
		}else{
			$and = "title_bg LIKE '%$where%' OR `text_bg` LIKE '%$where%' OR title_en LIKE '%$where%' OR `text_en` LIKE '%$where%'";
		}
		$sql = "SELECT * FROM projects WHERE $and ORDER BY menu";
		$result = mysql_query($sql) or fail(mysql_error().$sql);
		$projects=array();
		if (mysql_num_rows($result)>0){
			while($myrow = mysql_fetch_assoc($result)){
				$myrow["children"]=projects_array_page($myrow["id"],$lang);
				$myrow["image"]='n';
				$sql_i = "SELECT `path` FROM images WHERE stat_id='".$myrow["id"]."' AND modul='projects'";
				$result_i = mysql_query($sql_i) or fail(mysql_error().$sql_i);
				if(mysql_num_rows($result_i)>0){
					$myrow["image"]='y';
				}
				$projects[]=$myrow;
			}
		}
	}else{
		$projects=projects_array_page(0,$lang);
	}

	$sql = "SELECT id, title_bg FROM projects WHERE lang='$lang' AND `show`='y' ORDER BY menu";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$subs=array();
	if (mysql_num_rows($result)>0){
		while( $myrow = mysql_fetch_assoc($result)){
			$subs[]=$myrow;
		}
	}
	$smarty->assign("subs",$subs);

	$smarty->assign("content",$projects);
	$smarty->assign("lang",$lang);
	return $smarty->fetch("projects_first.tpl");
}

function projects_delmessage(){
  global $dbh, $vars;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
  
	$smarty->caching = false;
	
	$smarty = new Smarty();
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	
	$smarty->assign("loc",$vars["loc"]);
	
	$sql = "SELECT title_bg FROM projects where id=".$rec ;
	$result = mysql_query($sql);
	$myrow = mysql_fetch_assoc($result); 
	$smarty->assign($myrow);
	
	return $smarty->fetch("delmessage.tpl");
}

function projects_delsubcat(){
	global $dbh,$vars,$config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");	

	//if we have children reorder them hierarchy
	$sql1 = "SELECT id1, floor FROM projects where id=".$rec;
    $result1 =  mysql_query($sql1) or fail(mysql_error().$sql1);
    $myrow1 = mysql_fetch_array($result1);
    $floor = $myrow1['floor'];
	projects_delete_hierarchy($rec, $myrow1['id1'], $lang);
	
	$sql = "DELETE FROM projects WHERE id=".$rec;
	mysql_query($sql) or fail(mysql_error());

	/*
	 * delete images
	 */
	$sql="DELETE FROM images WHERE stat_id='$rec' AND modul='projects'";
	mysql_query($sql) or fail(mysql_error().$sql);
	
	$file=array();
	if (file_exists("../".$config['projects'].$rec."/")){
		if ($myDirectory = opendir("../".$config['projects'].$rec."/")){
			while($file=readdir($myDirectory)){
				unlink("../".$config['projects'].$rec."/".$file);
			}
		}
		rmdir(getcwd()."../".$config['projects'].$rec."/");
	}

	if($floor>0){
		for ($i=0; $i < $floor; $i++) { 
			$fid[] = $rec.$i;
		}
		$sql_fimg = "SELECT * FROM images where stat_id IN(".implode(',', $fid).") AND modul='floor'";
		$fimage = array();
		$result_fimg = mysql_query($sql_fimg) or fail(mysql_error().$sql_fimg);
		while($myrow_fimg = mysql_fetch_assoc($result_fimg)){
			$file=array();
			$sql="DELETE FROM images WHERE stat_id='".$myrow_fimg['stat_id']."' AND modul='floor'";
			mysql_query($sql) or fail(mysql_error().$sql);
			if (file_exists("../".$config['projects'].$rec."/floor/".$myrow_fimg['stat_id']."/")){
				if ($myDirectory = opendir("../".$config['projects'].$rec."/floor/".$myrow_fimg['stat_id']."/")){
					while($file=readdir($myDirectory)){
						unlink("../".$config['projects'].$rec."/floor/".$myrow_fimg['stat_id']."/".$file);
					}
				}
				rmdir(getcwd()."../".$config['projects'].$rec."/floor/".$myrow_fimg['stat_id']."/");
			}
		}

	}
	
	header("Location:main.php?act=projects&lang=".$lang);
	exit();
}

function projects_newcateg(){
	global $dbh,$vars,$config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$error=isset($_GET['error'])?trim($_GET['error']):"";
	$error=sec_check(base64_decode($error),"str","");
	
	$smarty = new Smarty();
	$smarty->caching = false;
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	$smarty->assign("loc",$vars["loc"]);
	$smarty->assign("towns",$config["towns"]);
	$smarty->assign("cat",projects_array(0,$lang));
	
	if ($error){
		$smarty->assign("error",$vars["loc"]["projects"][$error]);
	}

	$sql_ = "SELECT id FROM projects ORDER BY id DESC LIMIT 1";
	$result_ =  mysql_query($sql_) or fail(mysql_error().$sql_);
	if(mysql_num_rows($result_)>0){
		$arr = mysql_fetch_assoc($result_);
		$smarty->assign("last_id", $arr['id']+1);
	}

	$sql_t = "SELECT * FROM towns";
	$result_t =  mysql_query($sql_t) or fail(mysql_error().$sql_t);
	$towns = array();
	if(mysql_num_rows($result_t)>0){
		while($myrow = mysql_fetch_assoc($result_t)){
			$towns[] = $myrow; 
		}
	}
	$smarty->assign("towns",$towns);

	$smarty->assign("action","main.php?act=projects&act1=newadd&lang=$lang");
	$smarty->assign("maintitle",$vars["loc"]["projects"][12]);
	return $smarty->fetch("projects_newsedit.tpl");
}

function projects_newadd(){
	global $dbh, $config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($_GET['lang'],"str","");
  	if ($_POST["title_bg"]!=''){
  		$arr = explode('|',$_POST["cat_id"]);
  		$cat_id = $arr[0];
  		$level = $arr[1];
  		// if not create then more of one level
		if($cat_id>0){
			$sql_ = "SELECT * FROM projects where id=".$cat_id." AND id1=0";
			$result_ =  mysql_query($sql_) or fail(mysql_error().$sql_);
			if(mysql_num_rows($result_)==0){
				$_GET["error"]=base64_encode(1001);
				//return projects_newcateg();
			}
		}
	 	$sql = "INSERT INTO projects (" .
						" `title_bg`,".
						" `title_en`,".
						" `lang`,".
						" `text_bg`,".
			    		" `text_en`,".
			    		" `short_text_bg`,".
			    		" `short_text_en`,".
			    		" `keywords_bg`,".
			    		" `keywords_en`,".
			    		" `free_text_bg`,".
			    		" `free_text_en`,".
			    		" `town`,".
			    		" `sqm`,".
			    		" `floor`,".
			    		" `rooms`,".
			    		" `id1`,".
			    		" `level`,".
			    		" `show`,".
			    		" `datein`".")".
			    		" VALUES (" .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"NOW())";
		$sql = sprintf($sql,
				quote_smart($_POST["title_bg"]),
	  			quote_smart($_POST["title_en"]),
	  			quote_smart($lang),
	  			quote_smart($_POST["text_bg"]),
	  			quote_smart($_POST["text_en"]),
	  			quote_smart($_POST["short_text_bg"]),
		   		quote_smart($_POST["short_text_en"]),
		   		quote_smart($_POST["keywords_bg"]),
		   		quote_smart($_POST["keywords_en"]),
		   		quote_smart($_POST["free_text_bg"]),
		   		quote_smart($_POST["free_text_en"]),
		   		quote_smart($_POST["town"]),
	  			quote_smart($_POST["sqm"]),
	  			quote_smart($_POST["floor"]),
	  			quote_smart($_POST["rooms"]),
	  			quote_smart($cat_id),
	  			quote_smart($level),
	  			quote_smart('y')
		);
		//echo $sql;
		//exit;
		$result = mysql_query($sql) or fail(mysql_error());
		$rec=mysql_insert_id();
		// send mail to administrator
		if($rec>0 && $_SESSION['client_level']==0){
			/*$mail = new PHPMailer();
			$mail->From     = $config['mailfrom'];
			$mail->FromName = 'Site administration';
			$mail->Mailer   = "sendmail";
			$mail->Subject  = iconv("UTF-8","cp1251//TRANSLIT","Ново статия/категория за одобрение");
			$mail->CharSet	= "windows-1251";
			$mail->IsHTML(false);
		    $mail->Body    	= iconv("UTF-8","cp1251//TRANSLIT","Ново статия/категория за одобрение '".$_POST["title"]."'.");
		    $mail->AddAddress($config['adminmail']);
			$mail->Send();*/
		}
		else{
			if($rec!='' && $_FILES["image"]["name"]){
				image_new($config['projects'], $rec, $config['projectsinfo'], $_FILES["image"]["tmp_name"], $_FILES["image"]["name"], $rec, 'projects');
			}
			header("Location: main.php?act=projects&lang=".$lang);
			exit();
		}	
  	}else{
  		$_GET["error"]=base64_encode(1000);
		return projects_newcateg();
   	}	
}

function projects_newimage(){
	global $dbh,$vars,$config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$id=isset($_POST['id'])?trim($_POST['id']):"";
	$id=sec_check($id,"int","0");
	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");	
	
	image_new($config['projects'], $_POST["id"], $config['projectsinfo'], $_FILES["image"]["tmp_name"], $_FILES["image"]["name"], $id, $act);
}

function projects_resizepic(){
	global $dbh,$vars,$config;
	$id=isset($_GET['rec'])?trim($_GET['rec']):"";
	$id=sec_check($id,"int","0");
	
	resize_pic($id, $_GET["pic"], $config['projectsinfo']);
}

function projects_editpic(){
	global $dbh,$vars,$config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$id=isset($_GET['id'])?trim($_GET['id']):"";
	$id=sec_check($id,"int","0");
	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");
	$act1=isset($_GET['func'])?trim($_GET['func']):"";
	$act1=sec_check($act1,"str","");
	$do=isset($_GET['act1'])?trim($_GET['act1']):"";
	$do=sec_check($do,"str","");
	
	return edit_pic($config['projectsinfo'], $id, $act, $act1, $do, $lang);
}

function projects_delpic(){
	global $dbh,$vars,$config;
	$id=isset($_GET['id'])?trim($_GET['id']):"";
	$id=sec_check($id,"int","0");

	image_delete($config['projectsinfo'], $id);
}

function projects_delfloorpic(){
	global $dbh,$vars,$config;
	$id=isset($_GET['id'])?trim($_GET['id']):"";
	$id=sec_check($id,"int","0");

	image_delete($config['floorinfo'], $id);
}

function projects_edit(){
	global $dbh,$vars,$config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");
	
	$smarty->caching = false;
	$smarty = new Smarty();
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	$smarty->assign("loc",$vars["loc"]);
	//$smarty->assign("towns",$config["towns"]);
	$smarty->assign("cat",projects_array(0,$lang));
	
    $sql = "SELECT * FROM projects where id=".$rec;
    $result = mysql_query($sql) or fail(mysql_error());
	$myrow = mysql_fetch_assoc($result);
	
	$smarty->assign("action","main.php?act=projects&act1=updatecat&subcat=".$rec."&lang=".$lang);	
	$smarty->assign("rec",$rec);
	$smarty->assign("maintitle",$vars["loc"]["projects"][9]);
	
	$smarty->assign("dir","../",$config["projects"]);
	////images
	$sql_img = "SELECT * FROM images where stat_id='$rec' AND modul='$act'";
	$image = array();
	$result_img = mysql_query($sql_img) or fail(mysql_error().$sql_img);
	while($myrow_img = mysql_fetch_assoc($result_img)){
		$image[]=$myrow_img;
	}
	$smarty->assign("image",$image);

	$sql_t = "SELECT * FROM towns";
	$result_t =  mysql_query($sql_t) or fail(mysql_error().$sql_t);
	$towns = array();
	if(mysql_num_rows($result_t)>0){
		while($myrow_t = mysql_fetch_assoc($result_t)){
			$towns[] = $myrow_t; 
		}
	}
	$smarty->assign("towns",$towns);

	////images floor
	/*if($myrow['floor']>0){
		$fid = array();
		for ($i=0; $i <= $myrow['floor']; $i++) { 
			$fid[] = $rec.$i;
		}
		$sql_fimg = "SELECT * FROM images where stat_id IN(".implode(',', $fid).") AND modul='floor'";
		$fimage = array();
		$result_fimg = mysql_query($sql_fimg) or fail(mysql_error().$sql_fimg);
		while($myrow_fimg = mysql_fetch_assoc($result_fimg)){
			$fimage[]=$myrow_fimg;
		}
		// floors
		$sql_a = "SELECT floor FROM images where stat_id IN(".implode(',', $fid).") AND modul='floor'";
		$a = array();
		$result_a = mysql_query($sql_a) or fail(mysql_error().$sql_a);
		while($myrow_a = mysql_fetch_assoc($result_a)){
			$a[]=$myrow_a['floor'];
		}
		$app = array();
		for ($j=1; $j <= $myrow['floor']; $j++) { 
			if(in_array($j, $a)){
				continue;
			}
			$apps[$j] = $j;
		}
		$smarty->assign("floor_app",$apps);
		$smarty->assign("fimgenl",$config['floorinfo'][0]["enl"]);
		$smarty->assign("floor_images",$fimage);
		$smarty->assign("floor_count", count($myrow['floor']));
	}*/

	$smarty->assign("imgenl",$config['projectsinfo'][0]["enl"]);
	$myrow["max_file"]=ini_get('upload_max_filesize');
	$smarty->assign($myrow);
	
	return $smarty->fetch("projects_newsedit.tpl");
}

function projects_updatecat(){
	global $dbh, $config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$subcat=isset($_GET['subcat'])?trim($_GET['subcat']):"";
	$subcat=sec_check($subcat,"int","0");
	
	$arr = explode('|',$_POST["cat_id"]);
	$cat_id = $arr[0];
	$level = $arr[1];
	if($cat_id!=$_POST["old_cat_id"]){
		
		// if not create then more of one level
		if($cat_id>0){
			$sql_ = "SELECT * FROM projects where id=".$cat_id." AND id1=0";
			$result_ =  mysql_query($sql_) or fail(mysql_error().$sql_);
			if(mysql_num_rows($result_)==0){
				//header("Location:main.php?act=projects&act1=edit&rec=$subcat&lang=".$lang."&error=".base64_encode(1001));
				//exit;	
			}
		}

		$sql = sprintf("UPDATE projects SET " .
		    		" title_bg=%s,".
		    		" text_bg=%s,".
		    		" title_en=%s,".
		    		" text_en=%s,".
		    		" short_text_en=%s,".
		    		" short_text_bg=%s,".
		    		" long_title=%s,".
		    		" keywords_bg=%s,".
		    		" keywords_en=%s,".
		    		" free_text_bg=%s,".
		    		" free_text_en=%s,".
		    		" showimages=%s,".
		    		" id1=%s,".
		    		" level=%s,".
		    		" town=%s,".
		    		" sqm=%s,".
		    		" rooms=%s,".
		    		" `floor`=%s".
		    		" WHERE id=".
		    		quote_smart($subcat),
		    		quote_smart($_POST["title_bg"]),
		    		quote_smart($_POST["text_bg"]),
		   			quote_smart($_POST["title_en"]),
		    		quote_smart($_POST["text_en"]),
		   			quote_smart($_POST["short_text_bg"]),
		   			quote_smart($_POST["short_text_en"]),
		   			quote_smart($_POST["long_title"]),
		   			quote_smart($_POST["keywords_bg"]),
		   			quote_smart($_POST["keywords_en"]),
		   			quote_smart($_POST["free_text_bg"]),
		   			quote_smart($_POST["free_text_en"]),
		   			quote_smart($_POST["showimages"]),
		   			quote_smart($cat_id),
		   			quote_smart($level),
		   			quote_smart($_POST["town"]),
		   			quote_smart($_POST["sqm"]),
		   			quote_smart($_POST["rooms"]),
		   			quote_smart($_POST["floor"])
		);
		mysql_query($sql)or fail(mysql_error().$sql);
	}
	else{
		$sql = sprintf("UPDATE projects SET " .
		    		" title_bg=%s,".
		    		" text_bg=%s,".
		    		" title_en=%s,".
		    		" text_en=%s,".
		    		" short_text_bg=%s,".
		    		" short_text_en=%s,".
		    		" long_title=%s,".
		    		" keywords_bg=%s,".
		    		" keywords_en=%s,".
		    		" free_text_bg=%s,".
		    		" free_text_en=%s,".
		    		" showimages=%s,".
		    		" town=%s,".
		    		" sqm=%s,".
		    		" rooms=%s,".
		    		" `floor`=%s".
		    		" WHERE id=".
		    		quote_smart($subcat),
		    		quote_smart($_POST["title_bg"]),
		    		quote_smart($_POST["text_bg"]),
		   			quote_smart($_POST["title_en"]),
		    		quote_smart($_POST["text_en"]),
		   			quote_smart($_POST["short_text_bg"]),
		   			quote_smart($_POST["short_text_en"]),
		   			quote_smart($_POST["long_title"]),
		   			quote_smart($_POST["keywords_bg"]),
		   			quote_smart($_POST["keywords_en"]),
		   			quote_smart($_POST["free_text_bg"]),
		   			quote_smart($_POST["free_text_en"]),
		   			quote_smart($_POST["showimages"]),
		   			quote_smart($_POST["town"]),
		   			quote_smart($_POST["sqm"]),
		   			quote_smart($_POST["rooms"]),
		   			quote_smart($_POST["floor"])
		);
		mysql_query($sql)or fail(mysql_error().$sql);
	}
	if($subcat!='' && $_FILES["image"]["name"]){
		image_new($config['projects'], $subcat, $config['projectsinfo'], $_FILES["image"]["tmp_name"], $_FILES["image"]["name"], $subcat, 'projects');
	}
	header("Location:main.php?act=projects&lang=".$lang."");
	exit();
	
}

function projects_showarticle(){
	global $dbh;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	
	$sql = "UPDATE projects SET `show`='y' WHERE id=".$rec;
	mysql_query($sql) or fail(mysql_error().$sql);

	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function projects_hidearticle(){
	global $dbht;
	$rec=sec_check($_GET['rec'],"int","0");
	
	$sql = "UPDATE projects SET `show`='n' WHERE id=".$rec;
	mysql_query($sql) or fail(mysql_error().$sql);

	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function projects_showmenu(){
	global $dbh;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	
	$sql = "SELECT MAX(c1.menu)+1 AS menu 
			FROM projects AS c
			LEFT JOIN projects AS c1 ON (c.id1=c1.id1 AND c.lang=c1.lang)
			WHERE c.id='$rec' AND c.lang='$lang'";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$myrow = mysql_fetch_assoc($result);
	$menu = $myrow["menu"];

	$sql = "UPDATE projects SET `menu`='$menu' WHERE id=".$rec;
	mysql_query($sql) or fail(mysql_error().$sql);

	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function projects_hidemenu(){
	global $dbh;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	
	$sql = "UPDATE projects SET `menu`='0' WHERE id=".$rec;
	mysql_query($sql) or fail(mysql_error().$sql);	

	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function projects_show(){
	global $dbh;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	if($_SESSION['client_level']==1){
		$sql = "UPDATE projects SET `show`='y' WHERE id=".$rec;
		mysql_query($sql) or fail(mysql_error().$sql);
	}
	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function projects_hide(){
	global $dbht;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	if($_SESSION['client_level']==1){
		$sql = "UPDATE projects SET `show`='n' WHERE id=".$rec;
		mysql_query($sql) or fail(mysql_error().$sql);
	}
	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function projects_sortui(){
	global $dbh,$vars;
	$lang=sec_check($_GET['lang'],"str","");
	
	$smarty = new Smarty();
	$smarty->caching = false;
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	
	$sql = "SELECT * FROM projects WHERE lang='".$lang."' AND menu>0 ORDER BY menu";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$i=0;
	$sort=array();
	if (mysql_num_rows($result)>0){  
		while( $myrow = mysql_fetch_assoc($result)){
			$sort[$i]=$myrow;
			$i++;
		}
	}
	$smarty->assign("action","main.php?act=projects&amp;act1=sort");
	$smarty->assign("sort",$sort);

	return $smarty->fetch("content_sort.tpl");
}

function projects_sort(){
	global $dbh;

	$i=1;
	foreach($_POST["item"] as $id){
		$sql = "UPDATE projects SET menu='$i' where id='$id'";
		mysql_query($sql) or fail(mysql_error().$sql);
		$i++;
	}
	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();	
}

function projects_sorthome(){
	global $dbh,$vars;
	$lang=sec_check($_GET['lang'],"str","");
	$rec=sec_check($_GET['rec'],"str","");
	
	$smarty->caching = false;
	$smarty = new Smarty();
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	
	$sql = "SELECT * FROM projects where id1=".$rec." and lang='".$lang."' AND home!=0 ORDER BY home";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$i=0;
	$sort=array();
	if (mysql_num_rows($result)>0){  
		while( $myrow = mysql_fetch_assoc($result)){
			$sort[$i]=$myrow;
			$i++;
		}
	}
	$smarty->assign("sort",$sort);
	return $smarty->fetch("projects_sort.tpl");
}

function projects_array($id,$lang){
	global $dbh,$vars;
	
	$sql = "SELECT * FROM projects WHERE id1='$id' AND lang='$lang' ORDER BY menu";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$subs=array();
	if (mysql_num_rows($result)>0){
		while( $myrow = mysql_fetch_assoc($result)){
			$myrow["children"]=projects_array($myrow["id"],$lang);
			$subs[]=$myrow;
		}
	}
	return $subs;
}

function projects_array_page($id,$lang){
	global $dbh,$vars;
	
	$sql = "SELECT * FROM projects WHERE id1='$id' AND lang='$lang' ORDER BY menu";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$subs=array();
	$dirArray=array();
	if (mysql_num_rows($result)>0){
		while( $myrow = mysql_fetch_assoc($result)){
			$myrow["children"]=projects_array_page($myrow["id"],$lang);
			$myrow["image"]='n';
			$sql_i = "SELECT `path` FROM images WHERE stat_id='".$myrow["id"]."' AND modul='projects'";
			$result_i = mysql_query($sql_i) or fail(mysql_error().$sql_i);
			if(mysql_num_rows($result_i)>0){
				$myrow["image"]='y';
			}
			$subs[]=$myrow;
		}
	}
	return $subs;
}

function projects_delete_hierarchy($id,$id1,$lang){
	global $dbh,$vars;
	
	$sql = "SELECT id, id1 FROM projects WHERE id1='$id' AND lang='$lang'";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	if (mysql_num_rows($result)>0){
		while( $myrow = mysql_fetch_assoc($result)){
			$children=projects_array($myrow["id"],$lang);
			if(count($children)==0){
				$sql = sprintf("UPDATE projects SET id1=%s WHERE id=".quote_smart($myrow['id']), quote_smart($id1));
				mysql_query($sql) or fail(mysql_error());
				projects_delete_hierarchy($myrow["id"], $myrow["id1"], $lang);
			}else{
				$sql = sprintf("UPDATE projects SET id1=%s WHERE id=".quote_smart($myrow['id']), quote_smart($id1));
				mysql_query($sql) or fail(mysql_error());
			}
		}
		return true;
	}
	return false;
}