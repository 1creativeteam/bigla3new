<?php
require("./loc/home_$language.php");

function home_main(){
 
 	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");
	$act1=isset($_GET['act1'])?trim($_GET['act1']):"";
	$act1=sec_check($act1,"str","");
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rez = "";
    //$menu = "";
	switch($act1){
		case "":
			$rez.=home_first();
	        break;
		case "sortui":
			$rez.=home_sortui();
	        break;
	    default:
			$funcname=$act."_".$act1;
			$rez.=$funcname();
		}
	return $rez;
//	die($rez);
}

function home_first(){
	global $dbh,$vars;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$page=isset($_GET['page'])?trim($_GET['page']):"";
	$page=sec_check($page,"int", 0);
	$where=isset($_GET['where'])?trim($_GET['where']):"";
	
	$smarty = new Smarty();
	$smarty->caching = false;
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	$smarty->assign("loc",$vars["loc"]);
	
	if ($lang=="") {
	    $lang="bg";
	}
	
	$smarty->assign("lang",$lang);
	
	$and="";
	if($where){
		if(is_numeric($where)){
			$and = "WHERE id='$where'";
		}else{
			$and = "WHERE title_bg LIKE '%$where%' OR title_en LIKE '%$where%'";
		}
	}

	$sql = "SELECT * FROM home $and";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$all=mysql_num_rows($result);
	/*
	 * paging
	 */
	$perpage=20;
	if ($all!=0){
		$smarty->assign("paging",paging($all, $perpage));
	}

	$sql = "SELECT * FROM home $and ORDER BY menu LIMIT $page, $perpage";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$home=array();
	if (mysql_num_rows($result)>0){
		while( $myrow = mysql_fetch_assoc($result)){
			$myrow["image"]='n';
			$sql_i = "SELECT `path` FROM images WHERE stat_id='".$myrow["id"]."' AND modul='home'";
			$result_i = mysql_query($sql_i) or fail(mysql_error().$sql_i);
			if(mysql_num_rows($result_i)>0){
				$myrow["image"]='y';
			}
			$home[]=$myrow;
		}
	}
	$smarty->assign("home",$home);

	return $smarty->fetch("home_first.tpl");
}

function home_delmessage(){
  global $dbh, $vars;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
  
	$smarty->caching = false;
	
	$smarty = new Smarty();
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	
	$smarty->assign("loc",$vars["loc"]);
	
	$sql = "SELECT * FROM home where id=".$rec ;
	$result = mysql_query($sql);
	$myrow = mysql_fetch_assoc($result); 
	$smarty->assign($myrow);
	
	return $smarty->fetch("delmessage.tpl");
}

function home_delsubcat(){
	global $dbh,$vars,$config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");	

	$sql = "DELETE FROM home WHERE id=".$rec;
	$result = mysql_query($sql) or fail(mysql_error());

	/*
	 * delete images
	 */
	$sql="DELETE FROM images WHERE stat_id='$rec' AND modul='home'";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	
	$file=array();
	if (file_exists("../".$config['home'].$rec."/")){
		if ($myDirectory = opendir("../".$config['home'].$rec."/")){
			while($file=readdir($myDirectory)){
				unlink("../".$config['home'].$rec."/".$file);
			}
		}
		rmdir(getcwd()."/../".$config['home'].$rec."/");
	}
	
	header("Location:main.php?act=home&lang=".$lang);
	exit();
}

function home_newcateg(){
	global $dbh,$vars;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$error=isset($_GET['error'])?trim($_GET['error']):"";
	$error=sec_check(base64_decode($error),"str","");
	
	$smarty->caching = false;
	
	$smarty = new Smarty();
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	$smarty->assign("loc",$vars["loc"]);
	
	if ($error){
		$smarty->assign("error",$vars["loc"]["home"][$error]);
	}
	$smarty->assign("action","main.php?act=home&act1=newadd&lang=$lang");
	$smarty->assign("maintitle",$vars["loc"]["home"][12]);
	return $smarty->fetch("home_edit.tpl");
}

function home_newadd(){
	global $dbh, $config;

	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($_GET['lang'],"str","");
  	if ($_POST["title_bg"]!=''){

  		$video_type = $videoId = '';
		if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $_POST['video'], $match)) {
			$videoId = $match[1];
			//$getUrl = 'http://i4.ytimg.com/vi/'.$videoId.'/hqdefault.jpg';
			$video_type = 1;
		} elseif (preg_match('/https?:\/\/(?:www\.)?vimeo.com\/(?:channels\/|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/', $_POST['video'], $match)) {
			$videoId = $match[3];
			//$vmeoApiUrl = 'http://vimeo.com/api/v2/video/'.$videoId.'.json';
			//$vmeoApi = json_decode(file_get_contents($vmeoApiUrl));
			//$getUrl = $vmeoApi[0]->thumbnail_large;
			$video_type = 2;
		}

	 	$sql = "INSERT INTO home (" .
						" `title_bg`,".
						" `title_en`,".
						" `video_url`,".
						" `video_type`,".
						" `show`,".
						" `link_bg`,".
						" `link_en`".")".
			    		" VALUES (" .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s)";
		$sql = sprintf($sql,
				quote_smart($_POST["title_bg"]),
	  			quote_smart($_POST["title_en"]),
	  			quote_smart($videoId),
	  			quote_smart($video_type),
	  			quote_smart(1),
	  			quote_smart($_POST["link_bg"]),
	  			quote_smart($_POST["link_en"])
		);
		
		mysql_query($sql) or fail(mysql_error());
		$id=mysql_insert_id();
		if($id!='' && $_FILES["image"]["name"] && $_POST['video']==''){
			image_new($config['home'], $id, $config['homeinfo'], $_FILES["image"]["tmp_name"], $_FILES["image"]["name"], $id, 'home');
		}
		header("Location: main.php?act=home&lang=".$lang);
		exit();
  	}else{
  		$_GET["error"]=base64_encode(1000);
		return home_newcateg();
   	}	
}

function home_newimage(){
	global $dbh,$vars,$config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$id=isset($_POST['id'])?trim($_POST['id']):"";
	$id=sec_check($id,"int","0");
	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");	
	
	image_new($config['home'], $_POST["id"], $config['homeinfo'], $_FILES["image"]["tmp_name"], $_FILES["image"]["name"], $id, $act);
}

function home_resizepic(){
	global $dbh,$vars,$config;
	$id=isset($_GET['rec'])?trim($_GET['rec']):"";
	$id=sec_check($id,"int","0");

	resize_pic($id, $_GET["pic"], $config['homeinfo']);
}

function home_editpic(){
	global $dbh,$vars,$config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$id=isset($_GET['id'])?trim($_GET['id']):"";
	$id=sec_check($id,"int","0");
	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");
	$act1=isset($_GET['func'])?trim($_GET['func']):"";
	$act1=sec_check($act1,"str","");
	$do=isset($_GET['act1'])?trim($_GET['act1']):"";
	$do=sec_check($do,"str","");
	
	return edit_pic($config['homeinfo'], $id, $act, $act1, $do, $lang);
}

function home_delpic(){
	global $dbh,$vars,$config;
	$id=isset($_GET['id'])?trim($_GET['id']):"";
	$id=sec_check($id,"int","0");

	image_delete($config['homeinfo'], $id);
}

function home_edit(){
	global $dbh,$vars,$config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");
	
	$smarty->caching = false;
	$smarty = new Smarty();
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	$smarty->assign("loc",$vars["loc"]);
	
	$sql = "SELECT * FROM home where id=".$rec;
    $result = mysql_query($sql) or fail(mysql_error());
	$myrow = mysql_fetch_assoc($result);
	
	$smarty->assign("action","main.php?act=home&act1=updatecat&id=".$rec."&lang=".$lang);	
	$smarty->assign("rec",$rec);
	$smarty->assign("maintitle",$vars["loc"]["home"][9]);
	
	$smarty->assign("dir","../",$config["home"]);
	////images
	$sql_img = "SELECT * FROM images where stat_id='$rec' AND modul='$act'";
	$image = array();
	$result_img = mysql_query($sql_img) or fail(mysql_error().$sql_img);
	while($myrow_img = mysql_fetch_assoc($result_img)){
		$image[]=$myrow_img;
	}
	$smarty->assign("image",$image);
	$smarty->assign("imgenl",$config['homeinfo'][0]["enl"]);
	$myrow["max_file"]=ini_get('upload_max_filesize');
	if($myrow['video_type']==1){
		$myrow['video_path'] = 'http://youtu.be/'.$myrow['video_url'];
	}elseif($myrow['video_url']!='' && $myrow['video_type']==2){
		$myrow['video_path'] = 'http://vimeo.com/'.$myrow['video_url'];
	}
	$smarty->assign($myrow);
	
	return $smarty->fetch("home_edit.tpl");
}

function home_updatecat(){
	global $dbh, $config;
	$id=isset($_GET['id'])?trim($_GET['id']):"";
	$id=sec_check($id,"int","0");
	
	/*if($data['VIDEO_URL']!='' && $data['VIDEO_TYPE']==1){
		$image_path = $base_img_path = 'http://i4.ytimg.com/vi/'.$data['VIDEO_URL'].'/hqdefault.jpg';
		$fancy_path = 'http://youtu.be/'.$data['VIDEO_URL'];
		$type = 'youtube';
	}elseif($data['VIDEO_URL']!='' && $data['VIDEO_TYPE']==2){
		$vmeoApiUrl = 'http://vimeo.com/api/v2/video/'.$data['VIDEO_URL'].'.json';
		$vmeoApi = json_decode(file_get_contents($vmeoApiUrl));
		$image_path = $base_img_path = $vmeoApi[0]->thumbnail_large;
		$fancy_path = 'http://vimeo.com/'.$data['VIDEO_URL'];
		$type = 'vimeo';
	}*/
	$video_type = $videoId = '';
	if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $_POST['video'], $match)) {
		$videoId = $match[1];
		//$getUrl = 'http://i4.ytimg.com/vi/'.$videoId.'/hqdefault.jpg';
		$video_type = 1;
	} elseif (preg_match('/https?:\/\/(?:www\.)?vimeo.com\/(?:channels\/|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/', $_POST['video'], $match)) {
		$videoId = $match[3];
		//$vmeoApiUrl = 'http://vimeo.com/api/v2/video/'.$videoId.'.json';
		//$vmeoApi = json_decode(file_get_contents($vmeoApiUrl));
		//$getUrl = $vmeoApi[0]->thumbnail_large;
		$video_type = 2;
	}
	
	$sql = sprintf("UPDATE home SET " .
	    		" title_bg=%s,".
	    		" title_en=%s,".
	    		" video_url=%s,".
	    		" video_type=%s,".
	    		" link_bg=%s,".
	    		" link_en=%s".
	    		" WHERE id=".
	    		quote_smart($id),
	    		quote_smart($_POST["title_bg"]),
	    		quote_smart($_POST["title_en"]),
	    		quote_smart($videoId),
	    		quote_smart($video_type),
	    		quote_smart($_POST["link_bg"]),
	    		quote_smart($_POST["link_en"])
	);
	mysql_query($sql)or fail(mysql_error().$sql);
	if($id!='' && $_FILES["image"]["name"] && $_POST['video']==''){
		image_new($config['home'], $id, $config['homeinfo'], $_FILES["image"]["tmp_name"], $_FILES["image"]["name"], $id, 'home');
	}
	header("Location:main.php?act=home&lang=".$lang."");
	exit();
}

function home_showmenu(){
	global $dbh;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	
	$sql = "SELECT MAX(menu)+1 AS menu FROM home WHERE id='$rec'";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$myrow = mysql_fetch_assoc($result);
	$menu = $myrow["menu"];
	
	$sql = "UPDATE home SET `menu`='$menu' WHERE id=".$rec;
	mysql_query($sql) or fail(mysql_error().$sql);

	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function home_hidemenu(){
	global $dbh;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	
	$sql = "UPDATE home SET `menu`='0' WHERE id=".$rec;
	mysql_query($sql) or fail(mysql_error().$sql);	

	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function home_show(){
	global $dbh;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	
	if($_SESSION['client_level']==1){
		$sql = "UPDATE home SET `show`=1 WHERE id=".$rec;
		mysql_query($sql) or fail(mysql_error().$sql);
	}
	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function home_hide(){
	global $dbht;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");

	if($_SESSION['client_level']==1){
		$sql = "UPDATE home SET `show`=0 WHERE id=".$rec;
		mysql_query($sql) or fail(mysql_error().$sql);
	}
	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function home_sortui(){
	global $dbh,$vars;
	$lang=sec_check($_GET['lang'],"str","");
	
	$smarty = new Smarty();
	$smarty->caching = false;
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	
	$sql = "SELECT * FROM home WHERE menu>0 ORDER BY menu";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$sort=array();
	if (mysql_num_rows($result)>0){  
		while( $myrow = mysql_fetch_assoc($result)){
			$sort[]=$myrow;
		}
	}
	$smarty->assign("action","main.php?act=home&amp;act1=sort");
	$smarty->assign("sort",$sort);
	return $smarty->fetch("content_sort.tpl");
}

function home_sort(){
	global $dbh;
	$i=1;
	foreach($_POST["item"] as $id){
		$sql = "UPDATE home SET menu='$i' where id='$id'";
		mysql_query($sql) or fail(mysql_error().$sql);
		$i++;
	}
	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();	
}
