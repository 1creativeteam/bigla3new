<?php		
require("./loc/users_$language.php");	
function users_main(){
	
	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");
	$act1=isset($_GET['act1'])?trim($_GET['act1']):"";
	$act1=sec_check($act1,"str","");
	$rez = "";
	switch ($act1){
	case "":
		$rez.=users_all();
        break;
	default:
		if($_SESSION['client_level']==1){
			$funcname=$act."_".$act1;
			$rez.=$funcname();
		}
	}
	return $rez;
}
function users_new(){
	global $users_level;
	$content = "";
	$content.="<div class=\"form\"><h1 class='sifr'>Потребители</h1>";
    $content.="<form action=\"?act=users&act1=addnew\" method=\"POST\">";
    $content.="<table border=0 class=events align=left>";
    $content.="<tr><td>Име</td><td><input type=text name=mainname value=></td></tr>";
    $content.="<tr><td>Потребител</td><td><input type=text name=usrname maxlength=12 value=> максимум 12 символа</td></tr>";
    $content.="<tr><td>Парола</td><td><input type=password name=password maxlength=12 value=> максимум 12 символа</td></tr>";
    $content.="<tr><td>Тип на акаунта</td><td><select name=level>";
	foreach ($users_level as $key=>$text){
		$content.="<option value=$key>$text</option>";
	}
    $content.="</table>";
    $content.="<div style=\"clear:both;text-align:right;margin-top:10px;\"><input type=\"submit\" class=\"button\" value=\"Създай нов потребител\" /></div></form>";
    $content.="</div>";
	return $content;
}
function users_addnew(){
	global $dbh;
	
	$tmp1=$_POST['mainname'];
	$tmp2=$_POST['usrname'];
	$tmp3=$_POST['password'];
	$tmp4=$_POST['level'];
	$content = "";
   if (($tmp1!="") or ($tmp2!="") or ($tmp3!="")or ($tmp4!="")){

        // proverka za syshtestvuvasht takyv zapis
		$sql = "SELECT * FROM users WHERE username='$tmp2'";
        $result=mysql_query($sql);
		if (mysql_num_rows($result)!=0){
			header("Refresh:2; url=main.php?act=users");
			$content.= "<h2>Съобщение</h2><p color=red>Съществува потребител с име $tmp2!</p>";
        } else {
			$sql = "INSERT INTO users (name,username,pass,level) VALUES ('$tmp1','$tmp2','".md5($tmp3)."','$tmp4')";
			$result=mysql_query($sql);

	         $sql = "SELECT * FROM users where name='".$tmp1."'";
	         $result=mysql_query($sql);
			 if(mysql_num_rows($result)!=0){
		   	    header("Refresh:2; url=main.php?act=users");
			 	$content.= "<h2>Съобщение</h2><p>Потребителя е добавен</p>";
		    }else{
		    	header("Refresh:2; url=main.php?act=users");
				$content.= "<h2>Съобщение</h2><p>Има грешка при записа на данните. Опитайте отново!</p>";
			}
		}
    } else {
    	header("Refresh:2; url=main.php?act=users");
		$content.="<h2>Съобщение</h2><p>Трябва да попълните всички полета!<br>";
		$content.="Опитайте отново!<br></p>";
    }
	return $content;
}	
function users_edit(){
	global $dbh;
	$rec=$_GET['rec'];
	$sql = "SELECT * FROM users where user_id=".$rec;
	$result=mysql_query($sql);
	$content = "";
	$content.="<div class=\"form\">" .
	"<h1 class='sifr'>" .
	"<div style=\"float:right;font-size:12px;\"></div>" .
	"Потребители</h1>";
	if (mysql_num_rows($result)>0){
			$myrow=mysql_fetch_object($result);
	        //$content.= $users_error[$error];
	        $content.= "<form action=\"main.php?act=users&act1=update&rec=$rec\" method=\"POST\">";
	        $content.= "<table border=0 align=left class=events>";
	        $content.= "<tr><td>Име</td><td><input type=text name=mainname value=\"".$myrow->name."\"></td></tr>";
	        $content.= "<tr><td>Потребител</td><td><input type=text name=usrname maxlength=12 value=".$myrow->username."> максимум 12 символа</td></tr>";
	        $content.= "<tr><td>Парола</td><td><input type=password name=password maxlength=12 value=> максимум 12 символа</td></tr></table>";
	        $content.= "<div style=\"clear:both;text-align:right;margin-top:10px;\"><input type=\"submit\" class=\"button\" value=\"Промени данни\"></div>";
	        $content.= "</form>";
     }
	$content.="</div>";
	return $content;
}

function users_delete(){
	global $dbh;
	
	$rec=$_GET['rec'];
	$sql = "DELETE FROM users WHERE user_id=".$rec;
	$result=mysql_query($sql);
	header("Location:main.php?act=users");
	exit();
}

function users_update(){
	global $dbh;
	
	$rec=$_GET['rec'];
	$mainname=$_POST['mainname'];
	$usrname=$_POST['usrname'];
	$password=$_POST['password'];

	if(($mainname!="")and($usrname!="")and($password!="")){
		$sql = "UPDATE users SET name='".$mainname."',username='".$usrname."', pass='".md5($password)."' WHERE user_id=".$rec;
		$result=mysql_query($sql);
		/*
		 * logout user
		 */
		logout();
	}elseif(($mainname!="")and($usrname!="")and($password=="")){
		$sql = "UPDATE users SET name='".$mainname."',username='".$usrname."' WHERE user_id=".$rec;
		$result=mysql_query($sql);
		header("Location:main.php?act=users");
		exit();
	}else{
		header("Location:main.php?act=users&act1=edit&rec=$rec&error=1");
		exit();
	}
}	
function users_all(){
	global $dbh,$users_level;
	
	$sql = "SELECT * FROM users ORDER BY user_id ASC";
    $result=mysql_query($sql);
    $content = "";
	$content.="<div class=\"form\">" .
		"<h1 class='sifr'>" .
		($_SESSION['client_level']==1?"<div style=\"float:right;font-size:12px;\"><a href=\"?act=users&act1=new\" class=\"but new\" title=\"Нов потребител\"></a></div>":'').
		"Потребители</h1>";
	if (mysql_num_rows($result)>0){
	    $content.="<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">";
	    $content.="<tr><td width=10%>Име</td><td width=*>Потребител</td><td>Тип</td></tr>";
		while ($myrow=mysql_fetch_object($result)){
	    	$content.="<tr><td>".$myrow->name."</td><td>".$myrow->username."</td><td><b>".$users_level[$myrow->level]."</b></td>".($_SESSION['client_level']==1?"<td width=1%><a href=\"?act=users&act1=edit&rec=".$myrow->user_id."\" class=\"but edit\" title=\"редактирай\"></a></td><td width=1%><a href=\"?act=users&act1=delete&rec=".$myrow->user_id."\" class=\"but delete\" title=\"изтрий\"></a></td>":'')."</tr>";
		}
	}  
	$content.= "</table><br>";
	$content.="</div>";
	return $content;
}
?>
