<?php
function image_main(){
 
 	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");
	$do=isset($_GET['do'])?trim($_GET['do']):"";
	$do=sec_check($do,"str","");
	$rez = "";
    switch($do){
		case "":
			$rez.="";
	        break;
		default:
			$funcname=$act."_".$do;
			$rez.=$funcname();
		}
	return $rez;
}

function image_new($dir='', $statid, $imagesizes, $tmp_name='', $filename='', $id=0, $act='', $origsize=800, $floor=0){
	global $dbh,$vars;
	ini_set("error_reporting", E_ALL);
	ini_set("display_errors",1);
	
	umask(0);
	if (!is_dir("../".$dir.$statid)){
		mkdir("../".$dir.$statid);
	}
	if(is_uploaded_file($tmp_name) ){
		$image = strtolower($filename);
		$image=time()."_".mt_rand(0,1000).substr($image,strrpos($image,"."));	
		$imagenew=$image;
		$image="../".$dir.$statid."/".$image;
		move_uploaded_file($tmp_name,$image);

		$size=GetImageSize($image);
		/*
		 * Convert image to RGB jpeg
		 */
		switch($size["mime"]){
			case "image/jpeg":
				$ext = '.jpg';
                $im=imagecreatefromjpeg($image) or fail('Could not create image object!');
				$imagename=$dir.$id."/".str_replace(".jpg","",$imagenew);
				break;
			case "image/png":     
				$ext = '.png';
                $im=imagecreatefrompng($image) or fail('Could not create image object!');
				$imagename=$dir.$id."/".str_replace(".png","",$imagenew);
				break;
			case "image/gif":
				$ext = '.gif';
                $im=imagecreatefromgif($image) or fail('Could not create image object!');
				$imagename=$dir.$id."/".str_replace(".gif","",$imagenew);
				break;
			case "image/bmp" :
				$ext = '.bmp';
                $im=imagewbmp($image) or fail('Could not create image object!');
				$imagename=$dir.$id."/".str_replace(".bmp","",$imagenew);
               break;
            default:
				return "<h2>Грешка</h2><p>Снимката не може да бъде качена, не разпознат файл тип - ".$size['mime']."</p>";
		}
		
		if($origsize!=""){
			if($size[0]>$origsize){
				$th_x=$origsize;
				$th_y=round($size[1]*$th_x/$size[0]);
			}
			else{
				$th_x=$size[0];
				$th_y=$size[1];
			}
		
			$thumb=imagecreatetruecolor($th_x,$th_y);
			imagecopyresampled($thumb,$im,0,0,0,0,$th_x,$th_y,$size[0],$size[1]);
			$image=substr(strtolower($image),0,strrpos(strtolower($image),".")).".jpg";
			imagejpeg($thumb,$image,100);
			imagedestroy($thumb);

		}
		foreach($imagesizes as $k=>$v){
			if($v['fit']==true){
				
				if($size[0]>=$v['width'] && $v['width']>0){
					$th_x=$v['width'];
					$th_y=round($size[1]*$th_x/$size[0]);
				}elseif($size[1]>=$v['height'] && $v['height']>0){
					$th_y=$v['height'];
					$th_x=round($size[0]*$th_y/$size[1]);
      			}else{
					$th_x=$size[0];
					$th_y=$size[1];
				}

				$sx=0;
				$sy=0;
				$w=$size[0];
				$h=$size[1];
				
			}
			elseif($v['crop']==true){

				$th_x = $v['width'];
				$th_y = $v['height'];
				
				if($size[0]/$size[1]>=$th_x/$th_y){
					$w=round($size[1]*$th_x/$th_y);
					$h=$size[1];
					$sx=round(($size[0]-$w)/2);
					$sy=0;
				}
				else{
					$w=$size[0];
					$h=round($size[0]*$th_y/$th_x);
					$sx=0;
					$sy=(round($size[1]-$h)/2);
				}
			}
			else{
				if($size[0]>$v['width']){
					$th_x=$v['width'];
					$th_y=round($size[1]*$th_x/$size[0]);
				}
				else{
					$th_x=$size[0];
					$th_y=$size[1];
      			}
				if($th_y>$v['height']){
					$th_y=$v['height'];
					$th_x=round($size[0]*$th_y/$size[1]);
				}
				$sx=0;
				$sy=0;
				$w=$size[0];
				$h=$size[1];
				//echo "ok".$size[0].$size[1].$th_x.$th_y;			
			}
			$thumb=imagecreatetruecolor($th_x,$th_y);

			if($size["mime"] == 'image/png') {
				imagealphablending($thumb, false);
		      	imagesavealpha($thumb,true);
		      	$transparent = imagecolorallocatealpha($thumb, 255, 255, 255, 127);
		      	imagefilledrectangle($thumb, 0, 0, $th_x, $th_y, $transparent);
	        }
        	
        	imagecopyresampled($thumb,$im,0,0,0,0,$th_x,$th_y,$w,$h);
			//imagecopyresampled($thumb,$im,0,0,$sx,$sy,$th_x,$th_y,$w,$h);
			switch($size["mime"])
            {
                case "image/gif"	:
                	imagegif($thumb, str_replace(".jpg",$v['enl'].".gif",$image), 100);
                    break;
                case "image/jpeg" :
                   	imagejpeg($thumb, str_replace(".jpg",$v['enl'].".jpg",$image), 100);
                    break;
                case "image/png" :
                	imagepng($thumb, str_replace(".jpg",$v['enl'].".png",$image), 9);
                    break;
                case "image/bmp" :
                   	imagewbmp($thumb, str_replace(".jpg",$v['enl'].".bmp",$image), 100);
                    break;
                default 		:
               		imagejpeg($thumb,str_replace(".jpg",$v['enl'].".jpg",$image),100);
               	 break;
            }
			
			imagedestroy($thumb);
		}
		$sql=sprintf("INSERT INTO images(path,modul,stat_id,title,text,author, floor, ext) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)",
				quote_smart($imagename),
				quote_smart($act),
				quote_smart($id),
				quote_smart($_POST["title"]),
				quote_smart($_POST["text"]),
				quote_smart($_POST["author"]),
				quote_smart($floor),
				quote_smart($ext)
				);
		//die($sql);
		$tr = mysql_query($sql) or fail(mysql_error().$sql);
		imagedestroy($im);
	}
	else{
		return "<h2>Грешка</h2><p>Снимката не е качена!</p>";
	}

	//header("Location:".$_SERVER["HTTP_REFERER"]);
	//exit();
}

function image_resize(){
	global $dbh,$vars;
	
	umask(0);
	$image = $_SESSION["image"];
	$size = getimagesize($image);
	
	//return "<pre>".print_r($size,true)."</pre>";
	
	/*
	 * Convert image to RGB jpeg
	 */
	switch($size["mime"]){
		case "image/jpeg":
			$im=imagecreatefromjpeg($image) or fail('Could not create image object!');
			break;
		case "image/png":
			$im=imagecreatefrompng($image) or fail('Could not create image object!');
			break;
		case "image/gif":
			$im=imagecreatefromgif($image) or fail('Could not create image object!');
			break;
		default:
			return "<h2>Грешка</h2><p>Снимката не може да бъде качена, не разпознат файл тип - ".$size['mime']."</p>";
	}
	
	$sx=$_POST["sx"];
	$sy=$_POST["sy"];
	$w=$_POST["wx"];
	$h=$_POST["wy"];
	
	foreach($_SESSION["imageinfo"] as $k=>$v){
		
		if($v['height']==""){
			$v['height'] = $_SESSION["th_y"];
		}
		if($v['width']==""){
			$v['width'] =$_SESSION["th_x"];
		}

		if($_SESSION["th_x"]==$v['width'] && $_SESSION["th_y"]==$v['height']){

			$th_x=$v['width'];
			$th_y=$v['height'];
			
			$thumb=imagecreatetruecolor($th_x,$th_y);
			imagecopyresampled($thumb,$im,0,0,$sx,$sy,$th_x,$th_y,$w,$h);
			//die();
			
			imagejpeg($thumb,str_replace(".jpg",$v['enl'].".jpg",$image),100);
			imagedestroy($thumb);	
		}
	}
	imagedestroy($im);
	header("Location:".$_SESSION["goto"]);
	exit();
}

function resize_pic($id=0, $pic='', $imageinfo=''){
	global $dbh,$vars;
	
	$image="../".$pic;
	$size=getimagesize($image);
	$th_x=$_GET["th_x"];
	$th_y=$_GET["th_y"];
	if($th_x==""){
		$old_size=getimagesize(str_replace(".jpg", "", $image)."_X".$th_y.".jpg");
		$th_x = $old_size[0];
	}
	if($th_y==""){
		var_dump(str_replace(".jpg", "", $image)."_".$th_x."X.jpg");
		$old_size=getimagesize(str_replace(".jpg", "", $image)."_".$th_x."X.jpg");
		$th_y = $old_size[1];
	}
	
	$_SESSION["image"]=$image;
	$_SESSION["size"]=array(
						"x"=>$size[0],
						"y"=>$size[1],
						"th_x"=>$th_x,
						"th_y"=>$th_y
						);
	
	$_SESSION["action"]="main.php?act=image&do=resize";
	$_SESSION["th_x"]=$th_x;
	$_SESSION["th_y"]=$th_y;
	$_SESSION["imageinfo"]=$imageinfo;
	
	$_SESSION["goto"]=$_SERVER["HTTP_REFERER"];
	header("Location:resizeImage.php");
	exit();
}

function edit_pic($imagesizes, $id=0, $act='', $act1='', $do='', $lang=''){
	global $dbh,$vars;
	
	if($do!=''){
		$do='act1';
	}else{
		$do='do';
	}
	
	$smarty->caching = false;
	$smarty = new Smarty();
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	$smarty->assign("loc",$vars["loc"]);
	$smarty->assign("size", $imagesizes);
	$smarty->assign("backlink","main.php?act=$act&amp;$do=$act1&amp;rec=$id&amp;lang=$lang");
	$smarty->assign("dir","../");

	return $smarty->fetch("editpic.tpl");
}

function image_delete($imagesizes, $id){
	global $dbh,$vars;
	
	$sql_img = "SELECT * FROM images where id='$id'";
   	$result_img = mysql_query($sql_img) or fail(mysql_error().$sql_img);
	$myrow_img = mysql_fetch_assoc($result_img);
	if(is_file("../".$myrow_img['path'].".jpg")){
		unlink("../".$myrow_img['path'].".jpg");
		unlink("../".$myrow_img['path'].".gif");
		unlink("../".$myrow_img['path'].".png");
		unlink("../".$myrow_img['path'].".bmp");
		foreach($imagesizes as $k=>$v){
			unlink("../".$myrow_img['path'].$v['enl'].$myrow_img['ext']);
		}
	}		
	$sql="DELETE FROM images WHERE id='$id'";
	mysql_query($sql) or fail(mysql_error().$sql);
	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit;
}