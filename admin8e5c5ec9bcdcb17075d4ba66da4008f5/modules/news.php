<?php
require("./loc/news_$language.php");

function news_main(){
 
 	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");
	$act1=isset($_GET['act1'])?trim($_GET['act1']):"";
	$act1=sec_check($act1,"str","");
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rez = "";
    //$menu = "";
	switch($act1){
		case "":
			$rez.=news_first();
	        break;
		case "sortui":
			$rez.=news_sortui();
	        break;
	    default:
			$funcname=$act."_".$act1;
			$rez.=$funcname();
		}
	return $rez;
//	die($rez);
}

function news_first(){
	global $dbh,$vars;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$page=isset($_GET['page'])?trim($_GET['page']):"";
	$page=sec_check($page,"int", 0);
	$where=isset($_GET['where'])?trim($_GET['where']):"";
	
	$smarty = new Smarty();
	$smarty->caching = false;
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	$smarty->assign("loc",$vars["loc"]);
	
	if ($lang=="") {
	    $lang="bg";
	}
	
	$smarty->assign("lang",$lang);
	
	$and="";
	if($where){
		if(is_numeric($where)){
			$and = "WHERE id='$where'";
		}else{
			$and = "WHERE title_bg LIKE '%$where%' OR `text_bg` LIKE '%$where%' AND title_en LIKE '%$where%' OR `text_en` LIKE '%$where%'";
		}
	}

	$sql = "SELECT * FROM news $and";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$all=mysql_num_rows($result);
	/*
	 * paging
	 */
	$perpage=20;
	if ($all!=0){
		$smarty->assign("paging",paging($all, $perpage));
	}

	$sql = "SELECT * FROM news $and ORDER BY menu LIMIT $page, $perpage";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$news=array();
	if (mysql_num_rows($result)>0){
		while( $myrow = mysql_fetch_assoc($result)){
			$myrow["image"]='n';
			$sql_i = "SELECT `path` FROM images WHERE stat_id='".$myrow["id"]."' AND modul='news'";
			$result_i = mysql_query($sql_i) or fail(mysql_error().$sql_i);
			if(mysql_num_rows($result_i)>0){
				$myrow["image"]='y';
			}
			$news[]=$myrow;
		}
	}
	$smarty->assign("news",$news);

	return $smarty->fetch("news_first.tpl");
}

function news_delmessage(){
  global $dbh, $vars;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
  
	$smarty->caching = false;
	
	$smarty = new Smarty();
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	
	$smarty->assign("loc",$vars["loc"]);
	
	$sql = "SELECT title_bg FROM news where id=".$rec ;
	$result = mysql_query($sql);
	$myrow = mysql_fetch_assoc($result); 
	$smarty->assign($myrow);
	
	return $smarty->fetch("delmessage.tpl");
}

function news_delsubcat(){
	global $dbh,$vars,$config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");

	$sqlGallery = "SELECT * FROM news_gallery WHERE news_id=".$rec;
	$resultGallery = mysql_query($sqlGallery) or fail(mysql_error());

	if (mysql_num_rows($resultGallery) > 0) {
		while ($row = mysql_fetch_assoc($resultGallery)) {
		    $gallery[] = $row;
		}
	}

	foreach ($gallery as $picture) {
		if (isset($picture['picture'])) {
	    	if (is_file('../'.$picture['picture'])) {
	    		unlink('../'.$picture['picture']);
	    	}
	    }
	}

	/*
	 * delete gallery
	 */
	$sqlDeleteGallery = "DELETE FROM news_gallery WHERE news_id = ".$rec;
	$resultDeleteGallery = mysql_query($sqlDeleteGallery) or fail(mysql_error());

	/*
	 * delete news resource
	 */
	$sql = "DELETE FROM news WHERE id=".$rec;
	$result = mysql_query($sql) or fail(mysql_error());

	/*
	 * delete images
	 */
	$sql="DELETE FROM images WHERE stat_id='$rec' AND modul='news'";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	
	$file=array();
	if (file_exists("../".$config['news'].$rec."/")){
		if ($myDirectory = opendir("../".$config['news'].$rec."/")){
			while($file=readdir($myDirectory)){
				unlink("../".$config['news'].$rec."/".$file);
			}
		}
		rmdir(getcwd()."/../".$config['news'].$rec."/");
	}
	
	header("Location:main.php?act=news&lang=".$lang);
	exit();
}

function news_newcateg(){

	global $dbh,$vars;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$error=isset($_GET['error'])?trim($_GET['error']):"";
	$error=sec_check(base64_decode($error),"str","");
	
	$smarty->caching = false;
	
	$smarty = new Smarty();
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	$smarty->assign("loc",$vars["loc"]);

	$sqlCategories = "SELECT id,name_bg,name_en FROM categories";
	$resultCategories = mysql_query($sqlCategories) or fail(mysql_error().$sqlCategories);

	if (mysql_num_rows($resultCategories) > 0) {
		while ($row = mysql_fetch_assoc($resultCategories)) {
		    $categories[] = $row;
		}
	}

	if($error){
		$smarty->assign("error",$vars["loc"]["news"][$error]);
	}
	$smarty->assign("action","main.php?act=news&act1=newadd&lang=$lang");
	$smarty->assign("maintitle",$vars["loc"]["news"][1]);
	$smarty->assign("lang",$lang);
	$smarty->assign("categories",$categories);
	return $smarty->fetch("news_edit.tpl");
}

function news_newadd() {
	
	global $dbh, $config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($_GET['lang'],"str","");
	if ($_POST["title_bg"]!='') {
		
		$sql = "INSERT INTO news (" .
						" `title_bg`,".
						" `title_en`,".
						" `text_bg`,".
			    		" `text_en`,".
			    		" `categories_id`,".
			    		" `show`,".
			    		" `datein`".")".
			    		" VALUES (" .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%d," .
			    		"%s," .
			    		"CURDATE())";
		$sql = sprintf($sql,
				quote_smart($_POST["title_bg"]),
	  			quote_smart($_POST["title_en"]),
	  			quote_smart($_POST["text_bg"]),
	  			quote_smart($_POST["text_en"]),
	  			$_POST["categories_id"],
				quote_smart('y')
		);
		
		$result = mysql_query($sql) or fail(mysql_error());
		$rec = mysql_insert_id();

		// send mail
		if($rec>0 && $_SESSION['client_level']==0){
			/*$mail = new PHPMailer();
			$mail->From     = $config['mailfrom'];
			$mail->FromName = 'Site administration';
			$mail->Mailer   = "sendmail";
			$mail->Subject  = iconv("UTF-8","cp1251//TRANSLIT","Новина за одобрение");
			$mail->CharSet	= "windows-1251";
			$mail->IsHTML(false);
		    $mail->Body    	= iconv("UTF-8","cp1251//TRANSLIT","Нова новина за одобрение '".$_POST["title"]."'.");
		    $mail->AddAddress($config['adminmail']);
			$mail->Send();*/
		}
		if( isset($_FILES['gallery']) ) {

	   		$valid_formats = array("jpg", "png", "gif", "bmp");
			$path = "images/news/gallery/";
			$count = 0;

			if (!is_dir('../'.$path)){
				mkdir('../'.$path);
			}

			foreach ($_FILES['gallery']['name'] as $f => $name) {

			    if ($_FILES['gallery']['error'][$f] == 4) {
			        continue;
			    }

			    if ($_FILES['gallery']['error'][$f] == 0) {

			        if ( ! in_array(pathinfo($name, PATHINFO_EXTENSION), $valid_formats) ) {

						$message[] = "$name is not a valid format";
						continue;

					} else {

			            if(move_uploaded_file($_FILES["gallery"]["tmp_name"][$f], '../'.$path.$name))
			            $count++;

			        	$queryGallery = "INSERT INTO news_gallery (news_id, picture) VALUES ('". $rec ."','". $path.$name ."')";
			    		$resultGallery = mysql_query($queryGallery) or fail(mysql_error());

			    		mysql_insert_id();

			        }

			    }

			}
		}

		if ($_POST["photo"]=='on') {

			header('Location: main.php?act=news&act1=edit&rec='.$rec.'&lang='.$lang);
			exit();

		} else {

			if($rec!='' && $_FILES["image"]["name"]) {
				image_new($config['news'], $rec, $config['newsinfo'], $_FILES["image"]["tmp_name"], $_FILES["image"]["name"], $rec, 'news');
			}
	
			header("Location: main.php?act=news&lang=".$lang);
			exit();
		}

  	} else {
  		$_GET["error"]=base64_encode(1000);
		return news_newcateg();
   	}
}

function news_newimage(){
	global $dbh,$vars,$config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$id=isset($_POST['id'])?trim($_POST['id']):"";
	$id=sec_check($id,"int","0");
	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");	
	
	image_new($config['news'], $_POST["id"], $config['newsinfo'], $_FILES["image"]["tmp_name"], $_FILES["image"]["name"], $id, $act);
}

function news_resizepic(){
	global $dbh,$vars,$config;
	$id=isset($_GET['rec'])?trim($_GET['rec']):"";
	$id=sec_check($id,"int","0");

	resize_pic($id, $_GET["pic"], $config['newsinfo']);
}

function news_editpic(){
	global $dbh,$vars,$config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$id=isset($_GET['id'])?trim($_GET['id']):"";
	$id=sec_check($id,"int","0");
	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");
	$act1=isset($_GET['func'])?trim($_GET['func']):"";
	$act1=sec_check($act1,"str","");
	$do=isset($_GET['act1'])?trim($_GET['act1']):"";
	$do=sec_check($do,"str","");
	
	return edit_pic($config['newsinfo'], $id, $act, $act1, $do, $lang);
}

function news_delpic(){
	global $dbh,$vars,$config;
	$id=isset($_GET['id'])?trim($_GET['id']):"";
	$id=sec_check($id,"int","0");

	image_delete($config['newsinfo'], $id);
}

function news_edit(){
	global $dbh,$vars,$config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");
	
	$smarty->caching = false;
	$smarty = new Smarty();
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	$smarty->assign("loc",$vars["loc"]);
	
	$sql = "SELECT * FROM news where id=".$rec;
    $result = mysql_query($sql) or fail(mysql_error());
	$myrow = mysql_fetch_assoc($result);

	$sqlCategories = "SELECT id,name_bg,name_en FROM categories";
	$resultCategories = mysql_query($sqlCategories) or fail(mysql_error().$sqlCategories);

	if (mysql_num_rows($resultCategories) > 0) {
		while ($row = mysql_fetch_assoc($resultCategories)) {
		    $categories[] = $row;
		}
	}

	$sqlGallery = "SELECT * FROM news_gallery where news_id=".$rec;
	$resultGallery = mysql_query($sqlGallery) or fail(mysql_error());

	if (mysql_num_rows($resultGallery) > 0) {
		while ($row = mysql_fetch_assoc($resultGallery)) {
		    $gallery[] = $row;
		}
	}

	$smarty->assign("action","main.php?act=news&act1=updatecat&subcat=".$rec."&lang=".$lang);	
	$smarty->assign("rec",$rec);
	$smarty->assign("lang",$lang);
	$smarty->assign("gallery",$gallery);
	$smarty->assign("categories",$categories);
	$smarty->assign("maintitle",$vars["loc"]["news"][10]);
	
	$smarty->assign("dir","../",$config["news"]);
	////images
	$sql_img = "SELECT * FROM images where stat_id='$rec' AND modul='$act'";
	$image = array();
	$result_img = mysql_query($sql_img) or fail(mysql_error().$sql_img);
	while($myrow_img = mysql_fetch_assoc($result_img)){
		$image[]=$myrow_img;
	}
	$smarty->assign("image",$image);
	$smarty->assign("imgenl",$config['newsinfo'][0]["enl"]);
	$myrow["max_file"]=ini_get('upload_max_filesize');
	$smarty->assign($myrow);
	
	return $smarty->fetch("news_edit.tpl");
}

function news_updatecat(){

	global $dbh, $config;
	$subcat=isset($_GET['subcat'])?trim($_GET['subcat']):"";
	$subcat=sec_check($subcat,"int","0");
	
	$sql = sprintf("UPDATE news SET " .
	    		" title_bg=%s,".
	    		" title_en=%s,".
	    		" text_bg=%s,".
	    		" text_en=%s,".
	    		" categories_id=%d,".
	    		" showimages=%s".
	    		" WHERE id=".
	    		quote_smart($subcat),
	    		quote_smart($_POST["title_bg"]),
	    		quote_smart($_POST["title_en"]),
	    		quote_smart($_POST["text_bg"]),
	    		quote_smart($_POST["text_en"]),
	    		$_POST["categories_id"],
	    		quote_smart($_POST["showimages"])
	);
	
	mysql_query($sql)or fail(mysql_error().$sql);
	if($subcat!='' && $_FILES["image"]["name"]){
		image_new($config['news'], $subcat, $config['newsinfo'], $_FILES["image"]["tmp_name"], $_FILES["image"]["name"], $subcat, 'news');
	}

	if( isset($_FILES['gallery']) ) {

   		$valid_formats = array("jpg", "png", "gif", "bmp");
		$path = "images/news/gallery/";
		$count = 0;

		if (!is_dir('../'.$path)){
			mkdir('../'.$path);
		}

		foreach ($_FILES['gallery']['name'] as $f => $name) {

		    if ($_FILES['gallery']['error'][$f] == 4) {
		        continue;
		    }

		    if ($_FILES['gallery']['error'][$f] == 0) {

		        if ( ! in_array(pathinfo($name, PATHINFO_EXTENSION), $valid_formats) ) {

					$message[] = "$name is not a valid format";
					continue;

				} else {

		            if(move_uploaded_file($_FILES["gallery"]["tmp_name"][$f], '../'.$path.$name))
		            $count++;

		        	$queryGallery = "INSERT INTO news_gallery (news_id, picture) VALUES ('". $subcat ."','". $path.$name ."')";
		    		$resultGallery = mysql_query($queryGallery) or fail(mysql_error());

		    		mysql_insert_id();

		        }
		    }

		}
	}

	header("Location:main.php?act=news&lang=".$lang."");
	exit();
}

function news_delgallerypic() {
	global $dbh;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rec=isset($_GET['id'])?trim($_GET['id']):"";
	$rec=sec_check($rec,"int","0");

	$recordSql = "SELECT * FROM news_gallery WHERE id = ".$rec;
	$recordResult = mysql_query($recordSql) or fail(mysql_error());
	$mainResult = mysql_fetch_assoc($recordResult);

	if (isset($mainResult['picture'])) {
    	if (is_file('../'.$mainResult['picture'])) {
    		unlink('../'.$mainResult['picture']);
    	}
    }

	$sql = "DELETE FROM news_gallery WHERE id = ".$rec;
	$result = mysql_query($sql) or fail(mysql_error());

	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function news_showmenu(){
	global $dbh;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	
	$sql = "SELECT MAX(menu)+1 AS menu FROM news WHERE id='$rec'";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$myrow = mysql_fetch_assoc($result);
	$menu = $myrow["menu"];
	
	$sql = "UPDATE news SET `menu`='$menu' WHERE id=".$rec;
	mysql_query($sql) or fail(mysql_error().$sql);

	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function news_hidemenu(){
	global $dbh;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	
	$sql = "UPDATE news SET `menu`='0' WHERE id=".$rec;
	mysql_query($sql) or fail(mysql_error().$sql);	

	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function news_show(){
	global $dbh;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	
	if($_SESSION['client_level']==1){
		$sql = "UPDATE news SET `show`='y' WHERE id=".$rec;
		mysql_query($sql) or fail(mysql_error().$sql);
	}
	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function news_hide(){
	global $dbht;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");

	if($_SESSION['client_level']==1){
		$sql = "UPDATE news SET `show`='n' WHERE id=".$rec;
		mysql_query($sql) or fail(mysql_error().$sql);
	}
	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function news_showhome(){
	global $dbh,$vars;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	
	$sql = "SELECT MAX(home)+1 AS home FROM news WHERE id='$rec'";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$myrow = mysql_fetch_assoc($result);
	$home = $myrow["home"];

	$sql = "UPDATE news SET `home`='$home' WHERE id='$rec'";
	mysql_query($sql) or fail(mysql_error().$sql);
		
	header("Location:".$_SERVER["HTTP_REFERER"]);
}

function news_hidehome(){
	global $dbh,$vars;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");

	$sql = "UPDATE news SET `home`='0' WHERE id=".$rec;
	mysql_query($sql) or fail(mysql_error().$sql);
	
	header("Location:".$_SERVER["HTTP_REFERER"]);
}

function news_sortui(){
	global $dbh,$vars;
	$lang=sec_check($_GET['lang'],"str","");
	
	$smarty = new Smarty();
	$smarty->caching = false;
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	
	$sql = "SELECT * FROM news WHERE menu>0 ORDER BY menu";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$sort=array();
	if (mysql_num_rows($result)>0){  
		while( $myrow = mysql_fetch_assoc($result)){
			$sort[]=$myrow;
		}
	}
	$smarty->assign("action","main.php?act=news&amp;act1=sort");
	$smarty->assign("sort",$sort);
	return $smarty->fetch("content_sort.tpl");
}

function news_sort(){
	global $dbh;
	$i=1;
	foreach($_POST["item"] as $id){
		$sql = "UPDATE news SET menu='$i' where id='$id'";
		mysql_query($sql) or fail(mysql_error().$sql);
		$i++;
	}
	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();	
}
