<?php
require("./loc/towns_$language.php");

function towns_main(){
 
 	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");
	$act1=isset($_GET['act1'])?trim($_GET['act1']):"";
	$act1=sec_check($act1,"str","");
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rez = "";
    //$menu = "";
	switch($act1){
		case "":
			$rez.=towns_first();
	        break;
		case "sortui":
			$rez.=towns_sortui();
	        break;
	    default:
			$funcname=$act."_".$act1;
			$rez.=$funcname();
		}
	return $rez;
//	die($rez);
}

function towns_first(){
	global $dbh,$vars;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$page=isset($_GET['page'])?trim($_GET['page']):"";
	$page=sec_check($page,"int", 0);
	$where=isset($_GET['where'])?trim($_GET['where']):"";
	
	$smarty = new Smarty();
	$smarty->caching = false;
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	$smarty->assign("loc",$vars["loc"]);
	
	if ($lang=="") {
	    $lang="bg";
	}
	
	$smarty->assign("lang",$lang);
	
	$and="";
	if($where){
		if(is_numeric($where)){
			$and = "WHERE id='$where'";
		}else{
			$and = "WHERE town_bg LIKE '%$where%' OR town_en LIKE '%$where%'";
		}
	}

	$sql = "SELECT * FROM towns $and";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$all=mysql_num_rows($result);
	/*
	 * paging
	 */
	$perpage=20;
	if ($all!=0){
		$smarty->assign("paging",paging($all, $perpage));
	}

	//$sql = "SELECT * FROM towns $and ORDER BY menu LIMIT $page, $perpage";
	$sql = "SELECT * FROM towns $and LIMIT $page, $perpage";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$towns=array();
	if (mysql_num_rows($result)>0){
		while( $myrow = mysql_fetch_assoc($result)){
			$towns[]=$myrow;
		}
	}
	$smarty->assign("towns",$towns);

	return $smarty->fetch("towns_first.tpl");
}

function towns_delmessage(){
  global $dbh, $vars;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
  
	$smarty->caching = false;
	
	$smarty = new Smarty();
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	
	$smarty->assign("loc",$vars["loc"]);
	
	$sql = "SELECT town_bg as title_bg FROM towns where id=".$rec ;
	$result = mysql_query($sql);
	$myrow = mysql_fetch_assoc($result); 
	$smarty->assign($myrow);
	
	return $smarty->fetch("delmessage.tpl");
}

function towns_delsubcat(){
	global $dbh,$vars,$config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");	

	$sql = "DELETE FROM towns WHERE id=".$rec;
	$result = mysql_query($sql) or fail(mysql_error());
	
	header("Location:main.php?act=towns&lang=".$lang);
	exit();
}

function towns_newcateg(){
	global $dbh,$vars;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$error=isset($_GET['error'])?trim($_GET['error']):"";
	$error=sec_check(base64_decode($error),"str","");
	
	$smarty->caching = false;
	
	$smarty = new Smarty();
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	$smarty->assign("loc",$vars["loc"]);
	
	if ($error){
		$smarty->assign("error",$vars["loc"]["towns"][$error]);
	}
	$smarty->assign("action","main.php?act=towns&act1=newadd&lang=$lang");
	$smarty->assign("maintitle",$vars["loc"]["towns"][1]);
	return $smarty->fetch("towns_edit.tpl");
}

function towns_newadd(){
	global $dbh, $config;

	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($_GET['lang'],"str","");
  	if ($_POST["town_bg"]!=''){

	 	$sql = "INSERT INTO towns (" .
						" `town_bg`,".
						" `town_en`".")".
			    		" VALUES (" .
			    		"%s," .
			    		"%s)";
		$sql = sprintf($sql,
				quote_smart($_POST["town_bg"]),
	  			quote_smart($_POST["town_en"])
		);
		
		mysql_query($sql) or fail(mysql_error().$sql);
		header("Location: main.php?act=towns&lang=".$lang);
		exit();
  	}else{
  		$_GET["error"]=base64_encode(1000);
		return towns_newcateg();
   	}	
}

function towns_edit(){
	global $dbh,$vars,$config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");
	
	$smarty->caching = false;
	$smarty = new Smarty();
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	$smarty->assign("loc",$vars["loc"]);
	
	$sql = "SELECT * FROM towns where id=".$rec;
    $result = mysql_query($sql) or fail(mysql_error());
	$myrow = mysql_fetch_assoc($result);
	
	$smarty->assign("action","main.php?act=towns&act1=updatecat&id=".$rec."&lang=".$lang);	
	$smarty->assign("rec",$rec);
	$smarty->assign("maintitle",$vars["loc"]["towns"][9]);
	
	$smarty->assign($myrow);
	
	return $smarty->fetch("towns_edit.tpl");
}

function towns_updatecat(){
	global $dbh, $config;
	$id=isset($_GET['id'])?trim($_GET['id']):"";
	$id=sec_check($id,"int","0");
	
	$sql = sprintf("UPDATE towns SET " .
	    		" town_bg=%s,".
	    		" town_en=%s".
	    		" WHERE id=".
	    		quote_smart($id),
	    		quote_smart($_POST["town_bg"]),
	    		quote_smart($_POST["town_en"])
	);
	mysql_query($sql)or fail(mysql_error().$sql);

	header("Location:main.php?act=towns&lang=".$lang."");
	exit();
}

function towns_showmenu(){
	global $dbh;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	
	$sql = "SELECT MAX(menu)+1 AS menu FROM towns WHERE id='$rec'";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$myrow = mysql_fetch_assoc($result);
	$menu = $myrow["menu"];
	
	$sql = "UPDATE towns SET `menu`='$menu' WHERE id=".$rec;
	mysql_query($sql) or fail(mysql_error().$sql);

	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function towns_hidemenu(){
	global $dbh;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	
	$sql = "UPDATE towns SET `menu`='0' WHERE id=".$rec;
	mysql_query($sql) or fail(mysql_error().$sql);	

	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function towns_show(){
	global $dbh;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	
	if($_SESSION['client_level']==1){
		$sql = "UPDATE towns SET `show`=1 WHERE id=".$rec;
		mysql_query($sql) or fail(mysql_error().$sql);
	}
	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function towns_hide(){
	global $dbht;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");

	if($_SESSION['client_level']==1){
		$sql = "UPDATE towns SET `show`=0 WHERE id=".$rec;
		mysql_query($sql) or fail(mysql_error().$sql);
	}
	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function towns_sortui(){
	global $dbh,$vars;
	$lang=sec_check($_GET['lang'],"str","");
	
	$smarty = new Smarty();
	$smarty->caching = false;
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	
	$sql = "SELECT * FROM towns WHERE menu>0 ORDER BY menu";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$sort=array();
	if (mysql_num_rows($result)>0){  
		while( $myrow = mysql_fetch_assoc($result)){
			$sort[]=$myrow;
		}
	}
	$smarty->assign("action","main.php?act=towns&amp;act1=sort");
	$smarty->assign("sort",$sort);
	return $smarty->fetch("content_sort.tpl");
}

function towns_sort(){
	global $dbh;
	$i=1;
	foreach($_POST["item"] as $id){
		$sql = "UPDATE towns SET menu='$i' where id='$id'";
		mysql_query($sql) or fail(mysql_error().$sql);
		$i++;
	}
	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();	
}
