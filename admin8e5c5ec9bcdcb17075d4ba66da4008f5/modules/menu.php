<?php

require("./loc/menue_$language.php");

/*bottom rights*/

function menue_left(){

	global $menu_text,$vars;

	$userlavel=$_SESSION["client_level"];

	//echo $userlavel;

	//print_r($_SESSION);

 	$smarty = new Smarty();

	$smarty->template_dir = './templates';

	$smarty->compile_dir = './templates_c';

	$smarty->cache_dir = './cache';

	$smarty->config_dir = './configs';
	$smarty->assign("loc",$vars["loc"]);

	$acts=array();

	if ($d=opendir('modules/') ){ 

	    while ($files=readdir($d)) {       

	        if (is_file('modules/'.$files)) {   

		    	$acts[]=$files;

	   	    }

	    }

	    closedir($d);

	}

	foreach($acts as $k=>$v){

		if(($v!='menu.php' && isset($vars['menutitle'][substr($v,0,-4)])) && (($v!='approval.php' && $v!='calculator.php') || $_SESSION['client_level']==1)){

			$vars['menuact'][$userlavel][$k]=substr($v,0,-4);

			$vars['menutitle'][$k]=$vars['menutitle'][substr($v,0,-4)];

		}

	}

	$i=0;
	$menu=array();
 	foreach ($vars['menuact'][$userlavel] as $key => $menue){

		$menu[$i]["menue"]=$menue;

		$menu[$i]["key"]=$vars['menutitle'][$key];

		$i++;

	}

	sort($menu,SORT_NUMERIC);

	$smarty->assign("menu",$menu);

	//var_dump($menu);

	return $smarty->fetch("main_menu.tpl"); 

}

?>

