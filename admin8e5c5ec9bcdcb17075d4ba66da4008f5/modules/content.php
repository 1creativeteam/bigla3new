<?php
require("./loc/content_$language.php");

function content_main(){
 
 	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");
	$act1=isset($_GET['act1'])?trim($_GET['act1']):"";
	$act1=sec_check($act1,"str","");
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rez = "";
    //$menu = "";
	switch($act1){
		case "":
			$rez.=content_first();
	        break;
		case "sortui":
			$rez.=content_sortui();
	        break;
	    case "sorthome":
			$rez.=content_sorthome();
	        break;    
		default:
			$funcname=$act."_".$act1;
			$rez.=$funcname();
		}
	return $rez;
}

function content_first(){
	global $dbh,$vars;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$where=isset($_GET['where'])?trim($_GET['where']):"";
	
	$smarty = new Smarty();
	$smarty->caching = false;
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	$smarty->assign("loc",$vars["loc"]);
	
	if ($lang=="") {
	    $lang="bg";
	}

	if($where){
		if(is_numeric($where)){
			$and = "id='$where'";
		}else{
			$and = "title_bg LIKE '%$where%' OR `text_bg` LIKE '%$where%' OR title_en LIKE '%$where%' OR `text_en` LIKE '%$where%'";
		}
		$sql = "SELECT * FROM content WHERE $and ORDER BY menu";
		$result = mysql_query($sql) or fail(mysql_error().$sql);
		$content=array();
		if (mysql_num_rows($result)>0){
			while($myrow = mysql_fetch_assoc($result)){
				$myrow["children"]=content_array_page($myrow["id"],$lang);
				$myrow["image"]='n';
				$sql_i = "SELECT `path` FROM images WHERE stat_id='".$myrow["id"]."' AND modul='content'";
				$result_i = mysql_query($sql_i) or fail(mysql_error().$sql_i);
				if(mysql_num_rows($result_i)>0){
					$myrow["image"]='y';
				}
				$content[]=$myrow;
			}
		}
	}else{
		$content=content_array_page(0,$lang);
	}

	$sql = "SELECT id, title_bg FROM content WHERE lang='$lang' AND `show`='y' AND (`link` IS NULL OR  `link`='') ORDER BY menu";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$subs=array();
	if (mysql_num_rows($result)>0){
		while( $myrow = mysql_fetch_assoc($result)){
			$subs[]=$myrow;
		}
	}
	$smarty->assign("subs",$subs);

	$smarty->assign("content",$content);
	$smarty->assign("lang",$lang);
	return $smarty->fetch("content_first.tpl");
}

function content_delmessage(){
  global $dbh, $vars;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
  
	$smarty->caching = false;
	
	$smarty = new Smarty();
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	
	$smarty->assign("loc",$vars["loc"]);
	
	$sql = "SELECT title_bg FROM content where id=".$rec ;
	$result = mysql_query($sql);
	$myrow = mysql_fetch_assoc($result); 
	$smarty->assign($myrow);
	
	return $smarty->fetch("delmessage.tpl");
}

function content_delsubcat(){
	global $dbh,$vars,$config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");	

	//if we have children reorder them hierarchy
	$sql1 = "SELECT id1 FROM content where id=".$rec;
    $result1 =  mysql_query($sql1) or fail(mysql_error().$sql1);
    $myrow1 = mysql_fetch_array($result1);
	content_delete_hierarchy($rec, $myrow1['id1'], $lang);
	
	$sql = "DELETE FROM content WHERE id=".$rec;
	mysql_query($sql) or fail(mysql_error());

	/*
	 * delete images
	 */
	$sql="DELETE FROM images WHERE stat_id='$rec' AND modul='content'";
	mysql_query($sql) or fail(mysql_error().$sql);
	
	$file=array();
	if (file_exists("../".$config['content'].$rec."/")){
		if ($myDirectory = opendir("../".$config['content'].$rec."/")){
			while($file=readdir($myDirectory)){
				unlink("../".$config['content'].$rec."/".$file);
			}
		}
		rmdir(getcwd()."/../".$config['content'].$rec."/");
	}
	
	header("Location:main.php?act=content&lang=".$lang);
	exit();
}

function content_newcateg(){
	global $dbh,$vars;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$error=isset($_GET['error'])?trim($_GET['error']):"";
	$error=sec_check(base64_decode($error),"str","");
	
	$smarty = new Smarty();
	$smarty->caching = false;
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	$smarty->assign("loc",$vars["loc"]);
	
	$smarty->assign("cat",content_array(0,$lang));
	
	if ($error){
		$smarty->assign("error",$vars["loc"]["content"][$error]);
	}
	$smarty->assign("action","main.php?act=content&act1=newadd&lang=$lang");
	$smarty->assign("maintitle",$vars["loc"]["content"][12]);
	return $smarty->fetch("content_newsedit.tpl");
}

function content_newadd(){
	global $dbh, $config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($_GET['lang'],"str","");
  	if ($_POST["title_bg"]!=''){
  		// if not create then more of one level
		if($_POST["cat_id"]>0){
			$sql_ = "SELECT * FROM content where id=".$_POST["cat_id"]." AND id1=0";
			$result_ =  mysql_query($sql_) or fail(mysql_error().$sql_);
			if(mysql_num_rows($result_)==0){
				$_GET["error"]=base64_encode(1001);
				//return content_newcateg();
			}
		}
	 	$sql = "INSERT INTO content (" .
						" `title_bg`,".
						" `title_en`,".
						" `lang`,".
						" `text_bg`,".
			    		" `text_en`,".
			    		" `short_text`,".
			    		" `link`,".
			    		" `id1`,".
			    		" `show`,".
			    		" `datein`".")".
			    		" VALUES (" .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"%s," .
			    		"NOW())";
		$sql = sprintf($sql,
				quote_smart($_POST["title_bg"]),
	  			quote_smart($_POST["title_en"]),
	  			quote_smart($lang),
	  			quote_smart($_POST["text_bg"]),
	  			quote_smart($_POST["text_en"]),
	  			quote_smart($_POST["short_text"]),
	  			quote_smart($_POST["link"]),
	  			quote_smart($_POST["cat_id"]),
	  			quote_smart('y')
		);
		$result = mysql_query($sql) or fail(mysql_error());
		$rec=mysql_insert_id();
		// send mail to administrator
		if($rec>0 && $_SESSION['client_level']==0){
			/*$mail = new PHPMailer();
			$mail->From     = $config['mailfrom'];
			$mail->FromName = 'Site administration';
			$mail->Mailer   = "sendmail";
			$mail->Subject  = iconv("UTF-8","cp1251//TRANSLIT","Ново статия/категория за одобрение");
			$mail->CharSet	= "windows-1251";
			$mail->IsHTML(false);
		    $mail->Body    	= iconv("UTF-8","cp1251//TRANSLIT","Ново статия/категория за одобрение '".$_POST["title"]."'.");
		    $mail->AddAddress($config['adminmail']);
			$mail->Send();*/
		}
		if($_POST["photo"]=='on'){
			header('Location: main.php?act=content&act1=edit&rec='.$rec.'&lang='.$lang);
			exit();
		}
		else{
			if($rec!='' && $_FILES["image"]["name"]){
				image_new($config['content'], $rec, $config['contentinfo'], $_FILES["image"]["tmp_name"], $_FILES["image"]["name"], $rec, 'content');
			}
			header("Location: main.php?act=content&lang=".$lang);
			exit();
		}	
  	}else{
  		$_GET["error"]=base64_encode(1000);
		return content_newcateg();
   	}	
}

function content_newimage(){
	global $dbh,$vars,$config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$id=isset($_POST['id'])?trim($_POST['id']):"";
	$id=sec_check($id,"int","0");
	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");	
	
	image_new($config['content'], $_POST["id"], $config['contentinfo'], $_FILES["image"]["tmp_name"], $_FILES["image"]["name"], $id, $act);
}

function content_resizepic(){
	global $dbh,$vars,$config;
	$id=isset($_GET['rec'])?trim($_GET['rec']):"";
	$id=sec_check($id,"int","0");
	
	resize_pic($id, $_GET["pic"], $config['contentinfo']);
}

function content_editpic(){
	global $dbh,$vars,$config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$id=isset($_GET['id'])?trim($_GET['id']):"";
	$id=sec_check($id,"int","0");
	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");
	$act1=isset($_GET['func'])?trim($_GET['func']):"";
	$act1=sec_check($act1,"str","");
	$do=isset($_GET['act1'])?trim($_GET['act1']):"";
	$do=sec_check($do,"str","");
	
	return edit_pic($config['contentinfo'], $id, $act, $act1, $do, $lang);
}

function content_delpic(){
	global $dbh,$vars,$config;
	$id=isset($_GET['id'])?trim($_GET['id']):"";
	$id=sec_check($id,"int","0");

	image_delete($config['contentinfo'], $id);
}

function content_edit(){
	global $dbh,$vars,$config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	$act=isset($_GET['act'])?trim($_GET['act']):"";
	$act=sec_check($act,"str","");
	
	$smarty->caching = false;
	$smarty = new Smarty();
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	$smarty->assign("loc",$vars["loc"]);
	
	$smarty->assign("cat",content_array(0,$lang));
	
    $sql = "SELECT * FROM content where id=".$rec;
    $result = mysql_query($sql) or fail(mysql_error());
	$myrow = mysql_fetch_assoc($result);
	
	$smarty->assign("action","main.php?act=content&act1=updatecat&subcat=".$rec."&lang=".$lang);	
	$smarty->assign("rec",$rec);
	$smarty->assign("maintitle",$vars["loc"]["content"][9]);
	
	$smarty->assign("dir","../",$config["content"]);
	////images
	$sql_img = "SELECT * FROM images where stat_id='$rec' AND modul='$act'";
	$image = array();
	$result_img = mysql_query($sql_img) or fail(mysql_error().$sql_img);
	while($myrow_img = mysql_fetch_assoc($result_img)){
		$image[]=$myrow_img;
	}
	$smarty->assign("image",$image);
	$smarty->assign("imgenl",$config['contentinfo'][0]["enl"]);
	$myrow["max_file"]=ini_get('upload_max_filesize');
	$smarty->assign($myrow);
	
	return $smarty->fetch("content_newsedit.tpl");
}

function content_updatecat(){
	global $dbh, $config;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$subcat=isset($_GET['subcat'])?trim($_GET['subcat']):"";
	$subcat=sec_check($subcat,"int","0");
	
	if($_POST["cat_id"]!=$_POST["old_cat_id"]){
		
		// if not create then more of one level
		if($_POST["cat_id"]>0){
			$sql_ = "SELECT * FROM content where id=".$_POST["cat_id"]." AND id1=0";
			$result_ =  mysql_query($sql_) or fail(mysql_error().$sql_);
			if(mysql_num_rows($result_)==0){
				//header("Location:main.php?act=content&act1=edit&rec=$subcat&lang=".$lang."&error=".base64_encode(1001));
				//exit;	
			}
		}

		$sql = sprintf("UPDATE content SET " .
		    		" datein= NOW(),".
		    		" title_bg=%s,".
		    		" text_bg=%s,".
		    		" title_en=%s,".
		    		" text_en=%s,".
		    		" short_text=%s,".
		    		" long_title=%s,".
		    		" keywords=%s,".
		    		" showimages=%s,".
		    		" id1=%s,".
		    		" `link`=%s".
		    		" WHERE id=".
		    		quote_smart($subcat),
		    		quote_smart($_POST["title_bg"]),
		    		quote_smart($_POST["text_bg"]),
		   			quote_smart($_POST["title_en"]),
		    		quote_smart($_POST["text_en"]),
		   			quote_smart($_POST["short_text"]),
		   			quote_smart($_POST["long_title"]),
		   			quote_smart($_POST["keywords"]),
		   			quote_smart($_POST["showimages"]),
		   			quote_smart($_POST["cat_id"]),
		   			quote_smart($_POST["link"])
		);
		mysql_query($sql)or fail(mysql_error().$sql);
	}
	else{
		$sql = sprintf("UPDATE content SET " .
		    		" datein= NOW(),".
		    		" title_bg=%s,".
		    		" text_bg=%s,".
		    		" title_en=%s,".
		    		" text_en=%s,".
		    		" short_text=%s,".
		    		" long_title=%s,".
		    		" keywords=%s,".
		    		" showimages=%s,".
		    		" `link`=%s".
		    		" WHERE id=".
		    		quote_smart($subcat),
		    		quote_smart($_POST["title_bg"]),
		    		quote_smart($_POST["text_bg"]),
		   			quote_smart($_POST["title_en"]),
		    		quote_smart($_POST["text_en"]),
		   			quote_smart($_POST["short_text"]),
		   			quote_smart($_POST["long_title"]),
		   			quote_smart($_POST["keywords"]),
		   			quote_smart($_POST["showimages"]),
		   			quote_smart($_POST["link"])
		);
		mysql_query($sql)or fail(mysql_error().$sql);
	}
	if($subcat!='' && $_FILES["image"]["name"]){
		image_new($config['content'], $subcat, $config['contentinfo'], $_FILES["image"]["tmp_name"], $_FILES["image"]["name"], $subcat, 'content');
	}
	header("Location:main.php?act=content&lang=".$lang."");
	exit();
	
}

function content_showarticle(){
	global $dbh;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	
	$sql = "UPDATE content SET `show`='y' WHERE id=".$rec;
	mysql_query($sql) or fail(mysql_error().$sql);

	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function content_hidearticle(){
	global $dbht;
	$rec=sec_check($_GET['rec'],"int","0");
	
	$sql = "UPDATE content SET `show`='n' WHERE id=".$rec;
	mysql_query($sql) or fail(mysql_error().$sql);

	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function content_showhome(){
	global $dbh,$vars;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	
	$sql = "SELECT MAX(c1.home)+1 AS home FROM content AS c LEFT JOIN content AS c1 ON (c.id1=c1.id1 AND c.lang=c1.lang) WHERE c.id=".$rec." AND c.lang='".$lang."'";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$myrow = mysql_fetch_assoc($result);
	$home = $myrow["home"];

	$sql = "UPDATE content SET `home`='$home' WHERE id='$rec'";
	mysql_query($sql) or fail(mysql_error().$sql);
		
	header("Location:".$_SERVER["HTTP_REFERER"]);
}

function content_hidehome(){
	global $dbh,$vars;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");

	$sql = "UPDATE content SET `home`='0' WHERE id=".$rec;
	mysql_query($sql) or fail(mysql_error().$sql);
	
	header("Location:".$_SERVER["HTTP_REFERER"]);
}

function content_showmenu(){
	global $dbh;
	$lang=isset($_GET['lang'])?trim($_GET['lang']):"";
	$lang=sec_check($lang,"str","");
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	
	$sql = "SELECT MAX(c1.menu)+1 AS menu 
			FROM content AS c
			LEFT JOIN content AS c1 ON (c.id1=c1.id1 AND c.lang=c1.lang)
			WHERE c.id='$rec' AND c.lang='$lang'";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$myrow = mysql_fetch_assoc($result);
	$menu = $myrow["menu"];

	$sql = "UPDATE content SET `menu`='$menu' WHERE id=".$rec;
	mysql_query($sql) or fail(mysql_error().$sql);

	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function content_hidemenu(){
	global $dbh;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	
	$sql = "UPDATE content SET `menu`='0' WHERE id=".$rec;
	mysql_query($sql) or fail(mysql_error().$sql);	

	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function content_show(){
	global $dbh;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	if($_SESSION['client_level']==1){
		$sql = "UPDATE content SET `show`='y' WHERE id=".$rec;
		mysql_query($sql) or fail(mysql_error().$sql);
	}
	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function content_hide(){
	global $dbht;
	$rec=isset($_GET['rec'])?trim($_GET['rec']):"";
	$rec=sec_check($rec,"int","0");
	if($_SESSION['client_level']==1){
		$sql = "UPDATE content SET `show`='n' WHERE id=".$rec;
		mysql_query($sql) or fail(mysql_error().$sql);
	}
	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();
}

function content_sortui(){
	global $dbh,$vars;
	$lang=sec_check($_GET['lang'],"str","");
	
	$smarty = new Smarty();
	$smarty->caching = false;
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	
	$sql = "SELECT * FROM content WHERE lang='".$lang."' AND menu>0 ORDER BY menu";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$i=0;
	$sort=array();
	if (mysql_num_rows($result)>0){  
		while( $myrow = mysql_fetch_assoc($result)){
			$sort[$i]=$myrow;
			$i++;
		}
	}
	$smarty->assign("action","main.php?act=content&amp;act1=sort");
	$smarty->assign("sort",$sort);

	return $smarty->fetch("content_sort.tpl");
}

function content_sort(){
	global $dbh;

	$i=1;
	foreach($_POST["item"] as $id){
		$sql = "UPDATE content SET menu='$i' where id='$id'";
		mysql_query($sql) or fail(mysql_error().$sql);
		$i++;
	}
	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();	
}

function content_sorthome(){
	global $dbh,$vars;
	$lang=sec_check($_GET['lang'],"str","");
	$rec=sec_check($_GET['rec'],"str","");
	
	$smarty->caching = false;
	$smarty = new Smarty();
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	
	$sql = "SELECT * FROM content where id1=".$rec." and lang='".$lang."' AND home!=0 ORDER BY home";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$i=0;
	$sort=array();
	if (mysql_num_rows($result)>0){  
		while( $myrow = mysql_fetch_assoc($result)){
			$sort[$i]=$myrow;
			$i++;
		}
	}
	$smarty->assign("sort",$sort);
	return $smarty->fetch("content_sort.tpl");
}

function content_homeupdate(){
	global $dbh;

	$i=1;
	foreach($_POST["item"] as $id){
		$sql = "UPDATE content SET home='$i' where id='$id'";
		$result = mysql_query($sql) or fail(mysql_error().$sql);
		$i++;
	}
	header("Location:".$_SERVER["HTTP_REFERER"]);
	exit();	
}

function content_array($id,$lang){
	global $dbh,$vars;
	
	$sql = "SELECT * FROM content WHERE id1='$id' AND lang='$lang' ORDER BY menu";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$subs=array();
	if (mysql_num_rows($result)>0){
		while( $myrow = mysql_fetch_assoc($result)){
			$myrow["children"]=content_array($myrow["id"],$lang);
			$subs[]=$myrow;
		}
	}
	return $subs;
}

function content_array_page($id,$lang){
	global $dbh,$vars;
	
	$sql = "SELECT * FROM content WHERE id1='$id' AND lang='$lang' ORDER BY menu";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	$subs=array();
	$dirArray=array();
	if (mysql_num_rows($result)>0){
		while( $myrow = mysql_fetch_assoc($result)){
			$myrow["children"]=content_array_page($myrow["id"],$lang);
			$myrow["image"]='n';
			$sql_i = "SELECT `path` FROM images WHERE stat_id='".$myrow["id"]."' AND modul='content'";
			$result_i = mysql_query($sql_i) or fail(mysql_error().$sql_i);
			if(mysql_num_rows($result_i)>0){
				$myrow["image"]='y';
			}
			$subs[]=$myrow;
		}
	}
	return $subs;
}

function content_delete_hierarchy($id,$id1,$lang){
	global $dbh,$vars;
	
	$sql = "SELECT id, id1 FROM content WHERE id1='$id' AND lang='$lang'";
	$result = mysql_query($sql) or fail(mysql_error().$sql);
	if (mysql_num_rows($result)>0){
		while( $myrow = mysql_fetch_assoc($result)){
			$children=content_array($myrow["id"],$lang);
			if(count($children)==0){
				$sql = sprintf("UPDATE content SET id1=%s WHERE id=".quote_smart($myrow['id']), quote_smart($id1));
				mysql_query($sql) or fail(mysql_error());
				content_delete_hierarchy($myrow["id"], $myrow["id1"], $lang);
			}else{
				$sql = sprintf("UPDATE content SET id1=%s WHERE id=".quote_smart($myrow['id']), quote_smart($id1));
				mysql_query($sql) or fail(mysql_error());
			}
		}
		return true;
	}
	return false;
}