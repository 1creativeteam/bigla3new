<?php
session_start();
require_once("dbase.php");
//require_once("../application/config/site_config.php");
// Default locale
$def_loc="bg";

//admin email
$config['adminmail']="info@bigla.bg";

$config["sitaname"]="";
$config["sitaddress"]="";
$config['mailfrom']="";

//$config['conf']['site_url']="http://r5club.noavis-dev2.eu/";
$config['conf']['site_url']="http://bigla.bg";

$main_left['1']="menue_left";
// image dirs
$config['content']="images/content/";
$config['media']="images/media/";
$config['home']="images/home/";
$config['news']="images/news/";
$config['careers']="images/careers/";
$config['projects']="images/projects/";
$config['tintyava']="images/gallery/";

/*
ако не искаме да се кропва и оразмеряваме картинката по единия р-р
слагаме crop=false и fit=true. Като попълваме само р-ра по който искаме да се оразмери (пр.width=100)
другата променлива оставяме празна. Също се попълва като разширение само ползвания р-р(enl=_100)
дригия се оставя X
 */

/* for content */
$config['contentinfo'][]=array("width"=>"100","height"=>"67","crop"=>true,"fit"=>false,"enl"=>"_100X67");
$config['contentinfo'][]=array("width"=>"800","height"=>"530","crop"=>true,"fit"=>false,"enl"=>"_800X530");
$config['contentinfo'][]=array("width"=>"155","height"=>"250","crop"=>true,"fit"=>false,"enl"=>"_155X250");


/* for news and careers */
$config['newsinfo'][]=array("width"=>"200","height"=>"136","crop"=>true,"fit"=>false,"enl"=>"_200X136");
$config['newsinfo'][]=array("width"=>"300","height"=>"","crop"=>false,"fit"=>true,"enl"=>"_300X");

/* for home */
$config['homeinfo'][]=array("width"=>"100","height"=>"67","crop"=>true,"fit"=>false,"enl"=>"_100X67");
$config['homeinfo'][]=array("width"=>"1400","height"=>"576","crop"=>true,"fit"=>false,"enl"=>"_1400X576");

/* for projects */
$config['projectsinfo'][]=array("width"=>"200","height"=>"136","crop"=>true,"fit"=>false,"enl"=>"_200X136");
$config['projectsinfo'][]=array("width"=>"300","height"=>"","crop"=>false,"fit"=>true,"enl"=>"_300X");
//$config['projectsinfo'][]=array("width"=>"931","height"=>"634","crop"=>true,"fit"=>false,"enl"=>"_931X634");
$config['projectsinfo'][]=array("width"=>"622","height"=>"422","crop"=>true,"fit"=>false,"enl"=>"_622X422");
$config['projectsinfo'][]=array("width"=>"940","height"=>"447","crop"=>false,"fit"=>true,"enl"=>"_940X447");

$config['towns']=array(
	"1"=>"София",
	"2"=>"Пловдив",
	"3"=>"Варна"
	);


