<?php
ini_set('error_reporting', E_ALL);
ini_set('date.timezone', 'Europe/Sofia');
require("./configs/conf.inc.php");
require("function.php");
require_once("sec.inc.php");
//include("template.php");
require("login.php");


//$locname=set_locale();
$locname="bg";
$language=$locname;

require_once './loc/loc_'.$locname.'.php';

if ( @$dir=opendir('modules/') ){ 
    while ($file=readdir($dir)) {       
        if (is_file('modules/'.$file)) {   
	     require_once './modules/'.$file;
   	    }
    }
}
closedir($dir);                                                                                                                                                       
if(!check_login()) {
	header("Location:logon.php");
	exit();
} 

require_once(getcwd().'/lib/Smarty.class.php');

$smarty = new Smarty();
$smarty->template_dir = './templates';
$smarty->compile_dir = './templates_c';
$smarty->cache_dir = './cache';
$smarty->config_dir = './configs';

$smarty->assign("loc",$vars["loc"]);

$smarty->assign("image",$_SESSION["image"]);
$smarty->assign("size",$_SESSION["size"]);


$smarty->display("image_resize.tpl"); 

mysql_close();
?>