<?php
ini_set("error_reporting", E_ALL);
ini_set("display_errors",0);
//error_reporting(1);

#
# check variables
#
function sec_check($val,$type,$def_value){
	$IS_ERR=false;
	if($val==""){
//		die("asdfasdf");
		$rez=$def_value;
	}else{
		switch($type){
			case "int":
				if(!is_numeric($val)){
					$IS_ERR=true;
				}else{
					$rez=(int)$val;
				}
			break;
			case "str":
				if(!is_string($val)){
					$IS_ERR=true;
				}else{
					$rez=$val;
				}
			break;
			case "bool":
				if(!is_bool($val)){
					$IS_ERR=true;
				}else{
					$rez=(bool)$val;
				}
			break;
		}	
	}
	if($IS_ERR){
		return false;
	}
	return $rez;
}


function check_forms($formname){
	global $vars,$_POST;
	$form=$vars["forms"][$formname];
	$form_rez=array();
	foreach ($form as $key=>$value){
		$form_rez[$key]=true;
		switch($value["type"]){
			case "text":
				if(is_string($_POST[$key])){
					$form_rez[$key]=false;
					$_POST[$key]=trim(strip_tags($_POST[$key]));
					if($value["mandatory"] && $_POST[$key]==""){
						$form_rez[$key]=true;
					}
				}
				break;
			case "email":
				if(is_string($_POST[$key]) && emailcheck($_POST[$key])){
					$form_rez[$key]=false;
					$_POST[$key]=trim(strip_tags($_POST[$key]));
					if($value["mandatory"] && $_POST[$key]==""){
						$form_rez[$key]=true;
					}
				}
				break;
			case "date":
				$dateArray=split("-",$_POST[$key]);
				if(checkdate($dateArray[1], $dateArray[2], $dateArray[0])){
					$form_rez[$key]=false;
					if($value["mandatory"] && ($dateArray[1]==1970 && $dateArray[2]==1 && $dateArray[0]==1)){
						$form_rez[$key]=true;
					}
				}
				else{
					$form_rez[$key]=true;
				}
				/*
				else{
					$date=strtotime($_POST[$key]);
					if($date!=false){
						$form_rez[$key]=false;
						$_POST[$key] = date("Y-m-d",$date);
					}
				}
				*/
				break;
			case "integer":
			case "float":
			case "numeric":
				if(is_numeric($_POST[$key]) || $_POST[$key]==""){
					$form_rez[$key]=false;
					if($value["mandatory"] && $_POST[$key]==0){
						$form_rez[$key]=true;
					}
				}
				break;
		}
	}
	
	return $form_rez;
}

function emailcheck($email) {
	if (eregi("^.+@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", $email,$check)) {
		if (checkdnsrr(substr(strstr($check[0], '@'), 1), "ANY")) {
			return TRUE;
		}
	}
	return FALSE;
}

function quote_smart($value){
   if (get_magic_quotes_gpc()) {
       $value = stripslashes($value);
   }
   $value = "'" . mysql_real_escape_string($value) . "'";
   return $value;
}

function fail($message){
	global $vars, $config;
	
	$subject = "Error report from ".$config["conf"]["sitaddress"]."";
	
	$email=$config['email_to_error'];
	$message="There was an error on ".$config["conf"]["sitaddress"].":\n\n".$message."\n\n";
	$message.="_SERVER = ".print_r($_SERVER,true);
	$message.="_GET = ".print_r($_GET,true);
	$message.="_POST = ".print_r($_POST,true);
	$message.="_SESSION = ".print_r($_SESSION,true);
	$message.="_COOKIE = ".print_r($_COOKIE,true);
	
	$headers = "from: '".$vars["conf"]["sitaddress"]."' <http@'".$vars["conf"]["sitaddress"]."'>\n";
	$headers .= "MIME-Version: 1.0\n";
	echo $message;
	//mail($email, $subject, $message, $headers);
	
	//readfile ("Location:500.html");
	
}

function errorHandler($errno, $errmsg, $filename, $linenum, $vars){
	global $vars, $config;
	
	// set of errors for which a var trace will be saved
	$user_errors = E_USER_ERROR | E_ERROR | E_PARSE | E_CORE_ERROR | E_COMPILE_ERROR;
	
	if (($errno & $user_errors) > 0) {
		$subject = "Error report from ".$vars["conf"]["sitaddress"]."";
		
		$email=$config['email_to_error'];
		$message="There was an error on ".$vars["conf"]["sitaddress"].":\n\n".$errmsg."\n\n";
		$message.="_SERVER = ".print_r($_SERVER,true);
		$message.="_GET = ".print_r($_GET,true);
		$message.="_POST = ".print_r($_POST,true);
		$message.="_SESSION = ".print_r($_SESSION,true);
		$message.="_COOKIE = ".print_r($_COOKIE,true);
		
		if($_SERVER["SERVER_NAME"]=="localhost/atnf.biz"){
			die("<pre>$message</pre>"); 
		}
		$headers = "from: '".$config["sitaddress"]."' <http@'".$config["sitaddress"]."'>\n";
		$headers .= "MIME-Version: 1.0\n"; 
		echo $message;
		//mail($email, $subject, $message, $headers);
		
		//readfile ("Location:500.html");
	}
}

?>
