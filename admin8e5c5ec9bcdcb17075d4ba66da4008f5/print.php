<?php

header("Content-type:text/html; charset=utf-8");

require("./configs/conf.inc.php");
require("function.php");
require_once("sec.inc.php");

$locname="bg";
$language=$locname;
require_once './loc/loc_'.$locname.'.php';
 // open dbase  
$dbh = @mysql_connect($vars["conf"]["db"]["host"], $vars["conf"]["db"]["user"], $vars["conf"]["db"]["pass"])
		or die(mysql_error());

mysql_select_db($vars["conf"]["db"]["name"])
		or die(mysql_error());

mysql_query("SET CHARACTER SET 'utf8'");

if ( @$dir=opendir('modules/') ){ 
    while ($file=readdir($dir)) {       
        if (is_file('modules/'.$file)) {   
	     require_once './modules/'.$file;
   	    }
    }
}
closedir($dir);  

require_once(getcwd().'/lib/Smarty.class.php');
$smarty = new Smarty();

$smarty->template_dir = './templates';
$smarty->compile_dir = './templates_c';
$smarty->cache_dir = './cache';
$smarty->config_dir = './configs';

//$smarty->assign("loc",$vars["loc"]);

$smarty->assign("main_body",main_main());

$smarty->display("print.tpl");

mysql_close();
?>